#include "stdafx.h"
#include "IpAddressRecentFailChecker.h"
#include <algorithm>
#include "..\Resource\lang.h"

//////////////////////////////////////////////////////////////////////

ACCOUNT_CHECK IpAddressRecentFailChecker::Check(const DWORD dwIP) {
	if (dwIP == 0) return ACCOUNT_CHECK::Ok;
	
	// Is the IP in the list?
	const auto it = std::ranges::find_if(m_cache,
		[dwIP](const IPAddressCache & ptr) {
			return ptr.m_dwIP == dwIP;
		});
	
	if (it != m_cache.end()) {
		// Yes, use the already stored data for this IP

		const IPAddressCache pInfo = *it;
		m_cache.erase(it);
		m_cache.emplace_back(pInfo);
		return pInfo.GetState();
	} else {
		// No: add in the list
		AddNewIp(dwIP);
		return ACCOUNT_CHECK::Ok;
	}
}

bool IpAddressRecentFailChecker::IPAddressCache::IsLocalAddress(DWORD dwIP) noexcept {
	static constexpr DWORD _127_0_0_1   = 0x7F000001;
	static constexpr DWORD _192_168_1_0 = 0xC0A80100;

	if (dwIP == _127_0_0_1) return true;
	if ((dwIP & 0xFFFFFF00) == _192_168_1_0) return true;

	return false;
}

ACCOUNT_CHECK IpAddressRecentFailChecker::IPAddressCache::GetState() const {
	if (IsLocalAddress(m_dwIP)) return ACCOUNT_CHECK::Ok;

	const time_t tmCur = time(nullptr);

	if (m_nError >= 3) {
		long nSec = tmCur - m_tmError;
		if (nSec <= (15 * 60)) return ACCOUNT_CHECK::x3TimeError;
	} else if (m_nError >= 1 && ::GetLanguage() != LANG_KOR) {
		long nSec = tmCur - m_tmError;
		if (nSec <= 15) return ACCOUNT_CHECK::x1TimeError;
	}

	return ACCOUNT_CHECK::Ok;
}

void IpAddressRecentFailChecker::AddNewIp(const DWORD dwIP) {
	// Ensure has space
	if (m_cache.size() == m_cache.max_size()) {
		m_cache.erase(m_cache.begin());
	}
	
	// Add
	const time_t tmCur = time(nullptr);
	IPAddressCache pInfo = {
		.m_dwIP = dwIP,
		.m_nError = 0,
		.m_tmError = tmCur
	};

	m_cache.emplace_back(pInfo);
}

void IpAddressRecentFailChecker::SetError(const DWORD dwIP, const bool isError) {
	if (dwIP == 0) return;

	const auto pInfo = std::find_if(m_cache.begin(), m_cache.end(),
		[dwIP](const IPAddressCache & pInfo) { return pInfo.m_dwIP == dwIP; }
		);

	if (pInfo == m_cache.end()) return;
		
	if (isError) {
		++pInfo->m_nError;
	} else {
		pInfo->m_nError = 0;
	}

	pInfo->m_tmError = time(NULL);
}


//////////////////////////////////////////////////////////////////////

[[maybe_unused]] static void test_AccountMgr() {
	static constexpr auto testAssert = [](const bool ok) {
		if (!ok) {
			throw "Assert failed";
		}
	};

	const DWORD testerIp = 0x26137822; // Arbitrary value because we need one

	IpAddressRecentFailChecker mgr;
	ACCOUNT_CHECK check = mgr.Check(testerIp);
	testAssert(check == ACCOUNT_CHECK::Ok);
	mgr.SetError(testerIp, true);
	check = mgr.Check(testerIp);
	testAssert(check == ACCOUNT_CHECK::x1TimeError);

	Sleep(1000 * 16);

	check = mgr.Check(testerIp);
	testAssert(check == ACCOUNT_CHECK::Ok);

	mgr.SetError(testerIp, true);
	mgr.SetError(testerIp, true);
	mgr.SetError(testerIp, true);

	check = mgr.Check(testerIp);
	testAssert(check == ACCOUNT_CHECK::x3TimeError);

	Sleep(1000 * 60 * 15 + 1000);

	check = mgr.Check(testerIp);
	testAssert(check == ACCOUNT_CHECK::Ok);
}
