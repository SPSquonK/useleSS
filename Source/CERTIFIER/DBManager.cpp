#include "stdafx.h"
#include "dbmanager.h"
#include <chrono>
#include <mutex>
#include <string_view>

// Disable mutex related warnings
#pragma warning(disable: 26111)

#include "afxinet.h"
#include "dpcertifier.h"
#include "dpaccountclient.h"
#include "IpAddressRecentFailChecker.h"
#include "..\Resource\Lang.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
// global
CDbManager	g_DbManager;

/////////////////////////////////////////////////////////////////////////////////////////////////

CDbManager::~CDbManager() {
	// End all workers through completition port
	for (auto & worker : m_workers) {
		if (worker.ioCompletionPort) {
			PostQueuedCompletionStatus(worker.ioCompletionPort, 0, NULL, NULL);
			CLOSE_HANDLE(worker.ioCompletionPort);
		}
	}

	// Waiting for actual stop is performed by the destructor of std::jthread
}

bool CDbManager::CreateDbWorkers()
{
	shared_ptr_timed_mutex loadedDatabase = std::make_shared<std::timed_mutex>();
	loadedDatabase->lock();

	for (auto & [thread, ioCompletionPort] : m_workers) {
		ioCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
		ASSERT(ioCompletionPort);
		
		thread = std::jthread(&CDbManager::DbWorkerThread, this, ioCompletionPort, loadedDatabase);
		
		// Wait for the thread to unlock the mutex
		using namespace std::chrono_literals;
		if (!loadedDatabase->try_lock_for(10s)) {
			OutputDebugString("CERTIFIER.EXE\t// TIMEOUT\t// ODBC");
			return false;
		}
	}

	loadedDatabase->unlock();

	return true;
}


void CDbManager::DbWorkerThread(HANDLE hIOCP, shared_ptr_timed_mutex mutex) {
	IpAddressRecentFailChecker mgr;

	CQuery query;
	if (!query.Connect(3, "useless_account", "account", m_szLoginPWD)) {
		AfxMessageBox("Error : Not Connect useless_account DB");
	}

	mutex->unlock();
	mutex.reset();

	DWORD dwBytesTransferred	= 0;
	LPDWORD lpCompletionKey		= NULL;
	LPDB_OVERLAPPED_PLUS pData	= NULL;

	while (true) {
		const BOOL fOk = GetQueuedCompletionStatus( hIOCP,
										 &dwBytesTransferred,
										(LPDWORD)&lpCompletionKey,
										(LPOVERLAPPED*)&pData,
										INFINITE );

		if( fOk == FALSE ) 
		{	
			ASSERT( 0 );				// fatal error
			continue;
		}

		if( dwBytesTransferred == 0 )	// terminate
			return;
		
		switch( pData->nQueryMode )
		{
			using enum DB_OVERLAPPED_PLUS::QUERYMODE;
			case CERTIFY:
				Certify( query, pData, mgr );
				break;
			case CLOSE_EXISTING_CONNECTION:
				CloseExistingConnection( query, pData );
				break;
		}

		DeAlloc(pData);
	}
}

DB_OVERLAPPED_PLUS * CDbManager::Alloc(
	DB_OVERLAPPED_PLUS::QUERYMODE queryMode,
	const char * pszAccount, const char * pszPwd, DPID dpid
) {
	DB_OVERLAPPED_PLUS * ptr = m_memoryPool.construct();
	ptr->nQueryMode = queryMode;
	ptr->dpId = dpid;
	ptr->dwIP = g_dpCertifier.GetPlayerAddr(dpid);
	strcpy(ptr->AccountInfo.szAccount, pszAccount);
	strcpy(ptr->AccountInfo.szPassword, pszPwd);
	_tcslwr(ptr->AccountInfo.szAccount);
	return ptr;
}

void CDbManager::PostQ(DB_OVERLAPPED_PLUS * pData) {
	std::string_view accountSv = pData->AccountInfo.szAccount;
	const size_t nKey = std::hash<std::string_view>()(accountSv);
	const size_t nIOCP = nKey % DEFAULT_DB_WORKER_THREAD_NUM;
	::PostQueuedCompletionStatus(m_workers[nIOCP].ioCompletionPort, 1, NULL, (LPOVERLAPPED)pData);
}


////////

bool CDbManager::LoadEveSchoolAccount(void) {
	CScanner s;
	if (!s.Load("EveSchoolAccount.txt")) return false;

	while (true) {
		s.GetToken();
		if (s.tok == FINISHED) break;
		m_eveSchoolAccount.insert(s.Token.GetString());
	}

	return true;
}


////////

bool CDbManager::DBQryAccount(CQuery & pQuery, DB_OVERLAPPED_PLUS & pData) {
	char lpAddr[16] = { 0, }; g_dpCertifier.GetPlayerAddr(pData.dpId, lpAddr);

	char qryAccount[256]; std::sprintf(qryAccount, "LOGIN_STR ?, ?, '%s'", lpAddr);

	pQuery.BindParameter(1, pData.AccountInfo.szAccount, MAX_ACCOUNT);
	pQuery.BindParameter(2, pData.AccountInfo.szPassword, MAX_PASSWORD);

	return pQuery.Exec(qryAccount) && pQuery.Fetch();
}


//////// CERTIFY

void CDbManager::Certify( CQuery & query, LPDB_OVERLAPPED_PLUS pData, IpAddressRecentFailChecker & accountMgr )
{
	ACCOUNT_CHECK result = accountMgr.Check( pData->dwIP );
	switch( result )
	{
		case ACCOUNT_CHECK::x1TimeError:
			return g_dpCertifier.SendError(ERROR_15SEC_PREVENT, pData->dpId);
		case ACCOUNT_CHECK::x3TimeError:
			return g_dpCertifier.SendError(ERROR_15MIN_PREVENT, pData->dpId);
	}

	query.Clear();

	if (!DBQryAccount(query, *pData)) {
		return g_dpCertifier.SendError(ERROR_CERT_GENERAL, pData->dpId);
	}

	const int nError = query.GetInt( "fError" );
	if (nError != 0) {
		if (nError == 1) {
			accountMgr.SetError(pData->dwIP, true);
		}

		const int nCode = CertifyErrorToCode(nError);
		return g_dpCertifier.SendError(nCode, pData->dpId);
	}

	DWORD dwPCBangClass = query.GetInt("fPCZone");
	accountMgr.SetError(pData->dwIP, false);
	OnCertifyQueryOK(query, pData, dwPCBangClass);
}

int CDbManager::CertifyErrorToCode(int nError) {
	switch (nError) {
		case 1:  return ERROR_FLYFF_PASSWORD;
		case 3:  return ERROR_BLOCKGOLD_ACCOUNT;
		case 4:  return ERROR_FLYFF_AUTH;
		case 5:  return ERROR_FLYFF_PERMIT;
		case 6:  return ERROR_FLYFF_NEED_AGREEMENT;
		case 7:  return ERROR_FLYFF_NO_MEMBERSHIP;
		case 9:  return ERROR_FLYFF_DB_JOB_ING;
		case 91: return ERROR_FLYFF_EXPIRED_SESSION_PASSWORD;
		default: return ERROR_FLYFF_ACCOUNT;
	}
}

void CDbManager::OnCertifyQueryOK( CQuery & query, LPDB_OVERLAPPED_PLUS pData, DWORD dwPCBangClass )
{
	const int n18	= query.GetInt( "f18" );
	BYTE cbAccountFlag	= GetAccountFlag( n18, pData->AccountInfo.szAccount );
	if( ::GetLanguage() == LANG_THA )
	{
		if( (cbAccountFlag & ACCOUNT_FLAG_18) == 0x00 )
		{
			CTime cur = CTime::GetCurrentTime();
			if( cur.GetHour() >= 22 || cur.GetHour() < 6 ) 
			{
				g_dpCertifier.SendError( ERROR_TOO_LATE_PLAY, pData->dpId );
				return;
			}
		}
	}



	char lpAddr[16]	= { 0, };
	g_dpCertifier.GetPlayerAddr( pData->dpId, lpAddr );				

	g_dpAccountClient.SendAddAccount(pData->dpId,
		{
			.lpAddr = lpAddr,
			.lpszAccount = pData->AccountInfo.szAccount,
			.cbAccountFlag = cbAccountFlag,
			.dwPCBangClass = dwPCBangClass
		});
}

BYTE CDbManager::GetAccountFlag(int f18, LPCTSTR szAccount) const {
	BYTE cbAccountFlag = 0x00;

	if (f18 & 0x01) {
		cbAccountFlag |= ACCOUNT_FLAG_18;
	}

	if (m_eveSchoolAccount.contains(szAccount)) {
		cbAccountFlag |= ACCOUNT_FLAG_SCHOOLEVENT;
	}

	return cbAccountFlag;
}


//////// Close Existing Connection

void CDbManager::CloseExistingConnection(CQuery & query, LPDB_OVERLAPPED_PLUS pData) {
	query.Clear();

	// Check pasword
	if (!DBQryAccount(query, *pData)) return;
	if (query.GetInt("fError") != 0) return;

	// Disconnect
	g_dpAccountClient.SendCloseExistingConnection(pData->AccountInfo.szAccount);
}


//////// Add Account

void CDbManager::SQLAddAccount(CQuery & query, char * szAccount, char * szPassword, bool bGM) {
	query.Clear();
	char szSQL[100] = { 0, };

	const char chAuth = bGM ? 'P' : 'F';
	sprintf(szSQL, "uspAddAccount '?', '?', '%c'", chAuth);

	query.BindParameter(1, szAccount);
	query.BindParameter(2, szPassword);

	if (!query.Exec(szSQL)) {
		// error
	}
}
