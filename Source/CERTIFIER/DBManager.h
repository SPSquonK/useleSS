#pragma once

#include <boost/pool/object_pool.hpp>
#include "sqktd/static_string.h"

#include <thread>
#include "query.h"
#include "ar.h"
#include "mempool.h"
#include <dplay.h>
#include <map>
#include <mutex>
#include <memory>
#include <string>
#include <set>

using shared_ptr_timed_mutex = std::shared_ptr<std::timed_mutex>;

struct DB_OVERLAPPED_PLUS {
	enum class QUERYMODE {
		NEW_ACCOUNT,
		CERTIFY,
		CLOSE_EXISTING_CONNECTION,
	};
	
	struct ACCOUNT_INFO {
		char szAccount[MAX_ACCOUNT];
		char szPassword[MAX_PASSWORD];
	};

	QUERYMODE    nQueryMode;
	ACCOUNT_INFO AccountInfo;
	DWORD        dpId;
	DWORD        dwIP;
};

using LPDB_OVERLAPPED_PLUS = DB_OVERLAPPED_PLUS *;

class IpAddressRecentFailChecker;


class CDbManager {
public:
	static constexpr size_t DEFAULT_DB_WORKER_THREAD_NUM = 8;

	struct Worker {
		std::jthread thread;
		HANDLE ioCompletionPort = nullptr;
	};

private:
	boost::object_pool<DB_OVERLAPPED_PLUS>           m_memoryPool{ 512 };
	std::array<Worker, DEFAULT_DB_WORKER_THREAD_NUM> m_workers;

public:
	std::set<std::string>						m_eveSchoolAccount;
	char							m_szLoginPWD[256] = "";

public:
	~CDbManager();

	bool	CreateDbWorkers();

	bool	LoadEveSchoolAccount();

	void	PostQ(LPDB_OVERLAPPED_PLUS pData);

	[[nodiscard]] DB_OVERLAPPED_PLUS * Alloc(
		DB_OVERLAPPED_PLUS::QUERYMODE queryMode,
		const char * pszAccount, const char * pszPwd, DPID dpid
	);
	void DeAlloc(DB_OVERLAPPED_PLUS * const overlappedPlus) { m_memoryPool.destroy(overlappedPlus); }


private:
	void DbWorkerThread(HANDLE hIOCP, shared_ptr_timed_mutex mutex);

	static [[nodiscard]] bool DBQryAccount(CQuery & pQuery, DB_OVERLAPPED_PLUS & pData);

	void	Certify(CQuery & query, LPDB_OVERLAPPED_PLUS pData, IpAddressRecentFailChecker & accountMgr);
	static int CertifyErrorToCode(int nError);
	void	OnCertifyQueryOK(CQuery & qry, LPDB_OVERLAPPED_PLUS lpDbOverlappedPlus, DWORD dwPCBangClass);
	[[nodiscard]] BYTE GetAccountFlag(int f18, LPCTSTR szAccount) const;

	void	CloseExistingConnection(CQuery & qry, LPDB_OVERLAPPED_PLUS lpDbOverlappedPlus);

	void	SQLAddAccount( CQuery & query, char* szAccount, char* szPassword, bool bGM );
};

extern CDbManager g_DbManager;
