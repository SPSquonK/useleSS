#pragma once

#include <boost/container/static_vector.hpp>

enum class ACCOUNT_CHECK { Ok, x1TimeError, x3TimeError };

class IpAddressRecentFailChecker final {
public:
	struct IPAddressCache {
		DWORD        m_dwIP;
		unsigned int m_nError;
		time_t       m_tmError;

		[[nodiscard]] ACCOUNT_CHECK GetState() const;

		[[nodiscard]] static bool IsLocalAddress(DWORD dwIP) noexcept;
	};

private:
	boost::container::static_vector<IPAddressCache, 3> m_cache;

public:
	// Checks if the given IP address can try to log in, or if it recently failed
	[[nodiscard]] ACCOUNT_CHECK Check(DWORD dwIP);
	// Changes the error field of the IpAddressCache related to the ip of the last Check call
	void SetError(DWORD dwIP, bool isError);

private:
	void AddNewIp(DWORD dwIP);
};
