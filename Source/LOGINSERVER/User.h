#pragma once

#include <DPlay.h>
#include <map>
#include <string>
#include "MyMap.h"
#include "mempooler.h"

#include "dpmng.h"

#define	AUTH_PERIOD		300

class CLoginUser final {
public:
	DPID		m_dpid;
	DWORD		m_dwTime;
	char		m_pKey[MAX_ACCOUNT];
	DWORD		m_idPlayer;
	DWORD		m_tPingRecvd;
	DWORD		m_dwAuthKey;
	BOOL		m_bIllegal;

public:
	// Constructions
	CLoginUser( DPID dpid );

	void	SetExtra( const char* pKey, DWORD dwAuthKey );
};

class CLoginUserMng
{
private:
	std::map<DPID, std::unique_ptr<CLoginUser>>	m_dpid2User;
	std::map<std::string, CLoginUser *>	m_ac2User;

public:
	CMclCritSec		m_AddRemoveLock;
	
public:
//	Constructions
	virtual	~CLoginUserMng();
//	Operations

	void	Free( void );
	bool AddUser(DPID dpid);
	BOOL	AddUser( const char* pKey, CLoginUser * pUser );
	BOOL	RemoveUser( DPID dpid );
	CLoginUser *	GetUser( const char* pKey );
	CLoginUser *	GetUser( DPID dpid );
	[[nodiscard]] u_long GetCount();

	void	DestroyAbnormalPlayer( void );
};

inline u_long CLoginUserMng::GetCount() {
	CMclAutoLock	Lock(m_AddRemoveLock);
	return m_dpid2User.size();
}

extern CLoginUserMng g_LoginUserMng;
