#include "stdafx.h"
#include "user.h"
#include "dpdatabaseclient.h"
#include "dpcoreclient.h"
#include "dploginsrvr.h"

CLoginUser::CLoginUser( DPID dpid )
{
	m_pKey[0] = '\0';
	m_dwAuthKey	= 0;
	m_idPlayer	= 0;
	m_dpid	= dpid;
	m_tPingRecvd	= m_dwTime	= timeGetTime();
	m_bIllegal	= FALSE;
}

void CLoginUser::SetExtra( const char* pKey, DWORD dwAuthKey )
{
	strcpy( m_pKey, pKey );
	m_dwAuthKey	= dwAuthKey;
}

CLoginUserMng::~CLoginUserMng()
{
	Free();
}

void CLoginUserMng::Free() {
	CMclAutoLock Lock(m_AddRemoveLock);

	m_dpid2User.clear();
	m_ac2User.clear();
}

// dpid를 키로 콘테이너에 넣는다.
bool CLoginUserMng::AddUser(DPID dpid) {
	std::unique_ptr<CLoginUser> pUser = std::make_unique<CLoginUser>(dpid);

	CMclAutoLock Lock(m_AddRemoveLock);

	const bool inserted = m_dpid2User.emplace(dpid, std::move(pUser)).second;
	return inserted;
}

// pKey 키로 콘테이너에 넣는다.
BOOL CLoginUserMng::AddUser( const char* pKey, CLoginUser * pUser )
{
	// lock이 걸린 상태라고 가정됨 
	bool result = m_ac2User.emplace(pKey, pUser).second;
	return ( result == true );
}

CLoginUser * CLoginUserMng::GetUser(const char * pKey) {
	const auto i = m_ac2User.find(pKey);
	return i != m_ac2User.end() ? i->second : nullptr;
}

CLoginUser * CLoginUserMng::GetUser(DPID dpid) {
	const auto i = m_dpid2User.find(dpid);
	return i != m_dpid2User.end() ? i->second.get() : nullptr;
}

BOOL CLoginUserMng::RemoveUser( DPID dpid )
{
	CMclAutoLock Lock( m_AddRemoveLock );

	const auto i = m_dpid2User.find( dpid );
	if (i == m_dpid2User.end()) {
		WriteLog("RemoveUser(): dpid not found");
		return FALSE;
	}

	std::unique_ptr<CLoginUser> pUser	= std::move(i->second);
	m_dpid2User.erase( i );

	m_ac2User.erase( pUser->m_pKey );

	if( !pUser->m_bIllegal )
	{
		if( *pUser->m_pKey != '\0' )
		{
			g_dpDBClient.SendLeave( pUser->m_pKey, pUser->m_idPlayer, timeGetTime() - pUser->m_dwTime );
		}

		if( pUser->m_idPlayer > 0 )
			g_dpCoreClient.SendLeave( pUser->m_idPlayer );
	}
	else
	{
		WriteLog( "RemoveUser(): Illegal" );
	}

	pUser.reset();
	return TRUE;
}

void CLoginUserMng::DestroyAbnormalPlayer( void )
{
#ifdef __INTERNALSERVER
	return;
#endif	

	DWORD t		= timeGetTime();

	CMclAutoLock	Lock( m_AddRemoveLock );

	for (auto & pUser : m_dpid2User | std::views::values) {
		if( ( t - pUser->m_tPingRecvd ) > 90000 ) // 90
			g_dpLoginSrvr.DestroyPlayer( pUser->m_dpid );
	}
}

CLoginUserMng	g_LoginUserMng;
