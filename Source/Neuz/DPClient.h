#pragma once

#include "DPMng.h"
#include "Ar.h"
//#include "MsgHdr.h"		// 이거 include 시키지 말것. 빌드 느려짐.
#include "Obj.h"
#include "playerdata.h"

#include "RainbowRace.h"

#include "Housing.h"
#include "guild.h"

struct PLAYERDESTPOS {
	D3DXVECTOR3 vPos;
	BYTE	fForward;
#ifdef __IAOBJ0622
	OBJID	objidIAObj = NULL_ID;
#endif	// __IAOBJ0622
};

struct SNAPSHOT {
	std::optional<PLAYERDESTPOS>	playerdestpos;
	u_long	uFrameMove;
};

struct PLAYERANGLE {
	BOOL	fValid = false;
	int		nCounter = 0;
};

class CWndGuildVote;
class CDPClient : public CDPMng,
	public DPMngFeatures::SendPacketSole<CDPClient>,
	public DPMngFeatures::PacketHandler<CDPClient>
{
private:
	SNAPSHOT	m_ss;
	LONG		m_lError; 
	PLAYERANGLE		m_pa;

public:
	BOOL	m_fConn;
	BYTE	*m_pDump;
	int		m_nDumpSize;

#ifdef __EVE_NEWYEAR
	int		m_nCountdown;
#endif	// __EVE_NEWYEAR


	int		m_nMaxLoginGuild;
	u_long  m_uLoginPlayerIdGuild[ 200 ];
	u_long  m_uLoginGuildMulti[ 200 ];

	DWORD	m_dwReturnScroll;		// 귀환의 두루마리 

#ifdef __TRAFIC_1218
	CTraficLog	m_traficLog;
#endif	// __TRAFIC_1218

public:
//	Constructions
	CDPClient();
	virtual	~CDPClient();
//	Override
	virtual	void SysMessageHandler( LPDPMSG_GENERIC lpMsg, DWORD dwMsgSize, DPID idFrom );
	virtual void UserMessageHandler( LPDPMSG_GENERIC lpMsg, DWORD dwMsgSize, DPID idFrom );
//	Operations
	BOOL	Connect( LPSTR lpszAddr, USHORT uPort );
//	Writes
	LONG	GetErrorCode() { return m_lError; }
	LONG	GetNetError();
	void	SendJoin( BYTE nSlot, CMover* pMover, CRTMessenger* pRTMessenger, u_long uIdofMulti );

	void	PostPlayerAngle( bool f );
	void	FlushPlayerAngle( void );
	void	SendBlock( BYTE Gu, const char *szName, const char *szFrom );
	void	SendSkillFlag( int nSkill );
	void	SendChat( LPCSTR lpszChat );
	void	SendActMsg( CMover* pMover, DWORD dwMsg, int nParam1 = 0, int nParam2 = 0 );
	void	SendDoEquip( CItemElem* pItemElem, int nPart = -1, BOOL bResult = TRUE );
	void	SendError( int nCode, int nData );
	void	SendShipActMsg( CShip *pShip, DWORD dwMsg, int nParam1, int nParam2 );
	void	SendLocalPosFromIA( const D3DXVECTOR3 &vLocal, OBJID idIA );
	void	SendRemoveItem( CItemElem* pItemElem, int nNum );
	void	SendQueryPostMail( BYTE nItem, short nItemNum, char* lpszReceiver, int nGold, char* lpszTitle, char* lpszText );
	void	SendQueryRemoveMail( u_long nMail );
	void	SendQueryGetMailItem( u_long nMail );
	void	SendQueryGetMailGold( u_long nMail );
	void	SendQueryReadMail( u_long nMail );
	void	SendQueryMailBox( void );
	void	SendDropItem( DWORD dwItemId, short nITemNum, const D3DXVECTOR3 & vPos );
	void	SendConfirmPKPVP( u_long uidPlayer );
	void	OnSetDuel( OBJID objid, CAr & ar );
	void	OnPKRelation( OBJID objid, CAr & ar );

	void	SendConfirmTrade( CMover* pTrader );
	void	SendTrade( CMover* pTrader );
	void	SendTradePut( BYTE i, BYTE ItemType, BYTE nId, short ItemNum = 1 );
	void	SendTradePull( int i );
	void	SendTradeOk( void );
	void	SendTradeCancel( int nMode = 0 );
	void	SendMessageNote( u_long uidTo, LPCTSTR strMessage );
	void	SendTradePutGold( DWORD dwGold );
//raiders.2006.11.28
//	void	SendTradeClearGold( void );//	{	SendPacket<PACKETTYPE_TRADECLEARGOLD>();	}

	void	SendPVendorOpen( const char* szVendor );
	void	SendPVendorClose( OBJID objidVendor );
	void	SendRegisterPVendorItem( BYTE iIndex, BYTE nType, BYTE nId, short nNum, int nCost );
	void	SendUnregisterPVendorItem( BYTE iIndex );
	void	SendQueryPVendorItem( OBJID objidVendor );
	void	SendBuyPVendorItem( OBJID objidVendor, BYTE nItem, DWORD dwItemId, short nNum );
	void	SendRepairItem( DWORD* pdwIdRepair );
	void	SendMotion( DWORD dwMsg );
	void	OnMotion( OBJID objid, CAr & ar );
	void	SendSetHair( BYTE nHair, float r, float g, float b );	//, int nCost );
	void	SendPartySkillUse( int nSkill );
	void	SendPartyMemberCancle( u_long uLeader, int nMode );
	void	SendAddPartyMember(u_long uLeader);
	void	SendChangeTroup( BOOL bSendName, const char * szPartyName = "" );
	void	SendChangePartyName( const char * szPartyName );
	void	SendDuelRequest( CMover* pSrc, CMover* pDst );
	void	SendDuelYes( CMover* pSrc, CMover* pDst );
	void	SendDuelNo( CMover* pSrc );
	void	SendDuelPartyRequest( CMover* pSrc, CMover* pDst );
	void	SendDuelPartyYes( CMover* pSrc, CMover* pDst );
	void	SendDuelPartyNo( CMover* pSrc );
	void	SendPartyChangeLeader( u_long uChangerLeaderid );
	void	SendMoverFocus( u_long uidPlayer );
	void	SendChangeFace(DWORD dwFaceNum);
	
	void	SendScriptDialogReq( OBJID objid, LPCTSTR lpKey, int nGlobal1, QuestId nGlobal2, int nGlobal3, int nGlobal4 );
	void	SendOpenShopWnd( OBJID objid );
	void	SendCloseShopWnd( void );
	void	SendBuyItem( CHAR cTab, BYTE nId, short nNum, DWORD dwItemId );
	void	SendBuyChipItem( CHAR cTab, BYTE nId, short nNum, DWORD dwItemId );
	void	SendSellItem( BYTE nId, short nNum );
#ifdef __HACK_1023
	void	SendMeleeAttack( OBJMSG dwAtkMsg, OBJID objid, int nParam2, int nParam3, FLOAT fVal );
#else	// __HACK_1023
	void	SendMeleeAttack( OBJMSG dwAtkMsg, OBJID objid, int nParam2, int nParam3 );
#endif	// __HACK_1023

	void	SendUseSkill( DWORD skillId, OBJID objid, int nUseType = 0, int bControl = FALSE );
	void	SendSfxID( OBJID idTarget, int idSfxHit, DWORD dwType, DWORD dwSkill = NULL_ID, int nMaxDmgCnt = 1 );
	void	SendSetTarget( OBJID idTarget, BYTE bClear );
	void	SendTeleSkill( OBJID objid, D3DXVECTOR3 vPos );
		
	void	SendExp( EXPINTEGER nExp );

	void	SendOpenBankWnd( DWORD dwId );
	
	void	SendOpenGuildBankWnd();
	void	SendCloseGuildBankWnd();
	void	SendCloseBankWnd();

	void	SendDoUseSkillPoint(const MoverSkills & skills);
	void	SendEnterChattingRoom( u_long uidChatting );
	void	SendChatting( char * pszChat );
	void	SendOpenChattingRoom( void );
	void	SendCloseChattingRoom( void );

	void	SendPutItemGuildBank( BYTE nId, DWORD ItemNum, BYTE p_Mode );
	void	SendGetItemGuildBank( BYTE nId, DWORD ItemNum, BYTE p_Mode );
	void	SendPutItemBank( BYTE nSlot, BYTE nId, short ItemNum );
	void	SendGetItemBank( BYTE nSlot, BYTE nId, short ItemNum );
	void	SendPutItemBankToBank( BYTE nPutSlot, BYTE nSlot, BYTE nId, short ItemNum );
	void	SendPutGoldBank( BYTE nSlot, DWORD dwGold );
	void	SendGetGoldBank( BYTE nSlot, DWORD dwGold );
	void	SendPutGoldBankToBank( BYTE nPutSlot, BYTE nSlot, DWORD dwGold );
	void	SendStateModeCancel( DWORD dwStateMode, BYTE nFlag );

	void	SendCorrReq( CObj *pObj );
	void	SendCorrReq( OBJID idObj );
	void	SendCommandPlace( BYTE nType );
	void	SendScriptRemoveAllItem( DWORD dwItemId );
	void	SendScriptEquipItem( DWORD dwItemId, int nOption );
	void	SendScriptCreateItem( BYTE nItemType, DWORD dwItemId, short nNum, int nOption );
	void	SendScriptAddGold( int nGold );
	void	SendScriptRemoveQuest( int nQuestId );
	void	SendSetQuest( int nQuestIdx, int nQuest );		
	void	SendScriptReplace( DWORD dwWorld, D3DXVECTOR3 vPos );
	void	SendScriptReplaceKey( DWORD dwWorld, LPCSTR lpszKey );
	void    SendScriptAddExp( int nExp );
	void	SendCreateGuildCloak( void );
//________________________________________________________________________________
	void	SendPlayerMoved( void );
	void	SendPlayerBehavior( void );
	void	SendPlayerMoved2( BYTE nFrame = MAX_CORR_SIZE_150 );
	void	SendPlayerBehavior2( void );
	void	SendPlayerAngle( void );
		
	void	SendPlayerCorr( void );
	void	SendPlayerCorr2( void );
	void	SendPlayerDestObj( OBJID objid, float fRange = 0.0f );
	void	SendQueryGetPos( CMover* pMover );
	void	SendGetPos( const D3DXVECTOR3 & vPos, float fAngle, OBJID objid );
	void	SendCtrlCoolTimeCancel();		
	void	SendQueryGetDestObj( CMover* pMover );
	void	SendSkillTaskBar( void );

	void SendAddInTaskbar(std::optional<unsigned int> where, unsigned int nIndex, const SHORTCUT & shortcut);
	void	SendRemoveAppletTaskBar( BYTE nIndex );
	void	SendRemoveItemTaskBar( BYTE nSlotIndex, BYTE nIndex );
	void	SendAddFriendReqest( u_long uidPlayer );
	void	SendAddFriendNameReqest( const char * szName );
	void	SendFriendCancel( u_long uidLeader, u_long uidMember );
	void	SendAvailPocket( int nPocket, int nItem );
	void	SendMoveItem_Pocket( int nPocket1, int nItem, short nNum, int nPocket2 );
#ifdef __JEFF_11
	void	SendQuePetResurrection( int nItem );
#endif	// __JEFF_11
	void	SendGetFriendState();
	void	SendRemoveFriend( u_long uidPlayer );

	void	SendRandomScroll( OBJID objid, OBJID objid2 );
	void	SendEnchant( OBJID objid, OBJID objMaterialId );
	void	SendRemoveAttribute( OBJID objid );
	void	SendPiercingSize( OBJID objid1, OBJID objid2, OBJID objid3 );

	void	SendCommercialElem( DWORD dwItemId, DWORD dwItemId1 );
	void	SendCreateSfxObj( DWORD dwSfxId, u_long idPlayer = NULL_ID, BOOL bFlag = FALSE );
	void	SendSetNaviPoint( const D3DXVECTOR3 & Pos, OBJID objidTarget );
	void	OnSetNaviPoint( OBJID objid, CAr & ar );
	void	SendGuildInvite( OBJID objid );
	void	SendCreateGuild( const char* szGuild );
	void	SendDestroyGuild();
	void	SendRemoveGuildMember( u_long idMaster, u_long idPlayer );
	void	SendGuildLogo( DWORD dwLogo );
	void	SendGuildContribution( BYTE cbPxpCount, int nGold, BYTE cbItemFlag = 0);
	void    SendGuildNotice( const char* szNotice );
	void	SendGuildAuthority(const GuildPowerss & powers);
	void	SendGuilPenya( u_long uGuildId, DWORD dwType, DWORD dwPenty );
	void	SendGuildSetName( LPCTSTR szName );
	void	SendGuildRank( DWORD nVer );
	void	SendGuildMemberLv( u_long idMaster, u_long idPlayer, int nMemberLv );
	void	SendAddVote( const char* szTitle, const char* szQuestion, const char* szSelections[] );
	void	SendRemoveVote( u_long idVote );
	void	SendCloseVote( u_long idVote );
	void	SendCastVote( u_long idVote, BYTE cbSelection );
	void	UpdateGuildWnd();
	void	SendGuildClass( u_long idMaster, u_long idPlayer, BYTE nFlag );
	void	SendGuildNickName( u_long idPlayer, LPCTSTR strNickName );
	void	SendChgMaster( u_long idPlayer, u_long idPlayer2 );
	void	OnChgMaster( CAr & ar );
	void	OnSetWar( OBJID objid, CAr & ar );
	void	SendDeclWar( u_long idMaster, const char* szGuild );
	
	void	OnSurrender( CAr & ar );	// 항복
	void	OnQueryTruce( CAr & ar );

	void	OnDeclWar( CAr & ar );
	void	OnAcptWar( CAr & ar );
	void	OnWar( CAr & ar );
	void	OnWarDead( CAr & ar );
	void	OnWarEnd( CAr & ar );

	void	OnRemoveSkillInfluence( OBJID objid, CAr & ar );
	void	OnRemoveAllSkillInfluence( OBJID objid, CAr & ar );
	
	void	OnRequestGuildRank( CAr & ar );
		
//________________________________________________________________________________

	void	SendDoUseItem( DWORD dwItemId, OBJID objid, int nPart = -1 , BOOL bResult = TRUE );
	void	SendDoUseItemTarget( DWORD dwMaterial, DWORD dwTarget );
	void	SendRemoveItemLevelDown( DWORD dwId );
	void	SendAwakening( int nItem );
	void	SendBlessednessCancel( int nItem );

	void	SendSnapshot( BOOL fUnconditional = FALSE );
	void	PutPlayerDestPos( const PLAYERDESTPOS & playerDestPos, BYTE f = 0 );
	void	ClearPlayerDestPos( void );
	void	SendSfxHit( int idSfxHit, DWORD dwSkill = NULL_ID, OBJID idAttacker = NULL_ID );
	void	SendSfxClear( int idSfxHit, OBJID idMover = NULL_ID );
	void	SendQuerySetPlayerName( DWORD dwData, const char* lpszPlayer );
	void	SendSummonFriend( DWORD dwData, const char* lpszPlayer );
	void	SendSummonFriendConfirm( OBJID objid, DWORD dwData );
	void	SendSummonFriendCancel( OBJID objid, DWORD dwData );
	void	SendSummonParty( DWORD dwData );
	void	SendSummonPartyConfirm( OBJID objid, DWORD dwData );
	void	SendPetRelease( void );
	void	SendUsePetFeed( DWORD dwFeedId );	// dwFeedId : 먹이	// 입력 개수 제거
	void	SendMakePetFeed( DWORD dwMaterialId, short nNum, DWORD dwToolId );	// dwFeedId : 무기/방어구/전리품, dwToolId : 분쇄기( npc일 경우 NULL_ID )
	void	SendPetTamerMistake( DWORD dwId );
	void	SendPetTamerMiracle( DWORD dwId );
	void	SendFeedPocketInactive( void );
	void	SendModifyStatus(int nStrCount, int nStaCount, int nDexCount, int nIntCount);
	void	SendRemoveQuest( QuestId dwQuest );
	void	SendWantedGold( int nGold, LPCTSTR szMsg );
	void	SendWantedList();
	void	SendWantedName();

	void	SendWantedInfo( LPCTSTR szPlayer );
	void	SendReqLeave();	
	void	SendResurrectionOK();
	void	SendResurrectionCancel();
	void	SendChangePKPVPMode( DWORD dwMode, int nFlag );
	void	SendDoEscape( void );
	void	SendCheering( OBJID objid );
	void	OnSetCheerParam( OBJID objid, CAr & ar );
	void	SendQuerySetGuildName( LPCSTR pszGuild, BYTE nId );
	void	SendQueryEquip( OBJID objid );
	void	SendQueryEquipSetting( BOOL bAllow );
	void	SendReturnScroll( int nSelect );
	void	SendEndSkillQueue( void );
	void	SendGuildCombatWindow( void );
	void	SendGuildCombatApp( DWORD dwPenya );
	void	SendGuildCombatCancel( void );
	void	SendGCRequestStatusWindow( void );
	void	SendGCSelectPlayerWindow( void );
	void	SendGCSelectPlayer(std::vector<u_long> vecSelectPlayer, u_long uidDefender );
	void	SendGCSelectMap( int nMap );
	void	SendGCJoin( void );
	void	SendGCGetPenyaGuild( void );
	void	SendGCGetPenyaPlayer( void );
	void	SendGCTele( void );
	void	SendGCPlayerPoint( void );
	void	SendCreateMonster( DWORD dwItemId, D3DXVECTOR3 vPos );

	void SendKawibawiboStart();
	void SendKawibawiboGetItem();
	void SendReassembleStart(OBJID* pdwItemId, int count);
	void SendReassembleOpenWnd();
	void SendAlphabetOpenWnd();
	void SendAlphabetStart(OBJID* pdwItemId, int count, int nQuestionID);
	void SendFiveSystemOpenWnd();
	void SendFiveSystemBet( int nBetNum, int nBetPenya );
	void SendFiveSystemStart();
	void SendFiveSystemDestroyWnd();

	void OnUltimateWeapon( OBJID objid, CAr & ar );
	void OnUltimateTransWeapon( OBJID objid, CAr & ar );
	void OnUltimateMakeGem( CAr & ar );
	void OnUltimateSetGem( OBJID objid, CAr & ar );

	void SendUltimateMakeGem( OBJID objItemId );
	void SendUltimateSetGem( OBJID objItemWeapon, OBJID objItemGem );
	void SendUltimateRemoveGem( OBJID objItemWeapon, OBJID objItemGem );
	void SendUltimateEnchantWeapon( OBJID objItemWeapon, OBJID objItemGem );

	void OnExchange( OBJID objid, CAr & ar );
	void OnExchangeResult( CAr & ar );
	
	void SendExchange( int nMMIId, int nListNum );

	void	SendGC1to1TenderOpenWnd();
	void	SendGC1to1TenderView();
	void	SendGC1to1Tender( int nPenya );
	void	SendGC1to1TenderCancel();
	void	SendGC1to1TenderFailed();
	void	OnGC1to1TenderOpenWnd( CAr & ar );
	void	OnGC1to1TenderView( CAr & ar );
	
	void	OnGC1to1NowState( CAr & ar );
	void	OnGC1to1WarResult( CAr & ar );
	
	void	SendGC1to1MemberLienUpOpenWnd();
	void	SendGC1to1MemberLienUp(std::vector<u_long>& vecMemberId );
	void	OnGC1to1MemberLineUpOpenWnd( CAr & ar );

	void	SendGC1to1TeleportToNPC();
	void	SendGC1to1TeleportToStage();

	
	void	SendReqGuildBankLogList( BYTE byListType );
	void	OnGuildBankLogList( CAr & ar );
	void	OnSealChar( CAr & ar );
	void	OnSealCharGet( CAr & ar );

	void	SendReqHonorList();
	void	SendReqHonorTitleChange( int nChange );
	void	OnHonorListAck( CAr & ar ); 
	void	OnHonorChangeAck( OBJID objid,CAr & ar ); 

	void SendCreateAngel(const std::vector<ItemPos> & items);
	void OnAngel( OBJID objid, CAr& ar );
	void OnAngelInfo( OBJID objid, CAr & ar );

private:
	// Handlers
	void	OnGetClock( CAr & ar );
	void	OnKeepAlive( CAr & ar );
	void	OnError( CAr & ar );
	void	OnSnapshot( CAr & ar );
	void	OnJoin( CAr & ar );
	void	OnReplace( CAr & ar );
	void	OnWhisper( CAr & ar );
	void	OnSay( CAr & ar );
//	Snapshot handlers
	void	OnAddObj( OBJID objid, CAr & ar );
	void	OnRemoveObj( OBJID objid );
	void	OnChat( OBJID objid, CAr & ar );
	void	OnEventMessage( OBJID objid, CAr & ar );
	void	OnDamage( OBJID objid, CAr & ar );
	void	OnMoverDeath( OBJID objid, CAr & ar );
	void	OnCreateItem( CAr & ar );
	void	OnMoveItem( CAr & ar );
	void	OnDoEquip( OBJID objid, CAr & ar );
	void	OnShipActMsg( OBJID objid, CAr & ar );
		
	void	OnTrade( OBJID objid, CAr & ar );
	void	OnConfirmTrade( OBJID objid, CAr & ar );
	void	OnConfirmTradeCancel( OBJID objid, CAr & ar );
	void	OnTradePut( OBJID objid, CAr & ar );
	void	OnTradePutError( OBJID objid, CAr & ar );
	void	OnTradePull( OBJID objid, CAr & ar );
	void	OnTradePutGold( OBJID objid, CAr & ar );
//raiders.2006.11.28
//	void	OnTradeClearGold( OBJID objid );
	void	OnTradeCancel( OBJID objid, CAr & ar );
	void	OnTradeOk( OBJID objid, CAr & ar );
	void	OnTradeConsent( void );
	void	OnTradelastConfirm( void );
	void	OnTradelastConfirmOk( OBJID objid, CAr & ar );

	void	OnOpenShopWnd( OBJID objid, CAr & ar );
	void	OnPutItemBank( OBJID objid, CAr & ar );
	void	OnGetItemBank( OBJID objid, CAr & ar );
	void	OnPutGoldBank( OBJID objid, CAr & ar );
	void	OnMoveBankItem( OBJID objid, CAr & ar );
	void	OnUpdateBankItem( OBJID objid, CAr & ar );
	void	OnErrorBankIsFull( OBJID objid, CAr & ar );
	void	OnBank(OBJID objid, CAr & ar);
	
	void	OnGuildBankWindow( OBJID objid, CAr & ar );
	void	OnPutItemGuildBank( OBJID objid, CAr & ar );
	void	OnGetItemGuildBank( OBJID objid, CAr & ar );
	void	OnVendor( OBJID objid, CAr & ar );
	void	OnUpdateVendor( OBJID objid, CAr & ar );
	void	OnUpdateItemVariant( OBJID objid, CAr & ar );
	void	OnUpdateItemsQuantity( CAr & ar );
	void	OnPocketAttribute( CAr & ar );
	void	OnPocketAddItem( CAr & ar );
	void	OnPocketRemoveItem( CAr & ar );
#ifdef __JEFF_11
	void	OnQuePetResurrectionResult( CAr & ar );
#endif	// __JEFF_11
	void	OnSetDestParam( OBJID objid, CAr & ar );
	void	OnResetDestParam( OBJID objid, CAr & ar );
#ifdef __SPEED_SYNC_0108		// ResetDestParam speed 수정
	void	OnResetDestParamSync( OBJID objid, CAr & ar );
#endif // __SPEED_SYNC_0108		// ResetDestParam speed 수정
	void	OnSetPointParam( OBJID objid, CAr & ar );
	void	OnSetScale( OBJID objid, CAr & ar );
		
	void	OnSetPos( OBJID objid, CAr & ar );
	void	OnSetPosAngle( OBJID objid, CAr & ar );
	void	OnSetLevel( OBJID objid, CAr & ar );
	void	OnSetFlightLevel( OBJID objid, CAr & ar );
	void	OnSetExperience( OBJID objid, CAr & ar );
	void	OnSetFxp( OBJID objid, CAr & ar );
	void	OnText( CAr & ar );
#ifndef __S_SERVER_UNIFY
	void	OnDiagText( CAr & ar );
#endif // __S_SERVER_UNIFY
#ifdef __S_SERVER_UNIFY
	void	OnAllAction( CAr & ar );
#endif // __S_SERVER_UNIFY
	
	void	OnRevival( OBJID objid );
	void	OnRevivalLodestar( OBJID objid );
	void	OnRevivalLodelight( OBJID objid );

	void	OnSetGrowthLearningPoint( OBJID objid, CAr & ar );

	void	OnSetDestPos( OBJID objid, CAr & ar );
	void	OnSetDestAngle( OBJID objid, CAr & ar );
	void	OnSetMovePattern( OBJID objid, CAr & ar );
	void	OnMeleeAttack( OBJID objid, CAr & ar );
	void	OnMeleeAttack2( OBJID objid, CAr & ar );
	void	OnMagicAttack( OBJID objid, CAr & ar );
	void	OnRangeAttack( OBJID objid, CAr & ar );
	void	OnAttackSP( OBJID objid, CAr & ar );
	void	OnMoverSetDestObj( OBJID objid, CAr & ar );
	void	OnUseSkill( OBJID objid, CAr & ar );
	void	OnCreateSfxObj( OBJID objid, CAr & ar );
	void	OnRemoveSfxObj( OBJID objid, CAr & ar );
	void	OnCreateSfxAllow( OBJID objid, CAr & ar );				
	void	OnDefinedText( CAr & ar );
	void	OnChatText( CAr & ar );
	void	OnDefinedText1( CAr & ar );
	void	OnDefinedCaption( CAr & ar );
	void	OnCtrlCoolTimeCancel( CAr & ar );
	void	OnGameTimer( CAr & ar );
	void	OnGameJoin( CAr & ar );
	void	OnResurrection( OBJID objid );
	void	OnTaskBar( CAr & ar );
	void	OnErrorParty( CAr & ar );
	void	OnAddPartyMember( CAr & ar );
	void	OnPartyRequest( CAr & ar );
	void	OnPartyRequestCancel( CAr & ar );
	void	OnPartyExpLevel( CAr & ar );
	void	OnPartyChangeTroup( CAr & ar );
	void	OnPartyChangeName( CAr & ar );
	void	OnPartySkillCall( OBJID objid, CAr & ar );
	void	OnPartySkillBlitz( CAr & ar );
	void	OnPartySkillRetreat( OBJID objid );
	void	OnPartySkillSphereCircle( OBJID objid );
	void	OnSetPartyMode( CAr & ar );
	void	OnPartyChangeItemMode( CAr & ar );
	void	OnPartyChangeExpMode( CAr & ar );

	void	OnSetPartyMemberParam( CAr & ar );


	void	OnEnvironmentSetting( CAr & ar );

	void	OnEnvironmentEffect( CAr & ar );


	void	OnPartyChat( CAr & ar );
	void	OnMyPartyName( CAr & ar );
	void	OnPartyChangeLeader( CAr  & ar );
	void	OnSMMode( CAr & ar );
	void	OnSMModeAll( CAr & ar );
	void	OnResistSMMode( CAr & ar );
	void	OnCommercialElem( CAr & ar );
	void	OnMoverFocus( CAr & ar );
	void	OnPartyMapInfo( CAr & ar );

	void	OnChatting( OBJID objid, CAr & ar );
	
	void	OnCommonPlace( OBJID objid, CAr & ar );
	void	OnDoApplySkill( OBJID objid, CAr & ar );
	void	OnCommonSkill( OBJID objid, CAr & ar );
	void	OnFlyffEvent( CAr & ar );
	void	OnSetLocalEvent( CAr & ar );
	void	OnGameRate( CAr & ar );
	void	OnClearTarget( CAr & ar );
	
	void	OnEventLuaDesc( CAr & ar );
	void	OnRemoveAttributeResult( CAr & ar );
	void    OnMotionArrive( OBJID objid, CAr & ar );
	void	OnMonsterProp( CAr & ar );
	void	OnGMChat( CAr & ar );

	void	OnChangeFace( CAr & ar );
	
	void	OnGuildCombat( CAr& ar );
	void	OnQuestTextTime( CAr& ar );
	
	void	OnGCInWindow( CAr& ar );
	void	OnGCRequestStatus( CAr& ar );
	void	OnGCSelectPlayer( CAr& ar );	
	void	OnGuildCombatEnterTime( CAr & ar );
	void	OnGuildCombatNextTimeState( CAr & ar );
	void	OnGuildCombatState( CAr & ar );
	void	OnGCUserState( CAr & ar );
	void	OnGCGuildStatus( CAr & ar );
	void	OnGCJoinWarWindow( CAr & ar );
	void	OnGCGetPenyaGuild( CAr & ar );
	void	OnGCGetPenyaPlayer( CAr & ar );
	void	OnGCWinGuild( CAr & ar );
	void	OnGCBestPlayer( CAr & ar );
	void	OnGCTele( CAr & ar );
	void	OnGCDiagMessage( CAr & ar );	
	void	OnIsRequest( CAr & ar );
	public:
	void	OnGCLog( CAr & ar );
	private:
	void	OnGCLogRealTime( CAr & ar );
	void	OnGCPlayerPoint( CAr & ar );

	void	OnMiniGame( OBJID objid, CAr & ar );
	void	OnKawibawibo_Result( CAr & ar );
	void	OnReassemble_Result( CAr & ar );
	void	OnReassemble_OpenWnd( CAr & ar );
	void	OnAlphabet_OpenWnd( CAr & ar );
	void	OnAlphabet_Result( CAr & ar );
	void	OnFiveSystem_OpenWnd( CAr & ar );
	void	OnFiveSystem_Result( CAr & ar );
	void	OnSExpBoxInfo( OBJID objid, CAr & ar );
	void	OnSExpBoxCoolTime( OBJID objid, CAr & ar );
	void	OnFriendGameJoin( CAr & ar );
	void	OnAddFriend( CAr & ar );
	void	OnRemoveFriend( CAr & ar );
	void	OnAddFriendReqest( CAr & ar );
	void	OnAddFriendCancel( CAr & ar );
	void	OnFriendJoin( CAr & ar );
	void	OnFriendLogOut( CAr & ar );
	void	OnFriendNoIntercept( CAr & ar );
	void	OnFriendIntercept( CAr & ar );
	void	OnGetFriendState( CAr & ar );
	void	OnSetFriendState( CAr & ar );
	void	OnAddFriendError( CAr & ar );
	void	OnAddFriendNameReqest( CAr & ar );
	void	OnAddFriendNotConnect( CAr & ar );
	void	OnRemoveFriendState( CAr & ar );
	void	OnBlock( CAr & ar );
	void	OnDuelCount( CAr & ar );
	void	OnDuelRequest( CAr & ar );
	void	OnDuelStart( CAr & ar );
	void	OnDuelNo( CAr & ar );
	void	OnDuelCancel( CAr & ar );
	void	OnDuelPartyRequest( CAr & ar );
	void	OnDuelPartyStart( CAr & ar );
	void	OnDuelPartyNo( CAr & ar );
	void	OnDuelPartyCancel( CAr & ar );
	void	OnDuelPartyResult( CAr & ar );
	void	OnSkillFlag( CAr & ar );
	void	OnSetSkillState( CAr & ar );
	void	OnTagResult( CAr & ar );
	void	OnChangeShopCost( CAr & ar );
	void	OnItemDropRate( CAr & ar );
	void	OnSetGuildQuest( CAr & ar );
	void	OnRemoveGuildQuest( CAr & ar );
	void	OnSetQuest( OBJID objid, CAr & ar );
	void	OnScriptRemoveQuest( OBJID objid, CAr & ar );
	void	OnSetChangeJob( OBJID objid, CAr & ar );
	void	OnSetNearChangeJob( OBJID objid, CAr & ar );
	void	OnModifyMode( OBJID objid, CAr & ar );
	void	OnStateMode( OBJID objid, CAr & ar );
	void	OnReturnSay( OBJID objid, CAr & ar );
	void	OnClearUseSkill( OBJID objid );
	void	OnSetFame( OBJID objid, CAr & ar );
	void	OnSetFuel( OBJID objid, CAr & ar );
		
		//________________________________________________________________________________
	void	OnMoverMoved( OBJID objid, CAr & ar );
	void	OnMoverBehavior( OBJID objid, CAr & ar );
	void	OnMoverMoved2( OBJID objid, CAr & ar );
	void	OnMoverBehavior2( OBJID objid, CAr & ar );
	void	OnMoverAngle( OBJID objid, CAr & ar );					
	void	OnMoverCorr( OBJID objid, CAr & ar );
	void	OnMoverCorr2( OBJID objid, CAr & ar );	
	void	OnQueryGetPos( CAr & ar );
	void	OnGetPos( OBJID objid, CAr & ar );
	void	OnQueryGetDestObj( CAr & ar );
	void	OnGetDestObj( OBJID objid, CAr & ar );
	void	OnQueryPlayerData( CAr & ar );
	void	OnLogout( CAr & ar );
	void	OnCreateGuild( CAr & ar );
	void	OnDestroyGuild( CAr & ar );
	void	OnGuild( CAr & ar );
	void	OnSetGuild( OBJID objid, CAr & ar );
	void	OnAddGuildMember( CAr & ar );
	void	OnRemoveGuildMember( CAr & ar );
	void	OnGuildInvite( CAr & ar );
	void	OnAllGuilds( CAr & ar );
	void	OnGuildChat( CAr & ar );
	void	OnGuildMemberLv( CAr & ar );
	void	OnGuildClass( CAr & ar );
	void	OnGuildNickName( CAr & ar );
	void	OnGuildMemberLogin( CAr & ar );
	void	OnGuldMyGameJoin( CAr & ar );
	void	OnGuildError( CAr & ar );
	void	OnGuildLogo( CAr & ar );
	void	OnGuildContribution( CAr & ar );
	void	OnGuildNotice( CAr & ar );
	void	OnGuildAuthority( CAr & ar );
	void	OnGuildPenya( CAr & ar );
	void	OnGuildRealPenya( CAr & ar );
	void	OnGuildSetName( CAr & ar );
	void	OnGuildAddVote( CAr & ar );
	void	OnGuildModifyVote( CAr & ar );

	void	OnGuildRank( CAr & ar );
	void	OnCorrReq( OBJID objid, CAr & ar );
	void	OnPVendorOpen( OBJID objid, CAr & ar );
	void	OnPVendorClose( OBJID objid, CAr & ar );
	void	OnRegisterPVendorItem( OBJID objid, CAr & ar );
	void	OnUnregisterPVendorItem( OBJID objid, CAr & ar );
	void	OnPVendorItem( OBJID objid, CAr & ar );
	void	OnPVendorItemNum( OBJID objid, CAr & ar );

	void	OnSetHair( OBJID objid, CAr & ar );
	void	OnSetState( CAr & ar );
	void	OnCmdSetSkillLevel( CAr & ar );
	void	OnCreateSkillEffect( OBJID objid, CAr & ar );
	void	OnSetStun( OBJID objid, CAr & ar );
	void	OnSendActMsg( OBJID objid, CAr & ar );
	void	OnPushPower( OBJID objid, CAr & ar );
		
public:
//	void	SendDoCollect( CObj *pObj );
private:
//	void	OnDoCollect( OBJID objid, CAr & ar );
//	void	OnStopCollect( OBJID objid, CAr & ar );


	void	OnTag( OBJID objid, CAr & ar );
	void	OnRunScriptFunc( OBJID objid, CAr & ar );
	void	OnSchoolReport( CAr & ar );
	void	OnSexChange( OBJID objid, CAr & ar );
	void	OnRemoveQuest( CAr & ar );
	void	OnInitSkillPoint( CAr & ar );
	void	OnDoUseSkillPoint( CAr & ar );

#ifdef __EVE_NEWYEAR
	void	OnNewYear( CAr & ar )	{	m_nCountdown	= 60;	}
#endif	// __EVE_NEWYEAR
	void	OnResetBuffSkill( OBJID objid, CAr & ar );
//	void	OnEndRecoverMode( OBJID objid, CAr & ar );
	void	OnWantedInfo( CAr & ar );
	void	OnWantedList( CAr & ar );
	void	OnWantedName( CAr & ar );		
	void	OnResurrectionMessage();
	void	OnWorldMsg( OBJID objid, CAr & ar );
	void	OnSetPlayerName( CAr & ar );
	void	OnUpdatePlayerData( CAr & ar );
	void	OnEscape( OBJID objid, CAr & ar );
	void	OnSetActionPoint( OBJID objid, CAr & ar );
	void	OnEndSkillQueue();
	void	OnSnoop( CAr & ar );
	void	OnQueryEquip( OBJID objid, CAr & ar );
	void	OnReturnScrollACK( CAr & ar );
	void	OnSetTarget( OBJID objid, CAr & ar );
	void	OnPostMail( CAr & ar );
	void	OnRemoveMail( CAr & ar );
	void	OnRemoveMailItem( CAr & ar );
	void	OnMailBox( CAr & ar );


	//////////////////////////////////////////////////////////////////////////
	void	OnMailBoxReq( CAr & ar );
	//////////////////////////////////////////////////////////////////////////


	void	OnSummon( CAr & ar );
	void	OnSummonFriend( CAr & ar );
	void	OnSummonFriendConfirm( CAr & ar );
	void	OnSummonPartyConfirm( CAr & ar );
	void	OnRemoveGuildBankItem( CAr & ar );
#ifdef __EVENT_1101
	void	OnCallTheRoll( CAr & ar );
#endif	// __EVENT_1101

#ifdef __TRAFIC_1218
	void	OnTraficLog( CAr & ar );
#endif	// __TRAFIC_1218	

	void	OnPetCall( OBJID objid, CAr & ar );
	void	OnPetRelease( OBJID objid, CAr & ar );
	void	OnPetSetExp( OBJID objid, CAr & ar );
	void	OnPet( OBJID objid, CAr & ar );
	void	OnPetLevelup( OBJID objid, CAr & ar );
	void	OnPetState( OBJID objid, CAr & ar ); 
	void	OnPetFeed( OBJID objid, CAr & ar );
	void	OnPetFoodMill( OBJID objid, CAr & ar );

	void	OnSetSpeedFactor( OBJID objid, CAr & ar );
	
	void OnLegendSkillUp( CAr & ar );

public:
	void	SendQueryStartCollecting( void );
	void	SendQueryStopCollecting( void );
	void	OnStartCollecting( OBJID objid );
	void	OnStopCollecting( OBJID objid );
	void	OnRestartCollecting( OBJID objid, CAr & ar );

	void	SendOptionEnableRenderMask( BOOL bEnable );

#ifdef __NPC_BUFF
	void	SendNPCBuff( const char* szKey );
#endif // __NPC_BUFF

#ifdef __EVENTLUA_COUPON
	void	OnEventCoupon( CAr & ar );
#endif // __EVENTLUA_COUPON

	void	SendQueryPlayerData( u_long idPlayer );
	void	SendQueryPlayerData( const std::vector<PDVer> & vecPlayer );

public:
	void	SendSecretRoomTender( int nPenya );
	void	SendSecretRoomTenderCancelReturn();
	void	SendSecretRoomLineUpMember(std::vector<DWORD> vecLineUpMember );
	void	OnSecretRoomMngState( CAr & ar );
	void	OnSecretRoomInfo( CAr & ar );
	void	SendSecretRoomTenderOpenWnd();
	void 	OnSecretRoomTenderOpenWnd( CAr & ar );
	void	SendSecretRoomLineUpOpenWnd();
	void	OnSecretRoomLineUpOpenWnd( CAr & ar );
	void	SendSecretRoomEntrance();
	void	SendSecretRoomTeleportToNPC();
	void	SendSecretRoomTenderView();
	void	OnSecretRoomTenderView( CAr & ar );
	void	SendTeleportToSecretRoomDungeon();
private:

public:
	void	OnTaxInfo( CAr & ar );
	void	OnTaxSetTaxRateOpenWnd( CAr & ar );
	void	SendTaxRate( BYTE nCont, int nSalesTaxRate, int nPurchaseTaxRate );
private:

public:
	void	SendTeleportToHeavenTower( int nFloor );
private:

private:
	// 군주 입찰 결과 갱신
	void	OnElectionAddDeposit( CAr & ar );
	// 공약 설정 결과 갱신
	void	OnElectionSetPledge( CAr & ar );
	// 투표 결과 갱신
	void	OnElectionIncVote( CAr & ar );
	// 입후보 시작 상태로 변경
	void	OnElectionBeginCandidacy( CAr & ar );
	// 투표 시작 상태로 변경
	void	OnElectionBeginVote( CAr & ar );
	// 투표 종료 상태로 변경
	void	OnElectionEndVote( CAr & ar );
	// 군주 시스템 정보를 수신하여 복원
	void	OnLord( CAr & ar );
	// 군주 이벤트 시작 처리
	void	OnLEventCreate( CAr & ar );
	// 군주 이벤트 초기화 처리
	void	OnLEventInitialize( CAr & ar );
	// 군주 스킬 재사용 대기 시간 처리
	void	OnLordSkillTick( CAr & ar );
	// 군주 이벤트 지속 시간 처리
	void	OnLEventTick( CAr & ar );
	// 군주 스킬 사용
	void	OnLordSkillUse( OBJID objid, CAr & ar );
public:
	// 군주 입찰 처리 요청
	void	SendElectionAddDeposit( __int64 iDeposit );
	// 공약 설정 처리 요청
	void	SendElectionSetPledge( const char* szPledge );
	// 투표 처리 요청
	void	SendElectionIncVote( u_long idPlayer );
	// 군주 이벤트 시작 요청
	void	SendLEventCreate( int iEEvent, int iIEvent );
	// 군주 스킬 사용 요청
	void	SendLordSkillUse( int nSkill, const char* szTarget = "" );
	void	SendTransformItem( CTransformStuff & stuff );	// 알변환 요청
	void	SendPickupPetAwakeningCancel( DWORD dwItem );	// 픽업펫 각성 취소 요청

#ifdef __AZRIA_1023
	void	SendDoUseItemInput( DWORD dwData, const char* szInput );
#endif	// __AZRIA_1023
	void	SendClearPetName();
	void	OnSetPetName( OBJID objid, CAr & ar );

	void	SendTutorialState( int nState );
	void	OnTutorialState( CAr & ar );
private:
	void	OnRainbowRacePrevRankingOpenWnd( CAr & ar );
	void	OnRainbowRaceApplicationOpenWnd( CAr & ar );
	void	OnRainbowRaceNowState( CAr & ar );
	void	OnRainbowRaceMiniGameState( CAr & ar );
public:
	void	SendRainbowRacePrevRankingOpenWnd();
	void	SendRainbowRaceApplicationOpenWnd();
	void	SendRainbowRaceApplication();
	void	SendRainbowRaceReqFinish();

private:
	void	OnHousingAllInfo( CAr & ar );
	void	OnHousingSetFunitureList( CAr & ar );
	void	OnHousingSetupFurniture( CAr & ar );
	void	OnHousingPaperingInfo( CAr & ar );
	void	OnHousingSetVisitAllow( CAr & ar );
	void	OnHousingVisitableList( CAr & ar );
public:
	void	SendHousingReqSetupFurniture( HOUSINGINFO housingInfo );
	void	SendHousingReqSetVisitAllow( DWORD dwPlayerId, BOOL bAllow );
	void	SendHousingVisitRoom( DWORD dwPlayerId );
	void	SendHousingReqVisitableList();

	void	SendPropose( const char* pszTarget );
	void	SendRefuse();
	void	SendCouple();
	void	SendDecouple();
	void	OnCouple( CAr & ar );
	void	OnProposeResult( CAr & ar );
	void	OnCoupleResult( CAr & ar );
	void	OnDecoupleResult( CAr & ar );
	void	OnAddCoupleExperience( CAr & ar );

private:
	void	OnNPCPos( CAr & ar );
public:
	void	SendReqNPCPos( const char* szCharKey );
	void	OnPCBangInfo( CAr & ar );
#ifdef __VTN_TIMELIMIT
	void	OnAccountPlayTime( CAr & ar );
#endif // __VTN_TIMELIMIT
	void	SendSmeltSafety( OBJID objid, OBJID objMaterialId, OBJID objProtScrId, OBJID objSmeltScrId = NULL_ID );
	void	OnSmeltSafety( CAr & ar );
#ifdef __MAP_SECURITY
	void	OnWorldReadInfo( CAr & ar );
	void	SendMapKey( const char* szFileName, const char* szMapKey );
#endif // __MAP_SECURITY
#ifdef __QUIZ
	void	OnQuizSystemMessage( CAr & ar );
	void	OnQuizEventState( CAr & ar );
	void	OnQuizEventMessage( CAr & ar );
	void	OnQuizQuestion( CAr & ar );
	void	SendQuizEventEntrance();
	void	SendQuizEventTeleport( int nZone );
#endif // __QUIZ

	void	SendRemoveVis( int nPos );
	void	SendSwapVis( int nPos1, int nPos2 );
	void	OnActivateVisPet( CAr & ar );
	void	OnChangeMoverSfx( OBJID objId, CAr & ar );

	void	OnGuildHousePacket( CAr & ar );
	void	OnGuildHouseAllInfo( CAr & ar );
	void	OnGuildHouseRemove( CAr & ar );
	void	OnRestPoint( CAr & ar );
	void	SendTeleporterReq( const char* szCharKey, int nIndex );

	void	OnCheckedQuest( CAr & ar );
	void	SendCheckedQuestId( QuestId nQuestId, BOOL bCheck );

	void	OnInviteCampusMember( CAr & ar );
	void	OnUpdateCampus( CAr & ar );
	void	OnRemoveCampus( CAr & ar );
	void	OnUpdateCampusPoint( CAr & ar );
	void	SendInviteCampusMember( u_long idTarget );
	void	SendAcceptCampusMember( u_long idRequest );
	void	SendRefuseCampusMember( u_long idRequest );
	void	SendRemoveCampusMember( u_long idTarget );

#ifdef __PROTECT_AWAKE
	void	SendSelectedAwakeningValue( DWORD dwObjID, DWORD dwSerialNum, BYTE bySelectFlag );
	void	OnSafeAwakening( CAr& ar );
#endif

#ifdef __GUILD_HOUSE_MIDDLE
private:
	void	OnGuildHouseTenderMainWnd( CAr & ar );
	void	OnGuildHouseTenderInfoWnd( CAr & ar );
	void	OnGuildHouseTenderResult( CAr & ar );
public:
	void	SendGuildHouseTenderMainWnd( DWORD dwGHType, OBJID objNpcId );
	void	SendGuildHouseTenderInfoWnd( OBJID objGHId );
	void	SendGuildHouseTenderJoin( OBJID objGHId, int nTenderPerin, int nTenderPenya );
#endif // __GUILD_HOUSE_MIDDLE

//________________________________________________________________________________
//	Operator commands
	void	OnShout( CAr & ar );
	void	OnPlayMusic( CAr & ar );
	void	OnPlaySound( CAr & ar );
	void	OnGetPlayerAddr( CAr & ar );
	void	OnGetPlayerCount( CAr & ar );
	void	OnGetCorePlayer( CAr & ar );
	void	OnSystem( CAr & ar );
	void	OnCaption( CAr & ar );
	
	void    OnDisguise( OBJID objid, CAr & ar );
	void    OnNoDisguise( OBJID objid, CAr & ar );

private:
	BOOL	m_bEventTextColor;
};

extern CDPClient g_DPlay;
