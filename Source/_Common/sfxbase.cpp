#include "stdafx.h"
#include "model.h" 
#include "sfxbase.h"
#include "..\_Common\ParticleMng.h"
#include "Vector3Helper.h"

LPDIRECT3DVERTEXBUFFER9 CSfxMng::m_pSfxVB;
CSfxTexture g_SfxTex;
CSfxMng g_SfxMng;
CSfxMeshMng g_SfxMeshMng;

// 텍스쳐 애니메이션용 파일명 추출
// strTex : 기본 텍스쳐 파일명
// nIndex : 기본 텍스쳐 파일명에서 몇번째 뒤의 파일명인가
// ex) GetTextureName("babo03.jpg",5) 하면 리턴값 "babo08.jpg"
// 주의 : 확장자는 무조건 3자리
CString GetTextureName(CString strTex,int nIndex)
{
	CString strRet;
	CString strTemp1,strName,strExt;
	strTemp1=strTex.Left(strTex.GetLength()-4);
	strTemp1=strTemp1.Right(2);
	int nTexnumStart=atoi(strTemp1);
	strTemp1.Format("%02d",nTexnumStart+nIndex);
	strName=strTex.Left(strTex.GetLength()-6);
	strExt=strTex.Right(4);
	strRet=strName+strTemp1+strExt;
	return strRet;
}


///////////////////////////////////////////////////////////////////////////////

SfxKeyFrame SfxKeyFrame::FromFile(CResFile & file) {
	SfxKeyFrame Key;
	file.Read(&(Key.nFrame), sizeof(WORD));
	file.Read(&(Key.vPos), sizeof(D3DXVECTOR3));
	file.Read(&(Key.vPosRotate), sizeof(D3DXVECTOR3));
	file.Read(&(Key.vScale), sizeof(D3DXVECTOR3));
	file.Read(&(Key.vRotate), sizeof(D3DXVECTOR3));
	file.Read(&(Key.nAlpha), sizeof(int));
	return Key;
}



///////////////////////////////////////////////////////////////////////////////

void CSfxPart::AddAndAdjustKeyFrame(SfxKeyFrame frame) {
	auto iterationKey = m_aKeyFrames.begin();

	while (iterationKey != m_aKeyFrames.end()) {
		if (iterationKey->nFrame < frame.nFrame) {
			++iterationKey;
		} else if (iterationKey->nFrame == frame.nFrame) {
			*iterationKey = frame;
			return;
		} else {
			break;
		}
	}

	m_aKeyFrames.insert(iterationKey, frame);
}

SfxKeyFrame * CSfxPart::GetPrevKey(const WORD nFrame) {
	if (m_aKeyFrames.empty()) return nullptr;

	SfxKeyFrame * pKey = nullptr;
	for (SfxKeyFrame & pTempKey : m_aKeyFrames) {
		if (pTempKey.nFrame > nFrame) break;
		pKey = &pTempKey;
	}

	return pKey;
}

SfxKeyFrame* CSfxPart::GetNextKey(WORD nFrame)
{
	SfxKeyFrame * pKey = nullptr;

	for (SfxKeyFrame & pTempKey : m_aKeyFrames) {
		if (pTempKey.nFrame >= nFrame) {
			pKey = &pTempKey;
			break;
		}
	}

	return pKey;
}

#ifndef __WORLDSERVER
void CSfxPart::Render( D3DXVECTOR3 vPos, WORD nFrame, FLOAT fAngle, D3DXVECTOR3 vScale )
{
}
#endif
void CSfxPart::Render2( D3DXVECTOR3 vPos, WORD nFrame, D3DXVECTOR3 fAngle, D3DXVECTOR3 vScale )
{
}
void CSfxPart::Load(CResFile &file)
{
}
void CSfxPart::OldLoad(CResFile &file)
{
}
void CSfxPart::Load2(CResFile &file)
{
}

std::optional<CSfxPart::ComputedKeyResult> CSfxPart::GetComputedKey(WORD nFrame, bool usePreviousScale) const {
	const SfxKeyFrame * pPrevKey = nullptr; // <=
	const SfxKeyFrame * pNextKey = nullptr; // >

	// m_aKeyFrames is sorted by nFrame
	for (const SfxKeyFrame & key : m_aKeyFrames) {
		if (key.nFrame < nFrame) {
			pPrevKey = &key;
		} else if (key.nFrame == nFrame) {
			return ComputedKeyResult{ .key = key, .lerp = 0.0f };
		} else {
			pNextKey = &key;
			break;
		}
	}

	if (!pPrevKey || !pNextKey) return std::nullopt;

	const int dFrame = pNextKey->nFrame - pPrevKey->nFrame;
	// dFrame is > 0 because pPrevKey->nFrame < nFrame < pNextKey->nFrame

	SfxKeyFrame result;
	result.nFrame = nFrame;

	const float lerp = static_cast<float>(nFrame - pPrevKey->nFrame) / static_cast<float>(dFrame);
	D3DXVec3Lerp(&result.vPos, &pPrevKey->vPos, &pNextKey->vPos, lerp);
	D3DXVec3Lerp(&result.vPosRotate, &pPrevKey->vPosRotate, &pNextKey->vPosRotate, lerp);
	if (usePreviousScale) {
		result.vScale = pPrevKey->vScale;
	} else {
		D3DXVec3Lerp(&result.vScale, &pPrevKey->vScale, &pNextKey->vScale, lerp);
	}
	D3DXVec3Lerp(&result.vRotate, &pPrevKey->vRotate, &pNextKey->vRotate, lerp);
	result.nAlpha = static_cast<int>(std::lerp(pPrevKey->nAlpha, pNextKey->nAlpha, lerp));
	return ComputedKeyResult{ .key = result, .lerp = lerp };
}

CSfxPartBill::CSfxPartBill()
{
	m_nType=SFXPARTTYPE_BILL;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
}

void CSfxPartBill::Render2( D3DXVECTOR3 vPos, WORD nFrame, D3DXVECTOR3 fAngle, D3DXVECTOR3 vScale )
{
#ifdef __CLIENT	
	if( m_nTexFrame > 1 ) 
	{ // 텍스쳐 애니메이션일 경우
		SfxKeyFrame* pFirstKey=GetNextKey(0);
		if(nFrame<pFirstKey->nFrame) return;
		int nTexFrame= (m_nTexFrame*(nFrame-pFirstKey->nFrame)/m_nTexLoop)%m_nTexFrame; // 이 프레임에서 출력할 텍스쳐 번호
		m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(GetTextureName(m_strTex,nTexFrame)));
	}
	else 
		m_pd3dDevice->SetTexture( 0, g_SfxTex.Tex( m_strTex ) );

	const auto maybeKey = GetComputedKey(nFrame);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	D3DXVECTOR3 vScaleTemp = vScale;
	vScaleTemp.x *= Key.vScale.x;
	vScaleTemp.y *= Key.vScale.y;
	vScaleTemp.z *= Key.vScale.z;
	// 텍스쳐 타입에 따라 world 매트릭스를 설정한다
	switch(m_nBillType) 
	{
	case SFXPARTBILLTYPE_BILL: // 빌보드이면
		{ // 무조건 화면을 정면으로 바라보게 한다.
			D3DXMatrixRotationZ( &matAngle, DEGREETORADIAN( Key.vRotate.z ) );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
//			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matScale*matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM: // 바닥에 깔리는 놈이면
		{ // x축으로 90도 돌린다.
			D3DXMatrixRotationZ( &matAngle, DEGREETORADIAN( Key.vRotate.z - fAngle.z ) );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			D3DXMatrixRotationX( &matTemp2,DEGREETORADIAN( 90 ) );
			matTemp = matScale * matAngle * matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{ // 이건 기둥인데 아직 안했다.
			D3DXMatrixRotationZ( &matAngle, DEGREETORADIAN( Key.vRotate.z ) );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			matTemp2 = g_matView; matTemp2._41 = matTemp2._42 = matTemp2._43 = .0f;
//			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp = matScale * matAngle * g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL: // 보통이면.
		{ // 그냥 보통 오브젝트처럼...
			D3DXMATRIX mRot;
			D3DXVECTOR3 vRot  = DEGREETORADIAN(fAngle);
			D3DXVECTOR3 vTemp2=DEGREETORADIAN( Key.vRotate );
			D3DXMatrixRotationYawPitchRoll( &matAngle, vTemp2.y, vTemp2.x, vTemp2.z );
			D3DXMatrixRotationYawPitchRoll(&mRot,vRot.y,vRot.x,vRot.z);
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			matTemp = matScale * matAngle * mRot;
			break;
		}
	}
	D3DXMATRIX mRot;
	D3DXVECTOR3 vRot  = DEGREETORADIAN(fAngle);
	D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vPosRotate); // 위치회전값에 의한 새로운 위치도 계산하고
	D3DXMatrixRotationYawPitchRoll(&matTemp2,vTemp2.y,vTemp2.x,vTemp2.z);
	D3DXMatrixRotationYawPitchRoll(&mRot,vRot.y,vRot.x,vRot.z);

	matTemp2 = mRot * matTemp2;
	
	D3DXVec3TransformCoord(&vTemp,&Key.vPos,&matTemp2);
	D3DXMatrixTranslation( &matTrans, 
		vPos.x + ( vTemp.x * vScale.x ),
		vPos.y + ( vTemp.y * vScale.y ),
		vPos.z + ( vTemp.z * vScale.z )
		);
	matTemp1=matTemp*matTrans;

	switch(m_nAlphaType) { // 알파의 용도에 따라 렌더 스테이트를 바꿔준다
	case SFXPARTALPHATYPE_BLEND: // 블렌드이면
		{ // 반투명 설정으로...
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW: // 글로우면
		{ // 무조건 더하는걸로...
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}

	// 다른 part들도 기본적으로 이와 비슷하게 처리한다.

//	D3DXMATRIX matWorld;
//	m_pd3dDevice->GetTransform(D3DTS_WORLD,&matWorld);
	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matTemp1);
	m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha<<24 | 0x404040 );

	m_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2); // 이제 설정 다했으니 그려야지
//	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matWorld);
#endif //__CLIENT
}

#ifndef __WORLDSERVER
void CSfxPartBill::Render( D3DXVECTOR3 vPos, WORD nFrame, FLOAT fAngle, D3DXVECTOR3 vScale )
{
	if( m_nTexFrame > 1 ) 
	{ // 텍스쳐 애니메이션일 경우
		SfxKeyFrame* pFirstKey=GetNextKey(0);
		if(nFrame<pFirstKey->nFrame) return;
		int nTexFrame= (m_nTexFrame*(nFrame-pFirstKey->nFrame)/m_nTexLoop)%m_nTexFrame; // 이 프레임에서 출력할 텍스쳐 번호
		m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(GetTextureName(m_strTex,nTexFrame)));
	}
	else 
		m_pd3dDevice->SetTexture( 0, g_SfxTex.Tex( m_strTex ) );

	const auto maybeKey = GetComputedKey(nFrame);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	D3DXVECTOR3 vScaleTemp = vScale;
	vScaleTemp.x *= Key.vScale.x;
	vScaleTemp.y *= Key.vScale.y;
	vScaleTemp.z *= Key.vScale.z;
	// 텍스쳐 타입에 따라 world 매트릭스를 설정한다
	switch(m_nBillType) 
	{
	case SFXPARTBILLTYPE_BILL: // 빌보드이면
		{ // 무조건 화면을 정면으로 바라보게 한다.
			D3DXMatrixRotationZ( &matAngle, DEGREETORADIAN( Key.vRotate.z ) );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
//			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matScale*matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM: // 바닥에 깔리는 놈이면
		{ // x축으로 90도 돌린다.
			D3DXMatrixRotationZ( &matAngle, DEGREETORADIAN( Key.vRotate.z - fAngle ) );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			D3DXMatrixRotationX( &matTemp2,DEGREETORADIAN( 90 ) );
			matTemp = matScale * matAngle * matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{ // 이건 기둥인데 아직 안했다.
			D3DXMatrixRotationZ( &matAngle, DEGREETORADIAN( Key.vRotate.z ) );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			matTemp2 = g_matView; matTemp2._41 = matTemp2._42 = matTemp2._43 = .0f;
//			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp = matScale * matAngle * g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL: // 보통이면.
		{ // 그냥 보통 오브젝트처럼...
			D3DXVECTOR3 vTemp2=DEGREETORADIAN( Key.vRotate + D3DXVECTOR3( .0f, fAngle, .0f ) );
			D3DXMatrixRotationYawPitchRoll( &matAngle, vTemp2.y, vTemp2.x, vTemp2.z );
			D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
			matTemp = matScale * matAngle;
			break;
		}
	}
	D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vPosRotate+D3DXVECTOR3(.0f,fAngle,.0f)); // 위치회전값에 의한 새로운 위치도 계산하고
	D3DXMatrixRotationYawPitchRoll(&matTemp2,vTemp2.y,vTemp2.x,vTemp2.z);
	D3DXVec3TransformCoord(&vTemp,&Key.vPos,&matTemp2);
	D3DXMatrixTranslation( &matTrans, 
		vPos.x + ( vTemp.x * vScale.x ),
		vPos.y + ( vTemp.y * vScale.y ),
		vPos.z + ( vTemp.z * vScale.z )
		);
	matTemp1=matTemp*matTrans;

	switch(m_nAlphaType) { // 알파의 용도에 따라 렌더 스테이트를 바꿔준다
	case SFXPARTALPHATYPE_BLEND: // 블렌드이면
		{ // 반투명 설정으로...
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW: // 글로우면
		{ // 무조건 더하는걸로...
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}

	// 다른 part들도 기본적으로 이와 비슷하게 처리한다.

//	D3DXMATRIX matWorld;
//	m_pd3dDevice->GetTransform(D3DTS_WORLD,&matWorld);
	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matTemp1);
	m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha<<24 | 0x404040 );

	m_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2); // 이제 설정 다했으니 그려야지
//	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matWorld);
}
#endif

void CSfxPartBill::Load(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	m_bUseing = TRUE;
	
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}

	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}
void CSfxPartBill::Load2(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName=strTemp;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	file.Read(&m_bUseing,sizeof(BOOL));
	
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}
void CSfxPartBill::OldLoad(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}

CSfxPartParticle::CSfxPartParticle()
{
	m_nType=SFXPARTTYPE_PARTICLE;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
	
	m_nParticleCreate=0;
	m_nParticleCreateNum=0;

	m_nParticleFrameAppear=0;
	m_nParticleFrameKeep=0;
	m_nParticleFrameDisappear=0;

	m_fParticleStartPosVar=0;
	m_fParticleStartPosVarY=0;

	m_fParticleYLow=0;
	m_fParticleYHigh=0;
	m_fParticleXZLow=0;
	m_fParticleXZHigh=0;

	m_vParticleAccel=D3DXVECTOR3(.0f,.0f,.0f);

	m_vScale=D3DXVECTOR3(.0f,.0f,.0f);
	m_vScaleSpeed=D3DXVECTOR3(.0f,.0f,.0f);
	m_vRotation=D3DXVECTOR3(.0f,.0f,.0f);

	m_vRotationLow=D3DXVECTOR3(.0f,.0f,.0f);
	m_vRotationHigh=D3DXVECTOR3(.0f,.0f,.0f);


//#ifdef __ATEST
	m_bRepeatScal = FALSE;
	m_bRepeat = FALSE;
	m_fScalSpeedXLow  = 0;
	m_fScalSpeedXHigh = 0;
	m_fScalSpeedYLow  = 0;
	m_fScalSpeedYHigh = 0;
	m_fScalSpeedZLow  = 0;
	m_fScalSpeedZHigh = 0;
	m_vScaleEnd = D3DXVECTOR3(.0f,.0f,.0f);
	m_vScaleSpeed2 = D3DXVECTOR3(.0f,.0f,.0f);
//#endif
	
}

CSfxPartParticle::~CSfxPartParticle()
{
}
#ifndef __WORLDSERVER
void CSfxPartParticle::Render( D3DXVECTOR3 vPos, WORD nFrame, FLOAT fAngle, D3DXVECTOR3 vScale )
{
	m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(m_strTex));

	const auto maybeKey = GetComputedKey(nFrame);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	switch(m_nBillType) {
	case SFXPARTBILLTYPE_BILL:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(fAngle+Key.vRotate.z));
			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
//			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matScale*matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(fAngle+Key.vRotate.z));
			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			D3DXMatrixRotationX(&matTemp2,DEGREETORADIAN(90));
			matTemp=matScale*matAngle*matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(fAngle+Key.vRotate.z));
			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
//			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matScale*matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL:
		{
			D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vRotate);
			D3DXMatrixRotationYawPitchRoll(&matAngle,vTemp2.y,vTemp2.x,vTemp2.z);
			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp=matScale*matAngle;
			break;
		}
	}
	D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vPosRotate+D3DXVECTOR3(.0f,fAngle,.0f));
	D3DXMatrixRotationYawPitchRoll(&matTemp2,vTemp2.y,vTemp2.x,vTemp2.z);
	D3DXVec3TransformCoord(&vTemp,&Key.vPos,&matTemp2);
	//D3DXMatrixTranslation(&matTrans,vPos.x+vTemp.x,vPos.y+vTemp.y,vPos.z+vTemp.z);
	D3DXMatrixTranslation( &matTrans, 
		vPos.x + ( vTemp.x * vScale.x ), 
		vPos.y + ( vTemp.y * vScale.y ), 
		vPos.z + ( vTemp.z * vScale.z ) );
	
	matTemp1=matTemp*matTrans;

	switch(m_nAlphaType) 
	{
	case SFXPARTALPHATYPE_BLEND:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}
//	D3DXMATRIX matWorld;
//	m_pd3dDevice->GetTransform(D3DTS_WORLD,&matWorld);
	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matTemp1);
	m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha<<24 | 0x404040 );

	m_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
//	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matWorld);
}
#endif // __WORLDSERVER


void CSfxPartParticle::Load(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	m_bUseing = TRUE;
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}

	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));

	file.Read(&m_nParticleCreate,sizeof(WORD));
	file.Read(&m_nParticleCreateNum,sizeof(WORD));

	file.Read(&m_nParticleFrameAppear,sizeof(WORD));
	file.Read(&m_nParticleFrameKeep,sizeof(WORD));
	file.Read(&m_nParticleFrameDisappear,sizeof(WORD));

	file.Read(&m_fParticleStartPosVar,sizeof(FLOAT));
	file.Read(&m_fParticleStartPosVarY,sizeof(FLOAT));

	file.Read(&m_fParticleYLow,sizeof(FLOAT));
	file.Read(&m_fParticleYHigh,sizeof(FLOAT));
	file.Read(&m_fParticleXZLow,sizeof(FLOAT));
	file.Read(&m_fParticleXZHigh,sizeof(FLOAT));

	file.Read(&m_vParticleAccel,sizeof(D3DXVECTOR3));

	file.Read(&m_vScale,sizeof(D3DXVECTOR3));
	file.Read(&m_vScaleSpeed,sizeof(D3DXVECTOR3));
	m_vRotation = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vRotationLow = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vRotationHigh = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );

	m_bRepeatScal = FALSE;
	m_bRepeat = FALSE;
	m_fScalSpeedXLow   = 0;
	m_fScalSpeedXHigh  = 0;
	m_fScalSpeedYLow   = 0;
	m_fScalSpeedYHigh  = 0;
	m_fScalSpeedZLow   = 0;
	m_fScalSpeedZHigh  = 0;
	m_vScaleSpeed2 = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vScaleEnd    = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	
	// TODO: why do we ~~not 0~~ initialize nKey?
	int nKey = static_cast<int>(m_aKeyFrames.size());
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}
void CSfxPartParticle::Load2(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName=strTemp;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	file.Read(&m_bUseing,sizeof(BOOL));
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	
	file.Read(&m_nParticleCreate,sizeof(WORD));
	file.Read(&m_nParticleCreateNum,sizeof(WORD));
	
	file.Read(&m_nParticleFrameAppear,sizeof(WORD));
	file.Read(&m_nParticleFrameKeep,sizeof(WORD));
	file.Read(&m_nParticleFrameDisappear,sizeof(WORD));
	
	file.Read(&m_fParticleStartPosVar,sizeof(FLOAT));
	file.Read(&m_fParticleStartPosVarY,sizeof(FLOAT));
	
	file.Read(&m_fParticleYLow,sizeof(FLOAT));
	file.Read(&m_fParticleYHigh,sizeof(FLOAT));
	file.Read(&m_fParticleXZLow,sizeof(FLOAT));
	file.Read(&m_fParticleXZHigh,sizeof(FLOAT));
	
	file.Read(&m_vParticleAccel,sizeof(D3DXVECTOR3));
	
	file.Read(&m_vScale,sizeof(D3DXVECTOR3));
	file.Read(&m_vScaleSpeed,sizeof(D3DXVECTOR3));
	file.Read(&m_vRotation,sizeof(D3DXVECTOR3));
	file.Read(&m_vRotationLow,sizeof(D3DXVECTOR3));
	file.Read(&m_vRotationHigh,sizeof(D3DXVECTOR3));
	
	file.Read(&m_bRepeatScal,sizeof(BOOL));
	m_bRepeat = FALSE;
	file.Read(&m_fScalSpeedXLow,sizeof(FLOAT));
	file.Read(&m_fScalSpeedXHigh,sizeof(FLOAT));
	file.Read(&m_fScalSpeedYLow,sizeof(FLOAT));
	file.Read(&m_fScalSpeedYHigh,sizeof(FLOAT));
	file.Read(&m_fScalSpeedZLow,sizeof(FLOAT));
	file.Read(&m_fScalSpeedZHigh,sizeof(FLOAT));
	file.Read(&m_vScaleSpeed2,sizeof(D3DXVECTOR3));
	file.Read(&m_vScaleEnd,sizeof(D3DXVECTOR3));

	int nKey = static_cast<int>(m_aKeyFrames.size());
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}
void CSfxPartParticle::Load3(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;
	
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName=strTemp;
	
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	file.Read(&m_bUseing,sizeof(BOOL));
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	
	file.Read(&m_nParticleCreate,sizeof(WORD));
	file.Read(&m_nParticleCreateNum,sizeof(WORD));
	
	file.Read(&m_nParticleFrameAppear,sizeof(WORD));
	file.Read(&m_nParticleFrameKeep,sizeof(WORD));
	file.Read(&m_nParticleFrameDisappear,sizeof(WORD));
	
	file.Read(&m_fParticleStartPosVar,sizeof(FLOAT));
	file.Read(&m_fParticleStartPosVarY,sizeof(FLOAT));
	
	file.Read(&m_fParticleYLow,sizeof(FLOAT));
	file.Read(&m_fParticleYHigh,sizeof(FLOAT));
	file.Read(&m_fParticleXZLow,sizeof(FLOAT));
	file.Read(&m_fParticleXZHigh,sizeof(FLOAT));
	
	file.Read(&m_vParticleAccel,sizeof(D3DXVECTOR3));
	
	file.Read(&m_vScale,sizeof(D3DXVECTOR3));
	file.Read(&m_vScaleSpeed,sizeof(D3DXVECTOR3));
	file.Read(&m_vRotation,sizeof(D3DXVECTOR3));
	file.Read(&m_vRotationLow,sizeof(D3DXVECTOR3));
	file.Read(&m_vRotationHigh,sizeof(D3DXVECTOR3));
	
	file.Read(&m_bRepeatScal,sizeof(BOOL));
	file.Read(&m_bRepeat,sizeof(BOOL));
	file.Read(&m_fScalSpeedXLow,sizeof(FLOAT));
	file.Read(&m_fScalSpeedXHigh,sizeof(FLOAT));
	file.Read(&m_fScalSpeedYLow,sizeof(FLOAT));
	file.Read(&m_fScalSpeedYHigh,sizeof(FLOAT));
	file.Read(&m_fScalSpeedZLow,sizeof(FLOAT));
	file.Read(&m_fScalSpeedZHigh,sizeof(FLOAT));
	file.Read(&m_vScaleSpeed2,sizeof(D3DXVECTOR3));
	file.Read(&m_vScaleEnd,sizeof(D3DXVECTOR3));

	int nKey = static_cast<int>(m_aKeyFrames.size());
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}
void CSfxPartParticle::OldLoad(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));

	file.Read(&m_nParticleCreate,sizeof(WORD));
	file.Read(&m_nParticleCreateNum,sizeof(WORD));

	file.Read(&m_nParticleFrameAppear,sizeof(WORD));
	file.Read(&m_nParticleFrameKeep,sizeof(WORD));
	file.Read(&m_nParticleFrameDisappear,sizeof(WORD));

	file.Read(&m_fParticleStartPosVar,sizeof(FLOAT));
	file.Read(&m_fParticleStartPosVarY,sizeof(FLOAT));

	file.Read(&m_fParticleYLow,sizeof(FLOAT));
	file.Read(&m_fParticleYHigh,sizeof(FLOAT));
	file.Read(&m_fParticleXZLow,sizeof(FLOAT));
	file.Read(&m_fParticleXZHigh,sizeof(FLOAT));

	file.Read(&m_vParticleAccel,sizeof(D3DXVECTOR3));

	file.Read(&m_vScale,sizeof(D3DXVECTOR3));
	file.Read(&m_vScaleSpeed,sizeof(D3DXVECTOR3));
	m_vRotation = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vRotationLow = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vRotationHigh = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );

	m_bRepeatScal = FALSE;
	m_bRepeat = FALSE;
	m_fScalSpeedXLow   = 0;
	m_fScalSpeedXHigh  = 0;
	m_fScalSpeedYLow   = 0;
	m_fScalSpeedYHigh  = 0;
	m_fScalSpeedZLow   = 0;
	m_fScalSpeedZHigh  = 0;
	m_vScaleSpeed2 = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vScaleEnd    = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );

	int nKey = static_cast<int>(m_aKeyFrames.size());
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}

CSfxPartMesh::CSfxPartMesh()
{
	m_nType=SFXPARTTYPE_MESH;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
}
CSfxPartMesh::~CSfxPartMesh()
{
}
void CSfxPartMesh::Load(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	m_bUseing = TRUE;
/* 메쉬는 이런거 없다!
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}
*/
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}
void CSfxPartMesh::Load2(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName=strTemp;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	file.Read(&m_bUseing,sizeof(BOOL));
/* 메쉬는 이런거 없다!
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}
*/
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}


void CSfxPartMesh::OldLoad(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
}

#ifndef __WORLDSERVER
void CSfxPartMesh::Render( D3DXVECTOR3 vPos, WORD nFrame, FLOAT fAngle, D3DXVECTOR3 vScale )
{
	const auto maybeKey = GetComputedKey(nFrame);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	D3DXVECTOR3 vScaleTemp = vScale;
	vScaleTemp.x *= Key.vScale.x;
	vScaleTemp.y *= Key.vScale.y;
	vScaleTemp.z *= Key.vScale.z;
	
	{
		D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vRotate+D3DXVECTOR3(.0f,fAngle,.0f));
		D3DXMatrixRotationYawPitchRoll(&matAngle,vTemp2.y,vTemp2.x,vTemp2.z);
		D3DXMatrixScaling( &matScale, vScaleTemp.x, vScaleTemp.y, vScaleTemp.z );
		matTemp=matScale*matAngle;
		//matTemp=matScale*matAngle*g_matInvView;
	}

	D3DXVECTOR3 vTemp2 = DEGREETORADIAN( Key.vPosRotate + D3DXVECTOR3(.0f,fAngle,.0f) );
	D3DXMatrixRotationYawPitchRoll( &matTemp2, vTemp2.y, vTemp2.x, vTemp2.z );
	D3DXVec3TransformCoord( &vTemp, &Key.vPos, &matTemp2 );
	D3DXMatrixTranslation( &matTrans, 
		vPos.x + ( vTemp.x * vScale.x ), 
		vPos.y + ( vTemp.y * vScale.y ), 
		vPos.z + ( vTemp.z * vScale.z ) );
	matTemp1 = matTemp * matTrans;

	switch(m_nAlphaType) {
	case SFXPARTALPHATYPE_BLEND:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}
	m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha<<24 | 0x404040 );
	m_pd3dDevice->SetRenderState(D3DRS_ZENABLE,TRUE);
	m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
	m_pd3dDevice->SetTransform(D3DTS_WORLD,&matTemp1);
	CModelObject* pMesh=g_SfxMeshMng.Mesh(m_strTex);
	
	if(pMesh) 
	{
//		pMesh->m_nNoEffect = 1;
/*		DWORD v;
		pd3dDevice->GetTextureStageState( 0, D3DTSS_ALPHAOP,   &v);
		pd3dDevice->GetTextureStageState( 0, D3DTSS_ALPHAARG1, &v);
		pd3dDevice->GetTextureStageState( 0, D3DTSS_ALPHAARG2, &v );
		pd3dDevice->GetTextureStageState( 0, D3DTSS_COLORARG1, &v );
		pd3dDevice->GetTextureStageState( 0, D3DTSS_COLOROP,   &v );
		pd3dDevice->GetTextureStageState( 0, D3DTSS_COLORARG2, &v );
		pd3dDevice->GetRenderState( D3DRS_SRCBLEND, &v );
		pd3dDevice->GetRenderState( D3DRS_DESTBLEND, &v );
		pd3dDevice->GetRenderState( D3DRS_ALPHATESTENABLE, &v );
		pd3dDevice->GetRenderState( D3DRS_ALPHABLENDENABLE, &v ); 
*/	
		pMesh->m_nNoEffect = 1;

		if(m_nAlphaType == SFXPARTALPHATYPE_BLEND)
		{
			pMesh->m_nNoEffect = 0;
			m_pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
		}

		pMesh->Render(&matTemp1);

		m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha<<24 | 0x404040 );
		m_pd3dDevice->SetRenderState(D3DRS_ZENABLE,TRUE);
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
		m_pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
		m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
		m_pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
		m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_SELECTARG1 );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
	}
	//m_pd3dDevice->SetRenderState(D3DRS_ZENABLE,FALSE);
	//m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
}
#endif // __WORLDSERVER
CSfxPartCustomMesh::CSfxPartCustomMesh()
{
	m_nType=SFXPARTTYPE_CUSTOMMESH;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_nPoints=20;
	m_bUseing = TRUE;
}
CSfxPartCustomMesh::~CSfxPartCustomMesh()
{
}

void CSfxPartCustomMesh::Load(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	m_bUseing = TRUE;

	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}

	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
	file.Read(&m_nPoints,sizeof(int));
}
void CSfxPartCustomMesh::Load2(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName=strTemp;

	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strTex=strTemp;
	file.Read(&m_nTexFrame,sizeof(WORD));
	file.Read(&m_nTexLoop,sizeof(WORD));
	file.Read(&m_bUseing,sizeof(BOOL));
	if(m_nTexFrame>1) { // 텍스쳐 애니메이션일 경우
		for(i=0;i<m_nTexFrame;i++) { // 전부 로딩
			g_SfxTex.Tex(GetTextureName(strTemp,i));
		}
	}
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
	file.Read(&m_nPoints,sizeof(int));
}
void CSfxPartCustomMesh::OldLoad(CResFile &file)
{
	int i;
	char strTemp[255];
	int nStrLen;
	
	file.Read(&nStrLen,sizeof(int));
	file.Read(strTemp,nStrLen); strTemp[nStrLen]=0;
	m_strName = m_strTex=strTemp;
	m_nTexFrame=1;
	m_nTexLoop=1;
	m_bUseing = TRUE;
	
	file.Read(&m_nBillType,sizeof(SFXPARTBILLTYPE));
	file.Read(&m_nAlphaType,sizeof(SFXPARTALPHATYPE));
	int nKey;
	file.Read(&nKey,sizeof(int));
	for(i=0;i<nKey;i++) {
		AddAndAdjustKeyFrame(SfxKeyFrame::FromFile(file));
	}
	file.Read(&m_nPoints,sizeof(int));
}



#ifndef __WORLDSERVER
void CSfxPartCustomMesh::Render( D3DXVECTOR3 vPos, WORD nFrame, FLOAT fAngle, D3DXVECTOR3 vScale )
{
	if(m_nTexFrame>1) 
	{ // 텍스쳐 애니메이션일 경우
		SfxKeyFrame* pFirstKey=GetNextKey(0);
		if(nFrame<pFirstKey->nFrame) return;
		int nTexFrame= (m_nTexFrame*(nFrame-pFirstKey->nFrame)/m_nTexLoop)%m_nTexFrame; // 이 프레임에서 출력할 텍스쳐 번호
		m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(GetTextureName(m_strTex,nTexFrame)));
	}
	else m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(m_strTex));

	const auto maybeKey = GetComputedKey(nFrame);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	switch(m_nBillType) {
	case SFXPARTBILLTYPE_BILL:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z-fAngle));
//			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matAngle*matInv;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
//			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			D3DXMatrixRotationX(&matTemp2,DEGREETORADIAN(90));
			matTemp=matAngle*matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
//			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matAngle*matInv;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL:
		{
			D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vRotate+D3DXVECTOR3(.0f,fAngle,.0f));
			D3DXMatrixRotationYawPitchRoll(&matAngle,vTemp2.y,vTemp2.x,vTemp2.z);
//			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp=matAngle;
			break;
		}
	}
/*
	D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vPosRotate+D3DXVECTOR3(.0f,fAngle,.0f));
	D3DXMatrixRotationYawPitchRoll(&matTemp2,vTemp2.y,vTemp2.x,vTemp2.z);
	D3DXVec3TransformCoord(&vTemp,&Key.vPos,&matTemp2);
*/
	vTemp = Key.vPos;
//*
	D3DXMATRIX matAngle2;
	D3DXVECTOR3 v3Rot = DEGREETORADIAN(D3DXVECTOR3(.0f,fAngle,.0f));
	D3DXMatrixRotationYawPitchRoll(&matAngle2,v3Rot.y,v3Rot.x,v3Rot.z);
	D3DXVec3TransformCoord( &vTemp, &vTemp, &matAngle2 );
/**/
	//D3DXMatrixTranslation(&matTrans,vPos.x+vTemp.x,vPos.y+vTemp.y,vPos.z+vTemp.z);
	D3DXMatrixTranslation( &matTrans, 
		vPos.x + ( vTemp.x * vScale.x ), 
		vPos.y + ( vTemp.y * vScale.y ), 
		vPos.z + ( vTemp.z * vScale.z ) );
	
	matTemp1 = matTemp * matTrans;

	switch( m_nAlphaType ) 
	{
	case SFXPARTALPHATYPE_BLEND:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}
	// 파라미터에 따라서 버텍스 버퍼를 생성한다.
	// 커스텀 메쉬에서의 vScale의 용도는 다른 part와 다르다. vScale.x: 윗쪽 원의 반경 vScale.y: 원통의 높이 vScale.z: 아래쪽 원의 반경
	D3DSFXVERTEX *pVertices=new D3DSFXVERTEX[((m_nPoints*2)+1)*2];
	for(int i=0;i<(m_nPoints*2)+1;i++) 
	{
		pVertices[i*2  ].p = D3DXVECTOR3(sin( DEGREETORADIAN( 360/(m_nPoints*2)*i) )*Key.vScale.x, Key.vScale.y, cos( DEGREETORADIAN( 360/(m_nPoints*2)*i))*Key.vScale.x);
		pVertices[i*2+1].p = D3DXVECTOR3(sin( DEGREETORADIAN( 360/(m_nPoints*2)*i) )*Key.vScale.z, 0           , cos( DEGREETORADIAN( 360/(m_nPoints*2)*i))*Key.vScale.z);
		pVertices[i*2  ].p.x *= vScale.x;
		pVertices[i*2  ].p.y *= vScale.y;
		pVertices[i*2  ].p.z *= vScale.z;
		pVertices[i*2+1].p.x *= vScale.x;
		pVertices[i*2+1].p.y *= vScale.y;
		pVertices[i*2+1].p.z *= vScale.z;
		
		pVertices[i*2].tv1 = Key.vPosRotate.y+0.02f; pVertices[i*2+1].tv1 = Key.vPosRotate.z;
//		pVertices[i*2].tv1 = Key.vPosRotate.y; pVertices[i*2+1].tv1 = Key.vPosRotate.z;
		if(Key.vPosRotate.x==.0f) 
		{
			if(i/2*2==i) 
			{
				pVertices[i*2].tu1=.0f; pVertices[i*2+1].tu1=.0f;
			}
			else 
			{
				pVertices[i*2].tu1=1.0f; pVertices[i*2+1].tu1=1.0f;
			}
		}
		else 
		{
			pVertices[i*2].tu1=Key.vPosRotate.x*((FLOAT)i)/(m_nPoints*2); pVertices[i*2+1].tu1=Key.vPosRotate.x*((FLOAT)i)/(m_nPoints*2);
		}
	}

//	m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );
	m_pd3dDevice->SetTransform( D3DTS_WORLD, &matTemp1 );
	m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha << 24 | 0x404040 );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CW );
	m_pd3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, m_nPoints * 4, pVertices, sizeof( D3DSFXVERTEX ) );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
	m_pd3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, m_nPoints * 4, pVertices, sizeof( D3DSFXVERTEX ) );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	safe_delete_array( pVertices );
}
#endif

void CSfxPartCustomMesh::Render2( D3DXVECTOR3 vPos, WORD nFrame, D3DXVECTOR3 fAngle, D3DXVECTOR3 vScale )
{
#ifdef __CLIENT
	if(m_nTexFrame>1) 
	{ // 텍스쳐 애니메이션일 경우
		SfxKeyFrame* pFirstKey=GetNextKey(0);
		if(nFrame<pFirstKey->nFrame) return;
		int nTexFrame= (m_nTexFrame*(nFrame-pFirstKey->nFrame)/m_nTexLoop)%m_nTexFrame; // 이 프레임에서 출력할 텍스쳐 번호
		m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(GetTextureName(m_strTex,nTexFrame)));
	}
	else m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(m_strTex));

	const auto maybeKey = GetComputedKey(nFrame);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	switch(m_nBillType) {
	case SFXPARTBILLTYPE_BILL:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z-fAngle.z));
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matAngle*matInv;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			D3DXMatrixRotationX(&matTemp2,DEGREETORADIAN(90));
			matTemp=matAngle*matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			D3DXMatrixInverse(&matInv,NULL,&matTemp2);
			matTemp=matAngle*matInv;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL:
		{
			D3DXMATRIX mRot;
			D3DXVECTOR3 vRot  = DEGREETORADIAN(fAngle);
			D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vRotate);
			D3DXMatrixRotationYawPitchRoll(&matAngle,vTemp2.y,vTemp2.x,vTemp2.z);
			D3DXMatrixRotationYawPitchRoll(&mRot,vRot.y,vRot.x,vRot.z);
			matTemp=matAngle * mRot;
			break;
		}
	}

	vTemp = Key.vPos;

	D3DXMATRIX matAngle2;
	D3DXVECTOR3 v3Rot = DEGREETORADIAN(fAngle);
	D3DXMatrixRotationYawPitchRoll(&matAngle2,v3Rot.y,v3Rot.x,v3Rot.z);
	D3DXVec3TransformCoord( &vTemp, &vTemp, &matAngle2 );

	D3DXMatrixTranslation( &matTrans, 
		vPos.x + ( vTemp.x * vScale.x ), 
		vPos.y + ( vTemp.y * vScale.y ), 
		vPos.z + ( vTemp.z * vScale.z ) );
	
	matTemp1 = matTemp * matTrans;

	switch( m_nAlphaType ) 
	{
	case SFXPARTALPHATYPE_BLEND:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}
	// 파라미터에 따라서 버텍스 버퍼를 생성한다.
	// 커스텀 메쉬에서의 vScale의 용도는 다른 part와 다르다. vScale.x: 윗쪽 원의 반경 vScale.y: 원통의 높이 vScale.z: 아래쪽 원의 반경
	D3DSFXVERTEX *pVertices=new D3DSFXVERTEX[((m_nPoints*2)+1)*2];
	for(int i=0;i<(m_nPoints*2)+1;i++) 
	{
		pVertices[i*2  ].p = D3DXVECTOR3(sin( DEGREETORADIAN( 360/(m_nPoints*2)*i) )*Key.vScale.x, Key.vScale.y, cos( DEGREETORADIAN( 360/(m_nPoints*2)*i))*Key.vScale.x);
		pVertices[i*2+1].p = D3DXVECTOR3(sin( DEGREETORADIAN( 360/(m_nPoints*2)*i) )*Key.vScale.z, 0           , cos( DEGREETORADIAN( 360/(m_nPoints*2)*i))*Key.vScale.z);
		pVertices[i*2  ].p.x *= vScale.x;
		pVertices[i*2  ].p.y *= vScale.y;
		pVertices[i*2  ].p.z *= vScale.z;
		pVertices[i*2+1].p.x *= vScale.x;
		pVertices[i*2+1].p.y *= vScale.y;
		pVertices[i*2+1].p.z *= vScale.z;
		
		pVertices[i*2].tv1 = Key.vPosRotate.y+0.1f; pVertices[i*2+1].tv1 = Key.vPosRotate.z;
		if(Key.vPosRotate.x==.0f) 
		{
			if(i/2*2==i) 
			{
				pVertices[i*2].tu1=.0f; pVertices[i*2+1].tu1=.0f;
			}
			else 
			{
				pVertices[i*2].tu1=1.0f; pVertices[i*2+1].tu1=1.0f;
			}
		}
		else 
		{
			pVertices[i*2].tu1=Key.vPosRotate.x*((FLOAT)i)/(m_nPoints*2); pVertices[i*2+1].tu1=Key.vPosRotate.x*((FLOAT)i)/(m_nPoints*2);
		}
	}

	m_pd3dDevice->SetTransform( D3DTS_WORLD, &matTemp1 );
	m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, Key.nAlpha << 24 | 0x404040 );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CW );
	m_pd3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, m_nPoints * 4, pVertices, sizeof( D3DSFXVERTEX ) );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
	m_pd3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, m_nPoints * 4, pVertices, sizeof( D3DSFXVERTEX ) );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	safe_delete_array( pVertices );
#endif //__CLIENT
}

CSfxPart* CSfxBase::AddPart(SFXPARTTYPE nType)
{
	CSfxPart* pSfxPart;
	switch(nType) {
		case SFXPARTTYPE_BILL:
			{
				pSfxPart=new CSfxPartBill;
				break;
			}
		case SFXPARTTYPE_PARTICLE:
			{
				pSfxPart=new CSfxPartParticle;
				break;
			}
		case SFXPARTTYPE_MESH:
			{
				pSfxPart=new CSfxPartMesh;
				break;
			}
		case SFXPARTTYPE_CUSTOMMESH:
			{
				pSfxPart=new CSfxPartCustomMesh;
				break;
			}
	}
	pSfxPart->m_nType=nType;
	pSfxPart->m_strTex="";
	pSfxPart->m_nBillType=SFXPARTBILLTYPE_BILL;
	pSfxPart->m_nAlphaType=SFXPARTALPHATYPE_BLEND;
	m_aParts.emplace_back(std::unique_ptr<CSfxPart>(pSfxPart));
	return pSfxPart;
}


BOOL CSfxBase::Load(const std::string & filename)
{
	m_aParts.clear();

	CString strFilename=_T( MakePath( DIR_SFX, filename.c_str() ) );
	CResFile file;
	if(file.Open(strFilename,"rb")) 
	{
		char strTemp[9];
		file.Read(strTemp,8);
		strTemp[8]=0;
		CString strTemp2(strTemp);
		if(strTemp2.Left(6)=="SFX0.1") { // 버젼체크. 현재는 구버젼인지 신버젼인지만 체크해서 버젼에 맞게 로딩.
			int nPart;
			file.Read(&nPart,sizeof(int));
			m_aParts.reserve(nPart);
			for( int i=0;i<nPart;i++) 
			{
				SFXPARTTYPE nType;
				file.Read(&nType,sizeof(SFXPARTTYPE));
				CSfxPart * pPart=AddPart(nType);
				pPart->Load(file);
			}
		}
		else 
		if(strTemp2.Left(6)=="SFX0.2") { // 버젼체크. 현재는 구버젼인지 신버젼인지만 체크해서 버젼에 맞게 로딩.
			// 신버젼. 현재 SFX0.2
			int nPart;
			file.Read(&nPart,sizeof(int));
			m_aParts.reserve(nPart);
			for( int i=0;i<nPart;i++) 
			{
				SFXPARTTYPE nType;
				file.Read(&nType,sizeof(SFXPARTTYPE));
				CSfxPart * pPart=AddPart(nType);
				pPart->Load2(file);
			}
		}
		else
		if(strTemp2.Left(6)=="SFX0.3") { // 버젼체크. 현재는 구버젼인지 신버젼인지만 체크해서 버젼에 맞게 로딩.
			// 신버젼. 현재 SFX0.3
			int nPart;
			file.Read(&nPart,sizeof(int));
			for( int i=0;i<nPart;i++) 
			{
				SFXPARTTYPE nType;
				file.Read(&nType,sizeof(SFXPARTTYPE));
				CSfxPart * pPart=AddPart(nType);
				if( nType == SFXPARTTYPE_PARTICLE )
					pPart->Load3(file);
				else
					pPart->Load2(file);
			}
		}
		else
		{
			// 버젼 정보 없으면 구버젼
			file.Seek(0,SEEK_SET); // 위치 다시 돌려놓고 로딩
			int nPart;
			file.Read(&nPart,sizeof(int));
			m_aParts.reserve(nPart);
			for( int i=0;i<nPart;i++) 
			{
				SFXPARTTYPE nType;
				file.Read(&nType,sizeof(SFXPARTTYPE));
				CSfxPart * pPart=AddPart(nType);
				pPart->OldLoad(file);
			}
		}
	}
	else 
	{
		LPCTSTR szErr = Error( "CSfxBase::Load %s 찾을 수 없음", strFilename );
		//ADDERRORMSG( szErr );
		return FALSE;
	}
	file.Close();
	return TRUE;
}


CSfxMng::~CSfxMng()
{
	m_sfxBases.clear();

#ifdef __BS_EFFECT_LUA
	close_lua_sfx( );
#endif	//__BS_EFFECT_LUA
}

CSfxBase* CSfxMng::GetSfxBase( std::string_view strSfxName )
{
	const auto it = m_sfxBases.find(strSfxName);
	if (it != m_sfxBases.end()) {
		return it->second.get();
	}

	// strSfxName 로드
	CSfxBase* pSfxBase = new CSfxBase;

	if( pSfxBase->Load(std::string(strSfxName) + ".sfx"))
	{
		m_sfxBases.emplace(strSfxName, pSfxBase);
		return pSfxBase;
	}
	else 
	{
		safe_delete( pSfxBase );
		return NULL;
	}
}

HRESULT CSfxMng::RestoreDeviceObjects()
{
	m_pd3dDevice->CreateVertexBuffer( sizeof(D3DSFXVERTEX)*4, D3DUSAGE_WRITEONLY, D3DFVF_D3DSFXVERTEX, D3DPOOL_SYSTEMMEM, &m_pSfxVB, NULL );
	D3DSFXVERTEX* pVertices;
	m_pSfxVB->Lock( 0,sizeof(D3DSFXVERTEX)*4,(void**)&pVertices, 0 );
	pVertices->p=D3DXVECTOR3(-m_fScale,m_fScale,.0f);
	pVertices->tu1=.0f;
	pVertices->tv1=.0f;
	pVertices++;
	pVertices->p=D3DXVECTOR3(m_fScale,m_fScale,.0f);
	pVertices->tu1=1.0f;
	pVertices->tv1=.0f;
	pVertices++;
	pVertices->p=D3DXVECTOR3(m_fScale,-m_fScale,.0f);
	pVertices->tu1=1.0f;
	pVertices->tv1=1.0f;
	pVertices++;
	pVertices->p=D3DXVECTOR3(-m_fScale,-m_fScale,.0f);
	pVertices->tu1=.0f;
	pVertices->tv1=1.0f;
	pVertices++;

	m_pSfxVB->Unlock();
	return S_OK;
}
HRESULT CSfxMng::InvalidateDeviceObjects()
{
	SAFE_RELEASE(m_pSfxVB);
	return S_OK;
}
HRESULT CSfxMng::DeleteDeviceObjects()
{
	return S_OK;
}

#ifdef __CLIENT
#ifndef __VM_0820
MemPooler<CSfxModel>*	CSfxModel::m_pPool	= new MemPooler<CSfxModel>( 256 );
#endif	// __VM_0820
#endif	// __CLIENT
CSfxModel::CSfxModel()
{
	m_vPos=D3DXVECTOR3(0,0,0);
	m_vRotate=D3DXVECTOR3(0,0,0);
	m_vScale=D3DXVECTOR3(1,1,1);
	m_nCurFrame=0;
	m_pSfxBase=NULL;
}
CSfxModel::~CSfxModel()
{
	DeleteAll();
}

void CSfxModel::DeleteAll() {
	m_apParticles.clear();
}

std::vector<CSfxModel::Particles> CSfxModel::DuplicateStructure(CSfxBase & pSfxBase) {
	std::vector<CSfxModel::Particles> result;
	result.reserve(pSfxBase.m_aParts.size());
	for (const auto & pSfxPart : pSfxBase.m_aParts) {
		if (pSfxPart->m_nType == SFXPARTTYPE_PARTICLE) {
			result.emplace_back(Particles::value_type());
		} else {
			result.emplace_back(std::nullopt);
		}
	}
	return result;
}

void CSfxModel::SetSfx( CSfxBase* pSfxBase )
{
	m_vPos=D3DXVECTOR3(0,0,0);
	m_vRotate=D3DXVECTOR3(0,0,0);
	m_vScale=D3DXVECTOR3(1,1,1);
	m_nCurFrame=0;
	m_pSfxBase=NULL;
	
	m_pSfxBase = pSfxBase;

	m_apParticles = DuplicateStructure(*pSfxBase);

	//     - z
	//   3 | 2
	// - --+-- + x
	//   0 | 1
	//
	//   7 | 6
	// - --+-- + x
	//   4 | 5
	// 바운드 박스 사이즈 임의 지정. 이것이 있어야 컬링이 가능
	m_vMin = D3DXVECTOR3( -1.0f, 0.0f, -1.0f );
	m_vMax = D3DXVECTOR3(  1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[0] = D3DXVECTOR3( -1.0f, 1.0f, -1.0f ); 
	m_BB.m_vPos[1] = D3DXVECTOR3(  1.0f, 1.0f, -1.0f ); 
	m_BB.m_vPos[2] = D3DXVECTOR3(  1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[3] = D3DXVECTOR3( -1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[4] = D3DXVECTOR3( -1.0f, 0.0f, -1.0f );
	m_BB.m_vPos[5] = D3DXVECTOR3(  1.0f, 0.0f, -1.0f );
	m_BB.m_vPos[6] = D3DXVECTOR3(  1.0f, 0.0f,  1.0f );
	m_BB.m_vPos[7] = D3DXVECTOR3( -1.0f, 0.0f,  1.0f );
}
void CSfxModel::SetSfx( LPCTSTR szSfxName )
{
	m_pSfxBase = g_SfxMng.GetSfxBase( szSfxName );
	if( m_pSfxBase ) 
	{
		m_apParticles = DuplicateStructure(*m_pSfxBase);
	}
	//     - z
	//   3 | 2
	// - --+-- + x
	//   0 | 1
	//
	//   7 | 6
	// - --+-- + x
	//   4 | 5
	// 바운드 박스 사이즈 임의 지정. 이것이 있어야 컬링이 가능
	m_vMin = D3DXVECTOR3( -1.0f, 0.0f, -1.0f );
	m_vMax = D3DXVECTOR3(  1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[0] = D3DXVECTOR3( -1.0f, 1.0f, -1.0f ); 
	m_BB.m_vPos[1] = D3DXVECTOR3(  1.0f, 1.0f, -1.0f ); 
	m_BB.m_vPos[2] = D3DXVECTOR3(  1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[3] = D3DXVECTOR3( -1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[4] = D3DXVECTOR3( -1.0f, 0.0f, -1.0f );
	m_BB.m_vPos[5] = D3DXVECTOR3(  1.0f, 0.0f, -1.0f );
	m_BB.m_vPos[6] = D3DXVECTOR3(  1.0f, 0.0f,  1.0f );
	m_BB.m_vPos[7] = D3DXVECTOR3( -1.0f, 0.0f,  1.0f );
}

void CSfxModel::SetSfx( DWORD dwIndex )
{
	MODELELEM * lpModelElem = prj.m_modelMng.GetModelElem( OT_SFX, dwIndex );

	if( lpModelElem )
	{
		m_pSfxBase = g_SfxMng.GetSfxBase( lpModelElem->m_szName );

		if( m_pSfxBase ) 
		{
			m_apParticles = DuplicateStructure(*m_pSfxBase);
		}
	}
	//     - z
	//   3 | 2
	// - --+-- + x
	//   0 | 1
	//
	//   7 | 6
	// - --+-- + x
	//   4 | 5
	// 바운드 박스 사이즈 임의 지정. 이것이 있어야 컬링이 가능
	m_vMin = D3DXVECTOR3( -1.0f, 0.0f, -1.0f );
	m_vMax = D3DXVECTOR3(  1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[0] = D3DXVECTOR3( -1.0f, 1.0f, -1.0f ); 
	m_BB.m_vPos[1] = D3DXVECTOR3(  1.0f, 1.0f, -1.0f ); 
	m_BB.m_vPos[2] = D3DXVECTOR3(  1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[3] = D3DXVECTOR3( -1.0f, 1.0f,  1.0f );
	m_BB.m_vPos[4] = D3DXVECTOR3( -1.0f, 0.0f, -1.0f );
	m_BB.m_vPos[5] = D3DXVECTOR3(  1.0f, 0.0f, -1.0f );
	m_BB.m_vPos[6] = D3DXVECTOR3(  1.0f, 0.0f,  1.0f );
	m_BB.m_vPos[7] = D3DXVECTOR3( -1.0f, 0.0f,  1.0f );
}

BOOL CSfxModel::Render2( const D3DXMATRIX* pmWorld )
{
#ifdef __CLIENT
	if(m_pSfxBase) 
	{
		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,		D3DTOP_SELECTARG1);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,	D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,		D3DTOP_MODULATE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,	D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2,	D3DTA_TFACTOR);
		m_pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,		D3DTOP_DISABLE);
		m_pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,		D3DTOP_DISABLE);
		
		m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE);
		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
		
		m_pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
		
		for( size_t i = 0 ; i < m_pSfxBase->m_aParts.size(); ++i ) 
		{
			auto & pSfxPart = m_pSfxBase->m_aParts[i];

			if(pSfxPart->m_bUseing == FALSE )
				continue;
			
			switch(pSfxPart->m_nType )
			{
			case SFXPARTTYPE_BILL:
				{
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					
					m_pd3dDevice->SetVertexShader( NULL );
					m_pd3dDevice->SetVertexDeclaration( NULL );
					m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
					m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );

					pSfxPart->Render2( m_vPos, m_nCurFrame, m_vRotate, m_vScale );

					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
				}
				break;
			case SFXPARTTYPE_PARTICLE:
				{
					//  파티클이 프로세스 안되었으면 아직 생성되지 않았다.
					// m_apPart는 생성된 파티클들의 배열들.
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					if( i < m_apParticles.size() )
					{
						m_pd3dDevice->SetVertexShader( NULL );
						m_pd3dDevice->SetVertexDeclaration( NULL );
						m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
						m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );
						RenderParticles2( m_vPos, m_nCurFrame, m_vRotate, (CSfxPartParticle*)pSfxPart.get(), m_apParticles[ i ], m_vScale);
					}
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
				}
				break;
			case SFXPARTTYPE_MESH:
				{
				pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
				}
				break;
			case SFXPARTTYPE_CUSTOMMESH:
				{
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					m_pd3dDevice->SetVertexShader( NULL );
					m_pd3dDevice->SetVertexDeclaration( NULL );
					m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
					pSfxPart->Render2( m_vPos, m_nCurFrame, m_vRotate, m_vScale );
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
				}
				break;
			}
		}
		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
	}
#endif //__CLIENT
	return TRUE;
}
void CSfxModel::RenderParticles2( D3DXVECTOR3 vPos, WORD nFrame, D3DXVECTOR3 fAngle, CSfxPartParticle* pPartParticle, Particles & pParticles, D3DXVECTOR3 vScale )
{
#ifdef __CLIENT
	// 파티클 part는 다른 part들과 다르게 모델 오브젝트에서 직접 렌더한다.
	// 똑같은 sfx가 여러개 나올수도 있는데 각 파티클들의 데이터를 공유할 수 없기 때문이다.

	// 기본 world 매트릭스의 계산 등은 빌보드part의 그것과 크게 다르지 않다.
	m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(pPartParticle->m_strTex));

	const auto maybeKey = pPartParticle->GetComputedKey(nFrame, true);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	switch(pPartParticle->m_nBillType) 
	{
	case SFXPARTBILLTYPE_BILL:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			matTemp=matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			D3DXMatrixRotationX(&matTemp2,DEGREETORADIAN(90));
			matTemp=matAngle*matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			matTemp=matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL:
		{
			D3DXMATRIX mRot;
			D3DXVECTOR3 vRot  = DEGREETORADIAN(fAngle);
			D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vRotate);
			D3DXMatrixRotationYawPitchRoll(&matAngle,vTemp2.y,vTemp2.x,vTemp2.z);
			D3DXMatrixRotationYawPitchRoll(&mRot,vRot.y,vRot.x,vRot.z);
			//			D3DXMatrixScaling(&matScale,Key.vScale.x,Key.vScale.y,1.0f);
			matTemp=matAngle * mRot;
			break;
		}
	}
	switch(pPartParticle->m_nAlphaType) 
	{
	case SFXPARTALPHATYPE_BLEND:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}

	D3DXMATRIX  mRot;
	D3DXVECTOR3 vAngle = DEGREETORADIAN( fAngle );
	D3DXVECTOR3 vTemp2 = DEGREETORADIAN( Key.vPosRotate );
	
	D3DXMatrixRotationYawPitchRoll( &mRot, vAngle.y, vAngle.x, vAngle.z );
	D3DXMatrixRotationYawPitchRoll( &matTemp2, vTemp2.y, vTemp2.x, vTemp2.z );
	matTemp2 = mRot * matTemp2;
	D3DXVec3TransformCoord( &vTemp, &(Key.vPos), &matTemp2 );

	// 위에서 산출된 기본 world 매트릭스를 로컬 원점으로 현재 생성되어 있는 모든 파티클들을 렌더한다.
	D3DXVECTOR3 vTemp3;
	CString		TexName;
	
	if (!pParticles) return; // Probably not needed, TODO: check the logic

	for (Particle & vParticle : *pParticles) {
		Particle * pParticle = &vParticle;

		if(pPartParticle->m_nTexFrame>1) 
		{ // 텍스쳐 애니메이션일 경우
			int nTexFrame= (pPartParticle->m_nTexFrame*(pParticle->nFrame)/pPartParticle->m_nTexLoop)%pPartParticle->m_nTexFrame; // 이 프레임에서 출력할 텍스쳐 번호
			CString tempTexName = GetTextureName(pPartParticle->m_strTex,nTexFrame);
			if(TexName != tempTexName)
			{
				m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(tempTexName));
				TexName = tempTexName;
			}
		}
		else 
		{
			if(TexName != pPartParticle->m_strTex)
			{
				m_pd3dDevice->SetTexture( 0, g_SfxTex.Tex( pPartParticle->m_strTex ) );
				TexName = pPartParticle->m_strTex;
			}
		}

		if( pPartParticle->m_bRepeatScal )
		{
			
			if( D3DXVec3Length( &pParticle->vScale ) >= D3DXVec3Length( &pParticle->vScaleEnd) )
				pParticle->bSwScal = TRUE;
			else
				if( D3DXVec3Length( &pParticle->vScale ) <= D3DXVec3Length( &pParticle->vScaleStart) )
					pParticle->bSwScal = FALSE;
				
				if( pParticle->bSwScal )
					pParticle->vScale -= pParticle->vScaleSpeed;
				else
					pParticle->vScale += pParticle->vScaleSpeed;
		}

		vTemp3 = DEGREETORADIAN( Key.vRotate + Key.vPosRotate );
		D3DXMatrixRotationYawPitchRoll( &matTemp2, vTemp3.y, vTemp3.x, vTemp3.z );
		matTemp2 *= mRot;
		D3DXVec3TransformCoord( &vTemp3, &(pParticle->vPos), &matTemp2 );
		/*
		D3DXMatrixTranslation( &matTrans, 
			vTemp.x + ( vTemp3.x ) + vPos.x, 
			vTemp.y + ( vTemp3.y ) + vPos.y, 
			vTemp.z + ( vTemp3.z ) + vPos.z );
		
*/
		D3DXMatrixTranslation( &matTrans, 
			vTemp.x + ( vTemp3.x * m_vScale.x) + vPos.x, 
			vTemp.y + ( vTemp3.y * m_vScale.y) + vPos.y, 
			vTemp.z + ( vTemp3.z * m_vScale.z) + vPos.z );

		D3DXMatrixScaling( &matScale, pParticle->vScale.x * m_vScale.x, pParticle->vScale.y * m_vScale.y, 1.0f * m_vScale.z );
		//D3DXMatrixScaling( &matScale, pParticle->vScale.x, pParticle->vScale.y, 1.0f );

		//D3DXMATRIX mScale;
		//D3DXMatrixScaling( &mScale, vScale.x, vScale.y, vScale.z );
		//D3DXMatrixMultiply( &matScale, &matScale, &mScale );

//#ifdef __ATEST
		D3DXMATRIX matRot;
		D3DXVECTOR3 v3Rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		
		if (maybeKey->lerp != 0.0f) {
			v3Rot = D3DXR::LerpWith0(pParticle->vRotation, maybeKey->lerp);
		}
		
		D3DXMatrixRotationYawPitchRoll(&matRot, DEGREETORADIAN(v3Rot.y), DEGREETORADIAN(v3Rot.x), DEGREETORADIAN(v3Rot.z) );
		
		matTemp1 = matRot * matScale * matTemp * matTrans;
//#else
//		matTemp1 = matScale * matTemp * matTrans;
//#endif

		m_pd3dDevice->SetTransform( D3DTS_WORLD, &matTemp1 );

		// 설정된 파라미터와 현재 frame에 따라 알파값 계산
		int nTempAlpha=0;
		if(pParticle->nFrame<pPartParticle->m_nParticleFrameAppear) 
		{ // 나타나는 중이다
			nTempAlpha = pParticle->nFrame*Key.nAlpha/pPartParticle->m_nParticleFrameAppear;
		}
		else 
		if(pParticle->nFrame>pPartParticle->m_nParticleFrameKeep) 
		{ // 사라지는 중이다
			nTempAlpha = Key.nAlpha - (Key.nAlpha * (pParticle->nFrame-pPartParticle->m_nParticleFrameKeep) / (pPartParticle->m_nParticleFrameDisappear-pPartParticle->m_nParticleFrameKeep));
		}
		else 
		{ // 둘 다 아니면 알파값 유지중이다
			nTempAlpha=Key.nAlpha;
		}
		m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, nTempAlpha<<24 | 0x404040 );

		m_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
	}
#endif //__CLIENT
}

#ifndef __WORLDSERVER
//void CSfxModel::Render(void)
BOOL CSfxModel::RenderZ( const D3DXMATRIX* pmWorld )
{
	if(m_pSfxBase) 
	{
		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,		D3DTOP_SELECTARG1);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,	D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,		D3DTOP_MODULATE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,	D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2,	D3DTA_TFACTOR);
		m_pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,		D3DTOP_DISABLE);
		m_pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,		D3DTOP_DISABLE);

		m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE);
		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

		m_pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);

		for( size_t i = 0 ; i < m_pSfxBase->m_aParts.size(); ++i ) 
		{
			auto & pSfxPart = m_pSfxBase->m_aParts[i];
			if( pSfxPart->m_bUseing == FALSE )
				continue;

			switch(pSfxPart->m_nType )
			{
			case SFXPARTTYPE_BILL:
				{
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					m_pd3dDevice->SetVertexShader( NULL );
					m_pd3dDevice->SetVertexDeclaration( NULL );
					m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
					m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );
					pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
				}
				break;
			case SFXPARTTYPE_PARTICLE:
				{
					//  파티클이 프로세스 안되었으면 아직 생성되지 않았다.
					// m_apPart는 생성된 파티클들의 배열들.
					
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					
					if( i < m_apParticles.size() )
					{
						m_pd3dDevice->SetVertexShader( NULL );
						m_pd3dDevice->SetVertexDeclaration( NULL );
						m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
						m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );
						RenderParticles( m_vPos, m_nCurFrame, m_vRotate.y, (CSfxPartParticle*)pSfxPart.get(), m_apParticles[ i ], m_vScale);
					}

					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
					
				}
				break;
			case SFXPARTTYPE_MESH:
				{
				pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
				}
				break;
			case SFXPARTTYPE_CUSTOMMESH:
				{
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
					m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
					pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
					
				}
				break;
			}
		}
		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
	}
	return TRUE;
}

BOOL CSfxModel::Render( const D3DXMATRIX* pmWorld )
{
	if(m_pSfxBase) 
	{
		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,		D3DTOP_SELECTARG1);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,	D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,		D3DTOP_MODULATE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,	D3DTA_TEXTURE);
		m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2,	D3DTA_TFACTOR);
		m_pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,		D3DTOP_DISABLE);
		m_pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,		D3DTOP_DISABLE);

		m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE);
		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

		m_pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
		
		for( size_t i = 0 ; i < m_pSfxBase->m_aParts.size(); ++i ) 
		{
			auto & pSfxPart = m_pSfxBase->m_aParts[i];
			if(pSfxPart->m_bUseing == FALSE )
				continue;

			switch(pSfxPart->m_nType )
			{
			case SFXPARTTYPE_BILL:
				{
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					m_pd3dDevice->SetVertexShader( NULL );
					m_pd3dDevice->SetVertexDeclaration( NULL );
					m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
					m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );
					pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
				}
				break;
			case SFXPARTTYPE_PARTICLE:
				{
					//  파티클이 프로세스 안되었으면 아직 생성되지 않았다.
					// m_apPart는 생성된 파티클들의 배열들.
					
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					
					if( i < m_apParticles.size() )
					{
						m_pd3dDevice->SetVertexShader( NULL );
						m_pd3dDevice->SetVertexDeclaration( NULL );
						m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
						m_pd3dDevice->SetStreamSource( 0, CSfxMng::m_pSfxVB, 0, sizeof( D3DSFXVERTEX ) );
						RenderParticles( m_vPos, m_nCurFrame, m_vRotate.y, (CSfxPartParticle*)pSfxPart.get(), m_apParticles[ i ], m_vScale);
					}

					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
					
				}
				break;
			case SFXPARTTYPE_MESH:
				{
				pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
				}
				break;
			case SFXPARTTYPE_CUSTOMMESH:
				{
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
					m_pd3dDevice->SetFVF( D3DFVF_D3DSFXVERTEX );
					pSfxPart->Render( m_vPos, m_nCurFrame, m_vRotate.y, m_vScale );
					m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
				}
				break;
			}
		}
		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
		m_pd3dDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
	}
	return TRUE;
}

void CSfxModel::RenderParticles( D3DXVECTOR3 vPos, WORD nFrame, FLOAT fAngle, CSfxPartParticle* pPartParticle, Particles & pParticles, D3DXVECTOR3 vScale )
{
	// 파티클 part는 다른 part들과 다르게 모델 오브젝트에서 직접 렌더한다.
	// 똑같은 sfx가 여러개 나올수도 있는데 각 파티클들의 데이터를 공유할 수 없기 때문이다.

	// 기본 world 매트릭스의 계산 등은 빌보드part의 그것과 크게 다르지 않다.
	//m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(pPartParticle->m_strTex));

	const auto maybeKey = pPartParticle->GetComputedKey(nFrame, true);
	if (!maybeKey) return;
	const SfxKeyFrame & Key = maybeKey->key;

	D3DXMATRIX matTemp,matTemp1,matTemp2;
	D3DXMATRIX matTrans,matAngle,matScale,matInv;
	D3DXVECTOR3 vTemp;
	switch(pPartParticle->m_nBillType) 
	{
	case SFXPARTBILLTYPE_BILL:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			matTemp=matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_BOTTOM:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z-fAngle));
			D3DXMatrixRotationX(&matTemp2,DEGREETORADIAN(90));
			matTemp=matAngle*matTemp2;
			break;
		}
	case SFXPARTBILLTYPE_POLE:
		{
			D3DXMatrixRotationZ(&matAngle,DEGREETORADIAN(Key.vRotate.z));
			matTemp2=g_matView;	matTemp2._41=matTemp2._42=matTemp2._43=.0f;
			matTemp=matAngle*g_matInvView;
			break;
		}
	case SFXPARTBILLTYPE_NORMAL:
		{
			D3DXVECTOR3 vTemp2=DEGREETORADIAN(Key.vRotate+D3DXVECTOR3(.0f,fAngle,.0f));
			D3DXMatrixRotationYawPitchRoll(&matAngle,vTemp2.y,vTemp2.x,vTemp2.z);
			matTemp=matAngle;
			break;
		}
	}
	switch(pPartParticle->m_nAlphaType) 
	{
	case SFXPARTALPHATYPE_BLEND:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			break;
		}
	case SFXPARTALPHATYPE_GLOW:
		{
			m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			break;
		}
	}

	D3DXVECTOR3 vTemp2 = DEGREETORADIAN( Key.vPosRotate + D3DXVECTOR3(.0f,fAngle,.0f) );
	D3DXMatrixRotationYawPitchRoll( &matTemp2, vTemp2.y, vTemp2.x, vTemp2.z );
	D3DXVec3TransformCoord( &vTemp, &(Key.vPos), &matTemp2 );

	if (!pParticles) return;

	// 위에서 산출된 기본 world 매트릭스를 로컬 원점으로 현재 생성되어 있는 모든 파티클들을 렌더한다.
	D3DXVECTOR3 vTemp3;
	Particle* pParticle = NULL;
	CString TexName;

	for (Particle & vParticle : *pParticles) {
		Particle * pParticle = &vParticle;

		if(pPartParticle->m_nTexFrame>1) 
		{ // 텍스쳐 애니메이션일 경우
			int nTexFrame= (pPartParticle->m_nTexFrame*(pParticle->nFrame)/pPartParticle->m_nTexLoop)%pPartParticle->m_nTexFrame; // 이 프레임에서 출력할 텍스쳐 번호
			CString tempTexName = GetTextureName(pPartParticle->m_strTex,nTexFrame);
			if(TexName != tempTexName)
			{
				m_pd3dDevice->SetTexture(0,g_SfxTex.Tex(tempTexName));
				TexName = tempTexName;
			}
		}
		else 
		{
			if(TexName != pPartParticle->m_strTex)
			{
				m_pd3dDevice->SetTexture( 0, g_SfxTex.Tex( pPartParticle->m_strTex ) );
				TexName = pPartParticle->m_strTex;
			}
		}

		if( pPartParticle->m_bRepeatScal )
		{

			if( D3DXVec3Length( &pParticle->vScale ) >= D3DXVec3Length( &pParticle->vScaleEnd) )
				pParticle->bSwScal = TRUE;
			else
			if( D3DXVec3Length( &pParticle->vScale ) <= D3DXVec3Length( &pParticle->vScaleStart) )
				pParticle->bSwScal = FALSE;

			if( pParticle->bSwScal )
				pParticle->vScale -= pParticle->vScaleSpeed;
			else
				pParticle->vScale += pParticle->vScaleSpeed;
		}

		vTemp3 = DEGREETORADIAN( Key.vRotate + Key.vPosRotate + D3DXVECTOR3( .0f, fAngle, .0f ) );
		D3DXMatrixRotationYawPitchRoll( &matTemp2, vTemp3.y, vTemp3.x, vTemp3.z );
		D3DXVec3TransformCoord( &vTemp3, &(pParticle->vPos), &matTemp2 );

		D3DXMatrixTranslation( &matTrans, 
			vTemp.x + ( vTemp3.x * m_vScale.x) + vPos.x, 
			vTemp.y + ( vTemp3.y * m_vScale.y) + vPos.y, 
			vTemp.z + ( vTemp3.z * m_vScale.z) + vPos.z );

		D3DXMatrixScaling( &matScale, pParticle->vScale.x * m_vScale.x, pParticle->vScale.y * m_vScale.y, 1.0f * m_vScale.z );
		//D3DXMatrixScaling( &matScale, pParticle->vScale.x, pParticle->vScale.y, 1.0f );
		//D3DXVec3TransformCoord(&vPos,&vPos,&matRot);

//#ifdef __ATEST
		D3DXMATRIX matRot;
		D3DXVECTOR3 v3Rot = D3DXVECTOR3(0.0f,0.0f,0.0f);
		
		if (maybeKey->lerp != 0.0f) {
			v3Rot = D3DXR::LerpWith0(pParticle->vRotation, maybeKey->lerp);
		}

		D3DXMatrixRotationYawPitchRoll(&matRot, DEGREETORADIAN(v3Rot.y), DEGREETORADIAN(v3Rot.x), DEGREETORADIAN(v3Rot.z) );
		
		matTemp1 = matRot * matScale * matTemp * matTrans;
//#else
//		matTemp1 = matScale * matTemp * matTrans;
//#endif
		
		
		
		//D3DXMATRIX mScale;
		//D3DXMatrixScaling( &mScale, vScale.x, vScale.y, vScale.z );
		//D3DXMatrixMultiply( &matScale, &matScale, &mScale );
		
		m_pd3dDevice->SetTransform( D3DTS_WORLD, &matTemp1 );

		// 설정된 파라미터와 현재 frame에 따라 알파값 계산
		int nTempAlpha=0;
		if(pParticle->nFrame<pPartParticle->m_nParticleFrameAppear) 
		{ // 나타나는 중이다
			nTempAlpha = pParticle->nFrame*Key.nAlpha/pPartParticle->m_nParticleFrameAppear;
		}
		else 
		if(pParticle->nFrame>pPartParticle->m_nParticleFrameKeep) 
		{ // 사라지는 중이다
			nTempAlpha = Key.nAlpha - (Key.nAlpha * (pParticle->nFrame-pPartParticle->m_nParticleFrameKeep) / (pPartParticle->m_nParticleFrameDisappear-pPartParticle->m_nParticleFrameKeep));
		}
		else 
		{ // 둘 다 아니면 알파값 유지중이다
			nTempAlpha=Key.nAlpha;
		}
		m_pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, nTempAlpha<<24 | 0x404040 );

		m_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
	}
}
#endif 
BOOL CSfxModel::Process(void)
{
	BOOL ret=TRUE;

	m_nCurFrame++;
	
	// 이 sfx에 파티클 part가 포함되어 있다면 파티클의 생성, 파괴 등을 처리한다.
	for( int i = 0; i < m_apParticles.size(); i++ ) 
	{
		Particles & pParticles = m_apParticles[i];
		
		if( pParticles ) 
		{
			CSfxPartParticle * pPartParticle = (CSfxPartParticle*)(m_pSfxBase->m_aParts[i].get());

			if( pPartParticle->m_bUseing == FALSE )
				continue;

			// 파티클 이동, 생성 및 제거

			for (auto pParticle = pParticles->begin(); pParticle != pParticles->end(); /* noop */) {
				pParticle->vPos += pParticle->vSpeed;
				pParticle->vSpeed += pPartParticle->m_vParticleAccel;

				if( !pPartParticle->m_bRepeatScal )
					pParticle->vScale += pPartParticle->m_vScaleSpeed;

				pParticle->nFrame++;

				if (pParticle->nFrame >= pPartParticle->m_nParticleFrameDisappear) {
					pParticle = pParticles->erase(pParticle);
				} else {
					++pParticle;
				}
			}

			int nStartFrame = 0;
			int nEndFrame = 0;
			if( !pPartParticle->m_aKeyFrames.empty() )
			{
				nStartFrame = pPartParticle->m_aKeyFrames.front().nFrame;
				nEndFrame = pPartParticle->m_aKeyFrames.back().nFrame;
			}
			if( m_nCurFrame >= nStartFrame && m_nCurFrame <= nEndFrame - pPartParticle->m_nParticleFrameDisappear ) 
			{
				float fRand1 = xRandomF(0, 1.f);
				float fRand2 = xRandomF(0, 1.f);
				float fRand3 = xRandomF(0, 1.f);

				if( pPartParticle->m_nParticleCreate == 0 ) 
				{
					if( m_nCurFrame == nStartFrame ) 
					{
						for( int i = 0; i < pPartParticle->m_nParticleCreateNum; i++ ) 
						{
							Particle & pParticle = pParticles->emplace_back();
							pParticle.nFrame = 0;
							float fAngle = xRandomF(0.f, 360.f);
							pParticle.vPos = 
								D3DXVECTOR3(
									sin(fAngle)*pPartParticle->m_fParticleStartPosVar,
									fRand1*pPartParticle->m_fParticleStartPosVarY,
									cos(fAngle)*pPartParticle->m_fParticleStartPosVar
							);
							float fFactor = pPartParticle->m_fParticleXZLow + fRand2*( pPartParticle->m_fParticleXZHigh - pPartParticle->m_fParticleXZLow );
							pParticle.vSpeed=
								D3DXVECTOR3(
									sin(fAngle)*fFactor,
									pPartParticle->m_fParticleYLow + fRand2 * (pPartParticle->m_fParticleYHigh - pPartParticle->m_fParticleYLow ),
									cos(fAngle)*fFactor
							);
							pParticle.vScaleStart = pParticle.vScale = pPartParticle->m_vScale;
//#ifdef __ATEST
							pParticle.vRotation = D3DXVECTOR3( pPartParticle->m_vRotationLow.x + fRand1 * 
								                                (pPartParticle->m_vRotationHigh.x-pPartParticle->m_vRotationLow.x ),
																pPartParticle->m_vRotationLow.y + fRand3 * 
																(pPartParticle->m_vRotationHigh.y-pPartParticle->m_vRotationLow.y ),
																pPartParticle->m_vRotationLow.z + fRand2 * 
																(pPartParticle->m_vRotationHigh.z-pPartParticle->m_vRotationLow.z ) );
						
							pParticle.bSwScal   = FALSE;
							pParticle.vScaleEnd = pPartParticle->m_vScaleEnd;
							pParticle.vScaleSpeed = D3DXVECTOR3( pPartParticle->m_fScalSpeedXLow + fRand3 * 
								(pPartParticle->m_fScalSpeedXHigh-pPartParticle->m_fScalSpeedXLow ),
								pPartParticle->m_fScalSpeedYLow + fRand2 * 
								(pPartParticle->m_fScalSpeedYHigh-pPartParticle->m_fScalSpeedYLow),
								pPartParticle->m_fScalSpeedZLow + fRand1 * 
								(pPartParticle->m_fScalSpeedZHigh-pPartParticle->m_fScalSpeedZLow) );
//#endif
							
						}
					}
				}
				else 
				{
					if( ( (m_nCurFrame) - nStartFrame ) % pPartParticle->m_nParticleCreate == 0 ) 
					{
						for( int i = 0; i < pPartParticle->m_nParticleCreateNum; i++ ) 
						{
							Particle & pParticle = pParticles->emplace_back();
							pParticle.nFrame=0;
							float fAngle=xRandomF(0.f, 360.f);
							pParticle.vPos=
								D3DXVECTOR3(
									sin(fAngle)*pPartParticle->m_fParticleStartPosVar,
									fRand1*pPartParticle->m_fParticleStartPosVarY,
									cos(fAngle)*pPartParticle->m_fParticleStartPosVar
							);
							float fFactor = pPartParticle->m_fParticleXZLow+fRand2*(pPartParticle->m_fParticleXZHigh-pPartParticle->m_fParticleXZLow);
							pParticle.vSpeed=
								D3DXVECTOR3(
									sin(fAngle)*fFactor,
									pPartParticle->m_fParticleYLow+fRand2*(pPartParticle->m_fParticleYHigh-pPartParticle->m_fParticleYLow),
									cos(fAngle)*fFactor
							);
							pParticle.vScaleStart = pParticle.vScale = pPartParticle->m_vScale;
//#ifdef __ATEST
							pParticle.vRotation = D3DXVECTOR3( pPartParticle->m_vRotationLow.x + fRand1 * 
								(pPartParticle->m_vRotationHigh.x-pPartParticle->m_vRotationLow.x ),
								pPartParticle->m_vRotationLow.y + fRand3 * 
								(pPartParticle->m_vRotationHigh.y-pPartParticle->m_vRotationLow.y ),
								pPartParticle->m_vRotationLow.z + fRand2 * 
								(pPartParticle->m_vRotationHigh.z-pPartParticle->m_vRotationLow.z ) );

							
							pParticle.bSwScal   = FALSE;
							pParticle.vScaleEnd = pPartParticle->m_vScaleEnd;
							pParticle.vScaleSpeed = D3DXVECTOR3( pPartParticle->m_fScalSpeedXLow + fRand3 * 
								(pPartParticle->m_fScalSpeedXHigh-pPartParticle->m_fScalSpeedXLow ),
								pPartParticle->m_fScalSpeedYLow + fRand2 * 
								(pPartParticle->m_fScalSpeedYHigh-pPartParticle->m_fScalSpeedYLow),
								pPartParticle->m_fScalSpeedZLow + fRand1 * 
								(pPartParticle->m_fScalSpeedZHigh-pPartParticle->m_fScalSpeedZLow) );
							
							
//#endif

						}
					}
					else
						ret = FALSE;
				}
			}
			if( pParticles->size() > 0 ) 
				ret = FALSE;
			
			if( m_nCurFrame < nStartFrame ) 
				ret = FALSE;

			if( pPartParticle->m_bRepeat )
			{
				if( nEndFrame >= 0 )
				{
					FLOAT f = (FLOAT)m_nCurFrame / (FLOAT)nEndFrame;
					
					if( f >= 0.65f )
					{
						m_nCurFrame = (int)( nEndFrame * 0.6f );
					}
				}
				else
				{
					Error( " CSfxModel::Process()에서 nEndFrame < 0" );
				}
			}
			
		}
		else 
		{
			if( m_pSfxBase->m_aParts[i]->GetNextKey(m_nCurFrame))
				ret = FALSE;
		}
	}
	return ret;
}


///////////////////////////////////////////////////////////////////////////////

LPDIRECT3DTEXTURE9 CSfxTexture::Tex(const CString & str) {
	if (str.GetLength() == 0) return nullptr;
	
	const auto it = m_aTextures.find(str);
	if (it != m_aTextures.end()) return it->second.get();

	LPDIRECT3DTEXTURE9 pTex;
	HRESULT hr = LoadTextureFromRes(_T(MakePath(DIR_SFXTEX, str.GetString())),
		D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_A1R5G5B5,
		D3DPOOL_MANAGED, D3DX_FILTER_TRIANGLE | D3DX_FILTER_MIRROR,
		D3DX_FILTER_TRIANGLE | D3DX_FILTER_MIRROR, 0, NULL, NULL, &pTex);

	if (hr != D3D_OK) {
		TRACE("CSfxTexture::AddTex에서 텍스춰 없음 %s", str);
		return nullptr;
	}

	m_aTextures.emplace(str, unique_ptr_release<IDirect3DTexture9>(pTex));
	return pTex;
}


///////////////////////////////////////////////////////////////////////////////

CModelObject * CSfxMeshMng::Mesh(const CString & str) {
	if (str.IsEmpty()) return nullptr;

	// Look for the mesh in cache
	const auto it = m_aMeshs.find(str);
	if (it != m_aMeshs.end()) return it->second.get();

	// Load the mesh
	CModelObject * pMesh = new CModelObject;
	pMesh->InitDeviceObjects();

	const int nTex = g_Option.m_nTextureQuality;
	g_Option.m_nTextureQuality = 0;

	pMesh->LoadModel(str);
	pMesh->RestoreDeviceObjects();

	g_Option.m_nTextureQuality = nTex;

	m_aMeshs.emplace(str, std::unique_ptr<CModelObject>(pMesh));

	return pMesh;
}

HRESULT CSfxMeshMng::RestoreDeviceObjects() {
	for (auto & pMesh : m_aMeshs | std::views::values) {
		pMesh->RestoreDeviceObjects();
	}
	return S_OK;
}

HRESULT CSfxMeshMng::InvalidateDeviceObjects() {
	for (auto & pMesh : m_aMeshs | std::views::values) {
		pMesh->InvalidateDeviceObjects();
	}

	return S_OK;
}

HRESULT CSfxMeshMng::DeleteDeviceObjects() {
	for (auto & pMesh : m_aMeshs | std::views::values) {
		pMesh->DeleteDeviceObjects();
	}

	return S_OK;
}


///////////////////////////////////////////////////////////////////////////////

#ifdef __BS_EFFECT_LUA

//gmpbigsun: 외부(루아)에서 sfx를 돌리기 위함 ( 20100201 )

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SfxModelSet
SfxModelSet::SfxModelSet( const OBJID idMaster, const char* szSfxName, const char* szBoneName, BOOL bLoop, int nState )
{
	_idMaster = idMaster;

	_pModel = std::make_unique<CSfxModel>();
	_pModel->SetSfx( szSfxName );

	// 최대 프레임을 찾는다 
	for (const auto & pSfxPart : _pModel->m_pSfxBase->m_aParts) {
		const auto & keyFrames = pSfxPart->m_aKeyFrames;
		if (!keyFrames.empty()) {
			_nMaxFrame = std::max(_nMaxFrame, static_cast<int>(keyFrames.back().nFrame));
		}
	}

	strcpy( _szFileName, szSfxName );
	strcpy( _szBone, szBoneName );
	_bLoop = bLoop;

	_nState = nState;
}


bool SfxModelSet::Update() {
	CMover * pMaster = prj.GetMover(_idMaster);
	if (!pMaster) return false;

	CModelObject * pMasterModel = (CModelObject *)pMaster->GetModel();
	if (!pMasterModel) return false;

	auto pkPos = pMasterModel->GetPosBone(_szBone);
	if (!pkPos) return false;

	const D3DXMATRIX * const pMat = pMaster->GetMatrixWorldPtr();
	D3DXVec3TransformCoord(&_pModel->m_vPos, &pkPos.value(), pMat);

	//looping
	if (_pModel->m_nCurFrame > _nMaxFrame) {
		if (_bLoop)
			_pModel->m_nCurFrame = 0;
		else
			return false;
	}

	_pModel->Process();

	return true;
}

void SfxModelSet::Render() {
	_pModel->Render();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CSfxModelMng

CSfxModelMng* CSfxModelMng::_pThis = nullptr;

CSfxModelMng * CSfxModelMng::GetThis() {
	if (!_pThis) _pThis = new CSfxModelMng;

	return _pThis;
}

void CSfxModelMng::Free() { SAFE_DELETE(_pThis); }

bool CSfxModelMng::AddData(SfxModelSet::ConstructorParams sfxParams) { 
	// 꽉 찼는가?
	static constexpr int MAX_PLAY = 7;

	const auto iter = _cDatas.find(sfxParams.idMaster);
	if (iter != _cDatas.end()) {
		auto & rSMS = iter->second;

		if (rSMS.size() > MAX_PLAY + 1) return false; // Overfull

		// If the bone name is the same, delete the existing effect and
		// add it to the looping property
		const auto iter2 = std::ranges::find_if(rSMS,
			[&](const SfxModelSet & existingSfx) {
				return /* Clamp allows duplicates */ existingSfx._bLoop 
					/* location is the same Delete existing sfx */
					&& strcmp(existingSfx._szBone, sfxParams.szBoneName) == 0;
			}
		);

		if (iter2 != rSMS.end()) {
			rSMS.erase(iter2);
		} else if (rSMS.size() > MAX_PLAY) {
			return false; // Full
		}

		rSMS.emplace_back(sfxParams);
		return true;
	} else {
		// new data
		_cDatas[sfxParams.idMaster].emplace_back(sfxParams);
		return true;
	}
}

BOOL CSfxModelMng::SubData(const OBJID objID) {
	return _cDatas.erase(objID) > 0;
}

BOOL CSfxModelMng::SubData( OBJID objID, const int nState )
{
	// 해당 상태에 해당하는 모든 sfx를 삭제한다.
	auto finder = _cDatas.find( objID );
	if (finder == _cDatas.end()) return FALSE;

	BOOL bOK = FALSE;

	auto & kSubData = finder->second;

	for (auto pSfx = kSubData.begin(); pSfx != kSubData.end(); /*none*/) {
		if (pSfx->_idMaster == objID) {
			if (nState == pSfx->_nState) {
				pSfx = kSubData.erase(pSfx);

				bOK = TRUE;		//ok 찾아서 지웠음.
			} else {
				++pSfx;
			}
		}
	}

	return bOK;
}


BOOL CSfxModelMng::SubData( OBJID objID, const char* szBone )
{
	//해당 본에 링크되어 있는 모든 sfx를 삭제한다.
	auto finder = _cDatas.find(objID);
	if (finder == _cDatas.end()) return FALSE;

	BOOL bOK = FALSE;

	auto & kSubData = finder->second;

	for (auto pSfx = kSubData.begin(); pSfx != kSubData.end(); /*none*/) {
		if (pSfx->_idMaster == objID) {
			if (0 == strcmp(pSfx->_szBone, szBone)) {
				pSfx = kSubData.erase(pSfx);

				bOK = TRUE;		//ok 찾아서 지웠음.
			} else {
				++pSfx;
			}
		}
	}

	return bOK;
}

void CSfxModelMng::Update( )
{
	if (!_cWaitingObj.empty()) {
		for (const auto & [moverId, nState] : _cWaitingObj) {
			if (CMover * pMover = prj.GetMover(moverId)) {
				// Official v21 does this instad of Removing / adding flags:
				//if (pMover->IsNPC()) {
				//	run_lua_sfx(nState, pMover->GetId(), pMover->GetNameO3D());
				//}

				pMover->m_pActMover->RemoveStateFlag(nState);		//빼고
				pMover->m_pActMover->AddStateFlag(nState);		//넣으면 lua가 발동
			}
		}

		_cWaitingObj.clear();
	}

	for (auto iter = _cDatas.begin(); iter != _cDatas.end(); ) {
		auto & cSMS = iter->second;

		//sub loop
		for (auto pSfx = cSMS.begin(); pSfx != cSMS.end(); ) {
			if (pSfx->Update()) {
				++pSfx;
			} else {
				pSfx = cSMS.erase(pSfx);
			}
		}

		// main loop
		if (cSMS.empty()) {
			iter = _cDatas.erase(iter);
		} else {
			++iter;
		}
	}
}

void CSfxModelMng::Render() {
	for (auto & cSMS : _cDatas | std::views::values) {
		for (SfxModelSet & pSfx : cSMS) {
			pSfx.Render();
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// lua for sfx

lua_State* g_SfxLua = NULL;

int call_sfx( lua_State* L )
{
	// 루아에서 call ( lua glue )
	int n = lua_gettop( L ); 

	SfxModelSet::ConstructorParams sfxParams;
	sfxParams.idMaster = lua_tointeger( L, 1 );				// master id
	[[maybe_unused]] int nKind = lua_tointeger( L, 2 );				// kind of the effect
	sfxParams.szSfxName = lua_tostring( L, 3 );		// sfx
	sfxParams.szBoneName = lua_tostring( L, 4 );		// bone
	sfxParams.bLoop = lua_tointeger( L, 5 );				// is loop ?
	sfxParams.nState = lua_tointeger( L, 6 );				// state ( create, battle ... )

	CMover* pMover = prj.GetMover(sfxParams.idMaster);
	if (!pMover) {
		assert(0);
		return 0;
	}

	//! 생성하기 전에 필히 검사해야함.
	CSfxModelMng::GetThis()->AddData(sfxParams);
	return 0;
}

int call_mte( lua_State* L )
{
	// 루아에서 call ( lua glue )
	// multitexture effect
	int n = lua_gettop( L ); 

	int who = lua_tointeger( L, 1 );				// master id
	int nKind = lua_tointeger( L, 2 );				// kind of the effect
	const char* sz2ndDDS = lua_tostring( L, 3 );	// 2nd dds

	CMover* pMover = prj.GetMover( who );
	if( !pMover )
	{
		assert( 0 );
		return 0;
	}

	if( !pMover->IsNPC( ) )
		return 0;

	CModelObject* pModel = (CModelObject*)pMover->GetModel();
	if( !pModel )
		return 0;

	CObject3D* pObj = pModel->GetObject3D( );
	if( !pObj )
		return 0;

	pObj->m_dwEffect_EX |= XE_MTE;

	char szDDS[ 64 ];
	strcpy( szDDS, sz2ndDDS );
	strcat( szDDS, ".dds" );
	BOOL bOK = pMover->SetDataMTE( pMover->GetNameO3D(), szDDS );

	if( !bOK )
		Error( "please check the mte texture %s", pMover->GetNameO3D() );

	return 0;
}

int stop_sfx_bybone( lua_State* L )
{
	//2010_05_10 gmpbigsun : 일단 만들어놨따.. 혹시나 어떤상태가 끝나면 어떤본에 링크된 sfx만 지우고싶을때를 대비해서..
	int n = lua_gettop( L );

	int who = lua_tointeger( L, 1 );
	const char* szBone = lua_tostring( L, 2 );

	CMover* pMover = prj.GetMover( who );
	if( !pMover )
	{
		assert( 0 );
		return 0;
	}

	BOOL bOK = CSfxModelMng::GetThis()->SubData( who, szBone );

	return bOK;
}

void open_lua_sfx()
{
	if( !g_SfxLua )
 	{
		g_SfxLua = luaL_newstate();
//		luaL_openlibs(g_SfxLua);

		// push function
		lua_register( g_SfxLua, "call_sfx", call_sfx );
		lua_register( g_SfxLua, "call_mte", call_mte );
	}
}

void close_lua_sfx()
{
	if( g_SfxLua )
		lua_close( g_SfxLua );
}


void run_lua_sfx( int nState, OBJID caller, const char* szMoverName )
{
	static const CString strPATH( "SFX\\Scripts\\" );
	static const CString strFORMAT(".lua");

	// setup 
	if( !g_SfxLua )
		open_lua_sfx( );

	// caller는 무버만 지원한다. 현재 2010_0204
	CMover* pWho = prj.GetMover( caller );
	if( !pWho )
	{
		// 흠 생성이 되는도중 불릴수가 있다. 이런경우는 prj에서 못찼을껏이므로 등록상태로 두고 다음 프레임에 시도한다.
		CSfxModelMng::GetThis()->_cWaitingObj.emplace( caller, nState );
		return;
	}

	char szFunc[ 64 ];
	GetFileTitle( szMoverName, szFunc );
	CString strFileName = CString( szFunc ) + strFORMAT;
	CString strFullPath = strPATH + strFileName;
	
	// res를 취급하는 테섭 및 정섭은 buffer를 얻어와서 루아를 돌리자 
#ifdef __INTERNALSERVER
	if( 0 != luaL_dofile( g_SfxLua, strFullPath.GetBuffer(0) ) )
	{
	//	Error( "run_lua_sfx : %s", (char*)lua_tostring( g_SfxLua, -1 ) );
		return;
	}
#else
	// res로 merge된 파일은 CResFile을 이용해 버퍼를 얻고 루아를 호출 
	CResFile file;
	BOOL bRet = file.Open( strFullPath.GetBuffer(0), "rt" );
	if( bRet == FALSE )	
	{
		return;
	}

	static char pBuff[ 2048 ];
	int nLength = file.GetLength();
	if( nLength > 2047 )
	{
		Error( "error!! overflowed size of lua file %s", strFullPath.GetBuffer(0) );
		return;
	}

	if( nLength < 2 )		// 존재이유가 없는 길이 이다 .
		return;

	pBuff[ 0 ] = 0;
	file.Read( pBuff, nLength );
	pBuff[ nLength ] = 0;
	
	// TODO : buffer를 얻어와서 call lua
	int a = luaL_loadbuffer( g_SfxLua, pBuff, nLength, NULL );
	int b = lua_pcall(g_SfxLua, 0, LUA_MULTRET, 0);

	if( (a | b) != 0 )
	{
		Error( "error run_lua_sfx : %s", (char*)lua_tostring( g_SfxLua, -1 ) );
		return;
	}
#endif //__INTERNALSERVER

	//만약 몬스터에 대해서 일반적인 lua를 적용하고, 특별한 경우만 몬스터개개인에 대해서 lua를 지정한다면 
	// 우선순위는 특별히 지정된경우다 ,

	if( OBJSTAF_COMBAT == nState )
		strcat( szFunc, "_battle" );
	else
	if( OBJSTA_NONE == nState ) 
	{
		//생성상태로 간주한다.. 즉 생성되고 아무것도 아닌상태란 걸로 해석하기 바람.
		strcat( szFunc, "_create" );
	}
	else
	{
		Error( "SFXLUA is not supported the state : %d ", nState );
		return;
	}

	lua_getglobal( g_SfxLua, szFunc );
	lua_pushnumber( g_SfxLua, caller );

//	lua_call( g_SfxLua, 1, 0 ) ;

//_DEBUG
	if( lua_pcall( g_SfxLua, 1, 0, 0 ) != 0 ) 
	{
		char* szError = (char*)lua_tostring( g_SfxLua, -1 );
		Error( "run_lua_sfx : %s", szError );
	}
}

BOOL stop_sfx( OBJID caller )
{
	return CSfxModelMng::GetThis()->SubData( caller );
}

#endif //__BS_EFFECT_LUA