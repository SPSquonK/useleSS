#include "StdAfx.h"
#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
#include "MapInformationManager.h"
#include "defineMapComboBoxData.h"
#include "Project.h"
#include "ContinentDef.h"

//-----------------------------------------------------------------------------
CMapInformationManager::CMapInformationManager( void ) : 
m_pPCArrowTexture( NULL ), 
m_pPartyPCArrowTexture( NULL ), 
m_pTeleportationPositionTexture( NULL ), 
m_pDestinationPositionTexture( NULL ), 
m_pNPCPositionTexture( NULL ), 
m_pUserMarkPositionTexture( NULL ), 
m_pRainbowNPCInformationPack( NULL ),
m_nextUserMarkID( 1 )
{
}
//-----------------------------------------------------------------------------
CMapInformationManager::~CMapInformationManager( void )
{
	DeleteAllMapInformation();
}
//-----------------------------------------------------------------------------
BOOL CMapInformationManager::LoadMapInformationData( void )
{
	LoadMapIconTexture();
	LoadPropMapComboBoxData();
	LoadMapMonsterInformationPack();
	LoadRainbowNPCInformationPack();

	return TRUE;
}
//-----------------------------------------------------------------------------
void CMapInformationManager::DeleteAllMapInformation( void )
{
	DeleteAllUserMarkPositionInfo();

	SAFE_DELETE( m_pRainbowNPCInformationPack );

	m_RealPositionRectMap.clear();
	m_MapMonsterInformationPackMap.clear();
	m_MapCategoryVector.clear();
	m_MapNameVector.clear();
	m_NPCNameVector.clear();
}
//-----------------------------------------------------------------------------
CTexture* const CMapInformationManager::GetPCArrowTexture( void )
{
	return m_pPCArrowTexture;
}
//-----------------------------------------------------------------------------
CTexture* const CMapInformationManager::GetPartyPCArrowTexture( void )
{
	return m_pPartyPCArrowTexture;
}
//-----------------------------------------------------------------------------
CTexture* const CMapInformationManager::GetTeleportationPositionTexture( void )
{
	return m_pTeleportationPositionTexture;
}
//-----------------------------------------------------------------------------
CTexture* const CMapInformationManager::GetDestinationPositionTexture( void )
{
	return m_pDestinationPositionTexture;
}
//-----------------------------------------------------------------------------
CTexture* const CMapInformationManager::GetNPCPositionTexture( void )
{
	return m_pNPCPositionTexture;
}
//-----------------------------------------------------------------------------
CTexture* const CMapInformationManager::GetUserMarkPositionTexture( void )
{
	return m_pUserMarkPositionTexture;
}
//-----------------------------------------------------------------------------
CMapMonsterInformationPack* CMapInformationManager::FindMapMonsterInformationPack( DWORD dwFirstKey )
{
	auto Iterator = m_MapMonsterInformationPackMap.find( dwFirstKey );
	if( Iterator != m_MapMonsterInformationPackMap.end() )
	{
		return Iterator->second.get();
	}
	return NULL;
}
//-----------------------------------------------------------------------------
CRect CMapInformationManager::FindRealPosition(BYTE byFirstKey) const {
	auto Iterator = m_RealPositionRectMap.find(byFirstKey);
	if (Iterator != m_RealPositionRectMap.end()) {
		return Iterator->second;
	}
	return CRect(-1, -1, -1, -1);
}
//-----------------------------------------------------------------------------
CRainbowNPCInformationPack* CMapInformationManager::GetRainbowNPCInformationPack( void )
{
	return m_pRainbowNPCInformationPack;
}
//-----------------------------------------------------------------------------
BOOL CMapInformationManager::InsertUserMarkPositionInfo( const CString& strName, FLOAT fPositionX, FLOAT fPositionY )
{
	if (m_UserMarkPositionInfoList.size() >= USER_MARK_MAX_SIZE) {
		g_WndMng.PutString(TID_ERROR_MAP_EX_MARK_MAX_NUMBER);
		return FALSE;
	}

	CUserMarkPositionInfo pUserMarkPositionInfo;
	pUserMarkPositionInfo.dwID = m_nextUserMarkID++;
	pUserMarkPositionInfo.strName = strName;
	pUserMarkPositionInfo.fPositionX = fPositionX;
	pUserMarkPositionInfo.fPositionY = fPositionY;
	m_UserMarkPositionInfoList.push_back( pUserMarkPositionInfo );

	return TRUE;
}
//-----------------------------------------------------------------------------
void CMapInformationManager::DeleteUserMarkPositionInfo(DWORD dwID) {
	const auto it = std::find_if(
		m_UserMarkPositionInfoList.begin(), m_UserMarkPositionInfoList.end(),
		[dwID](const CUserMarkPositionInfo & info) { return info.dwID == dwID; }
	);

	if (it != m_UserMarkPositionInfoList.end()) {
		m_UserMarkPositionInfoList.erase(it);
	}
}
//-----------------------------------------------------------------------------
CUserMarkPositionInfo * const CMapInformationManager::FindUserMarkPositionInfo(DWORD dwID) {
	for (CUserMarkPositionInfo & rUserMarkPositionInfo : m_UserMarkPositionInfoList) {
		if (dwID == rUserMarkPositionInfo.dwID) {
			return &rUserMarkPositionInfo;
		}
	}
	return nullptr;
}
//-----------------------------------------------------------------------------
void CMapInformationManager::DeleteAllUserMarkPositionInfo(void) {
	m_UserMarkPositionInfoList.clear();
}
//-----------------------------------------------------------------------------
BOOL CMapInformationManager::LoadMapIconTexture( void )
{
	m_pPCArrowTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, _T( "ImgMapArrow.bmp" ) ), 0xffff00ff );

	m_pPartyPCArrowTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, _T( "ImgMapArrowParty.bmp" ) ), 0xffff00ff );

	m_pTeleportationPositionTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, "ButtTeleport.bmp"), 0xffff00ff );

	m_pDestinationPositionTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, "ButtDestination.bmp"), 0xffff00ff );

	m_pNPCPositionTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, "MapNPCPositionMark.bmp"), 0xffff00ff );

	m_pUserMarkPositionTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, "WndRainbowRaceLadderFail.bmp"), 0xffff00ff );

	return TRUE;
}
//-----------------------------------------------------------------------------
BOOL CMapInformationManager::LoadPropMapComboBoxData( void )
{
	CScript script;
	if( script.Load( _T( "propMapComboBoxData.inc" ) ) == FALSE )
	{
		return FALSE;
	}

	DWORD dwMapComboBoxDataID = script.GetNumber(); // ID;
	while( script.tok != FINISHED )
	{
		script.GetToken(); // {
		CMapComboBoxData::Category eMapComboBoxCategory = static_cast< CMapComboBoxData::Category >( MCC_MAP_CATEGORY );
		CString strMapComboBoxTitle = _T( "" );
		CString strMapPictureFileName = _T( "" );
		CRect rectRealPosition( 0, 0, 0, 0 );
		CPoint pointNPCPosition( -1, -1 );
		CString strMapMonsterInformationFileName = _T( "" );
		BYTE byLocationID = CONT_NODATA;
		DWORD dwMapComboBoxDataParentID = MCD_NONE;
		int nBlock = 1;
		while( nBlock > 0 && script.tok != FINISHED )
		{
			script.GetToken();
			if( script.Token == _T( "{" ) )
			{
				++nBlock;
			}
			else if( script.Token == _T( "}" ) )
			{
				--nBlock;
			}
			else if( script.Token == _T( "SetCategory" ) )
			{
				script.GetToken(); // (
				eMapComboBoxCategory = static_cast< CMapComboBoxData::Category >( script.GetNumber() );
				script.GetToken(); // )
				script.GetToken(); // ;
			}
			else if( script.Token == _T( "SetTitle" ) )
			{
				script.GetToken(); // (
				strMapComboBoxTitle = prj.GetLangScript( script );
			}
			else if( script.Token == _T( "SetPictureFile" ) )
			{
				script.GetToken(); // (
				strMapPictureFileName = prj.GetLangScript( script );
			}
			else if( script.Token == _T( "SetRealPositionRect" ) )
			{
				script.GetToken(); // (
				rectRealPosition.left = script.GetNumber();
				script.GetToken(); // ,
				rectRealPosition.top = script.GetNumber();
				script.GetToken(); // ,
				rectRealPosition.right = script.GetNumber();
				script.GetToken(); // ,
				rectRealPosition.bottom = script.GetNumber();
				script.GetToken(); // )
				script.GetToken(); // ;
			}
			else if( script.Token == _T( "SetMonsterInformationFile" ) )
			{
				script.GetToken(); // (
				strMapMonsterInformationFileName = prj.GetLangScript( script );
			}
			else if( script.Token == _T( "SetLocationID" ) )
			{
				script.GetToken(); // (
				byLocationID = static_cast< BYTE >( script.GetNumber() );
				script.GetToken(); // )
				script.GetToken(); // ;
			}
			else if( script.Token == _T( "SetNPCPosition" ) )
			{
				script.GetToken(); // (
				pointNPCPosition.x = script.GetNumber();
				script.GetToken(); // ,
				pointNPCPosition.y = script.GetNumber();
				script.GetToken(); // )
				script.GetToken(); // ;
			}
			else if( script.Token == _T( "SetParentID" ) )
			{
				script.GetToken(); // (
				dwMapComboBoxDataParentID = script.GetNumber();
				script.GetToken(); // )
				script.GetToken(); // ;
			}
		}

		CMapComboBoxData pMapComboBoxData = CMapComboBoxData( dwMapComboBoxDataID, eMapComboBoxCategory, strMapComboBoxTitle );
		pMapComboBoxData.strPictureFileName = strMapPictureFileName;
		if( strMapPictureFileName != _T( "" ) )
		{
			CTexture* pMapTexture = CWndBase::m_textureMng.AddTexture( MakePath( DIR_THEME, strMapPictureFileName ), 0xff000000 );
			pMapComboBoxData.pMapTexture = pMapTexture;
		}
		if( eMapComboBoxCategory == MCC_MAP_NAME )
		{
			m_RealPositionRectMap[ byLocationID ] = rectRealPosition;
		}
		pMapComboBoxData.strMapMonsterInformationFileName = strMapMonsterInformationFileName;
		pMapComboBoxData.byLocationID = byLocationID;
		pMapComboBoxData.pointNPCPosition = pointNPCPosition;
		pMapComboBoxData.dwParentID = dwMapComboBoxDataParentID;

		switch( eMapComboBoxCategory )
		{
		case MCC_MAP_CATEGORY:
			{
				m_MapCategoryVector.push_back( pMapComboBoxData );
				break;
			}
		case MCC_MAP_NAME:
			{
				m_MapNameVector.push_back( pMapComboBoxData );
				break;
			}
		case MCC_NPC_NAME:
			{
				m_NPCNameVector.push_back( pMapComboBoxData );
				break;
			}
		}
		dwMapComboBoxDataID = script.GetNumber(); // ID
	}
	return TRUE;
}
//-----------------------------------------------------------------------------
BOOL CMapInformationManager::LoadMapMonsterInformationPack( void )
{
	for( const CMapComboBoxData & pMapComboBoxData : m_MapNameVector )
	{
		if( pMapComboBoxData.strMapMonsterInformationFileName == _T( "" ) )
		{
			continue;
		}

		std::unique_ptr<CMapMonsterInformationPack> pMapMonsterInformationPack = std::make_unique<CMapMonsterInformationPack>();
		if( pMapMonsterInformationPack->LoadScript( MakePath( DIR_THEME, pMapComboBoxData.strMapMonsterInformationFileName ) ) == FALSE )
		{
			continue;
		}

		m_MapMonsterInformationPackMap[ pMapComboBoxData.GetID() ] = std::move(pMapMonsterInformationPack);
	}

	return TRUE;
}
//-----------------------------------------------------------------------------
BOOL CMapInformationManager::LoadRainbowNPCInformationPack( void )
{
	m_pRainbowNPCInformationPack = new CRainbowNPCInformationPack;
	m_pRainbowNPCInformationPack->LoadScript( MakePath( DIR_THEME, _T( "texMapRainbow_NPC.inc" ) ) );

	return TRUE;
}
//-----------------------------------------------------------------------------
#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM