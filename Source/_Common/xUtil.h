#ifndef		__XUTIL_H__
#define		__XUTIL_H__

#include <random>

extern LARGE_INTEGER	g_llFreq;
extern int				g_nMaxTri;

/////////////////////////////////////////////////////////////////////////////
// random

class RandomNumberGenerator {
private:
	std::random_device rd;
	std::mt19937 generator;

public:
	RandomNumberGenerator() : rd(), generator(rd()) {}

	[[nodiscard]] DWORD xRand() {
		return std::uniform_int_distribution<DWORD>()(generator);
	}

	[[nodiscard]] DWORD xRandom(DWORD num) {
		if (num != 0) {
			return std::uniform_int_distribution<DWORD>(0, num - 1)(generator);
		} else {
			return 0;
		}
	}

	[[nodiscard]] DWORD xRandom(DWORD minInclusive, DWORD maxExclusive) {
		if (maxExclusive <= minInclusive) return minInclusive;

		return minInclusive + xRandom(maxExclusive - minInclusive);
	}

	[[nodiscard]] int random(int nNum) {
		if (nNum != 0) return std::uniform_int_distribution<int>(0, nNum - 1)(generator);
		return 0;
	}

	[[nodiscard]] float xRandomF(float num) {
		if (num <= 0.0f) return 0.0f;
		return std::uniform_real_distribution<float>(0, num)(generator);
	}

	[[nodiscard]] float xRandomF(float min, float max) {
		if (min > max && !(max > min)) return xRandomF(max, min);
		return std::uniform_real_distribution<float>(min, max)(generator);
	}

	template<std::integral T>
	[[nodiscard]] T Random(T minInclusive, T maxExclusive) {
		if (minInclusive >= maxExclusive) return minInclusive;
		return std::uniform_int_distribution<T>(minInclusive, maxExclusive)(generator);
	}
};

extern RandomNumberGenerator g_next;

inline DWORD xRand() { return g_next.xRand(); }
inline DWORD xRandom(DWORD num) { return g_next.xRandom(num); }
inline DWORD xRandom(DWORD minEx, DWORD maxEx) { return g_next.xRandom(minEx, maxEx); }
inline float xRandomF(float num) { return g_next.xRandomF(num); }
inline float xRandomF(float min, float max) { return g_next.xRandomF(min, max); }
inline int random(int nNum) { return g_next.random(nNum); }

template<std::integral T>
inline T Random(T minInclusive, T maxExclusive) {
	return g_next.Random(minInclusive, maxExclusive);
}

template<std::floating_point T>
inline T RandomIntegerAsFloat(int minInclusive, int maxExclusive) {
	return static_cast<T>(Random<int>(minInclusive, maxExclusive));
}

template<std::integral T>
inline T RandomExcept(T minInclusive, T maxExclusive, T excludedValue) {
	while (true) {
		const T draw = Random(minInclusive, maxExclusive);
		if (draw != excludedValue) return draw;
	}
}

inline bool RandomCoinFlip() { return Random<int>(0, 2) == 0; }

/////////////////////////////////////////////////////////////////////////////
// PATH
//LPCTSTR		GetFileName( LPCTSTR szSrc );
//LPCTSTR		GetFileTitle( LPCTSTR szSrc );
//LPCTSTR		GetFileExt( LPCTSTR szSrc );
//LPCTSTR		GetFilePath( LPCTSTR szSrc );
void	GetFileName( LPCTSTR szSrc, LPTSTR szFileName );
void	GetFileTitle( LPCTSTR szSrc, LPTSTR szFileTitle );
void	GetFileExt( LPCTSTR szSrc, LPTSTR szFileExt );
void	GetFilePath( LPCTSTR szSrc, LPTSTR szFilePath );

/////////////////////////////////////////////////////////////////////////////
// ERROR
LPCTSTR		Error( LPCTSTR str, ... );
CString     GetNumberFormatEx( LPCTSTR szNumber );

/////////////////////////////////////////////////////////////////////////////
// ETC
enum  OSTYPE
{
	WINDOWS_UNKNOWN,
	WINDOWS_NT351,
	WINDOWS_95,
	WINDOWS_NT,
	WINDOWS_98,
	WINDOWS_ME,
	WINDOWS_2000,
	WINDOWS_XP,
	WINDOWS_SERVER_2003
};

extern OSTYPE g_osVersion;

OSTYPE GetCPUInfo(const OSVERSIONINFO & versionInformation);

[[nodiscard]] inline bool IsEmpty(LPCTSTR str) { return str[0] == '\0'; }

void	MakeEven( long& x );
void	StringTrimRight( char* szString );

void	SetLanguageInfo( int nLanguage, int nSubLanguage );		
int		GetLanguage();
int		GetSubLanguage();

void	SetUse2ndPassWord( BOOL bUse );
BOOL	IsUse2ndPassWord();

/////////////////////////////////////////////////////////////////////////////
#ifdef __PROF
#define		MAX_PROF_TIME	256
#define		MAX_PROF_STACK	256

struct	PROF_LIST
{
	int		m_nIdx;
	LARGE_INTEGER	m_lnPrev;
	int		m_nCheckTime;
	char	m_szMsg[64];	// strlen 64의 스트링이 MAX_PROF_TIME개.
};

class CProf
{
	PROF_LIST	m_List[ MAX_PROF_TIME ];
	char	m_szResultLast[1024];		// 최종 결과를 출력하기 위한 버퍼
	int		m_Stack[MAX_PROF_STACK];	// last index stack
	int		m_nMaxStack;
		
	int	m_nMax;
	int	m_nStackCnt;
public:
	char	m_szResult[1024];		// 최종 결과를 출력하기 위한 버퍼

	CProf()
	{
		Init();
	}
	~CProf()
	{
	}
	void	Init()
	{
		QueryPerformanceFrequency( &g_llFreq );
		m_nMax = 0;
		memset( m_List, 0, sizeof(PROF_LIST) * MAX_PROF_TIME );
		memset( m_szResult, 0, sizeof(m_szResult) );
		memset( m_szResultLast, 0, sizeof(m_szResultLast) );
		m_nStackCnt = 0;
		m_nMaxStack = 0;
	}

	// 다시 처음의 메인루프로 돌아올때 함.
	void	Reset()
	{
		m_nMax = 0;		// 체크갯수 초기화
		m_nStackCnt = 0;
		m_nMaxStack = 0;
		strcpy( m_szResultLast, m_szResult );
	}
	void	Check1()
	{
		PROF_LIST	*pList = &m_List[ m_nMax ];
		
		QueryPerformanceCounter( &pList->m_lnPrev );
		pList->m_nIdx = m_nMax;		// Check1이 불리고 다시 Check1이 불려도 max는 계속 증가 하고 스택카운트만 올린다.
		m_nStackCnt ++;
		if( m_nMaxStack >= MAX_PROF_STACK )		Error( "CProf : stack overflow" );
		m_Stack[m_nMaxStack++] = m_nMax;		// 리스트인덱스 푸쉬

		m_nMax ++;
	}
	void	Check2( LPCTSTR szMsg )
	{
		PROF_LIST	*pList;
		LARGE_INTEGER	lnCnt2;
		char	*sz;

		m_nMaxStack --;
		if( m_nMaxStack < 0 )		Error( "CProf : stack under flow" );
		int nLastIdx = m_Stack[ m_nMaxStack ];		// 마지막으로 Check1 했던곳의 인덱스를 POP함.

		pList = &m_List[ nLastIdx ];

		QueryPerformanceCounter( &lnCnt2 );
		pList->m_nCheckTime = (int)(lnCnt2.QuadPart - pList->m_lnPrev.QuadPart);
		sz = pList->m_szMsg;
//		if( IsEmpty(sz) )
		if( strcmp(sz, szMsg) != 0 )
			strcpy( sz, szMsg );
	}
	void	Show( float fFPS )
	{
		Show2( fFPS );
		Error( "%s", m_szResult );
	}
	void	Show2( float fFPS )
	{
		if( fFPS <= 1.0f )	return;
		PROF_LIST	*pList = m_List;;
		int nTotal = (int)(g_llFreq.QuadPart / (int)fFPS);		// 한프레임 토탈 갱신시간
		int		i;
		char	*p = m_szResult;
		int		nLen = 0, nMaxLen = 0;
		
		memset( p, 0, 1024 );
		for( i = 0; i < m_nMax; i ++ )
		{
			if( IsEmpty(pList->m_szMsg) )		continue;	// 메시지가 없으면 출력하지 않음.
			sprintf( p, "%6.2f : %s   %f sec\r\n", ((double)pList->m_nCheckTime / nTotal) * 100, pList->m_szMsg, (double)pList->m_nCheckTime / g_llFreq.QuadPart );
			nLen = strlen(p);
			p += nLen;
			nMaxLen += nLen;
			pList ++;
		}
		sprintf( p, "1/%d : %f,  %f fps, MaxTri = %d", (int)fFPS, (double)g_llFreq.QuadPart / fFPS, fFPS, g_nMaxTri );
		nMaxLen += strlen(p);
	}
	

};
extern CProf	g_Prof;

#endif // prof

#ifdef	__PROF
#define		CHECK1()	g_Prof.Check1();
#define		CHECK2(A)	g_Prof.Check2(A);
#else
#define		CHECK1()	/##/
#define		CHECK2(A)	/##/
#endif

#endif
