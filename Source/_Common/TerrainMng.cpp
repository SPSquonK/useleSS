#include "stdafx.h"

void WaterTexList::Advance() {
	currentFrame += FrameAdvance;

	if (static_cast<size_t>(currentFrame) >= terrainIds.size()) {
		currentFrame = 0.0f;
	}
}

CTerrainMng::~CTerrainMng() {
	SAFE_DELETE_ARRAY(m_pWaterIndexList);
}

HRESULT CTerrainMng::DeleteDeviceObjects() {
	for (TERRAIN & terrain : m_terrains) {
		if (terrain.m_pTexture) {
			SAFE_RELEASE(terrain.m_pTexture);
		}
	}
	return S_OK;
}

BOOL CTerrainMng::LoadTexture( DWORD dwId )
{
	if (dwId >= m_terrains.size()) return FALSE;

	TERRAIN * lpTerrain = GetTerrain( dwId );
	if( lpTerrain && lpTerrain->m_pTexture == NULL )
	{
		CString strPath;
		if( g_Option.m_nTextureQuality == 0 )
			strPath = MakePath( DIR_WORLDTEX, lpTerrain->m_szTextureFileName );
		else if( g_Option.m_nTextureQuality == 1 )
			strPath = MakePath( DIR_WORLDTEXMID, lpTerrain->m_szTextureFileName );
		else
			strPath = MakePath( DIR_WORLDTEXLOW, lpTerrain->m_szTextureFileName );
		D3DXIMAGE_INFO srcInfo;
		HRESULT hr = LoadTextureFromRes( strPath, 
			D3DX_DEFAULT, D3DX_DEFAULT, 4, 0, D3DFMT_UNKNOWN, 
			D3DPOOL_MANAGED, D3DX_FILTER_TRIANGLE|D3DX_FILTER_MIRROR, 
			D3DX_FILTER_TRIANGLE|D3DX_FILTER_MIRROR, 0, &srcInfo, NULL, &lpTerrain->m_pTexture );
		
		if( FAILED( hr ) )
		{
			return FALSE;
		}
	}
	return TRUE;
}
BOOL CTerrainMng::LoadScript( LPCTSTR lpszFileName )
{
	CScanner scanner;
	if(scanner.Load(lpszFileName, FALSE )==FALSE)
		return FALSE;

	m_terrains.clear();

	int nBrace = 1;
	scanner.SetMark();
	int i = scanner.GetNumber(); // folder or id
	
	//	현재는 물만 이런 형태의 프레임을 사용할 것이라는 전제하에 
	//	하나만 되어 있지만 여러개를 사용할때는 이 부분을 구조체나
	//	클래스로 바꾸어서 다시 만들어야 할것이다.
	int FrameCnt = 0, ImageCnt = 0;
	FrameCnt = scanner.GetNumber();
	if ( nBrace == 1 && FrameCnt )
	{
		m_nWaterFrame = FrameCnt;
		m_pWaterIndexList = new WaterTexList[ m_nWaterFrame ];
		ZeroMemory( m_pWaterIndexList, m_nWaterFrame * sizeof( WaterTexList ) );
	}

	while( nBrace )
	{
		if( *scanner.token == '}' || scanner.tok == FINISHED )
		{
			nBrace--;
			if( nBrace > 0 )
			{
				scanner.SetMark();
				i = scanner.GetNumber();  // folder or id
				
				FrameCnt = scanner.GetNumber();
				if ( nBrace == 1 && FrameCnt )
				{
					m_nWaterFrame = FrameCnt;
					m_pWaterIndexList = new WaterTexList[ m_nWaterFrame ];
					ZeroMemory( m_pWaterIndexList, m_nWaterFrame * sizeof( WaterTexList ) );
					ImageCnt = 0;
				}
				else if ( nBrace == 2 && FrameCnt )
				{
					ImageCnt++;
				}				
				continue;
			}
			if( nBrace == 0 )
				continue;
		}
		//i = _ttoi( scanner.token );
		scanner.GetToken(); // { or filename

		if( *scanner.token == '{' )
		{
			nBrace++;
			scanner.SetMark();
			i = scanner.GetNumber(); // id
			//	여기서 추가하는 방법이 괄호후에 바로 문자열이 오지 못하게 하는 것 뿐인듯하다.
			//	마침 디폴트는 괄호 다음에 오는 것이 아닌 상태이기 때문에 가능하다.
			FrameCnt = scanner.GetNumber();	//	Count
			if ( i == 0 )
			{
				if ( nBrace == 2 && FrameCnt )
				{
					ImageCnt++;
				}
			}

			continue;
		}
		else
		{
			scanner.GoMark();
			i = scanner.GetNumber(); // id

			FrameCnt = scanner.GetNumber();	//	Count
			if ( nBrace == 3 )
			{
				m_pWaterIndexList[ImageCnt - 1].terrainIds.emplace_back(i);
			}
		}

		if (std::cmp_greater_equal(i, m_terrains.size())) {
			m_terrains.resize(i + 1, TERRAIN());
		}

		TERRAIN * lpTerrain = &m_terrains[i];
		scanner.GetToken();  // texture fileName
		strcpy( lpTerrain->m_szTextureFileName, scanner.token );
		scanner.GetNumber(); // block
		scanner.GetToken(); // sound
		scanner.SetMark();
		i = scanner.GetNumber();  // texture fileName
	}

	return TRUE;
}
