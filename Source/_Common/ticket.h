#pragma once

class CTicketProperty final {
public:
	[[nodiscard]] const FullPosition * GetTicketProp(DWORD dwItemId) const;
	[[nodiscard]] bool IsTarget(DWORD dwWorldId) const;
	BOOL LoadScript();
	[[nodiscard]] unsigned int GetExpandedLayer(DWORD dwWorldId) const;
private:
	std::map<DWORD, FullPosition> m_mapTicketProp;
	std::map<DWORD, unsigned int> m_expandedLayers;
};

extern CTicketProperty g_ticketProperties;
