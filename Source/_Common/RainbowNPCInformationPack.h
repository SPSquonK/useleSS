#pragma once

#include "RainbowNPCInformation.h"

#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT

class CRainbowNPCInformationPack : public CTexturePack
{
public:
	CRainbowNPCInformationPack( void );
	~CRainbowNPCInformationPack( void );

public:
	BOOL LoadScript( const CString& strFileName );

public:
	const CRainbowNPCInformation* GetRainbowNPCInformation( int nIndex ) const;

private:
	void DeleteAllRainbowNPCInformation( void );

private:
	std::vector< CRainbowNPCInformation * > m_RainbowNPCInformationVector;
};

#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM
