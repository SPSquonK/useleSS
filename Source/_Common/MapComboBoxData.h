#ifndef __MAP_COMBO_BOX_DATA_H__
#define __MAP_COMBO_BOX_DATA_H__

#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
class CMapComboBoxData
{
public:
	enum Category
	{
		MAP_CATEGORY, MAP_NAME, NPC_NAME
	};

public:
	CMapComboBoxData( DWORD dwID, Category eCategory, const CString& strTitle );

public:
	[[nodiscard]] DWORD GetID() const { return m_dwID; }
	[[nodiscard]] Category GetCategory() const { return m_eCategory; }
	[[nodiscard]] const CString & GetTitle() const { return m_strTitle; }
	CString strPictureFileName;
	CTexture * pMapTexture;
	CString strMapMonsterInformationFileName;
	BYTE byLocationID;
	CPoint pointNPCPosition;
	DWORD dwParentID;

private:
	DWORD m_dwID;
	Category m_eCategory;
	CString m_strTitle;
};

#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM

#endif // __MAP_COMBO_BOX_DATA_H__
