#pragma once

#include <D3D9.h>
#include <basetsd.h> 
#include "Weather.h"

class CWorld;

#define D3DFVF_D3DSKYBOXVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)
#define D3DFVF_D3DSUNVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)

struct D3DSKYBOXVERTEX
{
	D3DXVECTOR3 p;
	DWORD       c;
	FLOAT       tu1, tv1;
};

struct D3DSUNVERTEX
{
	D3DXVECTOR3 p;
	DWORD       c;
	FLOAT       tu1, tv1;
};

class CSkyBox
{
	CTimer  m_timerWeather;
	WEATHER m_nWeather;
	void	DrawRain();
	void	DrawSnow();

  float m_fCloud_u1, m_fCloud_v1;
	
public:
  enum class SKY_TYPE { SIDE, CLOUD, MOON, SUN };

	// 각종 텍스쳐
	LPDIRECT3DTEXTURE9 m_pMoonTexture;
	LPDIRECT3DTEXTURE9 m_pSkyBoxTexture;
	LPDIRECT3DTEXTURE9 m_pSkyBoxTexture2;
	LPDIRECT3DTEXTURE9 m_pSkyBoxTexture3;
	LPDIRECT3DTEXTURE9 m_pCloudTexture;
	LPDIRECT3DTEXTURE9 m_pCloudTexture2;
	LPDIRECT3DTEXTURE9 m_pCloudTexture3;
	LPDIRECT3DTEXTURE9 m_pSunTexture;
	LPDIRECT3DTEXTURE9 m_pSunTexture2;
	LPDIRECT3DTEXTURE9 m_pSnowTexture;
	LPDIRECT3DTEXTURE9 m_pLensTexture[8];
	LPDIRECT3DVERTEXBUFFER9 m_pTopSkyBoxVB;
	LPDIRECT3DVERTEXBUFFER9 m_pSideSkyBoxVB;
	LPDIRECT3DVERTEXBUFFER9 m_pSunVB;
	LPDIRECT3DVERTEXBUFFER9 m_pLensFlareVB[8];
	LPDIRECT3DVERTEXBUFFER9 m_pRainVB;
	LPDIRECT3DVERTEXBUFFER9 m_pSnowVB;
	LPDIRECT3DVERTEXBUFFER9 m_pStarVB;
	CWorld*        m_pWorld       ; // 자신이 속한 월드의 포인터를 갖는다. 

	D3DXVECTOR3 m_vVelocity[2000]; // 눈빗방울용 속도 
	D3DXVECTOR3 m_vFall[2000]; // 눈빗방울용 위치 배열

	DWORD m_nFall; // 현재 빗방울 갯수

	float m_fSunAngle; // 현재 태양의 각도

	BOOL m_bLockWeather; //gmpbigsun :  날씨 고정flag (대륙단위로 설정한 날씨는 어떠한 이유로든 변하지 않는다)

	CSkyBox();
	~CSkyBox();

	static	CSkyBox*	GetInstance();

	void SetWeather( WEATHER nWeather, bool bOnOff );		

	void InitFall();

	void SetVertices();

	// 처리 및 출력 
	void    Process();
	void    Render(CWorld* pWorld);
	void	DrawLensFlare();
	void    RenderFall(  );
		
	BOOL CheckSun();

  [[nodiscard]] WEATHER GetWeather() const noexcept { return m_nWeather; }

	IDirect3DTexture9* GetCurrSkyTexture( SKY_TYPE eType );

	// Direct3D 관련 오브젝트 초기화및 제거, 재설정 관련 
	HRESULT InitDeviceObjects();
	HRESULT RestoreDeviceObjects();
	HRESULT DeleteDeviceObjects();
	HRESULT InvalidateDeviceObjects();
};
