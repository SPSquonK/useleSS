#include "StdAfx.h"
#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
#include "MapComboBoxData.h"
#include "ContinentDef.h"

//-----------------------------------------------------------------------------
CMapComboBoxData::CMapComboBoxData( DWORD dwID, Category eCategory, const CString& strTitle ) : 
m_dwID( dwID ), 
m_eCategory( eCategory ), 
m_strTitle( strTitle ), 
strPictureFileName( _T( "" ) ), 
pMapTexture( NULL ), 
strMapMonsterInformationFileName( _T( "" ) ), 
byLocationID( CONT_NODATA ), 
pointNPCPosition( -1, -1 ), 
dwParentID( 0 )
{
}

#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM