#include "stdafx.h"
#include "Exchange.h"
#include <numeric>
#ifdef __WORLDSERVER
#include "defineText.h"
#include "User.h"
#include "DPSrvr.h"
#include "DPDatabaseClient.h"
#endif // __WORLDSERVER


//////////////////////////////////////////////////////////////////////
// Exchange/SetList

const CExchange::SetList * CExchange::FindSetList(int nMMIId) const {
	const auto it = m_mapExchange.find(nMMIId);
	return it != m_mapExchange.end() ? &it->second : nullptr;
}

const CExchange::Set * CExchange::FindSet(int nMMIId, int nListNum) const {
	const SetList * pSetList = FindSetList(nMMIId);
	return pSetList ? pSetList->FindSet(nListNum) : nullptr;
}

const CExchange::Set * CExchange::SetList::FindSet(int nListNum) const {
	if (nListNum < 0 || std::cmp_greater_equal(nListNum, vecSet.size())) {
		return nullptr;
	}

	return &vecSet[nListNum];
}

std::vector<int> CExchange::SetList::GetListTextId() const {
	std::vector<int> list;

	for (const Set & x : vecSet) {
		list.push_back(x.nSetTextId);
	}

	return list;
}

#ifdef __WORLDSERVER
int CExchange::ResultExchange(CUser * pUser, int nMMIId, int nListNum) const {
	const Set * pSet = FindSet(nMMIId, nListNum);
	return pSet ? pSet->ResultExchange(pUser) : EXCHANGE_FAILED;
}
#endif


//////////////////////////////////////////////////////////////////////
// Set

#ifdef __WORLDSERVER

// Check if the user has all items for this exchange
//
// Return:
// - The list of slots to modify if they have all required items
//   It is guaranteed that all SlotsToUse::missingCond == SlotsToUse::missingRemove == 0
// - std::nullopt if at least one item does not meet requirements
std::optional<std::vector<CExchange::SlotsToUse>> CExchange::Set::CheckCondition(CUser * pUser) const
{
	std::vector<SlotsToUse> result;

	bool bReturn = true;
	
	for (const RequiredItem & required : vecRequiredItem) {
		const DWORD itemId = required.dwItemId;
		const int totalRequired = required.removeNum + required.condNum;

		bool isBad = false;

		if (itemId == II_GOLD_SEED1) {
			const auto nItemCount = pUser->GetGold();
			isBad = nItemCount < totalRequired;
		} else {
			const auto localResult = required.SearchSlots(pUser);

			if (localResult.missingCond + localResult.missingRemove == 0) {
				result.emplace_back(localResult);
			} else {
				isBad = true;
			}
		}

		if( isBad )
		{
			if( !vecResultMsg.HasMessages() )
			{
				ItemProp* pItemProp = prj.GetItemProp( itemId );
				if( pItemProp )
					pUser->AddDefinedText( TID_EXCHANGE_FAIL, "\"%s\" %d", pItemProp->szName, totalRequired );
			}

			bReturn = false;
		}
	}
	
	for (const Point & point : vecCondPoint) {
		const int nPoint = pUser->GetPointByType( point.nType );
		if( nPoint < point.nPoint )
		{
			if( !vecResultMsg.HasMessages() )
			{
				switch( point.nType )
				{
				case POINTTYPE_CAMPUS:
					pUser->AddDefinedText( TID_GAME_EXCHANGE_CAMPUSPOINT_FAIL, "%d", point.nPoint );
					break;

				default:
					break;
				}
			}
			bReturn = false;
		}
	}

	if (!bReturn) return std::nullopt;

	return result;
}

// Gives nPayNum randomly drawn items for the pay list. Each item can be given only once
std::vector<CExchange::PayItem>	CExchange::Set::GetPayItemList() const
{
	std::vector<PayItem> payItem = vecPayItem;
	int nProb = 1000000;

	std::vector<PayItem> vecList;

	while (static_cast<int>(vecList.size()) < nPayNum) {
		int nRandom = random(nProb);

		auto i = payItem.begin();
		while (i != payItem.end() && nRandom >= i->nPayProb) {
			++i;
			nRandom -= i->nPayProb;
		}

		if (i == payItem.end()) {
			break;
		}

		vecList.emplace_back(*i);

		nProb -= i->nPayProb;
		payItem.erase(i);
	}

	return vecList;
}

bool CExchange::Set::IsFull(CUser * pUser, std::uint32_t freedSlots, std::uint32_t filledSlots) {
	const int nEmptyCount = pUser->m_Inventory.GetEmptyCount()
		+ static_cast<int>(freedSlots)
		- static_cast<int>(filledSlots);

	return nEmptyCount < 0;
}

int CExchange::Set::ResultExchange(CUser * pUser) const {
	// Check conditions
	auto slotssToUse = CheckCondition(pUser);
	if( !slotssToUse )
		return EXCHANGE_CONDITION_FAILED;
	
	
	// Check slots
	std::uint32_t nbOfFreedSlots = 0;
	for (const auto & slotsToUse : *slotssToUse) {
		nbOfFreedSlots += slotsToUse.numberOfEmptiedSlots;
	}

	if( IsFull( pUser, nbOfFreedSlots, minimalRequiredSlots ) )
		return EXCHANGE_INVENTORY_FAILED;

	// Remove items
	for (const RequiredItem & removeItem : vecRequiredItem) {
		if (removeItem.dwItemId == II_GOLD_SEED1 && removeItem.removeNum > 0) {
			pUser->AddGold(-removeItem.removeNum);
			g_DPSrvr.PutPenyaLog(pUser, "=", "EXCHANGE_REMOVE", removeItem.removeNum);
		}
	}

	ModifiedSlots modified;
	for (const auto & slotsToUse : *slotssToUse) {
		for (const auto & [dwId, quantity] : slotsToUse.slots) {
			CItemElem * pItemElem = pUser->GetItemId(dwId);

			const DWORD itemId = pItemElem->m_dwItemId;
			modified.Add(dwId, pItemElem->m_nItemNum - quantity);

			{
				CItemElem itemElem;
				itemElem.m_dwItemId = itemId;
				itemElem.m_nItemNum = quantity;

				LogItemInfo aLogItem;
				aLogItem.Action = "=";
				aLogItem.SendName = pUser->GetName();
				aLogItem.RecvName = "EXCHANGE_REMOVE";
				aLogItem.WorldId = pUser->GetWorld()->GetID();
				aLogItem.Gold = aLogItem.Gold2 = pUser->GetGold();
				g_DPSrvr.OnLogItem(aLogItem, &itemElem, itemElem.m_nItemNum);
			}
		}
	}

	if (modified) {
		pUser->UpdateItemsQuantity(modified);
		pUser->SendSnapshotNoTarget<SNAPSHOTTYPE_UPDATE_ITEMS_QUANTITY, ModifiedSlots>(modified);
	}

	// Remove points
	for (const Point & removePoint : vecRemovePoint) {
		if( removePoint.nType == POINTTYPE_CAMPUS )
			g_dpDBClient.SendUpdateCampusPoint( pUser->m_idPlayer, removePoint.nPoint, FALSE, 'E' );
	}
	
	// Get reward
	const std::vector<PayItem> payItems = GetPayItemList();
	for (const PayItem & payItem : payItems) {
		CItemElem itemElem;
		itemElem.m_dwItemId = payItem.dwItemId;
		itemElem.m_nItemNum = payItem.nItemNum;
		itemElem.m_byFlag = payItem.byFalg;
		itemElem.SetSerialNumber();

		// Sending should not happen as we are checking if there is enough space, but
		// better be safe
		pUser->CreateOrSendItem(itemElem, TID_GAME_PETTRADE_OK);

		LogItemInfo aLogItem;
		aLogItem.Action = "=";
		aLogItem.SendName = pUser->GetName();
		aLogItem.RecvName = "EXCHANGE_CREATE";
		aLogItem.WorldId = pUser->GetWorld()->GetID();
		aLogItem.Gold = aLogItem.Gold2 = pUser->GetGold();
		g_DPSrvr.OnLogItem( aLogItem, &itemElem, itemElem.m_nItemNum );

		if( !vecResultMsg.HasMessages() )
			pUser->AddDefinedText( TID_EXCHANGE_SUCCESS, "\"%s\" %d", itemElem.GetName(), itemElem.m_nItemNum );
	}
	return EXCHANGE_SUCCESS;
}
#endif


//////////////////////////////////////////////////////////////////////
// RequiredItem

CExchange::SlotsToUse CExchange::RequiredItem::SearchSlots(const CMover * pMover) const {
	std::int32_t leftToConsume = removeNum;
	std::int32_t leftRequired  = condNum;
	
	SlotsToUse result;

	using SlotToUse = SlotsToUse::SlotToUse;

	for (int i = 0; i < pMover->m_Inventory.GetMax(); ++i) {
		const CItemElem * pItemElem = pMover->m_Inventory.GetAtId(i);
		if (!pItemElem) continue;
		if (!IsUsableItem(pItemElem)) continue;
		if (pItemElem->m_dwItemId != dwItemId) continue;

		short usable = pItemElem->m_nItemNum;

		const bool canConsume = leftToConsume > 0
			&& !pMover->m_Inventory.IsEquip(pItemElem->m_dwObjId)
			&& !pMover->IsUsing(pItemElem);

		if (canConsume) {
			short take;
			if (usable <= leftToConsume) {
				++result.numberOfEmptiedSlots;
				take = usable;
			} else {
				take = leftToConsume;
			}

			result.slots.emplace_back(SlotToUse{ static_cast<DWORD>(i), take });

			leftToConsume -= take;
			usable -= take;
		}

		if (leftRequired > 0 && usable > 0) {
			leftRequired -= std::min<std::int32_t>(leftRequired, usable);
		}

		if (leftToConsume == 0 && leftRequired == 0) break;
	}

	result.missingRemove = leftToConsume;
	result.missingCond   = leftRequired;
	return result;
}

void CExchange::RequiredItem::AddInVectorMap(std::vector<RequiredItem> & items, RequiredItem item) {
	const auto it = std::find_if(items.begin(), items.end(),
		[&](const RequiredItem & existing) {
			return existing.dwItemId == item.dwItemId;
		}
	);

	if (it != items.end()) {
		it->condNum += item.condNum;
		it->removeNum += item.removeNum;
	} else {
		items.emplace_back(item);
	}
}


//////////////////////////////////////////////////////////////////////
// Loading


BOOL CExchange::Load_Script() {
	CScanner s;

	if (!s.Load("Exchange_Script.txt"))
		return FALSE;

	while (true) {
		s.GetToken();
		if (s.tok == FINISHED) break;

		CString strErrorMMIId = s.Token;
		int nMMIId = CScript::GetDefineNum(s.Token); //MMI ID
		s.GetToken(); // {
		SetList create = SetList::FromScanner(strErrorMMIId.GetString(), s);
		m_mapExchange.emplace(nMMIId, create);
	}
	return TRUE;
}

CExchange::SetList CExchange::SetList::FromScanner(LPCTSTR strMMIId, CScanner & s) {
	SetList create;

	while (true) {
		s.GetToken();
		if (*s.token == '}') break;

		if (s.Token == _T("DESCRIPTION")) {
			s.GetToken(); // {

			while (true) {
				s.GetToken(); // TextId
				if (*s.token == '}') break;
				create.vecDesciprtionId.push_back(CScript::GetDefineNum(s.Token));
			}
		} else if (s.Token == _T("SET")) {
			Set set = Set::FromScanner(strMMIId, s);
			create.vecSet.push_back(set);
		}
	}

	return create;
}

CExchange::Set CExchange::Set::FromScanner(LPCTSTR strMMIId, CScanner & s) {
	Set set;
	
				s.GetToken();	// List Text ID;
				set.nSetTextId = CScript::GetDefineNum( s.Token );
				s.GetToken(); // {
				s.GetToken();
				while( *s.token != '}' )
				{
					if( s.Token == _T( "RESULTMSG" ) )
					{
						std::vector<int> vecResultMsg;
						s.GetToken(); // {
						s.GetToken();
						while( *s.token != '}' )
						{
							vecResultMsg.push_back( CScript::GetDefineNum( s.Token ) ); // 조건 아이템
							s.GetToken();
						} // while - RESULTMSG

						if (vecResultMsg.empty()) {
							// ok
						} else if (vecResultMsg.size() == 2) {
							set.vecResultMsg.success = vecResultMsg[0];
							set.vecResultMsg.failure = vecResultMsg[1];
						} else {
							Error("CExchange::Load_Script() - Invalid RESULTMSG: found %zu messages but expected 0 or 2", vecResultMsg.size());
						}
					}
					else if( s.Token == _T( "CONDITION" ) )
					{
						s.GetToken(); // {
						s.GetToken();
						while( *s.token != '}' )
						{
							RequiredItem item;
							if( s.Token == "PENYA" )
								item.dwItemId = II_GOLD_SEED1;
							else
								item.dwItemId = CScript::GetDefineNum( s.Token ); // 조건 아이템
							item.condNum = s.GetNumber(); // 조건 갯수

							RequiredItem::AddInVectorMap(set.vecRequiredItem, item);

							s.GetToken();
						} // while - CONDITION
					}
					else if( s.Token == _T( "REMOVE" ) )
					{
						s.GetToken(); // {
						s.GetToken();
						while( *s.token != '}' )
						{
							RequiredItem item;
							if( s.Token == "PENYA" )
								item.dwItemId = II_GOLD_SEED1;
							else
								item.dwItemId = CScript::GetDefineNum( s.Token ); // 조건 아이템
							item.removeNum = s.GetNumber(); // 삭제 갯수

							RequiredItem::AddInVectorMap(set.vecRequiredItem, item);

							s.GetToken();
						} // while - REMOVE
					}
					else if( s.Token == _T( "CONDITION_POINT" ) )
					{
						s.GetToken(); // {
						s.GetToken();
						while( *s.token != '}' )
						{
							Point point;
							point.nType = CScript::GetDefineNum( s.Token );
							point.nPoint = s.GetNumber();	// 조건 포인트
							if( point.nType > 0 )
								set.vecCondPoint.push_back( point );
							else
								Error( "CExchange::Load_Script() - Invalid CONDITION_POINT Type" );
							s.GetToken();
						} // while - CONDITION_POINT
					}
					else if( s.Token == _T( "REMOVE_POINT" ) )
					{
						s.GetToken(); // {
						s.GetToken();
						while( *s.token != '}' )
						{
							Point point;
							point.nType = CScript::GetDefineNum( s.Token );
							point.nPoint = s.GetNumber();	// 차감 포인트
							if( point.nType > 0 )
								set.vecRemovePoint.push_back( point );
							else
								Error( "CExchange::Load_Script() - Invalid REMOVE_POINT Type" );
							s.GetToken();
						} // while - REMOVE_POINT
					}
					else if( s.Token == _T( "PAY" ) )
					{					
						set.nPayNum = s.GetNumber(); // 지급될 아이템 종류 수
						s.GetToken(); // {
						s.GetToken();
						while( *s.token != '}' )
						{
							PayItem item;
							item.dwItemId = CScript::GetDefineNum( s.Token ); // 지급 아이템
							item.nItemNum = s.GetNumber(); // 지급 갯수
							item.nPayProb = s.GetNumber(); // 지급 확률
							s.SetMark();
							s.GetToken();
							s.GoMark();
							if( s.tokenType == NUMBER )
								item.byFalg = static_cast<BYTE>( s.GetNumber() );
															
							set.vecPayItem.push_back(item);

							s.GetToken();
						}

					}
					s.GetToken();
				} // while - SET


#ifdef __WORLDSERVER
	set.ThrowIfInvalid(strMMIId);
#endif // __WORLDSERVER
	set.SplitCondAndRemove();
	set.minimalRequiredSlots = ComputeMinimalRequiredSlots(set.vecPayItem, set.nPayNum);

	return set;
}

void CExchange::Set::ThrowIfInvalid(LPCTSTR strMMIId) const {
	int sumProb = 0;

	for (const PayItem & payItem : vecPayItem) {
		sumProb += payItem.nPayProb;
	}

	if (sumProb > 1000000) {
		Error("Exchange_Script.txt -> In %s, sum of a set is %d > %d", strMMIId, sumProb, 1000000);
		throw std::runtime_error("Invalid Exchange_Script.txt");
	}

	if (std::cmp_greater(nPayNum, vecPayItem.size())) {
		Error("Exchange_Script.txt -> In %s, set can give %d but only has %zu items", strMMIId, nPayNum, vecPayItem.size());
		throw std::runtime_error("Invalid Exchange_Script.txt");
	}
}

void CExchange::Set::SplitCondAndRemove() {
	for (RequiredItem & requiredItem : vecRequiredItem) {
		if (requiredItem.condNum < requiredItem.removeNum) {
			Error("Exchange_Script.txt -> Item %lu is removed more than condition (%d removed, %d condition)",
				requiredItem.dwItemId, requiredItem.removeNum, requiredItem.condNum
			);
			throw std::runtime_error("Invalid Exchange_Script.txt");
		}

		requiredItem.condNum -= requiredItem.removeNum;
	}

#ifdef __CLIENT
	// Sort with the order [cond only..., both cond and remove..., remove...]
	// It is useful if you intend to display the required items in the UI
	std::stable_sort(
		vecRequiredItem.begin(), vecRequiredItem.end(),
		[](const RequiredItem & lhs, const RequiredItem & rhs) {
			static constexpr auto ComputeRank = [](const RequiredItem & item) {
				if (item.condNum != 0 && item.removeNum == 0) return 0;
				if (item.condNum != 0 && item.removeNum != 0) return 1;
				if (item.condNum == 0 && item.removeNum != 0) return 2;
				return 3; // No cond, no remove: should not be possible
			};

			return ComputeRank(lhs) < ComputeRank(rhs);
		}
	);
#endif
}

std::uint32_t CExchange::Set::ComputeMinimalRequiredSlots(std::span<const PayItem> payItems, int nPayNum) {
	std::vector<std::uint32_t> nbRequiredSlots;

	for (const PayItem & payItem : payItems) {
		const ItemProp * pItemProp = prj.GetItemProp(payItem.dwItemId);
		if (!pItemProp) return std::numeric_limits<std::uint32_t>::max();

		nbRequiredSlots.emplace_back(
			static_cast<DWORD>(payItem.nItemNum) / pItemProp->dwPackMax
			+ (payItem.nItemNum == pItemProp->dwPackMax ? 0 : 1)
		);
	}

	nbRequiredSlots.resize(nPayNum, 0);

	return std::reduce(nbRequiredSlots.begin(), nbRequiredSlots.end());
}

