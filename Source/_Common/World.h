#ifndef __WORLD_2002_1_22
#define __WORLD_2002_1_22

#include <array>
#include <boost/container/small_vector.hpp>
#include "ExistingObjects.h"
#include "Weather.h"

#ifdef __LAYER_1015
#define	ADDOBJ( pObj, bAddItToGlobalId, nLayer )	AddObj( (pObj), (bAddItToGlobalId), (nLayer) )
#else	// __LAYER_1015
#define	ADDOBJ( pObj, bAddItToGlobalId, nLayer )	AddObj( (pObj), (bAddItToGlobalId) )
#endif	// __LAYER_1015

#ifdef __WORLDSERVER
	#include "Respawn.h"
	#include "ServerDesc.h"
	#ifdef __LAYER_1015
		#include "..\WorldServer\layeredlinkmap.h"
	#else	// __LAYER_1015
		#include "..\WorldServer\LinkMap.h"
	#endif	// __LAYER_1015
#endif

////////////////////////////////////////////////////////////////////////////////////////////
// define 
///////////////////////////////////////////////////////////////////////////////////////////

#define	MAX_DYNAMICOBJ			81920
//#define	MAX_BKGND				20480
#define	CLOSEWORKER				(DWORD)-1
#define D3DFVF_BOUNDBOXVERTEX	(D3DFVF_XYZ|D3DFVF_DIFFUSE) 
#define MAX_DISPLAYOBJ			5000
#define MAX_DISPLAYSFX			500
#define MINIMAP_SIZE			256
#define WLD_EXTRA_WIDTH			10.0f

////////////////////////////////////////////////////////////////////////////////////////////
// extern 
///////////////////////////////////////////////////////////////////////////////////////////
extern LPDIRECT3DINDEXBUFFER9	g_pIB;
extern LPDIRECT3DINDEXBUFFER9	g_pExIB;
extern LPDIRECT3DINDEXBUFFER9	g_pGridIB;
extern WORD						g_anPrimitive[3];
extern WORD						g_anPrimitiveA[3];
extern WORD						g_anStartIndex[4];

////////////////////////////////////////////////////////////////////////////////////////////
// struct 선언 
///////////////////////////////////////////////////////////////////////////////////////////

struct BOUNDBOXVERTEX 
{ 
	D3DXVECTOR3 p; 
	DWORD		color; 
};

typedef struct tagLANDATTR
{
	DWORD dwColor;
	DWORD dwGenRegionAttr;
	DWORD dwPayRegionAttr;
	DWORD dwMusic;
	DWORD dwRepeat;
	DWORD dwCityName;
	DWORD dwLandName;
	DWORD dwPayID;
	CHAR  dwKey[ 64 ];
} LANDATTR,* LPLANDATTR; 

typedef struct tagLIGHTCOLOR 
{
	FLOAT r1,g1,b1,r2,g2,b2; 
} LIGHTCOLOR,* LPLIGHTCOLOR;

#ifdef __WORLDSERVER
// GuildCombat death (GS + 1vs1)
struct ON_DIE {
	CUser * pDie;
	CUser * pAttacker;
};
#endif	// __WORLDSERVER

struct REPLACEOBJ {
	CMover * pObj;
	DWORD		dwWorldID;
	D3DXVECTOR3	vPos;
#ifdef __LAYER_1015
	int		nLayer;
#endif	// __LAYER_1015
};


typedef std::vector< D3DXVECTOR3 >		Vec3D_Container;
typedef Vec3D_Container::iterator	Vec3D_Iter;

struct TextureSet
{
	TextureSet( ) { _bUse = FALSE; _pTexture = NULL; }
	~TextureSet( ) { _pTexture = NULL; }

	BOOL _bUse;
	std::string _strFileName;
	IDirect3DTexture9* _pTexture;
};

struct ENVIR_INFO
{
	// 대륙 환경 정보, 월드당 여러개의 대륙이 존재할수있음.
	ENVIR_INFO( )
	{
		_iWeather = WEATHER::NONE;
		_bUseEnvir = FALSE;
		_bUseRealData = FALSE;
#ifdef _DEBUG
		_VBforLine = NULL;
#endif 
	}

	int		_id;
	std::string _strName;
	BOOL	  _bUseEnvir;
	BOOL	  _bUseRealData;
	float   _fAmbient[ 3 ];
	float   _fDiffuse[ 3 ];
	float   _fFogStart;
	float   _fFogEnd;
	WEATHER _iWeather;

	TextureSet _kSky;
	TextureSet _kCloud;
	TextureSet _kSun;
	TextureSet _kMoon;

	Vec3D_Container _cVertices;

		
#ifdef _DEBUG
	IDirect3DVertexBuffer9* _VBforLine;
	~ENVIR_INFO( ) { SAFE_RELEASE( _VBforLine ); }
#endif

};

typedef std::vector< ENVIR_INFO >				ENVIR_INFO_Container;
typedef ENVIR_INFO_Container::iterator		ENVIR_INFO_Iter;

struct WORLD_ENVIR_INFO
{
	TextureSet _kSky[ 3 ];		// 0 : night, 1 : day, 2 : evening
	TextureSet _kCloud[ 3 ];
	TextureSet _kSun;
	TextureSet _kMoon;
};



////////////////////////////////////////////////////////////////////////////////////////////
// class foward선언 
////////////////////////////////////////////////////////////////////////////////////////////
class CObj;
class CAnim;
class CItem;
class CSfx;
class CCtrl;
class CMover;
class CLandscape;


class CWorld
{
	friend CLandscape;
	friend CObj;
	friend CMover;

public:
	CWorld();
	~CWorld();

public:
	DWORD			m_dwWorldID;
	int				m_nLandWidth;
	int				m_nLandHeight;
	int				WORLD_WIDTH;	 //		( MAP_SIZE * m_nLandWidth )
	int				WORLD_HEIGHT;	 //		( MAP_SIZE * m_nLandHeight )

	int				m_iMPU;			//gmpbigsun : MPU of the world

///////////////////////////////////////////////////////////////////////////////
#ifdef __WORLDSERVER
///////////////////////////////////////////////////////////////////////////////
private:
	int		m_cbRunnableObject;
public:
	[[nodiscard]] int GetRunnableObjectCount() const noexcept { return m_cbRunnableObject; }
	float*			m_apHeightMap;	
	LPWATERHEIGHT	m_apWaterHeight;		

#ifdef __LAYER_1015
	CLayeredLinkMap	m_linkMap;
#else	// __LAYER_1015
	CLinkMap		m_linkMap;
#endif	// __LAYER_1015


	ExistingObjects<CObj, MAX_DYNAMICOBJ> m_Objs;

	std::vector<CObj *> m_vecBackground;			// static 객체를 담는다.

	std::vector<CObj *> m_aModifyLink;

	std::vector<REPLACEOBJ> m_ReplaceObj;

//	CRIT_SEC		m_AddRemoveLock;
	struct AddRequest { CObj * pObj; bool addToGlobalId; };
	std::vector<AddRequest> m_aAddObjs;

	TCHAR			m_lpszWorld[64];
	u_long			m_cbUser;
#ifdef __LAYER_1021
	CLayerdRespawner	m_respawner;
#else	// __LAYER_1021
	CRespawner		m_respawner;		// 5.84m
#endif	// __LAYER_1021
	BOOL			m_bLoadScriptFlag;

	static constexpr size_t MAX_ON_DIE = 32;
	boost::container::small_vector<ON_DIE, MAX_ON_DIE> m_OnDie;

public:
	void			Process();
	void			ModifyView( CCtrl* pCtrl );
	BOOL			ReadWorld(); 
	u_long			Respawn()	{	return m_respawner.Spawn( this );	}
	// DWORD			GetObjCount() { return m_dwObjNum; }
	void			OnDie(CUser * pDie, CUser * pAttacker);
	void			_OnDie( void );
	CMover*			FindMover( LPCTSTR szName );
private:
	int				IsOverlapped( int c, int p, int r, int w );
	void			AddItToView( CCtrl* pCtrl );
	void			_add();
	void			_process();
	void			_modifylink();
	void			_replace();
	void			_delete();

///////////////////////////////////////////////////////////////////////////////
#else // end __WORLDSERVER
///////////////////////////////////////////////////////////////////////////////

public:
	static D3DLIGHT9	m_light;
	static D3DLIGHT9	m_lightFogSky;
	static boost::container::static_vector<CObj *, MAX_DISPLAYOBJ> m_objCull;
	static CCamera*		m_pCamera;
	static D3DXMATRIX	m_matProj;
	static D3DCOLOR		m_dwBgColor;
	static FLOAT		m_fFarPlane;
	static FLOAT		m_fNearPlane;
	static BOOL			m_bZoomView;
	static int			m_nZoomLevel;
	static CSkyBox		m_skyBox;
	static std::vector<CMover *> m_amvrSelect;
	CLandscape**		m_apLand;
	FLOAT				m_fElapsedTime;
	CObj*				m_pObjFocus;
	OBJID				m_idObjFocusOld;
	DWORD				m_dwAmbient;
	CMapStringToPtr		m_mapLight;
	DWORD				m_dwLightIndex;
	int					m_nVisibilityLand;
	FLOAT				m_fFogStartValue;		//
	FLOAT				m_fFogEndValue;			//
	FLOAT				m_fFogDensity;			//
	int					m_nCharLandPosX;
	int					m_nCharLandPosZ;

	static constexpr int BoundBoxVertexNum = 12;
	LPDIRECT3DVERTEXBUFFER9 m_pBoundBoxVertexBuffer;
	D3DMATERIAL9			m_baseMaterial;

	// 디버그 정보 관련 
	static BOOL			m_bViewLODTerrain;
	static BOOL			m_bMiniMapRender;
	BOOL				m_bViewGrid;
	BOOL				m_bViewGridPatch;
	BOOL				m_bViewGridLandscape;
	BOOL				m_bViewWireframe;
	BOOL				m_bViewSkybox;
	BOOL				m_bViewTerrain;
	BOOL				m_bViewAllObjects;
	BOOL				m_bViewFog;
	BOOL				m_bViewWater;
	BOOL				m_bViewName;
	BOOL				m_bViewHP;
	BOOL				m_bViewLight;
	BOOL				m_bViewWeather;
	BOOL				m_bViewBoundBox;
	BOOL				m_bViewLODObj;
	BOOL				m_bCullObj;
	BOOL				m_bProcess;
	BOOL				m_bViewObj;
	BOOL				m_bViewMover;
	BOOL				m_bViewItem;
	BOOL				m_bViewRegion;
	BOOL				m_bViewSpawn;
	BOOL				m_bViewHeightAttribute;
	BOOL				m_bViewIdState;
	// 디버그 정보 관련 끝 


	void			Process();
	void DeleteObjects();
	static DWORD	GetDiffuseColor() { return D3DCOLOR_ARGB( 255,(int)(m_lightFogSky.Diffuse.r * 255),(int)(m_lightFogSky.Diffuse.g * 255),(int)(m_lightFogSky.Diffuse.b * 255) ); }
	static DWORD	GetAmbientColor() { return D3DCOLOR_ARGB( 255,(int)(m_lightFogSky.Ambient.r * 255),(int)(m_lightFogSky.Ambient.g * 255),(int)(m_lightFogSky.Ambient.b * 255) ); }
	[[nodiscard]] std::pair<int, int> WorldPosToLand(D3DXVECTOR3 vPos) const;
	BOOL			LandInWorld( int x, int z );
	BOOL			IsVecInVisibleLand( D3DXVECTOR3 vPos, D3DXVECTOR3 vCenterPos, int nVisibilityLand );
	BOOL			IsVecInRange( D3DXVECTOR3 vPos, D3DXVECTOR3 vCenterPos, FLOAT fRadius );
	void			SetObjFocus( CObj* pObj, BOOL bSend = TRUE );
	CObj*			GetObjFocus() { return m_pObjFocus; }
	CObj*			GetObjByName(const char * ObjName);
	void			ForceTexture(LPDIRECT3DTEXTURE9 pNewTex);
	BOOL			ProcessObjCollision(D3DXVECTOR3 vPos, CObj* pTargetObj,CObj* pWallObj);
	BOOL			TestOBBIntersect(BBOX* BoxA, BBOX* BoxB);
	BOOL			TestTriIntersect(CObj* pWallObj, CObj* pTargetObj);
	CLandscape*		GetLandscape( CObj* pObj );
	CLandscape*		GetLandscape( D3DXVECTOR3 vPos );
	CLandscape*		GetLandscape( int x, int z );
	CPatch*			GetPatch( D3DXVECTOR3 vPos );
	void			ProcessAllSfx( void );

///////////////////////////////////////////////////////////////////////////////
#endif	//end !__WORLDSERVER
///////////////////////////////////////////////////////////////////////////////

public:
	CMapWordToPtr   m_mapCreateChar;
	FLOAT			m_fMaxHeight;
	FLOAT			m_fMinHeight;
	std::vector<CObj *> m_aDeleteObjs; // can not contain null values
	TCHAR			m_szFilePath[ MAX_PATH ];
	TCHAR			m_szFileName[ 64  ];
	TCHAR			m_szWorldName[ 128 ];
	TCHAR			m_szKeyRevival[ 32 ];
	DWORD			m_dwIdWorldRevival; 
	BOOL			m_bFly;
	BOOL			m_bIsIndoor;
	DWORD			m_dwDiffuse;
	D3DXVECTOR3		m_v3LightDir;
	DWORD           m_dwIdMusic;
	int				m_nPKMode;
	BOOL			m_bSrc;

//	CRegionElemArray	m_aBeginPos;
	CRegionElemArray	m_aRegion;
	CRegionElemArray	m_aStructure;

	LIGHTCOLOR m_k24Light[24];				//월드당 24시간 Light정보를 외부Data에서 가져옴 

	std::string m_strCurContName;
	BOOL m_bProcessingEnvir;			
	DWORD m_dwOldTime;
	DWORD m_dwAddedTime;

	float m_fOldFogStart[2];
	float m_fOldFogEnd[2];
	WEATHER m_iOldWorldWeather;
	BOOL    m_bUsing24Light;

	ENVIR_INFO m_kOldContinent;
	ENVIR_INFO m_kCurContinent;
	ENVIR_INFO_Container m_cContinents;

	//for the world
	WORLD_ENVIR_INFO m_kWorldEnvir;

#ifdef __BS_CHANGEABLE_WORLD_SEACLOUD
	TextureSet m_kSeaCloud;
#endif;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// World.cpp
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
	[[nodiscard]] DWORD GetID() const noexcept { return m_dwWorldID; }

#ifdef __JEFF_11_4
	BOOL	IsArena( void )	
		{	return( m_dwWorldID == WI_WORLD_ARENA );	}
#endif	// __JEFF_11_4

	void			CalcBound();
	BOOL			VecInWorld( const D3DXVECTOR3& vPos );
	BOOL			VecInWorld( FLOAT x, FLOAT z );
	void			ClipX( FLOAT& x );
	void			ClipZ( FLOAT& z );

	void			Free();

#ifdef __WORLDSERVER
	void			LoadAllMoverDialog();
#endif

#ifdef __LAYER_1015
	BOOL			AddObj( CObj* pObj, BOOL bAddItToGlobalId, int nLayer );
	void			DeleteLayerControls( int nLayer );
#else	// __LAYER_1015
	BOOL			AddObj( CObj* pObj, BOOL bAddItToGlobalId = FALSE );
#endif	// __LAYER_1015
	void			RemoveObj( CObj* pObj );
	void			DeleteObj( CObj* pObj );
	void			DestroyObj( CObj* pObj );
	bool DoNotAdd(CUser * pObj);
	BOOL			AddObjArray( CObj* pObj );
	void			RemoveObjArray( CObj* pObj );
	BOOL			InsertObjLink( CObj* pObj );
	BOOL			RemoveObjLink( CObj* pObj );
	BOOL			RemoveObjLink2( CObj* pObj );
	void			AddMoveLandObj( CObj* pObj );
#ifdef __LAYER_1015
	CObj*			GetObjInLinkMap( const D3DXVECTOR3 & vPos, LinkType dwLinkType, int nLinkLevel, int nLayer );
	BOOL			SetObjInLinkMap( const D3DXVECTOR3 & vPos, LinkType dwLinkType, int nLinkLevel, CObj* pObj, int nLayer );
#else	// __LAYER_1015
	CObj*			GetObjInLinkMap( const D3DXVECTOR3 & vPos, LinkType dwLinkType, int nLinkLevel );
	BOOL			SetObjInLinkMap( const D3DXVECTOR3 & vPos, LinkType dwLinkType, int nLinkLevel, CObj* pObj );
#endif	// __LAYER_1015

	FLOAT			GetFullHeight( const D3DXVECTOR3& vPos );
	// 2008/09/24 康: GetFullHeight가 정확한 오브젝트 위 y좌표를
	// 반환하지 않아 임시로 ProcessUnderCollision을 참고하여 만들었다.
	FLOAT			GetItemHeight( const D3DXVECTOR3 & vPos );
	FLOAT			GetUnderHeight( const D3DXVECTOR3 &vPos );
	FLOAT			GetOverHeightForPlayer( const D3DXVECTOR3 &vPos, CObj* pExceptionObj = NULL );
	FLOAT			GetOverHeight( const D3DXVECTOR3 &vPos, CObj* pExceptionObj = NULL );
	FLOAT			GetLandHeight( float x, float z );
	FLOAT			GetLandHeight( const D3DXVECTOR3& vPos );
	FLOAT			GetLandHeight_Fast( float x, float z );
	void			GetLandTri( float x, float z, D3DXVECTOR3* pTri );
	BOOL			GetLandTri2( float x, float z, D3DXVECTOR3* pTri2 );
	int				GetLandTris( float x, float z, D3DXVECTOR3* pTris );
	LPWATERHEIGHT	GetWaterHeight( const D3DXVECTOR3& vPos )	{ return GetWaterHeight( (int)vPos.x, (int)vPos.z ); }
	LPWATERHEIGHT	GetWaterHeight( int x, int z );
	int				GetHeightAttribute( float x, float z );
	
#ifdef __LAYER_1015
	BOOL			ProcessCollision( D3DXVECTOR3 *pOut, const D3DXVECTOR3 &vPos, const D3DXVECTOR3 &vDir, int nSlideCnt, int nLayer );
#else	// __LAYER_1015
	BOOL			ProcessCollision( D3DXVECTOR3 *pOut, const D3DXVECTOR3 &vPos, const D3DXVECTOR3 &vDir, int nSlideCnt );
#endif	// __LAYER_1015
	BOOL			ProcessCollisionReflection( D3DXVECTOR3 *pOut, const D3DXVECTOR3 &vPos, const D3DXVECTOR3 &vDir, int nRecusCnt );
	FLOAT			ProcessUnderCollision( D3DXVECTOR3 *pOut, CObj **pObjColl, const D3DXVECTOR3 &vPos );	
	BOOL			IntersectObjLine( D3DXVECTOR3 *pOut, const D3DXVECTOR3 &vPos, const D3DXVECTOR3 &vEnd, BOOL bSkipTrans = FALSE, BOOL bWithTerrain = FALSE, BOOL bWithObject = TRUE );	
	BOOL			IntersectObjLine2( D3DXVECTOR3 *pOut, const D3DXVECTOR3 &vPos, const D3DXVECTOR3 &vEnd, BOOL bSkipTrans = FALSE, BOOL bWithTerrain = FALSE, BOOL bWithObject = TRUE );	
	FLOAT			IntersectRayTerrain2( const D3DXVECTOR3 &vPickRayOrig, const D3DXVECTOR3 &vPickRayDir );
	void			SendDamageAround( const D3DXVECTOR3 *pvPos, int nDmgType, CMover *pAttacker, int nApplyType, int nAttackID, float fRange );
		
#ifdef __WORLDSERVER
	BOOL	IsUsableDYO( CObj* pObj );
#ifdef __CHIPI_DYO
	BOOL	IsUsableDYO2( LPCHARACTER pCharacter );
#endif // __CHIPI_DYO
#endif	// __WORLDSERVER

#ifdef __CLIENT
public:
	int				GetDiffuseAvg( );
	BOOL			IsInContinent( );	//존재하는 어느대륙에라도 속해있는가?

	ENVIR_INFO*		GetContinentInfo( const std::string& name );

	//Hook the rendering
	BOOL			HookRenderSky( CSkyBox::SKY_TYPE eType, int numRender, float& fAlpha );	

protected:
	//for the world
	void			InitWorldEnvir( const char* filename );
	BOOL			ReadFile24Light( const char* filename );

	//for the continent
	void			ReadFileContinent( const char* filename );
	void			InitContinent( const char* filename );		//월드 진입시 초기화 ( 파일정보 추출 )
	void			InitAfterCreatedPlayer( );				//월드가 생성되고 주인공이 생성된 후 2차초기화 
	void			StartEV( const ENVIR_INFO& kOld );		//환경변화 시작 !				
	BOOL			CheckInOutContinent( );					//주인공에 대한 대륙진입 체크 ( 월드 <-> 대륙 )
	void			MoveInContinent( ENVIR_INFO& kInfo );
	void			MoveOutContinent( const std::string& oldname );
	void			HookUpdateLight( CLight* pLight );		//환경이 변해야 할때 Hooking the light 
	ENVIR_INFO*		GetInContinent( const D3DXVECTOR3& test_point );
	
	BOOL			HookRenderSky_Side( int numRender, ENVIR_INFO* pInfo, float& fAlpha );
	BOOL			HookRenderSky_Cloud( int numRender, ENVIR_INFO* pInfo,float& fAlpha );
	
	//debugging
	void			UpdateContinentLines( );
	void			RenderContinentLines( );
	
public:
	BOOL			IsWorldInstanceDungeon() { return ( WI_INSTANCE_OMINOUS <= GetID() && WI_INSTANCE_LAST_ID >= GetID() ); }
#endif // __CLIENT

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// World3D.cpp
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
	BOOL			IsWorldGuildHouse() { return ( WI_GUILDHOUSE_SMALL <= GetID() && WI_GUILDHOUSE_LARGE >= GetID() ); }

#ifndef __WORLDSERVER
public:
	BOOL			InitWorldEnvir( );		// 24시간 Light 설정 및 기타 환경 초기화 

	void			InProcessing( );		// gmpbigsun : 현재 월드로 진입시 한번 호출됨
	void			OutProcessing( );		// gmpibgsun : 현재 월드에서 퇴장시 한번 호출됨 

	// Render
	void			Projection( int nWidth, int nHeight );
	void			Render( CD3DFont* pFont = NULL );
	void			RenderBase( CD3DFont* pFont );
		
	// Light, Camera and etc...
	void			AddLight( CLight* pLight );
	CLight*			GetLight( LPCTSTR lpszKey );
	void			SetCamera( CCamera* pCamera ) { m_pCamera = pCamera; }
	CCamera*		GetCamera() { return m_pCamera; }
	void			SetLight( BOOL bLight );
	void			SetFogEnable( BOOL bEnable );

	// Culling
	void			UpdateCullInfo( D3DXMATRIX* pMatView, D3DXMATRIX* pMatProj );
	void			CalculateBound();

	// Pick and Intersect
	BOOL			ClientPointToVector( D3DXVECTOR3 *pOut, RECT rect, POINT point, D3DXMATRIX* pmatProj, D3DXMATRIX* pmatView, D3DXVECTOR3* pVector, BOOL bObject = FALSE );
	BOOL			IsPickTerrain(RECT rect, POINT point, D3DXMATRIX* pmatProj, D3DXMATRIX* pmatView );
	CObj*			PickObject( RECT rectClient, POINT ptClient, const D3DXMATRIX* pmatProj, const D3DXMATRIX* pmatView, DWORD dwObjectFilter = 0xffffffff, CObj* pExceptionObj = NULL, D3DXVECTOR3* pVector = NULL, BOOL bOnlyTopPick = FALSE, BOOL bOnlyNPC = FALSE  );
	CObj*			PickObject_Fast( RECT rectClient, POINT ptClient, const D3DXMATRIX* pmatProj, const D3DXMATRIX* pmatView, DWORD dwObjectFilter = 0xffffffff, CObj* pExceptionObj = NULL, BOOL bBoundBox = FALSE, BOOL bOnlyNPC = FALSE );
	BOOL			CheckBound( D3DXVECTOR3* vPos, D3DXVECTOR3* vDest, D3DXVECTOR3* vOut, FLOAT* fLength );
		
	void			RenderGrid();
	void			RenderGrids( CRect rect, WORD dx, DWORD color );
	void			RenderWorldGrids(int wx,int wy,CPoint ptLT,CPoint ptRB,WORD dx,DWORD color);
	
	// Direct3D 관련 오브젝트 초기화및 제거, 재설정 관련 
	HRESULT			InitDeviceObjects();
	HRESULT			RestoreDeviceObjects();
	HRESULT			DeleteDeviceObjects();
	HRESULT			InvalidateDeviceObjects();
	void			RenderTerrain();
	static HRESULT	StaticInitDeviceObjects();
	static HRESULT	StaticRestoreDeviceObjects();
	static HRESULT	StaticDeleteDeviceObjects();
	static HRESULT	StaticInvalidateDeviceObjects();

	BOOL			ReadWorld( D3DXVECTOR3 vPos, BOOL bEraseOldLand = TRUE );

private:
	void			RenderWater();
	void			RenderObject( CD3DFont* pFont = NULL );
	void			RenderBoundBoxVertex( CObj* pObj );
	void			SetBoundBoxVertex( const CObj* pObj );

	void			RenderObj(CObj* pObj);

	static CMover * RenderObject_IsTabbable(CObj * pObj);

#endif // !__WORLDSERVER

	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WorldFile.cpp
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
	BOOL			OpenWorld( LPCTSTR lpszFileName, BOOL bDir = FALSE );
	BOOL			OpenWorld( OBJID idWorld, BOOL bDir = FALSE );
#ifdef __LAYER_1021
private:
	[[nodiscard]] static bool IsLayerPlayer(CObj * pObj, int nLayer);

public:
	void	Invalidate( int nLayer, BOOL bInvalid = TRUE )	{	m_linkMap.Invalidate( nLayer, bInvalid );	}
	[[nodiscard]] bool HasSomeone(int nLayer) const;
	void	DriveOut( int nLayer );
	BOOL	LoadObject( int nLayer );
	BOOL	CreateLayer( int nLayer );
	BOOL	ReleaseLayer( int nLayer );		// 임의 호출 금지
	void	DestroyObj( int nLayer );
#else	// __LAYER_1021
	BOOL	LoadObject();
#endif	// __LAYER_1021
#ifdef __LAYER_1021
	void	Expand();
#endif	// __LAYER_1021
	BOOL			ReadRegion( CScript& s );
	BOOL			ReadRespawn( CScript& s );
	BOOL			LoadRegion();
	BOOL			LoadPatrol();

public:
	// Iterators
	struct Iterators {
		struct Sentinel {};

#ifdef __CLIENT
		class LinkMapIterator;
		class LandRange;
#endif
	};

#ifdef __CLIENT
	[[nodiscard]] Iterators::LandRange GetVisibleLands();
#endif

	template<LinkType dwLinkType>
	void ForLinkMap(
		const D3DXVECTOR3 & vPos,
		int nRange, int nLayer,
		auto && consumer
	);
};


#if !defined(__WORLDSERVER)

inline BOOL CWorld::LandInWorld( int x, int z )
{
	if( x < 0 || z < 0 || x >= m_nLandWidth || z >= m_nLandHeight )
		return FALSE;
	return TRUE;
}

inline BOOL CWorld::IsVecInVisibleLand( D3DXVECTOR3 vPos, D3DXVECTOR3 vCenterPos, int nVisibilityLand )
{
	if( (int)vPos.x < 0 || (int)vPos.z < 0 || (int)vPos.x >= WORLD_WIDTH * m_iMPU || (int)vPos.z >= WORLD_HEIGHT * m_iMPU )
		return FALSE;
	int x1 = int( vCenterPos.x ) / ( MAP_SIZE * m_iMPU );
	int z1 = int( vCenterPos.z ) / ( MAP_SIZE * m_iMPU );
	int x2 = int( vPos.x ) / ( MAP_SIZE * m_iMPU );
	int z2 = int( vPos.z ) / ( MAP_SIZE * m_iMPU );
	return x2 >= (int)x1 - nVisibilityLand && x2 <= (int)x1 + nVisibilityLand &&
		   z2 >= (int)z1 - nVisibilityLand && z2 <= (int)z1 + nVisibilityLand ;
}

inline BOOL CWorld::IsVecInRange( D3DXVECTOR3 vPos, D3DXVECTOR3 vCenterPos, FLOAT fRadius  )
{
	if( (int)vPos.x < 0 || (int)vPos.z < 0 || (int)vPos.x >= WORLD_WIDTH * m_iMPU || (int)vPos.z >= WORLD_HEIGHT * m_iMPU )
		return FALSE;
	if( vPos.x >= ( vCenterPos.x - fRadius ) && vPos.z >= ( vCenterPos.z - fRadius ) 
		&& vPos.x < ( vCenterPos.x + fRadius ) && vPos.z < ( vCenterPos.z + fRadius ) )
		return TRUE;
	return FALSE;
}

inline std::pair<int, int> CWorld::WorldPosToLand(const D3DXVECTOR3 vPos) const {
	return std::pair<int, int>(
		int(vPos.x) / (MAP_SIZE * m_iMPU),
		int(vPos.z) / (MAP_SIZE * m_iMPU)
		);
}

inline CLandscape* CWorld::GetLandscape( CObj* pObj )
{
	FLOAT rX = pObj->GetPos().x;
	FLOAT rZ = pObj->GetPos().z;
	int   mX = int( rX / ( MAP_SIZE * m_iMPU ) );
	int   mZ = int( rZ / ( MAP_SIZE * m_iMPU ) );
	int   nOffset = mX + mZ * m_nLandWidth;
	if( nOffset < 0 || nOffset >= m_nLandWidth * m_nLandHeight )
		return NULL;
	return m_apLand[ nOffset ];
}

inline CLandscape* CWorld::GetLandscape( D3DXVECTOR3 vPos )
{
	FLOAT rX = vPos.x;
	FLOAT rZ = vPos.z ;
	int   mX = int( rX / ( MAP_SIZE * m_iMPU ) );
	int   mZ = int( rZ / ( MAP_SIZE * m_iMPU ) );
	int   nOffset = mX + mZ * m_nLandWidth;
	if( nOffset < 0 || nOffset >= m_nLandWidth * m_nLandHeight )
		return NULL;
	return m_apLand[ nOffset ];
}

inline CLandscape* CWorld::GetLandscape( int x, int z )
{
	return m_apLand[ x + z * m_nLandWidth ];
}

inline CPatch* CWorld::GetPatch( D3DXVECTOR3 vPos )
{
	if( (int)vPos.x < 0 || (int)vPos.z < 0 || (int)vPos.x >= m_nLandWidth * MAP_SIZE * m_iMPU|| (int)vPos.z >= m_nLandHeight * MAP_SIZE * m_iMPU ) return NULL;
	FLOAT rX = vPos.x/m_iMPU;
	FLOAT rZ = vPos.z/m_iMPU ;
	int   mX = int( rX / ( MAP_SIZE ) );
	int   mZ = int( rZ / ( MAP_SIZE ) );
	FLOAT rPX = rX - mX*MAP_SIZE ;
	FLOAT rPZ = rZ - mZ*MAP_SIZE ;
	int   mPX = int( rPX / ( PATCH_SIZE ) );
	int   mPZ = int( rPZ / ( PATCH_SIZE ) );
	return &(m_apLand[ mX + mZ * m_nLandWidth ]->m_aPatches[mPZ][mPX]);
}
#endif	// !__WORLDSERVER


inline BOOL CWorld::VecInWorld( FLOAT x, FLOAT z )
{
//	if( x < 0 || z < 0 || (int)x >= m_nLandWidth * ( MAP_SIZE * MPU ) - 4 || 
//	(int)z >= m_nLandHeight * ( MAP_SIZE * MPU ) - 4 )
//		return FALSE;

	float inv_mpu = 1.0f / (float)m_iMPU;

	if( _isnan( x ) )
		return FALSE;
	if( x < 0.0f )
		return FALSE;
	if( (x * inv_mpu) >= ( WORLD_WIDTH-1 ) )	
	{
		return FALSE;
	}

	if( _isnan( z ) )
		return FALSE;
	if( z < 0.0f )
		return FALSE;
	if( (z * inv_mpu) >= ( WORLD_HEIGHT-1 ) )		
		return FALSE;

	return TRUE;
}

inline BOOL CWorld::VecInWorld( const D3DXVECTOR3 & vPos )
{
	if( _isnan( (double)vPos.y ) )
		return FALSE;

	return VecInWorld( vPos.x, vPos.z );
}

inline void CWorld::ClipX( FLOAT& x )
{
	if( x < WLD_EXTRA_WIDTH ) 
		x = WLD_EXTRA_WIDTH;
	else
	{
		float fMax = WORLD_WIDTH * m_iMPU - WLD_EXTRA_WIDTH;
		if( x > fMax )	
			x = fMax;
	}
}

inline void CWorld::ClipZ( FLOAT& z )
{
	if( z < WLD_EXTRA_WIDTH ) 
		z = WLD_EXTRA_WIDTH;
	else
	{
		float fMax = WORLD_HEIGHT * m_iMPU - WLD_EXTRA_WIDTH;
		if( z > fMax )	
			z = fMax;
	}
}


extern CObj *GetLastPickObj( void );

//gmpbigsun : MPU -> _pWorld->m_iMPU로 수정함.


#ifdef __CLIENT


class CWorld::Iterators::LinkMapIterator {
private:
	CWorld * m_pWorld;
	int m_x; int m_xMin; int m_xMax;
	int m_y; int m_yMin; int m_yMax;

public:
	LinkMapIterator(
		CWorld * pWorld
	) : m_pWorld(pWorld) {
		const int visibilityRange = m_pWorld->m_nVisibilityLand;

		const auto [x, y] = m_pWorld->WorldPosToLand(m_pWorld->m_pCamera->m_vPos);
		m_xMin = std::max(x - visibilityRange, 0);
		m_yMin = std::max(y - visibilityRange, 0);
		m_xMax = std::min(x + visibilityRange, m_pWorld->m_nLandWidth - 1);
		m_yMax = std::min(y + visibilityRange, m_pWorld->m_nLandHeight - 1);

		m_x = m_xMin;
		m_y = m_yMin;
		GoToAValidLand();
	}

	LinkMapIterator(Sentinel) {
		m_pWorld = nullptr;
		m_xMin = m_xMax = m_yMin = m_yMax = 0;
		m_x = m_y = 1;
	}

	CLandscape *& operator*() { return m_pWorld->m_apLand[m_y * m_pWorld->m_nLandWidth + m_x]; }

	bool operator==(Sentinel) const {
		return m_y > m_yMax;
	}

	LinkMapIterator & operator++() {
		Next();
		GoToAValidLand();
		return *this;
	}

private:
	void GoToAValidLand() {
		while (m_y <= m_yMax && !operator*()) {
			Next();
		}
	}

	void Next() {
		if (m_x >= m_xMax) {
			++m_y;
			m_x = m_xMin;
		} else {
			++m_x;
		}
	}

};

class CWorld::Iterators::LandRange {
private:
	CWorld * m_pWorld;

public:
	LandRange(CWorld * pWorld) : m_pWorld(pWorld) {}

	[[nodiscard]] LinkMapIterator begin() const {
		if (!m_pWorld->m_pCamera) return Sentinel{};
		return LinkMapIterator(m_pWorld);
	}

	[[nodiscard]] Sentinel end() const { return Sentinel{}; }
};

inline CWorld::Iterators::LandRange CWorld::GetVisibleLands() {
	return Iterators::LandRange(this);
}


#endif

#if defined(__CLIENT) || defined(__WORLDSERVER)
#include "WorldLandIterator.hpp"
#endif

#endif	// __WORLD_2002_1_22