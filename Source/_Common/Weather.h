#pragma once

#include <sqktd/util.hpp>

enum class WEATHER { NONE = 0, RAIN = 2, SNOW = 3 };

template<>
struct sqktd::enum_description<WEATHER> {
	static constexpr auto valid_values = std::array{
		WEATHER::NONE, WEATHER::RAIN, WEATHER::SNOW
	};
};

