#pragma once

class CExchange final {
public:
	enum { EXCHANGE_SUCCESS, EXCHANGE_FAILED, EXCHANGE_INVENTORY_FAILED, EXCHANGE_CONDITION_FAILED };
	
	struct ResultMessage {
		int success = 0;
		int failure = 0;
		[[nodiscard]] bool HasMessages() const noexcept { return success && failure; }
	};

	struct SlotsToUse {
		struct SlotToUse { DWORD dwId; short itemNum; };

		std::vector<SlotToUse> slots;
		std::uint32_t numberOfEmptiedSlots = 0;
		std::int32_t  missingCond          = 0;
		std::int32_t  missingRemove        = 0;
	};

	struct RequiredItem {
		DWORD dwItemId = NULL_ID;
		int   condNum = 0;
		int   removeNum = 0;

		[[nodiscard]] SlotsToUse SearchSlots(const CMover * pMover) const;

		static void AddInVectorMap(std::vector<RequiredItem> & items, RequiredItem item);
	};

	struct PayItem {
		DWORD	dwItemId = NULL_ID;
		int		nItemNum = 0;
		int		nPayProb = 0;
		BYTE	byFalg = 0;
	};

	struct Point {
		BYTE	nType = 0;
		int		nPoint = 0;
	};

	struct Set {
		static Set FromScanner(LPCTSTR strMMIId, CScanner & s);

		UINT	nSetTextId  = NULL_ID;
		ResultMessage       vecResultMsg;
		std::vector<RequiredItem> vecRequiredItem;
		std::vector<Point> vecCondPoint;
		std::vector<Point> vecRemovePoint;
		std::vector<PayItem> vecPayItem;
		int		nPayNum = 0;
		std::uint32_t minimalRequiredSlots = 0;

		[[nodiscard]] ResultMessage GetResultMsg() const { return vecResultMsg; }

#ifdef __WORLDSERVER
		[[nodiscard]] int ResultExchange(CUser * pUser) const;
		[[nodiscard]] std::optional<std::vector<SlotsToUse>> CheckCondition(CUser * pUser) const;
		[[nodiscard]] std::vector<PayItem> GetPayItemList() const;

		[[nodiscard]] static bool IsFull(CUser * pUser, std::uint32_t freedSlots, std::uint32_t filledSlots);
#endif // __WORLDSERVER

	private:
		Set() = default; /* Use FromScanner instead */

		void ThrowIfInvalid(LPCTSTR strMMIId) const;
		void SplitCondAndRemove();
		[[nodiscard]] static std::uint32_t ComputeMinimalRequiredSlots(std::span<const PayItem> payItems, int nPayNum);
	};

	struct SetList {
		[[nodiscard]] static SetList FromScanner(LPCTSTR strMMIId, CScanner & s);

		std::vector<int> vecDesciprtionId;
		std::vector<Set> vecSet;

		[[nodiscard]] const Set * FindSet(int nListNum) const;

		[[nodiscard]] std::vector<int> GetListTextId() const;
		[[nodiscard]] const std::vector<int> & GetDescId() const { return vecDesciprtionId; }
	};

	BOOL			Load_Script();					// Load Script
	
	[[nodiscard]] const SetList * FindSetList(int nMMIID) const;
	[[nodiscard]] const Set * FindSet(int nMMIID, int nListNum) const;
#ifdef __WORLDSERVER
	[[nodiscard]] int ResultExchange(CUser * pUser, int nMMIid, int nListNum) const;
#endif // __WORLDSERVER
		
	std::map<int, SetList> m_mapExchange;
};
