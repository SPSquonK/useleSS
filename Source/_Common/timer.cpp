#include "stdafx.h"
#include "timer.h"
#include <mmsystem.h>



///////////////////////////////////////////////////////////////////////////////////////

double CTimer::GetTime()
{
#ifdef __GLOBAL_COUNT_0705
	return (double)g_tmCurrent;
#else // __GLOBAL_COUNT_0705 
	return (double)timeGetTime();
#endif // __GLOBAL_COUNT_0705
}

void CTimer::Set(double fInterval, bool bFirstTimeOut) {
	m_fInterval = fInterval;
	m_curTime = GetTime();
	m_endTime = bFirstTimeOut ? m_curTime : m_curTime + fInterval;
}

#define HOUR_SUNRISE     6 
#define HOUR_SUNSET      18
#define SETRISE_VARIABLE 2

// 해가 뜨는 상황을 퍼센트로. 100에 가까우면 만땅, 0에 가까우면 나타나는 중임

int CGameTimer::GetSunPercent() 
{
	int nMin, nHour, nResult = 0;
	if( m_nHour >= HOUR_SUNRISE && m_nHour < HOUR_SUNSET )
	{
		nHour = m_nHour - HOUR_SUNRISE;
		nMin = nHour * 60 + m_nMin;	// 60 * 2 : nMin = 100 : x
		nResult = nMin * 100 / ( 60 * SETRISE_VARIABLE );
	}
	else
	if( m_nHour >= HOUR_SUNSET )
	{
		nHour = m_nHour - HOUR_SUNSET; 
		nMin =  nHour * 60 + m_nMin;	// 60 * 2 : nMin = 100 : x
		nResult = 100 - ( nMin * 100 / ( 60 * SETRISE_VARIABLE ) );
	}
	if( nResult < 0 ) nResult = 0;
	if( nResult > 100 ) nResult = 100;
//	if( nResult > 0 && nResult < 100 )
//	;//TRACE( "sun %d \n", nResult );
	return nResult;
}

// 달 혹은 별이 뜨는 상황을 퍼센트로. 100에 가까우면 만땅, 0에 가까우면 나타나는 중임

int CGameTimer::GetMoonPercent() {
	return 100 - GetSunPercent();
}
//1 = 1000배
//10 = 100배
//100 = 10배
//1000 = 1배
#define TIMESPEED 10
CGameTimer::CGameTimer()
{
	m_nSec = 0;
	m_nMin = 0;
	m_nHour = 0;
	m_nDay = 0;
	m_dBeginTime = 0;
	m_dCurrentTime = 0;
	SetCurrentTime( 3, 15 );
	m_bFixed = FALSE;
}
CGameTimer::CGameTimer( double dCurrentime )
{
	m_nSec = 0;
	m_nMin = 0;
	m_nHour = 0;
	m_nDay = 0;
	m_dBeginTime = 0;
	m_dCurrentTime = 0;
	SetCurrentTime( dCurrentime );
	m_bFixed = FALSE;
}
CGameTimer::~CGameTimer()
{
}
void CGameTimer::SetCurrentTime( double dCurrentime ) 
{ 
	m_dCurrentTime = dCurrentime; 
    m_dBeginTime = (double)timeGetTime();
}
double CGameTimer::GetCurrentTime() 
{ 
	double dTime = (double)timeGetTime();
	return dTime - m_dBeginTime + m_dCurrentTime;
}
void CGameTimer::SetCurrentTime( int nDay, int nHour )
{
	int nSec = ( ( nDay * 24 ) + nHour ) * 60 * 60;
	SetCurrentTime( nSec * TIMESPEED );
}
void CGameTimer::Compute()
{
	if( m_bFixed )
		return;
	long lCurTime = (long)( GetCurrentTime() );
	int nSec, nMin, nHour, nDay;
	lCurTime /= TIMESPEED;

	nSec    = lCurTime / 60;
	m_nSec  = lCurTime % 60;
	nMin    = nSec  / 60; // 60초로 나눈다.
	m_nMin  = nSec  % 60; // 60초로 나눈다.
	nHour   = nMin  / 24; // 24시간으로 나눈다.
	m_nHour = nMin  % 24; // 24시간으로 나눈다.
	nDay    = nHour / 30; // 30일로 나눈다.
	m_nDay  = nHour % 30; // 30일로나눈다.
	
	m_nHour++; // 1 based
	m_nDay++; // 1 based
}
void CGameTimer::SetTime( int nDay, int nHour, int nMin, int nSec )
{
	m_nDay = nDay;
	m_nHour = nHour;
	m_nMin = nMin;
	m_nSec = nSec;
}

///////////////////////////////////////////////
DWORD	m_lTime, m_dwOldTime;

#ifdef __BS_EFFECT_LUA
DWORD g_timeMTE = 0;
#endif;


static BOOL	s_bFrameSkip = TRUE;

void InitFST( void )
{
	m_lTime = 0;
	m_dwOldTime = timeGetTime();
	s_bFrameSkip = TRUE;
}

// 프레임 스키핑을 켜고/끈다
void	SetFrameSkip( BOOL bFlag )
{
	s_bFrameSkip = bFlag;
	if( bFlag == TRUE )		InitFST();
}

// 현재 플래그 돌려줌
BOOL	GetFrameSkip( void )
{
	return s_bFrameSkip;
}

static void UpdateTime( void )
{
	DWORD	dwTime = timeGetTime();

	m_lTime += (dwTime - m_dwOldTime);	// 경과된 시간을 더함
	if( m_lTime > 1000 )	m_lTime = 1000;

#ifdef __BS_EFFECT_LUA
	g_timeMTE += ( dwTime - m_dwOldTime );
	if( g_timeMTE > 20000 )	//20초마다 갱신
		g_timeMTE = 0;
#endif //__BS_EFFECT_LUA
	m_dwOldTime = dwTime;
}

BOOL	IsDrawTiming( void )
{
	if( s_bFrameSkip == FALSE )		return TRUE;		// 프레임 스킵이 꺼져있으면 무조건 드로우.
	UpdateTime();
	if( m_lTime >= 1000 )	
	{
		m_lTime = 0;
		return TRUE;
	}
	return (m_lTime < (1000 / FRAME_PER_SEC)) ? TRUE : FALSE;
}

void	SetFST( void )
{
	if( s_bFrameSkip == FALSE )		return;
	UpdateTime();
	while (m_lTime < (1000 / FRAME_PER_SEC))
		UpdateTime();

	m_lTime -= (1000 / FRAME_PER_SEC);
}










