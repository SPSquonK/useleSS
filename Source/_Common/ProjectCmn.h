#pragma once

#include <array>
#include <vector>
#include <string>
#include <exception>
#include <span>
#include <boost/container/small_vector.hpp>
#include <boost/container/static_vector.hpp>
#include "FlyFFTypes.h"
#include "defineJob.h"
#include <variant>
#include <bitset>

#define	MAX_OBJARRAY			8
#define	MAX_INVENTORY			42			// 60 //42
#define MAX_GUILDBANK			42
#define	MAX_BANK				42
#define	MAX_TRADE				25
#define	MAX_VENDITEM			30
#define	MAX_REPAIRINGITEM		25
#define	MAX_VENDORNAME			48
#define	MAX_EXPCHARACTER		200
#define	MAX_FXPCHARACTER		200
#define	MAX_CHARACTER_LIST		3 
#define	MAX_VENDOR_INVENTORY	100
#define	MAX_VENDOR_INVENTORY_TAB	4
#define	MAX_QUEST				100
#define	MAX_COMPLETE_QUEST		300 
#define MAX_CHECKED_QUEST		5
#define MAX_PARTYLEVEL			10

static constexpr size_t MAX_PROPMOVER = 2000; // MoverProp배열의 최대갯수 

#define	MAX_HONOR_TITLE		150	// 달인타이틀 제한
#define MAX_VENDOR_REVISION		20

#include "defineitemkind.h"

class CAr;
struct ItemProp;

struct tagColorText {
	DWORD dwColor;
	CString lpszData;
};

struct CtrlProp {
	DWORD	dwID          = 0;		// 변수명	
	TCHAR	szName[64]    = _T("");	// 한글명칭 
	DWORD	dwAI          = 0;		// AIInterface
	DWORD   dwCtrlKind1 = 0;
	DWORD   dwCtrlKind2 = 0;
	DWORD   dwCtrlKind3 = 0;
	DWORD   dwSfxCtrl   = 0;

	//길드 하우징 전용 오브젝트 인가?
	[[nodiscard]] bool IsGuildHousingObj() const noexcept { return CK1_GUILD_HOUSE == dwCtrlKind1; }
};

struct AddSkillProp
{
	static constexpr int NB_PROPS = 2;

	DWORD	dwID          = 0; // 변수명	
	DWORD	dwName        = 0; // 명칭
	DWORD	dwSkillLvl    = 0; // 스킬레벨
	DWORD	dwAbilityMin  = 0; // 최소능력
	DWORD	dwAbilityMax  = 0; // 최대능력
	DWORD	dwAttackSpeed = 0; // 공격속도
	DWORD	dwDmgShift    = 0; // 타격시 반동
	DWORD	nProbability  = 0; // 발동 확률
	DWORD	dwDestParam[2]   = { 0, 0 };		// 적용대상1
	DWORD	nAdjParamVal[2]  = { 0, 0 };	// 적용값1;
	DWORD	dwChgParamVal[2] = { 0, 0 };	//적용변화값1
	int		nDestData1[3] = { 0, 0, 0 };		// 적용데이타3개, DestParam[0]에 해당.
	DWORD	dwActiveSkill        = 0;		// 발동 마법
	DWORD	dwActiveSkillRate    = 0;	// 발동 마법 확률.
	DWORD	dwActiveSkillRatePVP = 0;	// 대인 발동 마법 확률.
	int		nReqMp               = 0;				//필요MP
	int		nReqFp               = 0;				//필요FP
	DWORD	dwCooldown           = 0;		// 쿨다운
	DWORD	dwCastingTime        = 0;		//기술 준비시간
	DWORD	dwSkillRange         = 0;		// 기술 시전거리	
	DWORD	dwCircleTime         = 0;		//유지시간
	DWORD   dwPainTime         = 0;         // 지속 피해시간
	DWORD	dwSkillTime          = 0;		//지속시간
	int		nSkillCount          = 0;		// 발사체 발사 개수. 개당공격력 = 총공격력 / nSkillCount;
	DWORD   dwSkillExp         = 0;         //스킬 경험치
	DWORD	dwExp                = 0;				//현재경험치
	DWORD	dwComboSkillTime     = 0;	//콤보스킬타임
	DWORD	dwAbilityMinPVP      = 0;	// 최소능력(대인)
	DWORD	dwAbilityMaxPVP      = 0;	// 최대능력(대인)
	DWORD	nProbabilityPVP      = 0;	// 발동 확률(대인)
};

enum class _FILEWITHTEXT
{	
	FILE_FILTER	= 0, 
	FILE_INVALID = 1, 
	FILE_NOTICE = 2,	
	FILE_GUILDCOMBAT_TEXT_1 = 3, 
	FILE_GUILDCOMBAT_TEXT_2 = 4, 
	FILE_GUILDCOMBAT_TEXT_3 = 5, 
	FILE_GUILDCOMBAT_TEXT_4 = 6, 
	FILE_GUILDCOMBAT_TEXT_5 = 7,
	FILE_GUILDCOMBAT_TEXT_6 = 8,
	FILE_ALLOWED_LETTER	= 9,
	FILE_GUILDCOMBAT_1TO1_TEXT_1 = 10,
	FILE_GUILDCOMBAT_1TO1_TEXT_2 = 11,
	FILE_GUILDCOMBAT_1TO1_TEXT_3 = 12,
	FILE_GUILDCOMBAT_1TO1_TEXT_4 = 13,
	FILE_GUILDCOMBAT_1TO1_TEXT_5 = 14,
	FILE_ALLOWED_LETTER2	= 15
};

using enum _FILEWITHTEXT;

// Item Property Type
struct IP_FLAG {
	static constexpr size_t BINDS = 0;				// 1 - 귀속 아이템 
	static constexpr size_t UNDESTRUCTABLE = 1;	//	2 - 삭제 불가 아이템
	static constexpr size_t EQUIP_BIND = 2;
	static constexpr size_t MAX = 3;
};

struct SKILL {
  DWORD dwSkill;
  DWORD dwLevel;
  const ItemProp * GetProp() const;
};

using LPSKILL = SKILL *;

struct ItemProp
{
  // !!! ZeroMemory-ing this struct must be a valid operation
	static constexpr size_t NB_PROPS = 3;

	DWORD	dwID = 0;		// 변수명	
	TCHAR	szName[64] = _T("");	// 한글명칭 

	DWORD	dwMotion;			// 동작 
	DWORD	dwNum;				// 기본생성개수	
	DWORD	dwPackMax;			// 최대곂침개수	
	DWORD	dwItemKind1;		// 1차아이템종류	
	DWORD	dwItemKind2;		// 2차아이템종류	
	DWORD	dwItemKind3;		// 3차아이템종류	
	DWORD	dwItemJob;			// 아이템직업소속 
	BOOL	bPermanence;		// 소비불가	
	DWORD	dwUseable;			// 사용시점	
	DWORD	dwItemSex;			// 사용성별	
	DWORD	dwCost;				// 가격	 
	DWORD	dwEndurance;		// 내구력	
	int		nLog;				// 로그
	int		nMaxRepair;			// 수리횟수
	DWORD	dwHanded;			// 잡는형식	
	std::bitset<IP_FLAG::MAX> dwFlag;				// 다목적 플래그	
	DWORD	dwParts;			// 장착위치	
	DWORD	dwPartsub;			// 장착위치	
	DWORD	bPartsFile;			// 파츠파일 사용여부 
	DWORD	dwExclusive;		// 제거될위치	
	DWORD	dwBasePartsIgnore;
	DWORD	dwItemLV;			// 아이템레벨	
	DWORD	dwItemRare;			// 희귀성	
	DWORD   dwShopAble;
	int		nShellQuantity;		// 최대장전개수	- IK가 채집도구일때는 최대 채집량이 된다.(삭제 예정)
	DWORD	dwActiveSkillLv;	// 발동 마법 레벨
	DWORD   dwFuelRe;			// 팩당연료충전량
	DWORD	dwAFuelReMax;		// 가속연료충전량
	DWORD	dwSpellType;		// 마법속성	
	DWORD	dwLinkKindBullet;	// 필요 소모장비 아이템	
	DWORD	dwLinkKind;			// 필요 아이템종류
	DWORD	dwAbilityMin;		// 최소능력치 - 공격력, 방어력, 성능 기타등등 
	DWORD	dwAbilityMax;		// 최대능력치 - 공격력, 방어력, 성능 기타등등 
	BOOL	bCharged;		
	SAI79::ePropType	eItemType;
	short	wItemEatk;			// 속성 데미지( 위 속성 타입으로 공격력을 설정한다. )
	DWORD   dwParry;			// 회피율 
	DWORD   dwblockRating;		// 블럭 수치 
	int		nAddSkillMin;		// 최소 추가 스킬
	int		nAddSkillMax;		// 최대 추가 스킬.
	DWORD	dwAtkStyle;			// 공격 스타일 
	DWORD	dwWeaponType;		// 무기종류 
  D3DXVECTOR3 blinkWingPos;
  float blinkWingAngle;
    DWORD	tmContinuousPain;	// 지속 피해 
	DWORD	dwRecoil;			// 반동	
	DWORD	dwLoadingTime;		// 장전시간	- IK가 채집도구일때는 채집속도(능력)이 된다.
	LONG	nAdjHitRate;		// 추가공격성공률	
	FLOAT	fAttackSpeed;		// 공격속도	
	DWORD	dwDmgShift;			// 타격시 반동	
	DWORD	dwAttackRange;		// 공격범위	
	int		nProbability;		// 적용확률
	DWORD	dwDestParam[NB_PROPS];		// 적용대상1	
	LONG	nAdjParamVal[NB_PROPS];	// 적용값1	
	DWORD	dwChgParamVal[NB_PROPS];	// 적용변화값1	
	int		nDestData1[NB_PROPS];		// 적용데이타값 3개, destParam1에만 해당됨.
	DWORD	dwActiveSkill;		// 발동 마법
	DWORD	dwActiveSkillRate;	// 발동 마법 확률.
	DWORD	dwReqMp;			// 필요MP	
	DWORD	dwReqFp;			// 필요FP	
	DWORD	dwReqDisLV;
  boost::container::static_vector<SKILL, 2> vRequiredSkills; // List of required skills
	DWORD	dwSkillReadyType;
	DWORD	dwSkillReady;		// 기술 준비시간	
	DWORD	_dwSkillRange;		// 기술 시전거리	
	DWORD	dwSfxElemental;
	DWORD	dwSfxObj;			// 생성할 효과 오브젝트 
	DWORD	dwSfxObj2;			// 생성할 효과 오브젝트 
	DWORD	dwSfxObj3;			// 발동효과, 타겟
	DWORD	dwSfxObj4;			// 지속되는 효과 이펙트.
	DWORD	dwSfxObj5;			// 발동효과, 시전자 
	DWORD	dwUseMotion;		// 사용 모션
	DWORD	dwCircleTime;		// 유지시간 
	DWORD	dwSkillTime;		// 지속시간	
	DWORD	dwExeTarget;
	DWORD	dwUseChance;
	DWORD	dwSpellRegion;		// 마법 범위 
	DWORD   dwReferStat1;
	DWORD   dwReferStat2;
	DWORD   dwReferTarget1;
	DWORD   dwReferTarget2;
	DWORD   dwReferValue1;
	DWORD   dwReferValue2;
	DWORD	dwSkillType;		// 기술속성	
	int		nItemResistElecricity;
	int		nItemResistFire;
	int		nItemResistWind;
	int		nItemResistWater;
	int		nItemResistEarth;
	LONG	nEvildoing;			// 악행	
	DWORD	dwExpertLV;	
	DWORD	dwExpertMax;		// 최대숙련레벨 
	DWORD	dwSubDefine;
	DWORD	dwExp;				// 현재경험치	
	DWORD	dwComboStyle;
	FLOAT	fFlightSpeed;		// 비행추진력(속도)
	FLOAT	fFlightLRAngle;		// 좌우 턴 각도.
	FLOAT	fFlightTBAngle;		// 상하 턴 각도.
	DWORD	dwFlightLimit;		// 비행제한레벨
	DWORD	dwFFuelReMax;		// 비행연료충전량
	DWORD	dwLimitLevel1;		// 제한레벨1
	int		nReflect;			// 리플렉션 옵션.
	DWORD	dwSndAttack1;		// 효과음 : 공격 1
	DWORD	dwSndAttack2;		// 효과음 : 공격 2
	QuestId	dwQuestId;
	TCHAR	szTextFileName[64];	// item에 GM command에 넣는 것에 사용

#ifdef __CLIENT
    TCHAR	szIcon[64];			// dds파일 이름 
	TCHAR	szCommand[256];		// 설명문 
#endif
	int		nVer;

#ifdef __VERIFY_0201
	TCHAR	szIcon[64];
	TCHAR	szCommand[256];		// 설명문 
#endif	// __VERIFY_0201

	[[nodiscard]] DWORD GetCoolTime() const noexcept { return dwSkillReady; }
	[[nodiscard]] bool IsUltimate() const noexcept {
		return (dwItemKind2 == IK2_WEAPON_DIRECT || dwItemKind2 == IK2_WEAPON_MAGIC)
			&& dwReferStat1 == WEAPON_ULTIMATE;
	}
	BOOL	IsVisPet()	{ return (dwItemKind3 == IK3_PET) && (dwReferStat1 == PET_VIS); }
	[[nodiscard]] bool IsVis() const noexcept { return dwItemKind3 == IK3_VIS; }
#ifdef __CLIENT
	BOOL	IsVisKey()	{ return (II_SYS_SYS_VIS_KEY01 == dwID ); }
	BOOL	IsPickupToBuff( ) { return ( II_SYS_SYS_SCR_PET_MAGIC == dwID ); }
#endif //__CLIENT

#ifdef __CLIENT
	[[nodiscard]] CTexture * GetTexture() const;
#endif

	[[nodiscard]] bool IsNeedTarget() const noexcept;
};

class CItemElem;

namespace ItemProps {
	class PiercingType {
		enum class V { None, LetterCard, NumericCard, Vis };
	public:
		using enum V;

		constexpr PiercingType(const V value) : m_value(value) {}
		constexpr operator V() const { return m_value; }

		constexpr bool IsOnEquipement() const {
			return m_value == V::LetterCard || m_value == V::NumericCard;
		}

		[[nodiscard]] unsigned int GetNumberOfPiercings() const;

	private:
		V m_value;
	};

	[[nodiscard]] bool IsOrichalcum(const CItemElem & item);
	[[nodiscard]] bool IsOrichalcum(const ItemProp & item);

	[[nodiscard]] bool IsMoonstone(const CItemElem & item);
	[[nodiscard]] bool IsMoonstone(const ItemProp & item);

#ifdef __CLIENT
	[[nodiscard]] CTexture * GetItemIconTexture(DWORD itemId);
#endif

}

// 직업에 따른 프로퍼티 ( propJob.inc에서 읽어들임 )
struct JobProp {
	static const JobProp NullObject;
	float AttackSpeed = 1.0f;			//공속 
	float FactorMaxHP = 1.0f;			//최대 HP 계산에 사용되는 factor
	float FactorMaxMP = 1.0f;			//최대 MP 계산에 사용되는 factor
	float FactorMaxFP = 1.0f;			//최대 FP 계산에 사용되는 factor
	float FactorDef = 1.0f;				//물리 방어력 계산에 사용되는 factor
	float FactorHPRecovery = 1.0f;		//HP회복 factor
	float FactorMPRecovery = 1.0f;		//MP회복 factor
	float FactorFPRecovery = 1.0f;		//FP회복 factor
	float MeleeSWD = 1.0f;				//WT_MELEE_SWD의 ATK factor
	float MeleeAXE = 1.0f;				//WT_MELEE_AXE의 ATK factor
	float MeleeSTAFF = 1.0f; 			//WT_MELEE_STAFF의 ATK factor
	float MeleeSTICK = 1.0f;			//WT_MELEE_STICK의 ATK factor
	float MeleeKNUCKLE = 1.0f;			//WT_MELEE_KNUCKLE의 ATK factor
	float MagicWAND = 1.0f;				//WT_MAGIC_WAND의 ATK factor 
	float Blocking = 1.0f;				//블록킹 factor
	float MeleeYOYO = 1.0f;				//요요의 ATK factor 
	float Critical = 1.0f;				//크리티컬 처리
};

struct JobSkills : std::array<std::vector<const ItemProp *>, MAX_JOB> {
	void Load(CFixedArray<ItemProp> & aPropSkill);
};

/*----------------------------------------------------------------------------------------------------*/
struct DROPKIND {
	DWORD	dwIK3;
	short	nMinUniq;
	short	nMaxUniq;
};

struct QUESTITEM {
	QuestId	dwQuest;
	DWORD   dwState;
	DWORD	dwIndex;
	DWORD	dwProbability;
	DWORD	dwNumber; 
};

class CDropItemGenerator final {
public:
	struct Item {
		DWORD	dwIndex;       // Id of item
		DWORD	dwProbability; // Probability over 3e+9
		DWORD	dwLevel;       // Upgrade of dropped item
		DWORD	dwNumber;      // Quantity

		[[nodiscard]] bool IsDropped(BOOL bUniqueMode, float fProbability = 0.0f) const;
	};

	struct Money {
		DWORD	dwNumber;      // Min quantity
		DWORD	dwNumber2;     // Max quantity, exclusive
	};

private:
	std::vector<Item>	m_items;
	boost::container::small_vector<Money, 1> m_money;

public:
	DWORD				m_dwMax = 0;

public:
	void Add(const Item  & rDropItem ) { m_items.emplace_back(rDropItem); }
	void Add(const Money & rDropMoney) { m_money.emplace_back(rDropMoney); }

	[[nodiscard]] std::span<const Item > GetItems() const { return m_items; }
	[[nodiscard]] std::span<const Money> GetMoney() const { return m_money; }
};

using CDropKindGenerator  = std::vector<DROPKIND>;
using CQuestItemGenerator = std::vector<QUESTITEM>;

struct MonsterTransform {
	float fHPRate = 0.0f;
	DWORD dwMonsterId = NULL_ID;
};
/*----------------------------------------------------------------------------------------------------*/

struct MoverProp : CtrlProp
{
	DWORD dwStr = 0;	// 힘,                  
	DWORD dwSta = 0;	// 체력,
	DWORD dwDex = 0;	// 민첩,
	DWORD dwInt = 0;	// 지능,
	DWORD dwHR  = 0;
	DWORD dwER  = 0;
	DWORD	dwBelligerence = 0;		// 호전성,
	DWORD	dwLevel   = 0;	// 레벨,
	DWORD	dwSize    = 0;		// 크기,
	DWORD dwClass   = 0;
	BOOL	bIfParts  = FALSE;	// 파츠냐?
	int		nChaotic  = 0;	// 나쁜놈 마이너스/ 좋은넘 플러스
	DWORD	dwUseable = 0;	// 방어 캐릭수,
	DWORD	dwAtkMin  = 0;	// 최소타격치,
	DWORD	dwAtkMax  = 0;	// 최대타격치,
	DWORD	dwAtk1    = 0;
	DWORD	dwAtk2    = 0;
	DWORD	dwAtk3    = 0;
	DWORD	dwAtk4    = 0;		// dwHorizontalRate가 이걸로 바뀜.
	FLOAT	fFrame    = 1.0f;	// 이동 시 프레임 가중치

	DWORD	dwAttackSpeed   = 0;	// 공격속도,
	DWORD	dwReAttackDelay = 0;
	DWORD	dwAddHp         = 0;		// ,
	DWORD	dwAddMp         = 0;		// ,
	DWORD	dwNaturalArmor  = 0;	// 자연방어력 

	SAI79::ePropType eElementType = SAI79::NO_PROP;
	short wElementAtk             = 0;		// 속성 데미지( 위 속성 타입으로 공격력을 설정한다. )

	DWORD	dwHideLevel = 0;	// 레벨 안보이는넘이냐..
	FLOAT	fSpeed = 0.0f;	// 이동속도,
	DWORD	dwFlying    = 0;	// 비행유무,
	DWORD	dwResisMgic = 0;	//마법저항 

	int		nResistElecricity = 0;
	int		nResistFire       = 0;
	int		nResistWind       = 0;
	int		nResistWater      = 0;
	int		nResistEarth      = 0;
	
	DWORD	dwSourceMaterial  = 0;	// 제공재료
	DWORD	dwMaterialAmount  = 0;	// 재료양
	DWORD	dwHoldingTime     = 0;	// 시체유지시간
	DWORD	dwCorrectionValue = 0;	// 아이템생성보정값
	EXPINTEGER	nExpValue = 0;
	int		nFxpValue = 0;		// 비행경험치.
	DWORD	bKillable = 0;	// 죽음유무,

	DWORD   dwSndDmg1  = 0;
	DWORD   dwSndDmg2  = 0;
	DWORD   dwSndIdle1 = 0;

	short m_nEvasionHP     = 0;
	short m_nEvasionSec    = 0;
	short m_nRunawayHP     = 0; // HP가 10 이하면 도주 
	short m_nCallHelperMax = 0; // 총 갯수 
	short m_nCallHP        = 0; // 도움요청하기 위한 HP
	short m_nCallHelperIdx  [ 5 ]; // 도움요청하기 Id
	short m_nCallHelperNum  [ 5 ]; // 도움요청하기 Id
	short m_bCallHelperParty[ 5 ]; // 도움요청하기 Id

	short m_dwAttackMoveDelay = 0;
	short m_dwRunawayDelay    = 0;

#if !defined(__CORESERVER) 
	int		m_nScanJob = 0;		// 타겟을 검색할때 특정 직업으로 검색하는가. 0 이면 ALL
	// 공격조건들은 비스트에서 선공으로 지정되어 있을때 얘기다.
	int		m_nHPCond         = 0;		// 타겟이 hp% 이하일때 공격을 함.
	int		m_nLvCond         = 0;		// 타겟과 레벨비교하여 낮으면 공격을 함.
	int		m_nRecvCondMe     = 0;	// 회복 조건.  내hp가 몇%이하로 떨어졌을때?
	int		m_nRecvCondHow    = 0;	// 회복할때 회복할 양 %단위
	int		m_nRecvCondMP     = 0;	// 회복할때 MP소모량 %단위
	BYTE	m_bMeleeAttack    = 0;	// 근접 공격 AI가 있는가?
	BYTE	m_bRecvCondWho    = 0;	// 누구를 치료할꺼냐. 0:지정안됨 1:다른놈 2:나 3:모두.
	BYTE	m_bRecvCond       = 0;	// 치료하냐? 0:치료안함 1:전투중에만 치료함 2:전투/비전투 모두 치료
	BYTE	m_bHelpWho        = 0;		// 도움요청시 - 0:부르지않음 1:아무나  2:같은종족만.
	BYTE	m_bRangeAttack[ MAX_JOB ];		// 각 직업별 원거리 공격 거리.
	int		m_nSummProb       = 0;	// 소환 확률 : 0이면 소환능력 없음.
	int		m_nSummNum        = 0;		// 한번에 몇마리나 소환하냐.
	int		m_nSummID         = 0;		// 어떤 몬스터?
	int		m_nHelpRangeMul   = 0;	// 도움요청 거리. 시야의 배수
	DWORD	m_tmUnitHelp      = 0;			// 헬프 타이머.
	int		m_nBerserkHP      = 0;		// 버서커가 되기 위한 HP%
	float	m_fBerserkDmgMul  = 0.0f;	// 버서커가 되었을때 데미지 배수.
	int		m_nLoot           = 0;			// 루팅몹인가.
	int		m_nLootProb       = 0;		// 루팅 확률
	QuestId   m_dwScanQuestId = QuestIdNone;
	DWORD   m_dwScanItemIdx = 0; 
	int		m_nScanChao       = 0;		// 카오, 비카오 검색
#endif // !__CORESERVER
	
#ifndef __CORESERVER
	float	m_fHitPoint_Rate    = 0.0f;		// 몬스터 최대 HP률 // dwAddHp * m_nHitPoint_Rate
	float	m_fAttackPower_Rate = 0.0f;	// 몬스터 최대 공격률 // dwAtkMin * m_nAttackPower_Rate
	float	m_fDefence_Rate     = 0.0f;		// 몬스터 최대 방어률 // dwAddHp * m_nDefence_Rate
	float	m_fExp_Rate         = 0.0f;			// 몬스터 최대 경험치률 // dwAddHp * m_nExp_Rate
	float	m_fItemDrop_Rate    = 0.0f;		// 몬스터 최대 아이템 드롭률 // dwAddHp * m_nItemDrop_Rate
	float	m_fPenya_Rate       = 0.0f;			// 몬스터 최대 페냐률 // dwAddHp * m_nPenya_Rate
	BOOL	m_bRate = FALSE;
#endif

	
	short   m_nAttackFirstRange = 0;
	CDropItemGenerator	m_DropItemGenerator;
	CQuestItemGenerator		m_QuestItemGenerator;
	CDropKindGenerator	m_DropKindGenerator;
	MonsterTransform	m_MonsterTransform;
	
	MoverProp()
	{
		memset( m_nCallHelperIdx, 0, sizeof(m_nCallHelperIdx) );
		memset( m_nCallHelperNum, 0, sizeof(m_nCallHelperNum) );
		memset( m_bCallHelperParty, 0, sizeof(m_bCallHelperParty) );

#if !defined(__CORESERVER) 
		memset( m_bRangeAttack, 0, sizeof(m_bRangeAttack) );
#endif // !__CORESERVER
	}
};

#ifdef __WORLDSERVER
enum class SpawnType : bool { Region, Script };

struct CtrlSpawnInfo {
	int       spawnId;
	SpawnType type;
};
#endif

#define TASKBAR_TOP    0
#define TASKBAR_BOTTOM 1
#define TASKBAR_LEFT   2
#define TASKBAR_RIGHT  3

// short cut
#define MAX_SLOT_APPLET			18
#define MAX_SLOT_ITEM			9
#define MAX_SLOT_QUEUE			5
#define	MAX_SLOT_ITEM_COUNT		8

enum class ShortcutType {
	None = 0,
	Etc = 1,
	Applet = 2,
	Motion = 3,
	Script = 4,
	Item = 5,
	Skill = 6,
	Object = 7,
	Chat = 8,
	SkillFun = 9,
	Emoticon = 10,
	Lord = 11,
	PartySkill = 12
};

#if defined ( __CLIENT )
class CWndBase;
#endif	// __CLIENT

class CAr;

struct SHORTCUT {
#if defined ( __CLIENT )
	CWndBase * m_pFromWnd = nullptr;
	CTexture * m_pTexture = nullptr;
	DWORD	m_dwItemId = 0;
#endif	// __CLIENT
	ShortcutType m_dwShortcut = ShortcutType::None;
	DWORD     m_dwId = 0;
	DWORD     m_dwIndex = 0;
	DWORD     m_dwData = NULL;
	TCHAR     m_szString[MAX_SHORTCUT_STRING] = ""; // SHORTCUT_CHAT일 경우 저장.
	BOOL IsEmpty() const { return m_dwShortcut == ShortcutType::None; }
	void Empty() { m_dwShortcut = ShortcutType::None; }

	friend CAr & operator<<(CAr & ar, const SHORTCUT & self);
	friend CAr & operator>>(CAr & ar, SHORTCUT & self);

	// ==========================================================================

	struct Source {
		struct Inventory { DWORD itemPos; };
		struct Penya {};
		struct Bag { int bagId; DWORD itemPos; };
		struct Bank { int slot; DWORD itemPos; };
		struct BankPenya { int slot; };
		struct Guild { DWORD itemPos; };
		struct GuildMoney {};
	};

	struct Sources {
		using ItemOrMoney = std::variant<
			Source::Inventory, Source::Penya,
			Source::Bank, Source::BankPenya,
			Source::Guild, Source::GuildMoney,
			Source::Bag
		>;
	};
};
using LPSHORTCUT = SHORTCUT *;

struct EXPPARTY {
	DWORD	Exp;
	DWORD	Point;
};

struct EXPCHARACTER {
	EXPINTEGER	nExp1;
	DWORD dwLPPoint;
	EXPINTEGER	nLimitExp;
};

#include "NameValidation.h"

class ProjectLoadError : public std::exception {
	const char * m_errorMessage;
	char m_fullMessage[512] = "Error when loading a file: ";
public:
	ProjectLoadError(const char * errorMessage, const char * fileName)
		: m_errorMessage(errorMessage) {
		if (errorMessage != nullptr && std::strlen(errorMessage) < 400) {
			std::strcat(m_fullMessage, errorMessage);
		}
		if (fileName != nullptr && std::strlen(fileName) < 70) {
			std::strcat(m_fullMessage, " - Filename= ");
			std::strcat(m_fullMessage, fileName);
		}
	}

	[[nodiscard]] const char * what() const { return m_fullMessage; }
};

#include "ProjectJob.h"

enum class LoadPropItemStyle {
	V15, V22SkillsSkip
};

/* A World ID - Position in the world pair */
struct FullPosition {
  DWORD       dwWorldId;
  D3DXVECTOR3 vPos;

  friend CAr & operator<<(CAr & ar, const FullPosition & self);
  friend CAr & operator>>(CAr & ar,       FullPosition & self);

  [[nodiscard]] static FullPosition Zero() {
    return FullPosition{ 0, D3DXVECTOR3(0.f, 0.f, 0.f) };
  }
};

