#pragma once

#define	MIN(x)	(1000*60*((int)x))
#define	SEC(x)	(1000*((int)x))

class CTimer final {
private:
	double m_curTime;
	double m_endTime;
	double m_fInterval;

public:
	CTimer(double fInterval = 0.0, bool bFirstTimeOut = false) {
		Set(fInterval, bFirstTimeOut);
	}

	void Set(double fInterval, bool bFirstTimeOut = false);
	void Reset() { Set(m_fInterval); }

	[[nodiscard]] bool IsTimeOut() const { return GetTime() >= m_endTime; }
	[[nodiscard]] double GetLeftTime() const { return GetTime() - m_curTime; }

private:
	[[nodiscard]] static double GetTime();
};

class CGameTimer
{
	double m_dCurrentTime;
	double m_dBeginTime;
public:
	BOOL m_bFixed;
	int m_nSec;
	int m_nMin;
	int m_nHour;
	int m_nDay;

	CGameTimer();
	CGameTimer( double dCurrentime );
	~CGameTimer();

	void SetCurrentTime( int nDay, int nHour );
	void SetCurrentTime( double dCurrentime );
	double GetCurrentTime();

	void SetTime( int nDay, int nHour, int nMin, int nSec );
	void SetFixed( BOOL bFixed ) { m_bFixed = bFixed; }
	BOOL IsFixed( ) { return m_bFixed; }

	int GetSunPercent(); // 해가 뜨는 상황을 퍼센트로. 100에 가까우면 만땅, 0에 가까우면 나타나는 중임
	int GetMoonPercent(); // 달 혹은 별이 뜨는 상황을 퍼센트로. 100에 가까우면 만땅, 0에 가까우면 나타나는 중임

	void Compute();
};


#define		FRAME_PER_SEC		60
#define		SEC1				PROCESS_COUNT

#ifdef __WORLDSERVER
#define		PROCESS_COUNT		(FRAME_PER_SEC / 4)
#else
#define		PROCESS_COUNT		FRAME_PER_SEC
#endif

BOOL	GetFrameSkip( void );
void	SetFrameSkip( BOOL bFlag );
void	InitFST( void );
BOOL	IsDrawTiming( void );
void	SetFST( void );
