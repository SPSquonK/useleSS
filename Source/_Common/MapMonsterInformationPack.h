#pragma once

#ifdef __CLIENT

class CMapMonsterInformation {
public:
	void InsertMonsterID(DWORD dwMonsterID) {
		m_vecMonsterID.push_back(dwMonsterID);
	}

	[[nodiscard]] std::span<const DWORD> GetMonsterIDs() const {
		return m_vecMonsterID;
	}

public:
	DWORD dwDropItemID = 0;
	CRect rectIconPosition = CRect(0, 0, 0, 0);

private:
	std::vector<DWORD> m_vecMonsterID;
};

class CMapMonsterInformationPack : public CTexturePack {
public:
	BOOL LoadScript( const CString& strFileName );

public:
	[[nodiscard]] const CMapMonsterInformation * GetMapMonsterInformation(int nIndex) const;

private:
	std::vector<CMapMonsterInformation> m_MapMonsterInformationVector;
};

#endif // __CLIENT
