#ifndef __MAP_INFORMATION_MANAGER_H__
#define __MAP_INFORMATION_MANAGER_H__

#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
#include "MapComboBoxData.h"
#include "MapMonsterInformationPack.h"
#include "RainbowNPCInformationPack.h"

struct CUserMarkPositionInfo {
	DWORD dwID = 0;
	CString strName;
	FLOAT fPositionX = 0.0f;
	FLOAT fPositionY = 0.0f;
};

class CMapInformationManager
{
public:
	static constexpr size_t USER_MARK_MAX_SIZE = 20;

	CMapInformationManager( void );
	~CMapInformationManager( void );

public:
	BOOL LoadMapInformationData( void );
	void DeleteAllMapInformation( void );

public:
	CTexture* const GetPCArrowTexture( void );
	CTexture* const GetPartyPCArrowTexture( void );
	CTexture* const GetTeleportationPositionTexture( void );
	CTexture* const GetDestinationPositionTexture( void );
	CTexture* const GetNPCPositionTexture( void );
	CTexture* const GetUserMarkPositionTexture( void );
	std::span<const CMapComboBoxData> GetMapCategoryVector() { return m_MapCategoryVector; }
	std::span<const CMapComboBoxData> GetMapNameVector() { return m_MapNameVector; }
	std::span<const CMapComboBoxData> GetNPCNameVector() { return m_NPCNameVector; }
	CMapMonsterInformationPack* FindMapMonsterInformationPack( DWORD dwFirstKey );
	[[nodiscard]] CRect FindRealPosition( BYTE byFirstKey ) const;
	CRainbowNPCInformationPack* GetRainbowNPCInformationPack( void );
	BOOL InsertUserMarkPositionInfo( const CString& strName, FLOAT fPositionX, FLOAT fPositionY );
	void DeleteUserMarkPositionInfo( DWORD dwID );
	CUserMarkPositionInfo* const FindUserMarkPositionInfo( DWORD dwID );
	void DeleteAllUserMarkPositionInfo( void );
	std::span<const CUserMarkPositionInfo> GetUserMarkPositionInfoList( void );

private:
	BOOL LoadMapIconTexture( void );
	BOOL LoadPropMapComboBoxData( void );
	BOOL LoadMapMonsterInformationPack( void );
	BOOL LoadRainbowNPCInformationPack( void );

private:
	CTexture* m_pPCArrowTexture;
	CTexture* m_pPartyPCArrowTexture;
	CTexture* m_pTeleportationPositionTexture;
	CTexture* m_pDestinationPositionTexture;
	CTexture* m_pNPCPositionTexture;
	CTexture* m_pUserMarkPositionTexture;
	std::vector<CMapComboBoxData> m_MapCategoryVector;
	std::vector<CMapComboBoxData> m_MapNameVector;
	std::vector<CMapComboBoxData> m_NPCNameVector;
	std::map< DWORD, std::unique_ptr<CMapMonsterInformationPack> >  m_MapMonsterInformationPackMap;
	std::map< BYTE, CRect > m_RealPositionRectMap;
	CRainbowNPCInformationPack* m_pRainbowNPCInformationPack;

	DWORD m_nextUserMarkID;
	std::vector<CUserMarkPositionInfo> m_UserMarkPositionInfoList;
};
#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM

#endif // __MAP_INFORMATION_MANAGER_H__
