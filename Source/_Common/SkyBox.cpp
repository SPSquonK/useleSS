#include "stdafx.h"
#include "defineObj.h"
#include "sfx.h"

#ifdef __JEFF_11
#include "environment.h"
#endif	// __JEFF_11

#include "Vector3Helper.h"


CSkyBox::CSkyBox()
{
	m_fSunAngle=10.0f;
	m_pWorld=NULL;

  m_fCloud_u1 = 0.f;
  m_fCloud_v1 = 0.f;

  if (g_Option.m_nTextureQuality == 0) {
    m_nFall = 400;
  } else if (g_Option.m_nTextureQuality == 1) {
    m_nFall = 300;
  } else if (g_Option.m_nTextureQuality == 2) {
    m_nFall = 300;
  }

	for( DWORD i = 0; i < m_nFall ; i++ )  
	{
		m_vFall[ i ] = D3DXVECTOR3( 0, 0, 0 );
		m_vVelocity[ i ] = D3DXVECTOR3( 0, 0.3f, 0 );
	}

	m_nWeather = WEATHER::NONE;

	m_bLockWeather = FALSE;
}
CSkyBox::~CSkyBox()
{

}


CSkyBox* CSkyBox::GetInstance()
{
	static CSkyBox	sSkyBox;
	return & sSkyBox;
}

void CSkyBox::SetWeather( WEATHER nWeather, bool bOnOff )
{
	if( m_bLockWeather )		//gmpbigsun(100112) : 현재 대륙날씨쪽에서 락을 시키고 풀어줌 
		return;

	if( !bOnOff )
	{
		if( nWeather == m_nWeather ) {
			m_timerWeather.Set( 0 ); // 내리는 것을 그치게 한다.
			m_nWeather = WEATHER::NONE;
    }
	}
	else {
		m_timerWeather.Set( MIN( 1 ) ); // 5분간 비나 눈이 내린다.
    
    if (m_nWeather != nWeather) {
      m_nWeather = nWeather;
      InitFall();
    }
  }
}

void CSkyBox::InitFall()
{
	for( int i = 0; i < (int)( m_nFall ); i++ ) 
	{
		m_vFall[i].x = g_pPlayer->GetPos().x + xRandomF(-12.5f, +12.5f);
		m_vFall[i].z = g_pPlayer->GetPos().z + xRandomF(-12.5f, +12.5f);
		m_vFall[i].y = g_pPlayer->GetPos().y + xRandomF( 10.0f,  20.0f);

		if( m_nWeather == WEATHER::RAIN )
		{
			int x = 0;
			int y = (-1) * (int)( xRandom( 10 ) ) + (-7);
			int z = 0;
			m_vVelocity[ i ] = D3DXVECTOR3( float( x ) / 800, float( y ) / 200 - 0.05f, float( z ) / 800  );
		}
		else
		{
			int x = xRandom( 50 ) - 25;
			int y = (-1) * (int)( xRandom( 10 ) );
			int z = xRandom( 50 ) - 25;
			m_vVelocity[ i ] = D3DXVECTOR3( float( x ) / 800, float( y ) / 200 - 0.05f, float( z ) / 800  );
		}
	}
}
void CSkyBox::Process()
{
	if(!g_Option.m_nWeatherEffect) return;

	if( m_pWorld ) 
	{
#ifdef __JEFF_11
#ifdef __CLIENT
		if( m_pWorld->GetID() == WI_WORLD_KEBARAS )
		{

			if( CEnvironment::GetInstance()->GetSeason() != SEASON_WINTER )
			{
				CEnvironment::GetInstance()->SetSeason( SEASON_WINTER );

				CWorld::m_skyBox.SetWeather( WEATHER::SNOW, true );
			}

		}
#endif	// __CLIENT
#endif	// __JEFF_11

		if( m_nWeather == WEATHER::RAIN )
		{
			for( int i = 0; i < (int)( m_nFall ); i++ ) 
			{
				FLOAT fheight = m_pWorld->GetLandHeight_Fast( m_vFall[ i ].x, m_vFall[ i ].z ) - 1.0f;
				if( fheight == 0 ) fheight = 100000.0f;

					if( fheight > m_vFall[ i ].y ) 
					{
						if( xRandom(10) >= 7 )
						{
							D3DXVECTOR3 vec3Tri[3];
							m_pWorld->GetLandTri( m_vFall[ i ].x, m_vFall[ i ].z, vec3Tri );
							const FLOAT ffHeight = m_pWorld->GetLandHeight( m_vFall[ i ].x, m_vFall[ i ].z );
									
							const D3DXVECTOR3 vVector1 = vec3Tri[2] - vec3Tri[0];
							const D3DXVECTOR3 vVector2 = vec3Tri[1] - vec3Tri[0];
							D3DXVECTOR3 vNormal;
							D3DXVec3Cross( &vNormal, &vVector1, &vVector2);	
							D3DXVec3Normalize( &vNormal, &vNormal );
								
							const D3DXVECTOR3 v3Up = D3DXVECTOR3( 0.0f, -1.0f, 0.0f );
							D3DXVECTOR3 v3Cross;
							D3DXVec3Cross( &v3Cross, &v3Up, &vNormal );
							const FLOAT fDot = D3DXVec3Dot( &v3Up, &vNormal );
							const FLOAT fTheta = acos( fDot );
							
							D3DXQUATERNION qDirMap;
							D3DXQuaternionRotationAxis( &qDirMap, &v3Cross, fTheta );

              D3DXVECTOR3 v3Pos = m_vFall[ i ];
							v3Pos.y = ffHeight+0.1f;

							D3DXVECTOR3 vYPW;
							QuaternionRotationToYPW( qDirMap, vYPW );
							
							// 물이 있으면 물효과
							LPWATERHEIGHT pWaterHeight = m_pWorld->GetWaterHeight( m_vFall[ i ] );

							if( pWaterHeight && 
								( pWaterHeight->byWaterTexture & (byte)(~MASK_WATERFRAME) )	== WTYPE_WATER )
							{
								const FLOAT fHeight = (FLOAT)pWaterHeight->byWaterHeight;
								v3Pos.y = fHeight;
								
								FLOAT fscal = (FLOAT)( xRandom(2)+1 );
								fscal *= 0.1f;

                CSfx *pObj = CreateSfx( XI_GEN_RAINCIRCLE01, v3Pos );
								pObj->SetScale( D3DXVECTOR3( fscal, fscal, fscal ) ); 
							}
							else
							{
								FLOAT fscal = xRandom(2)+0.05f;
								fscal *= 0.1f;

								CSfx *pObj = CreateSfx( XI_GEN_RAINCIRCLE01, v3Pos );
								pObj->SetScale( D3DXVECTOR3( fscal, fscal, fscal ) ); 

								pObj->m_pSfxObj->m_vRotate.x = D3DXToDegree(vYPW.x);
								pObj->m_pSfxObj->m_vRotate.y = D3DXToDegree(vYPW.y);
								pObj->m_pSfxObj->m_vRotate.z = D3DXToDegree(vYPW.z);
								
							}
						}
						m_vFall[i].x = g_pPlayer->GetPos().x + xRandomF(-12.5f, +12.5f);
						m_vFall[i].z = g_pPlayer->GetPos().z + xRandomF(-12.5f, +12.5f);
						m_vFall[i].y = g_pPlayer->GetPos().y + xRandomF( 10.0f,  20.0f);
					}
					else 
					{
						m_vFall[ i ] += m_vVelocity[ i ] * 1.5;
					}
				

			}
		}
		else
		if( m_nWeather == WEATHER::SNOW )
		{
			for( int i = 0; i < (int)( m_nFall ); i++) 
			{
				FLOAT fheight = m_pWorld->GetLandHeight_Fast( m_vFall[ i ].x, m_vFall[ i ].z ) - 1.0f;
				if( fheight == 0 ) 
					fheight = 100000.0f;

					if( fheight > m_vFall[ i ].y ) 
					{
						m_vFall[i].x = g_pPlayer->GetPos().x+ xRandomF(-35.f, +35.f);
						m_vFall[i].z = g_pPlayer->GetPos().z+ xRandomF(-35.f, +35.f);
						m_vFall[i].y = g_pPlayer->GetPos().y+ xRandomF( 10.f, 30.f);
					}
					else 
						m_vFall[ i ] += m_vVelocity[ i ];

			}			
		}

	}
}
void CSkyBox::Render( CWorld* pWorld )
{
	_PROFILE("CSkyBox::Render()");

	m_pWorld=pWorld;

	D3DXVECTOR3 vecLookAt = pWorld->m_pCamera->m_vLookAt - pWorld->m_pCamera->GetPos();
	const float angle1 = D3DXR::Angle(vecLookAt);
	
	// 하늘 출력 
	D3DXMATRIX mat, matWorld;
	float fTemp=-angle1/360.0f;

	D3DXMatrixIdentity( &matWorld );
	pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
	vecLookAt.z=(float)(sqrt((vecLookAt.x*vecLookAt.x)+(vecLookAt.z*vecLookAt.z)));
	vecLookAt.x=0;
	D3DXMatrixRotationY( &mat, D3DXToRadian(angle1) );
	D3DXVec3TransformCoord(  &vecLookAt,  &vecLookAt,  &mat );
	vecLookAt.y+=pWorld->m_pCamera->GetPos().y;

  D3DXVECTOR3 vecPos(0, 0, 0);
	vecPos.y+=pWorld->m_pCamera->GetPos().y;
  D3DXVECTOR3 vecTempLookAt=vecLookAt;

	D3DXMATRIX matView = D3DXR::LookAtLH010(vecPos, vecLookAt);
	matView._41=0;matView._42=0;matView._43=0;
	pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

	pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

	pd3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
	pd3dDevice->SetRenderState( D3DRS_ZENABLE, FALSE );
	pd3dDevice->SetRenderState( D3DRS_ALPHAFUNC, D3DCMP_ALWAYS );
	pd3dDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );		
	pd3dDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );		
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	pd3dDevice->SetFVF( D3DFVF_D3DSKYBOXVERTEX );
	pd3dDevice->SetVertexShader( NULL );

	D3DXMATRIX matProj, matProj2;
	pd3dDevice->GetTransform( D3DTS_PROJECTION, &matProj2 );
	D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4.0f, 1.0f, 0.1f, 300.0f );
	pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
	pd3dDevice->GetTransform( D3DTS_VIEW, &matView );
	mat=matView;

	pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,   TRUE );
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_SELECTARG1);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2,   D3DTA_TFACTOR);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
	pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
	pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );

	pd3dDevice->SetTransform( D3DTS_VIEW, &mat);
	pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj2 );

	// 병풍 하늘 출력 
	pd3dDevice->SetStreamSource( 0, m_pSideSkyBoxVB, 0, sizeof( D3DSKYBOXVERTEX ) );
	pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB( 255, 0, 0, 0 ) );
	// 시간에 따라 하늘 텍스쳐를 자연스럽게 바꿔치기한다.

	float fAlphaEX = 1.0f;			//added // SqK: Yes it was added, it didn't magically appeared...

  //gmpbigsun: Hook은 리턴값으로 기존 루틴을 타거나 타지 않는다 
	// FALSE인 경우( 루틴을 타야 하는경우 ) 곱해질 알파가 계산되어져 나오게 댐.
	if( !m_pWorld->HookRenderSky( SKY_TYPE::SIDE, 348/3, fAlphaEX ) )
	{
		IDirect3DTexture9* pSky1 = m_pSkyBoxTexture;
		IDirect3DTexture9* pSky2 = m_pSkyBoxTexture2;
		IDirect3DTexture9* pSky3 = m_pSkyBoxTexture3;

		WORLD_ENVIR_INFO& kWorldInfo = m_pWorld->m_kWorldEnvir;
		if( kWorldInfo._kSky[ 0 ]._bUse )
		{
			// use sky texture of the world ( not default )
			pSky1 = kWorldInfo._kSky[ 0 ]._pTexture;
			pSky2 = kWorldInfo._kSky[ 1 ]._pTexture;
			pSky3 = kWorldInfo._kSky[ 2 ]._pTexture;
		}

		if( !pSky1 || !pSky2 || !pSky3 )
		{
			assert( 0 );
			return;
		}

    constexpr auto RenderUnique = [](IDirect3DTexture9 * pSky, float fAlphaEX) {
      const int rstA = (int)(fAlphaEX * 255);
      pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(rstA, 0, 0, 0));
      pd3dDevice->SetTexture(0, pSky);
      pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 348 / 3);
    };

    constexpr auto RenderLerp = [](IDirect3DTexture9 * pSkyA, IDirect3DTexture9 * pSkyB, float fAlphaEX, int current, int over) {
      int rstA = (int)((255 - (current * 255 / over)) * fAlphaEX);
      pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(rstA, 0, 0, 0));
      pd3dDevice->SetTexture(0, pSkyA);
      pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 348 / 3);

      rstA = (int)((current * 255 / over) * fAlphaEX);
      pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(rstA, 0, 0, 0));
      pd3dDevice->SetTexture(0, pSkyB);
      pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 348 / 3);
    };


    if (g_GameTimer.m_nHour < 6) {
      RenderUnique(pSky3, fAlphaEX);
    } else if (g_GameTimer.m_nHour == 6) {
      RenderLerp(pSky3, pSky1, fAlphaEX, g_GameTimer.m_nMin, 59);
    } else if (g_GameTimer.m_nHour < 17) {
      RenderUnique(pSky1, fAlphaEX);
    } else if (g_GameTimer.m_nHour == 17 || g_GameTimer.m_nHour == 18) {
      const int nMin = g_GameTimer.m_nHour == 17 ? g_GameTimer.m_nMin : g_GameTimer.m_nMin + 60;
      RenderLerp(pSky1, pSky2, fAlphaEX, nMin, 119);
    } else if (g_GameTimer.m_nHour == 19) {
      RenderLerp(pSky2, pSky3, fAlphaEX, g_GameTimer.m_nMin, 59);
    } else {
      RenderUnique(pSky3, fAlphaEX);
    }
	}

	// 높은 구름 UV 스크롤 
	D3DXMatrixIdentity( &mat );
	mat._31 = m_fCloud_u1;
	mat._32 = m_fCloud_v1;
	m_fCloud_u1 += 0.0002f;
	m_fCloud_v1 += 0.0002f;
	pd3dDevice->SetTransform( D3DTS_TEXTURE0, &mat );
	pd3dDevice->SetTextureStageState( 0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2 );

	pd3dDevice->SetStreamSource( 0, m_pTopSkyBoxVB, 0, sizeof( D3DSKYBOXVERTEX ) );

	pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB( 255, 0, 0, 0 ) );

	if( !m_pWorld->HookRenderSky( SKY_TYPE::CLOUD, 28, fAlphaEX ) )
	{
		IDirect3DTexture9* pCloud1 = m_pCloudTexture;
		IDirect3DTexture9* pCloud2 = m_pCloudTexture2;
		IDirect3DTexture9* pCloud3 = m_pCloudTexture3;
	
		WORLD_ENVIR_INFO& kWorldInfo = m_pWorld->m_kWorldEnvir;
		if( kWorldInfo._kCloud[ 0 ]._bUse )
		{
			// use cloud texture of the world( not default )
			pCloud1 = kWorldInfo._kCloud[ 0 ]._pTexture;
			pCloud2 = kWorldInfo._kCloud[ 1 ]._pTexture;
			pCloud3 = kWorldInfo._kCloud[ 2 ]._pTexture;
		}

		if( !pCloud1 || !pCloud2 || !pCloud3 )
		{
			assert( 0 );
			return;
		}

    constexpr auto RenderUnique = [](IDirect3DTexture9 * pCloud, float fAlphaEX) {
      const int rstA = (int)(fAlphaEX * 255);
      pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(rstA, 0, 0, 0));
      pd3dDevice->SetTexture(0, pCloud);
      pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 28);
    };

    constexpr auto RenderLerp = [](IDirect3DTexture9 * pCloudA, IDirect3DTexture9 * pCloudB, float fAlphaEX, int current, int over) {
      int rstA = (int)((255 - (current * 255 / over)) * fAlphaEX);
      pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(rstA, 0, 0, 0));
      pd3dDevice->SetTexture(0, pCloudA);
      pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 28);

      rstA = (int)((current * 255 / over) * fAlphaEX);
      pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(rstA, 0, 0, 0));
      pd3dDevice->SetTexture(0, pCloudB);
      pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 28);
    };

    if (g_GameTimer.m_nHour < 6) {
      RenderUnique(pCloud3, fAlphaEX);
    } else if (g_GameTimer.m_nHour == 6) {
      RenderLerp(pCloud3, pCloud1, fAlphaEX, g_GameTimer.m_nMin, 59);
    } else if (g_GameTimer.m_nHour < 17) {
      RenderUnique(pCloud1, fAlphaEX);
    } else if (g_GameTimer.m_nHour == 17 || g_GameTimer.m_nHour == 18) {
      const int nMin = g_GameTimer.m_nHour == 17 ? g_GameTimer.m_nMin : g_GameTimer.m_nMin + 60;
      RenderLerp(pCloud1, pCloud2, fAlphaEX, nMin, 119);
    } else if (g_GameTimer.m_nHour == 19) {
      RenderLerp(pCloud2, pCloud3, fAlphaEX, g_GameTimer.m_nMin, 59);
    } else {
      RenderUnique(pCloud3, fAlphaEX);
    }
	}
	
	//태양 & 달 
	pd3dDevice->SetRenderState( D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB( 255, 255, 255, 255 ) );

  pd3dDevice->SetTextureStageState( 0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE );

	m_fSunAngle = g_GameTimer.m_nHour*60+g_GameTimer.m_nMin-720.0f+((float)g_GameTimer.m_nSec/60.0f);
	m_fSunAngle = -(float)(m_fSunAngle*m_fSunAngle)/2500.0f+45.0f;

	IDirect3DTexture9* pSun1 = m_pSunTexture;
	IDirect3DTexture9* pMoon = m_pMoonTexture;

	WORLD_ENVIR_INFO& kWorldInfo = m_pWorld->m_kWorldEnvir;

	if( kWorldInfo._kSun._bUse )
		pSun1 = kWorldInfo._kSun._pTexture;

	if( kWorldInfo._kMoon._bUse )
		pMoon = kWorldInfo._kMoon._pTexture;

	if( !pSun1 || !pMoon )
	{
		assert ( 0 );
		return;
	}


	
		if( (g_GameTimer.m_nHour >= 20 ) || ( g_GameTimer.m_nHour >= 1 && g_GameTimer.m_nHour <=6 ) )
		{
			D3DSUNVERTEX* pVB;
			m_pSunVB->Lock( 0, 0, (void**)&pVB, 0 );
			
			pVB[0].p=D3DXVECTOR3( -2.0f, 2.0f,30.0f); pVB[0].tu1=0.0f; pVB[0].tv1=0.0f;
			pVB[1].p=D3DXVECTOR3(  2.0f, 2.0f,30.0f); pVB[1].tu1=1.0f; pVB[1].tv1=0.0f;
			pVB[2].p=D3DXVECTOR3(  2.0f,-2.0f,30.0f); pVB[2].tu1=1.0f; pVB[2].tv1=1.0f;
			pVB[3].p=D3DXVECTOR3( -2.0f,-2.0f,30.0f); pVB[3].tu1=0.0f; pVB[3].tv1=1.0f;

			int nS = g_GameTimer.m_nHour;
			if( ( g_GameTimer.m_nHour >= 1 && g_GameTimer.m_nHour <=6 ) )
				nS = 24 + g_GameTimer.m_nHour;

			CTimeSpan cp( 0, nS, g_GameTimer.m_nMin, g_GameTimer.m_nSec );

			LONG nSec = (LONG)( cp.GetTotalSeconds() );

			m_fSunAngle = (nSec*0.00015f)+-90;
			D3DXMatrixRotationX( &mat,m_fSunAngle);

			D3DXVec3TransformCoord(&pVB[0].p,&pVB[0].p,&mat);
			D3DXVec3TransformCoord(&pVB[1].p,&pVB[1].p,&mat);
			D3DXVec3TransformCoord(&pVB[2].p,&pVB[2].p,&mat);
			D3DXVec3TransformCoord(&pVB[3].p,&pVB[3].p,&mat);

			pVB[0].c=pVB[1].c=pVB[2].c=pVB[3].c=D3DCOLOR_ARGB(255,255,255,255);
			
			m_pSunVB->Unlock();
			
			pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
			pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			
			pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
			pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
			
			pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
			pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
			pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
			pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );
			
			pd3dDevice->SetStreamSource( 0, m_pSunVB, 0, sizeof( D3DSUNVERTEX ) );
			pd3dDevice->SetTexture( 0, pMoon);
			pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);		
		}
		else
		if( (g_GameTimer.m_nHour >= 7 ) && ( g_GameTimer.m_nHour <= 17 ) )
		{
			D3DSUNVERTEX* pVB;
			m_pSunVB->Lock( 0, 0, (void**)&pVB, 0 );
			
			pVB[0].p=D3DXVECTOR3( -1.0f, 1.0f,30.0f); pVB[0].tu1=0.0f; pVB[0].tv1=0.0f;
			pVB[1].p=D3DXVECTOR3(  1.0f, 1.0f,30.0f); pVB[1].tu1=1.0f; pVB[1].tv1=0.0f;
			pVB[2].p=D3DXVECTOR3(  1.0f,-1.0f,30.0f); pVB[2].tu1=1.0f; pVB[2].tv1=1.0f;
			pVB[3].p=D3DXVECTOR3( -1.0f,-1.0f,30.0f); pVB[3].tu1=0.0f; pVB[3].tv1=1.0f;

			int nS = g_GameTimer.m_nHour;

			CTimeSpan cp( 0, nS, g_GameTimer.m_nMin, g_GameTimer.m_nSec );

			LONG nSec = (LONG)( cp.GetTotalSeconds() );

			m_fSunAngle = (nSec*0.00015f)+-90;
			D3DXMatrixRotationX( &mat,m_fSunAngle);

			D3DXVec3TransformCoord(&pVB[0].p,&pVB[0].p,&mat);
			D3DXVec3TransformCoord(&pVB[1].p,&pVB[1].p,&mat);
			D3DXVec3TransformCoord(&pVB[2].p,&pVB[2].p,&mat);
			D3DXVec3TransformCoord(&pVB[3].p,&pVB[3].p,&mat);

			pVB[0].c=pVB[1].c=pVB[2].c=pVB[3].c=D3DCOLOR_ARGB(255,255,255,255);
			
			m_pSunVB->Unlock();
			
			
			pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,   TRUE );
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
			pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
			pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
			pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ONE );
			pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );
		
			pd3dDevice->SetStreamSource( 0, m_pSunVB, 0, sizeof( D3DSUNVERTEX ) );
			pd3dDevice->SetTexture( 0, pSun1);
			pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);	
			
			//////////////////////////////////////////////////////////////////////////
			// 태양 출력 2
			//////////////////////////////////////////////////////////////////////////
			
			pd3dDevice->GetTransform( D3DTS_VIEW, &matView );
			matView._41=0.0f;
			matView._42=0.0f;
			matView._43=0.0f;
			pd3dDevice->SetTransform( D3DTS_VIEW, &matView );
			
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
			pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
			pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
			pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ONE );
			pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
			pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );
			
			pd3dDevice->SetStreamSource( 0, m_pStarVB, 0, sizeof( D3DSUNVERTEX ) );
			
			m_pLensFlareVB[0]->Lock( 0, 0, (void**)&pVB, 0 );
			pVB[0].p=D3DXVECTOR3( -5.0f, 5.0f,0.0f); pVB[0].tu1=0.0f; pVB[0].tv1=0.0f;
			pVB[1].p=D3DXVECTOR3(  5.0f, 5.0f,0.0f); pVB[1].tu1=1.0f; pVB[1].tv1=0.0f;
			pVB[2].p=D3DXVECTOR3(  5.0f,-5.0f,0.0f); pVB[2].tu1=1.0f; pVB[2].tv1=1.0f;
			pVB[3].p=D3DXVECTOR3( -5.0f,-5.0f,0.0f); pVB[3].tu1=0.0f; pVB[3].tv1=1.0f;
			pVB[0].c=pVB[1].c=pVB[2].c=pVB[3].c=D3DCOLOR_ARGB(255,255,255,255);
			m_pLensFlareVB[0]->Unlock();
			
			D3DXMatrixRotationX( &mat,m_fSunAngle);
			matWorld._43=30;
			D3DXMatrixMultiply(&matWorld,&matWorld,&mat);
			
			D3DXMATRIX  matView2,matView3,matWorld2;
			matView3=matWorld;
			
			pd3dDevice->GetTransform( D3DTS_VIEW, &matView2 );
			pd3dDevice->GetTransform( D3DTS_WORLD, &matWorld2 );
			D3DXMatrixInverse(&matView2,&fTemp,&matView2);
			matWorld=matView2;
			matWorld._41=matView3._41;matWorld._42=matView3._42;matWorld._43=matView3._43;
			
			pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
			
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,   D3DTA_TEXTURE);
			pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2,   D3DTA_DIFFUSE);
			pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
			pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
			pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );
			pd3dDevice->SetStreamSource( 0, m_pLensFlareVB[0], 0, sizeof( D3DSUNVERTEX ) );
			pd3dDevice->SetTexture( 0, m_pSunTexture2);
			pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
			pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld2 );
		}


	
	//////////////////////////////////////////////////////////////////////////
	// 상태 초기화 
	//////////////////////////////////////////////////////////////////////////
		
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);

	pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
	pd3dDevice->SetRenderState( D3DRS_ZENABLE, TRUE );
}


BOOL CSkyBox::CheckSun()
{
	D3DXMATRIX mat,matProj;
	pd3dDevice->GetTransform( D3DTS_PROJECTION, &matProj );
	D3DXVECTOR3 vSun( 0.0f,0.0f,300.0f);
	D3DXVECTOR3 vPos(m_pWorld->m_pCamera->m_vPos);

	D3DXMatrixRotationX( &mat,m_fSunAngle);
	D3DXVec3TransformCoord(&vSun,&vSun,&mat);
	vSun+=vPos;
	D3DXMATRIX matView = D3DXR::LookAtLH010(vPos, vSun);

	const CRect rect(0,0,800,600);
	const CPoint ptCursor(rect.CenterPoint());

	if( m_pWorld->IsPickTerrain( rect, ptCursor, &matProj, &matView ) )
		return FALSE;
	if( m_pWorld->PickObject_Fast( rect, ptCursor, &matProj, &matView, 0xffffffff, NULL, TRUE ) )
		return FALSE;

	return TRUE;
}

void CSkyBox::DrawLensFlare()
{
	if( !((g_GameTimer.m_nHour >= 7 ) && ( g_GameTimer.m_nHour <= 17 )) )
		return;

	if( g_GameTimer.m_nHour < 12 )
	{
		if(m_fSunAngle<-84.6f) 
			return;
	}
	else
	{
		if(m_fSunAngle>-81.66f) 
			return;
	}

	
	
	if(!CheckSun()) return;
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,   TRUE );
	pd3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
	pd3dDevice->SetRenderState( D3DRS_ZENABLE, FALSE );

	D3DSUNVERTEX* pVB;
	D3DXVECTOR3 vecPos( 0, 0, 0 );
	D3DXVECTOR3 vecLookAt = m_pWorld->m_pCamera->m_vLookAt;
	D3DXVECTOR3 vecTempLookAt;
	vecLookAt -= m_pWorld->m_pCamera->GetPos();
	
	const float angle1 = D3DXR::Angle(vecLookAt);
	// 하늘 출력 
	D3DXMATRIX mat, matWorld;
	float fTemp=-angle1/360.0f;

	D3DXMatrixIdentity( &matWorld );
	pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
	vecLookAt.z=(float)(sqrt((vecLookAt.x*vecLookAt.x)+(vecLookAt.z*vecLookAt.z)));
	vecLookAt.x=0;
	D3DXMatrixRotationY( &mat, D3DXToRadian(angle1) );
	D3DXVec3TransformCoord(  &vecLookAt,  &vecLookAt,  &mat );
	vecLookAt.y+=m_pWorld->m_pCamera->GetPos().y;
	vecPos.y+=m_pWorld->m_pCamera->GetPos().y;
	vecTempLookAt=vecLookAt;

	D3DXMATRIX matView = D3DXR::LookAtLH010(vecPos, vecLookAt);
	matView._41=0;matView._42=0;matView._43=0;
	pd3dDevice->SetTransform( D3DTS_VIEW, &matView );


	pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

	D3DXMatrixRotationX( &mat,m_fSunAngle);
	matWorld._43=30;
	D3DXMatrixMultiply(&matWorld,&matWorld,&mat);

	D3DXMATRIX  matView2,matView3;
	matView3=matWorld;

	pd3dDevice->GetTransform( D3DTS_VIEW, &matView2 );
	D3DXMatrixInverse(&matView2,&fTemp,&matView2);
	matWorld=matView2;
	matWorld._41=matView3._41;matWorld._42=matView3._42;matWorld._43=matView3._43;

	pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );

	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,   D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
	pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
	pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );

	vecTempLookAt.y-=m_pWorld->m_pCamera->GetPos().y;
	D3DXVec3Normalize(&vecTempLookAt,&vecTempLookAt);
	vecTempLookAt*=30.0f;
	D3DXVECTOR3 vecSunPos=D3DXVECTOR3(matWorld._41,matWorld._42,matWorld._43);
	D3DXVECTOR3 vecDelta=vecSunPos-vecTempLookAt;
	vecTempLookAt=vecSunPos;
	float fLength=D3DXVec3Length(&vecDelta);
	int nAlpha=0;
	if(fLength<20) nAlpha=255-(int)(fLength*255/20.0f);

	float afRadius[ 8 ] = { 25.0f, 2.0f, 2.0f, 1.0f, 1.0f, 1.0f, 3.0f, 4.0f };
	DWORD adwColor[ 8 ] = 
	{
		D3DCOLOR_ARGB(nAlpha,255,240,220),
		D3DCOLOR_ARGB(nAlpha,255,235,200),
		D3DCOLOR_ARGB(nAlpha,255,235,200),
		D3DCOLOR_ARGB(nAlpha,255,235,200),
		D3DCOLOR_ARGB(nAlpha,50,50,100),
		D3DCOLOR_ARGB(nAlpha,100,0,0),
		D3DCOLOR_ARGB(nAlpha,200,255,200),
		D3DCOLOR_ARGB(nAlpha,255,255,255)
	};
	for( int i = 0; i < 8; i++ )
	{
		m_pLensFlareVB[ i ]->Lock( 0, 0, (void**)&pVB, 0 );
		const float radius = afRadius[ i];
		pVB[ 0 ].p=D3DXVECTOR3( -radius, radius,0.0f); pVB[0].tu1=0.0f; pVB[0].tv1=0.0f;
		pVB[ 1 ].p=D3DXVECTOR3(  radius, radius,0.0f); pVB[1].tu1=1.0f; pVB[1].tv1=0.0f;
		pVB[ 2 ].p=D3DXVECTOR3(  radius,-radius,0.0f); pVB[2].tu1=1.0f; pVB[2].tv1=1.0f;
		pVB[ 3 ].p=D3DXVECTOR3( -radius,-radius,0.0f); pVB[3].tu1=0.0f; pVB[3].tv1=1.0f;
		pVB[ 0 ].c = pVB[ 1 ].c = pVB[ 2 ].c = pVB[ 3 ].c = adwColor[ i ];
		m_pLensFlareVB[ i ]->Unlock();
	}
	for( int i = 0; i < 7; i++ )
	{
		pd3dDevice->SetStreamSource(0,m_pLensFlareVB[ i + 1 ],0,sizeof(D3DSUNVERTEX));
		pd3dDevice->SetTexture( 0, m_pLensTexture[ 6 - i ]);
		vecTempLookAt-=vecSunPos/10;	vecTempLookAt-=vecDelta/10;
		matWorld._41=vecTempLookAt.x;matWorld._42=vecTempLookAt.y;matWorld._43=vecTempLookAt.z;
		pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
		pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
	}

	D3DXMatrixIdentity( &matWorld );
	pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
	
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
	pd3dDevice->SetRenderState( D3DRS_ZENABLE, TRUE );
}

void CSkyBox::RenderFall() {
	if (!g_Option.m_nWeatherEffect) return;

	if (m_nWeather == WEATHER::RAIN) {
		DrawRain();
	} else if (m_nWeather == WEATHER::SNOW) {
		DrawSnow();
	}
}

void CSkyBox::DrawRain()
{
	pd3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,   TRUE );

	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_SELECTARG2);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
	pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
	pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
	pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );
	pd3dDevice->SetStreamSource(0,m_pRainVB,0,sizeof(D3DSUNVERTEX));
	pd3dDevice->SetTexture( 0, NULL);

	D3DXMATRIX matTemp,matWorld,matView;
	pd3dDevice->GetTransform( D3DTS_WORLD, &matTemp );
	D3DXMatrixIdentity(&matWorld);
	
	if( m_pWorld ) 
	{
		D3DXVECTOR3 vPos    = m_pWorld->m_pCamera->m_vPos;
		D3DXVECTOR3 vLookAt = m_pWorld->m_pCamera->m_vLookAt;
		vPos.y = 0;
		vLookAt.y = 0;

		D3DXMATRIX matWorld = D3DXR::LookAtLH010(vPos, vLookAt);

    D3DXMatrixInverse(&matWorld, nullptr, &matWorld);
		for(int i=0;i<(int)( m_nFall );i++) 
		{
			matWorld._41 = m_vFall[i].x; 
			matWorld._42 = m_vFall[i].y; 
			matWorld._43 = m_vFall[i].z;
			pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
			pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
		}

	}
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
}

void CSkyBox::DrawSnow()
{
	pd3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,   TRUE );
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1,   D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
	pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,   D3DTA_DIFFUSE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE);
	pd3dDevice->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);
	pd3dDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
	pd3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
	pd3dDevice->SetTexture( 0, m_pSunTexture);
	pd3dDevice->SetFVF( D3DFVF_D3DSUNVERTEX );
	pd3dDevice->SetStreamSource(0,m_pSnowVB,0,sizeof(D3DSUNVERTEX));

	D3DXMATRIX  matTemp,matWorld,matView;
	
	pd3dDevice->GetTransform( D3DTS_WORLD, &matTemp );
	pd3dDevice->GetTransform( D3DTS_VIEW, &matView );
	D3DXMatrixInverse(&matView,nullptr,&matView);
	matWorld=matView;

	if( ::GetLanguage() == LANG_JAP )
	{
		D3DXMATRIX matScal;
		D3DXMatrixScaling( &matScal, 2.5f, 2.5f, 2.5f );
		matWorld *= matScal;
	}

	for(int i=0;i<(int)( m_nFall );i++) 
	{
		matWorld._41=m_vFall[i].x; matWorld._42=m_vFall[i].y; matWorld._43=m_vFall[i].z;
		pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );
		pd3dDevice->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, 2);
	}
	pd3dDevice->SetTransform( D3DTS_WORLD, &matTemp );
	pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
}

void CSkyBox::SetVertices()
{
	float tiled=1.0f;
	float radius=110.0f;
	float x,z;
	if( m_pTopSkyBoxVB ) 
	{
		D3DSKYBOXVERTEX* pVB;
		m_pTopSkyBoxVB->Lock( 0, 0, (void**)&pVB, 0 );

		pVB[0].p=D3DXVECTOR3(0.0f,100.0f,0.0f);
		pVB[0].tu1=tiled;
		pVB[0].tv1=tiled;
		pVB[0].c=D3DCOLOR_ARGB(255,255,255,255);
		for(int i = 1; i < 30; i++ ) 
		{
			x=(float)sin( 2 * D3DX_PI / 28 * i ) * radius;
			z=(float)cos( 2 * D3DX_PI / 28 * i ) * radius;
			pVB[ i ].p   = D3DXVECTOR3( x, 40.0f, z );
			pVB[ i ].tu1 = ( x + radius ) * tiled / radius;
			pVB[ i ].tv1 = ( z + radius ) * tiled / radius;
			pVB[ i ].c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
		}
		m_pTopSkyBoxVB->Unlock();
	}
	if( m_pSideSkyBoxVB ) 
	{
		D3DSKYBOXVERTEX* pVB;
		m_pSideSkyBoxVB->Lock( 0, 0, (void**)&pVB, 0 );
		for( int i = 0; i < 28; i++ ) 
		{
			FLOAT x1 = (float)sin( 2 * D3DX_PI / 28 * i ) * radius;
			FLOAT z1 = (float)cos( 2 * D3DX_PI / 28 * i ) * radius;
			FLOAT x2 = (float)sin( 2 * D3DX_PI / 28 * ( i + 1 ) ) * radius;
			FLOAT z2 = (float)cos( 2 * D3DX_PI / 28 * ( i + 1 ) ) * radius;
			FLOAT fu1 = 1.0f / 28.0f * i;
			FLOAT fu2 = 1.0f / 28.0f * ( i + 1 );

			pVB->p   = D3DXVECTOR3( x1, 150.0f, z1 );
			pVB->tu1 = fu1;
			pVB->tv1 = 0.0f;
			pVB->c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
			pVB++;

			pVB->p   = D3DXVECTOR3( x1, 0.0f, z1 );
			pVB->tu1 = fu1;
			pVB->tv1 = 0.9f;
			pVB->c   = D3DCOLOR_ARGB( 255, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x2, 150.0f, z2 );
			pVB->tu1 = fu2;
			pVB->tv1 = 0.0f;
			pVB->c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
			pVB++;

			pVB->p   = D3DXVECTOR3( x2, 150.0f, z2 );
			pVB->tu1 = fu2;
			pVB->tv1 = 0.0f;
			pVB->c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x1, 0.0f, z1 );
			pVB->tu1 = fu1;
			pVB->tv1 = 0.9f;
			pVB->c   = D3DCOLOR_ARGB( 255, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x2, 0.0f, z2 );
			pVB->tu1 = fu2;
			pVB->tv1 = 0.9f;
			pVB->c   = D3DCOLOR_ARGB( 255, 255, 255, 255 );
			pVB++;
						
			//////////////////

			pVB->p   = D3DXVECTOR3( x1, 0.0f, z1 );
			pVB->tu1 = fu1;
			pVB->tv1 = 0.9f;
			pVB->c   = D3DCOLOR_ARGB( 255, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x1, -20.0f, z1 );
			pVB->tu1 = fu1;
			pVB->tv1 = 1.0f;
			pVB->c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x2, 0.0f, z2 );
			pVB->tu1 = fu2;
			pVB->tv1 = 0.9f;
			pVB->c   = D3DCOLOR_ARGB( 255, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x2, 0.0f, z2 );
			pVB->tu1 = fu2;
			pVB->tv1 = 0.9f;
			pVB->c   = D3DCOLOR_ARGB( 255, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x1, -20.0f, z1 );
			pVB->tu1 = fu1;
			pVB->tv1 = 1.0f;
			pVB->c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
			pVB++;
			
			pVB->p   = D3DXVECTOR3( x2, -20.0f, z2 );
			pVB->tu1 = fu2;
			pVB->tv1 = 1.0f;
			pVB->c   = D3DCOLOR_ARGB( 0, 255, 255, 255 );
			pVB++;

			
		}

		m_pSideSkyBoxVB->Unlock();
	}
}

HRESULT CSkyBox::InitDeviceObjects()
{
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "Moon.dds"),
										&m_pMoonTexture ) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}


	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "sundisk.bmp"), &m_pSunTexture ) ) )
	{
		return	D3DAPPERR_MEDIANOTFOUND;
	}

	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "flare_3.tga"),
									   &m_pSunTexture2 ) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_001.tga"),
									   &(m_pLensTexture[0])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_002.tga"),
									   &(m_pLensTexture[1])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_003.tga"),
									   &(m_pLensTexture[2])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_004.tga"),
									   &(m_pLensTexture[3])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_005.tga"),
									   &(m_pLensTexture[4])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_006.tga"),
									   &(m_pLensTexture[5])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_007.tga"),
									   &(m_pLensTexture[6])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}
	if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "lenzflare_008.tga"),
									   &(m_pLensTexture[7])) ) )
	{
		return D3DAPPERR_MEDIANOTFOUND;
	}

	if( g_Option.m_nTextureQuality == 2 )
	{
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox01_low.dds" ), &m_pSkyBoxTexture ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox02_low.dds"), &m_pSkyBoxTexture2 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox03_low.dds"),  &m_pSkyBoxTexture3 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud01_low.dds"), &m_pCloudTexture ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud02_low.dds"), &m_pCloudTexture2 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud03_low.dds"), &m_pCloudTexture3 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
	}
	else
	if( g_Option.m_nTextureQuality == 1 )
	{
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox01_low.dds" ), &m_pSkyBoxTexture ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox02_low.dds"), &m_pSkyBoxTexture2 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox03.dds"),  &m_pSkyBoxTexture3 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud01_low.dds"), &m_pCloudTexture ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud02_low.dds"), &m_pCloudTexture2 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud03.dds"), &m_pCloudTexture3 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
	}
	else
	{
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox01.dds" ), &m_pSkyBoxTexture ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox02.dds"), &m_pSkyBoxTexture2 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "skybox03.dds"),  &m_pSkyBoxTexture3 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud01.dds"), &m_pCloudTexture ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud02.dds"), &m_pCloudTexture2 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
		if( FAILED( LoadTextureFromRes( MakePath( DIR_WEATHER, "cloud03.dds"), &m_pCloudTexture3 ) ) )
		{ return D3DAPPERR_MEDIANOTFOUND; }
	}
	return S_OK;
}
HRESULT CSkyBox::RestoreDeviceObjects()
{
	if( m_pTopSkyBoxVB ) return S_OK;

	pd3dDevice->CreateVertexBuffer( 30 *sizeof(D3DSKYBOXVERTEX),
									D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
									&m_pTopSkyBoxVB, NULL);
	pd3dDevice->CreateVertexBuffer( 348 *sizeof(D3DSKYBOXVERTEX),
									D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
									&m_pSideSkyBoxVB, NULL);
	pd3dDevice->CreateVertexBuffer( 4 *sizeof(D3DSUNVERTEX),
									D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
									&m_pSunVB, NULL);

	for(int i=0;i<8;i++)
		pd3dDevice->CreateVertexBuffer( 4 *sizeof(D3DSUNVERTEX),
										D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
										&m_pLensFlareVB[i],NULL);
	pd3dDevice->CreateVertexBuffer( 4*sizeof(D3DSUNVERTEX),
									D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
									&m_pRainVB, NULL);
	pd3dDevice->CreateVertexBuffer( 4 *sizeof(D3DSUNVERTEX),
									D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
									&m_pSnowVB, NULL);
	pd3dDevice->CreateVertexBuffer( 4 *sizeof(D3DSUNVERTEX),
									D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM ,
									&m_pStarVB, NULL);
	D3DSUNVERTEX* pRainVB;
	m_pRainVB->Lock( 0, 0, (void**)&pRainVB, 0 );
	pRainVB[0].p=D3DXVECTOR3( 0.0f, 2.0f, 0.0f); pRainVB[0].tu1=0.0f; pRainVB[0].tv1=0.0f;
	pRainVB[1].p=D3DXVECTOR3( 0.01f,2.0f, 0.0f); pRainVB[1].tu1=1.0f; pRainVB[1].tv1=0.0f;
	pRainVB[2].p=D3DXVECTOR3( 0.01f,0.0f, 0.0f); pRainVB[2].tu1=0.0f; pRainVB[2].tv1=1.0f;
	pRainVB[3].p=D3DXVECTOR3( 0.0f, 0.0f, 0.0f); pRainVB[3].tu1=1.0f; pRainVB[3].tv1=1.0f;
	pRainVB[0].c=pRainVB[1].c=D3DCOLOR_ARGB(0,128,128,128);
	pRainVB[2].c=pRainVB[3].c=D3DCOLOR_ARGB(100,128,128,128);
	m_pRainVB->Unlock();

	D3DSUNVERTEX* pSnowVB;
	m_pSnowVB->Lock( 0, 0, (void**)&pSnowVB, 0 );
	pSnowVB[0].p=D3DXVECTOR3( -0.05f,  0.05f, 0.0f); pSnowVB[0].tu1=0.0f; pSnowVB[0].tv1=0.0f;
	pSnowVB[1].p=D3DXVECTOR3(  0.05f,  0.05f, 0.0f); pSnowVB[1].tu1=1.0f; pSnowVB[1].tv1=0.0f;
	pSnowVB[2].p=D3DXVECTOR3(  0.05f, -0.05f, 0.0f); pSnowVB[2].tu1=1.0f; pSnowVB[2].tv1=1.0f;
	pSnowVB[3].p=D3DXVECTOR3( -0.05f, -0.05f, 0.0f); pSnowVB[3].tu1=0.0f; pSnowVB[3].tv1=1.0f;
	pSnowVB[0].c=pSnowVB[1].c=D3DCOLOR_ARGB(200,200,200,200);
	pSnowVB[2].c=pSnowVB[3].c=D3DCOLOR_ARGB(200,200,200,200);
	m_pSnowVB->Unlock();

	D3DSUNVERTEX* pStarVB;
	m_pStarVB->Lock( 0, 0, (void**)&pStarVB, 0 );
	pStarVB[0].p=D3DXVECTOR3(-0.1f, 50.0f, -0.1f); pStarVB[0].tu1=0.0f; pStarVB[0].tv1=0.0f;
	pStarVB[1].p=D3DXVECTOR3( 0.1f, 50.0f, -0.1f); pStarVB[1].tu1=1.0f; pStarVB[1].tv1=0.0f;
	pStarVB[2].p=D3DXVECTOR3( 0.1f, 50.0f,  0.1f); pStarVB[2].tu1=1.0f; pStarVB[2].tv1=1.0f;
	pStarVB[3].p=D3DXVECTOR3(-0.1f, 50.0f,  0.1f); pStarVB[3].tu1=0.0f; pStarVB[3].tv1=1.0f;
	pStarVB[0].c=pStarVB[1].c=D3DCOLOR_ARGB(255,255,200,100);
	pStarVB[2].c=pStarVB[3].c=D3DCOLOR_ARGB(255,255,200,100);
	m_pStarVB->Unlock();

	SetVertices();
	HRESULT hr=S_OK;
	return hr;
}
HRESULT CSkyBox::InvalidateDeviceObjects()
{
	SAFE_RELEASE(m_pTopSkyBoxVB);
	SAFE_RELEASE(m_pSideSkyBoxVB);
	SAFE_RELEASE(m_pSunVB);
	for(int i=0;i<8;i++) SAFE_RELEASE(m_pLensFlareVB[i]);
	SAFE_RELEASE(m_pRainVB);
	SAFE_RELEASE(m_pSnowVB);
	SAFE_RELEASE(m_pStarVB);
	return S_OK;
}
HRESULT CSkyBox::DeleteDeviceObjects()
{
	SAFE_RELEASE(m_pMoonTexture);
	SAFE_RELEASE(m_pSkyBoxTexture);
	SAFE_RELEASE(m_pSkyBoxTexture2);
	SAFE_RELEASE(m_pSkyBoxTexture3);
	SAFE_RELEASE(m_pCloudTexture);
	SAFE_RELEASE(m_pCloudTexture2);
	SAFE_RELEASE(m_pCloudTexture3);
	SAFE_RELEASE(m_pSunTexture);
	SAFE_RELEASE(m_pSunTexture2);
	SAFE_RELEASE(m_pSnowTexture);
	for( int i=0;i<8;i++) SAFE_RELEASE(m_pLensTexture[i]);

	SAFE_RELEASE(m_pTopSkyBoxVB);
	SAFE_RELEASE(m_pSideSkyBoxVB);
	SAFE_RELEASE(m_pSunVB);
	for( int i=0;i<8;i++) SAFE_RELEASE(m_pLensFlareVB[i]);
	SAFE_RELEASE(m_pRainVB);
	SAFE_RELEASE(m_pSnowVB);
	SAFE_RELEASE(m_pStarVB);
	return S_OK;
}

IDirect3DTexture9 * CSkyBox::GetCurrSkyTexture(SKY_TYPE eType) {
	if (g_GameTimer.m_nHour <= 6 || g_GameTimer.m_nHour > 19) {
		return SKY_TYPE::SIDE == eType ? m_pSkyBoxTexture3 : m_pCloudTexture3;
	} else if (g_GameTimer.m_nHour < 17) {
		return SKY_TYPE::SIDE == eType ? m_pSkyBoxTexture : m_pCloudTexture;
	} else if (g_GameTimer.m_nHour >= 17 && g_GameTimer.m_nHour <= 19) {
		return SKY_TYPE::SIDE == eType ? m_pSkyBoxTexture2 : m_pCloudTexture2;
	}

	return m_pSkyBoxTexture3;
}
