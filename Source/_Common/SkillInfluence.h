#pragma once

#define	BUFF_ITEM	0
#define	BUFF_SKILL	1
#define	BUFF_PET	2
#define	BUFF_ITEM2	3
#define	BUFF_EQUIP	4	// 장착류 아이템에 특정 DST(DST_GIFTBOX)에 대해 버프 아이콘만 출력
#define	MAX_SKILLBUFF_COUNT	28
