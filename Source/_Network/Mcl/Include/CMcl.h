#pragma once

/////////////////////////////////////////////////////////////////////////
//
// FILE: CMcl.h
//
// Copyright (c) 1997 by Aaron Michael Cohen and Mike Woodring
//
/////////////////////////////////////////////////////////////////////////


#include <process.h>


class CMclCritSec final {
private:
	CRITICAL_SECTION m_CritSec;

public:
	// constructor creates a CRITICAL_SECTION inside
	// the C++ object...
	CMclCritSec() {
		::InitializeCriticalSection(&m_CritSec);
	}

	// destructor...
	~CMclCritSec() {
		::DeleteCriticalSection(&m_CritSec);
	}

	CMclCritSec(const CMclCritSec &) = delete;
	CMclCritSec operator=(const CMclCritSec &) = delete;

	// enter the critical section...
	void Enter() {
		::EnterCriticalSection(&m_CritSec);
	}

	// leave the critical section...
	void Leave() {
		::LeaveCriticalSection(&m_CritSec);
	}
};


class CMclAutoLock final {
private:
	CMclCritSec * m_pcCritSec;

public:
	// constructors...
	CMclAutoLock(CMclCritSec & rCMclCritSec) {
		m_pcCritSec = &rCMclCritSec;
		m_pcCritSec->Enter();
	}

	// destructor...
	~CMclAutoLock() {
		m_pcCritSec->Leave();
	}

	CMclAutoLock(const CMclAutoLock &) = delete;
	CMclAutoLock operator=(const CMclAutoLock &) = delete;
};

