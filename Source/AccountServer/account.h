#ifndef __ACCOUNT_H__
#define	__ACCOUNT_H__

#include "mempooler.h"
#include <map>
#include <string>

#define	MAX_CERTIFIER			32


class CAccount final {
public:
	enum class State { Initial, SelectCharacter, Joined };
	CAccount( const LoggedAccount & loggedAccount, DWORD dpid1, DWORD dpid2, DWORD dwPCBangClass );

	[[nodiscard]] bool IsAdult() const noexcept;

public:
	DWORD			m_dpid1;
	DWORD			m_dpid2;
	DWORD			m_dwIdofServer;
	BYTE			m_cbAccountFlag;
	SAccountName m_lpszAccount;
	BOOL			m_fRoute;
	DWORD			m_dwPing;
	DWORD			m_dwAuthKey;
	State			m_cbRef;
	u_long			m_uIdofMulti;
	DWORD     m_dwPCBangClass;
};

class CAccountMng
{
private:
	std::map<std::string, CAccount*>	m_stringToPAccount;
	std::map<DWORD, int>			m_dpidToIndex;
	std::map<DWORD, CAccount*>	m_adpidToPAccount[MAX_CERTIFIER];
	int		m_nSizeof;
	int		m_nOldHour;
#ifndef __EUROPE_0514
	DWORD	m_dwSerial;
#endif	// __EUROPE_0514
public:
	int						m_nCount;
	std::map<DPID, DWORD>		m_2IdofServer;

	int		m_nYear;
	int		m_nMonth;
	int		m_nDay;
	int		m_nHour;
	int		m_nMinute;
public:
//	Constructions
	CAccountMng();
	virtual	~CAccountMng();
public:
//	Attributes
	int			GetIndex( DWORD dpid1 );
	int			GetIdofServer( DWORD dpid );
	CAccount*	GetAccount( LPCTSTR lpszAccount );
	CAccount*	GetAccount( DWORD dpid1, DWORD dpid2 );
	CAccount*	GetAccount( LPCTSTR lpszAccount, DWORD dwSerial );
#ifdef __LOG_PLAYERCOUNT_CHANNEL
	[[nodiscard]] const auto & GetMapAccount() const { return m_stringToPAccount; }
#endif // __LOG_PLAYERCOUNT_CHANNEL

//	Operations
	void	Clear( void );
	void	AddConnection( DWORD dpid );
	void	RemoveConnection( DWORD dpid );
	void	AddIdofServer( DWORD dpid, DWORD dwIdofServer );
#ifdef __SERVERLIST0911
	DWORD	RemoveIdofServer( DWORD dpid, BOOL bRemoveConnection = TRUE );
#else	// __SERVERLIST0911
	void	RemoveIdofServer( DWORD dpid, BOOL bRemoveConnection = TRUE );
#endif	// __SERVERLIST0911

	[[nodiscard]] bool AddAccount( LoggedAccount & loggedAccount, DWORD dpid1, DWORD dpid2, DWORD dwPCBangClass );
	void	RemoveAccount( LPCTSTR lpszAccount );	
	void	RemoveAccount( DWORD dpid1, DWORD dpid2 );
	void	PreventExcess();
	BOOL	IsTimeCheckAddr();

public:
	CMclCritSec		m_AddRemoveLock;
};

extern CAccountMng g_AccountMng;

#endif	// __ACCOUNT_H__