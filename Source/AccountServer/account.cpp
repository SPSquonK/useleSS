#include "stdafx.h"
#include "account.h"
#include "dpsrvr.h"
#include "dpdbsrvr.h"
#include "MsgHdr.h"
#include "dbmanager.h"

CAccountMng			g_AccountMng;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------

CAccount::CAccount( const LoggedAccount & loggedAccount, DWORD dpid1, DWORD dpid2, DWORD dwPCBangClass )
{
	_tcscpy( m_lpszAccount, loggedAccount.szAccount );
	m_cbAccountFlag = loggedAccount.cbAccountFlag;
	m_dwAuthKey = loggedAccount.dwAuthKey;

	m_dpid1				= dpid1;
	m_dpid2				= dpid2;

	m_fRoute			= FALSE;
	m_dwIdofServer		= 0;
	m_dwPing			= timeGetTime();
	m_uIdofMulti		= NULL_ID;
	m_cbRef				= State::Initial;
	m_dwPCBangClass = dwPCBangClass;
}

bool CAccount::IsAdult() const noexcept { return m_cbAccountFlag & ACCOUNT_FLAG_18; }

//----------------------------------------------------------------------------------------------------------------------------------------------------------------

CAccountMng::CAccountMng()
{
	m_nSizeof	= 0;
	m_nCount	= 0;
	m_nOldHour  = 0;
#ifndef __EUROPE_0514
	m_dwSerial	= 0;
#endif	// __EUROPE_0514

	CTime tTime = CTime::GetCurrentTime();
	m_nYear = tTime.GetYear() - 1;
	m_nMonth = tTime.GetMonth();
	m_nDay = tTime.GetDay();
	m_nHour = tTime.GetHour();
	m_nMinute = tTime.GetMinute();
}

CAccountMng::~CAccountMng()
{
	Clear();
}

void CAccountMng::Clear( void )
{
	CMclAutoLock	Lock( m_AddRemoveLock );
	
	for( auto i	= m_stringToPAccount.begin(); i != m_stringToPAccount.end(); ++i )
		safe_delete( i->second );
	m_stringToPAccount.clear();

	for( int j = 0; j < m_nSizeof; j++ )
		m_adpidToPAccount[j].clear();
	m_dpidToIndex.clear();
}

int CAccountMng::GetIndex( DWORD dpid1 )
{
// locked
	auto i		= m_dpidToIndex.find( dpid1 );
	if( i  != m_dpidToIndex.end() )
		return i->second;
	return( -1 );
}

int CAccountMng::GetIdofServer( DWORD dpid )
{
// locked
	auto i	= m_2IdofServer.find( dpid );
	if( i != m_2IdofServer.end() )
		return i->second;
	return( -1 );
}

bool CAccountMng::AddAccount( LoggedAccount & loggedAccount, DWORD dpid1, DWORD dpid2, DWORD dwPCBangClass )
{
	CMclAutoLock	Lock( m_AddRemoveLock );

	const auto i1	= m_stringToPAccount.find( loggedAccount.szAccount );
	if( i1 == m_stringToPAccount.end() )
	{
		const int nIndex = GetIndex(dpid1);
		if (nIndex < 0 || nIndex >= MAX_CERTIFIER) return false;

		auto i2 = m_adpidToPAccount[nIndex].find(dpid2);
		if (i2 != m_adpidToPAccount[nIndex].end()) return false;

		loggedAccount.dwAuthKey = xRandom(0x00000001UL, ULONG_MAX);
		CAccount* pAccount	= new CAccount( loggedAccount, dpid1, dpid2, dwPCBangClass);
		m_stringToPAccount.emplace(loggedAccount.szAccount, pAccount);
		m_adpidToPAccount[nIndex].emplace(dpid2, pAccount);
		m_nCount++;

		return true;
	} else {
		CAccount * pAccount = GetAccount(loggedAccount.szAccount);
		if (!pAccount) return false;
		if (pAccount->m_fRoute) return false;
		if (timeGetTime() - pAccount->m_dwPing <= MIN(2)) return false;

		g_dpSrvr.DestroyPlayer(pAccount->m_dpid1, pAccount->m_dpid2);
		RemoveAccount(pAccount->m_dpid1, pAccount->m_dpid2);
		return AddAccount(loggedAccount, dpid1, dpid2, dwPCBangClass);
	}
}

// 빌링서버에게 유저가 나갔음을 알리는 경우는 태국의 경우만 그러하다.
// 이는 빌링업체(페이레터)의 편의성을 봐주기 위해서이다.
// 게임서버가 능동적으로 게임 플레이 가능시간을 차감하지 않기 때문에, 
// 주기적 시간 차감을 함에도 불구하고, 로그아웃을  빌링서버에게 이를 알려줘야 하는 경우가 생겼다. 
//

// 예: 
// step1: 유저A가 로그인 ( 2분 플레이 시간이 남은 유저 )
// step2: 빌링서버에게 남은 시간을 물어봄 2분으로 응답 
// step3: 게임 플레이 
// step4: 게임 시간 만료, 끊어냄
// step5: 빌링서버에서 A유저가 플레이 하는 지 물어봄 
// step6: 플레이중이지 않음으로 응답 -> 빌링서버는 유저 A에 대해서 시간을 차감하지 않음 -> 플레이 시간은 그대로 2분 
// step7: 유저A가 로그인 -> 로그인 가능상태 

// 해결책A: 주기적으로 물어볼 때 남은 시간을 받아서 끊어냄. 
//		단점: 주기가 20분이라면 18분동안 무료 플레이를 할 수 있음  
// 해결책B: 게임서버가 시간을 차감하고, 빌링서버는 이를 받아서 저장만 한다.
//		단점: ?

// 페이레터는 해결책 A를 주로 사용했기에 이를 수용해서 로그아웃을 빌링서버에 알려줌. 

// 넵... 게임 시작에서 게임가능한 티켓이 없다고 요청이 갔는데 그다음에 바로 게임 종료 요청이 오거든요.
// 아.. 넵, 그때는 안보내 주실수는 없는지요? ^^; 별상관은 없지만 일단 빌링에서는 한번더 확인을 해야 하니까요.. 

void CAccountMng::RemoveAccount( LPCTSTR lpszAccount )
{
	CMclAutoLock	Lock( m_AddRemoveLock );

#ifdef __INTERNALSERVER
	time_t cur = time(NULL);
	WriteLog( "CAccountMng::RemoveAccount(%s) - %s\n", lpszAccount, ctime( &cur ));
#endif

	const auto i	= m_stringToPAccount.find( lpszAccount );
	if( i != m_stringToPAccount.end() )
	{
		CAccount* pAccount = i->second;

		if( pAccount->m_fRoute && pAccount->m_cbRef == CAccount::State::Joined )
		{
			const u_long uId	= pAccount->m_dwIdofServer * 100 + pAccount->m_uIdofMulti;

			g_dpSrvr.m_servers.write([&](CListedServers & servers) {
				if (CListedServers::Channel * channel = servers.GetFromUId(uId)) {
					--channel->lCount;
				}
				});
		}

		g_DbManager.UpdateTracking( FALSE, lpszAccount );	// 유저가 logoff했음을 디비에 쓴다.

		safe_delete( pAccount );
		m_stringToPAccount.erase( i );
		m_nCount--;
	}
}

void CAccountMng::RemoveAccount( DWORD dpid1, DWORD dpid2 )
{
	CAccount* pAccount;
	int nIndex;
	CMclAutoLock	Lock( m_AddRemoveLock );

	nIndex	= GetIndex( dpid1 );
	if( nIndex >= 0 && nIndex < m_nSizeof )
	{
		auto i	= m_adpidToPAccount[nIndex].find( dpid2 );
		if( i != m_adpidToPAccount[nIndex].end() )
		{
			pAccount	= i->second;
			m_adpidToPAccount[nIndex].erase( i );

			if( FALSE == pAccount->m_fRoute )
			{
				RemoveAccount( pAccount->m_lpszAccount );	// erase & delete
			}
		}
	}
}

CAccount* CAccountMng::GetAccount( LPCTSTR lpszAccount )
{
// locked
	const auto i	= m_stringToPAccount.find( lpszAccount );
	if( i != m_stringToPAccount.end() )
		return i->second;
	return NULL;
}

CAccount* CAccountMng::GetAccount( DWORD dpid1, DWORD dpid2 )
{
// locked
	int nIndex	= GetIndex( dpid1 );
	if( nIndex >= 0 && nIndex < m_nSizeof )
	{
		const auto i	= m_adpidToPAccount[nIndex].find( dpid2 );
		if( i != m_adpidToPAccount[nIndex].end() )
		{
			return i->second;
		}
	}
	return NULL;
}

void CAccountMng::AddConnection( DWORD dpid1 )
{
	CMclAutoLock	Lock( m_AddRemoveLock );

	auto i	= m_dpidToIndex.find( dpid1 );
	if( i == m_dpidToIndex.end() )
	{
		m_dpidToIndex.emplace(dpid1, m_nSizeof);
		m_nSizeof++;
	}
//	else
//	{
//	}
}

void CAccountMng::RemoveConnection( DWORD dpid1 )
{
	CMclAutoLock	Lock( m_AddRemoveLock );
	const int nIndex	= GetIndex( dpid1 );
	if( nIndex >= 0 && nIndex < m_nSizeof )
	{
		auto	i1	= m_adpidToPAccount[nIndex].begin();
		while( i1 != m_adpidToPAccount[nIndex].end() )
		{
			CAccount * pAccount	= i1->second;
			auto	i2	= i1;
			++i1;
			m_adpidToPAccount[nIndex].erase( i2 );
			RemoveAccount( pAccount->m_lpszAccount );	// erase & delete
		}
	}
}

void CAccountMng::AddIdofServer( DPID dpid, DWORD dwIdofServer )
{
	int nIdofServer;
	CMclAutoLock	Lock( m_AddRemoveLock );

	nIdofServer		= GetIdofServer( dpid );
	if( nIdofServer < 0 )
		m_2IdofServer.emplace(dpid, dwIdofServer);
}

#ifdef __SERVERLIST0911
DWORD		CAccountMng::RemoveIdofServer( DWORD dpid, BOOL bRemoveConnection )
#else	// __SERVERLIST0911
void CAccountMng::RemoveIdofServer( DWORD dpid, BOOL bRemoveConnection )
#endif	// __SERVERLIST0911
{
	CAccount* pAccount;
	CMclAutoLock	Lock( m_AddRemoveLock );
	DWORD dwIdofServer	= GetIdofServer( dpid );

	auto i	= m_stringToPAccount.begin();
	while( i != m_stringToPAccount.end() )
	{
		pAccount	= i->second;
		++i;
		if( pAccount->m_dwIdofServer == dwIdofServer )
		{
			if( pAccount->m_fRoute )
				RemoveAccount( pAccount->m_lpszAccount );
			else
				RemoveAccount( pAccount->m_dpid1, pAccount->m_dpid2 );
		}
	}
	if( bRemoveConnection )
		m_2IdofServer.erase( dpid );

#ifdef __SERVERLIST0911
	return dwIdofServer;
#endif	// __SERVERLIST0911
}



// 오후 10시에 미성년자와 미등록 성인들의 접속을 종료시킨다.
void CAccountMng::PreventExcess()
{
	CTime cur = CTime::GetCurrentTime();

	if( cur.GetHour() == 22 && m_nOldHour != 22 )
	{	
		CAccount* pAccount;
		CMclAutoLock	Lock( m_AddRemoveLock );
		auto i	= m_stringToPAccount.begin();
		while( i != m_stringToPAccount.end() )
		{
			pAccount	= i->second;
			++i;

			if( pAccount->IsAdult() )	// 성인이면 skip
				continue;

			g_dpSrvr.CloseExistingConnection( pAccount->m_lpszAccount, 0 );
		}	
	}

	m_nOldHour = cur.GetHour();
}

CAccount* CAccountMng::GetAccount( LPCTSTR lpszAccount, DWORD dwKey )
{
	CMclAutoLock	Lock( m_AddRemoveLock );

	CAccount* pAccount = GetAccount( lpszAccount );
	if( pAccount == NULL )
		return NULL;		// may be logout

	if( dwKey && pAccount->m_dwAuthKey != dwKey )		// dwKey가 0이면 검사하지 않는다.
		return NULL;		// may be different account
	
	return pAccount;
}

// 날짜를 체크해서 계정들을 풀건지 검사
BOOL CAccountMng::IsTimeCheckAddr()
{
	CTime tTime = CTime::GetCurrentTime();
	if( m_nYear == tTime.GetYear() && m_nMonth == tTime.GetMonth() && m_nDay == tTime.GetDay() &&
		m_nHour == tTime.GetHour() && m_nMinute == tTime.GetMinute() )
	{
		return TRUE;
	}
	return FALSE;
}
