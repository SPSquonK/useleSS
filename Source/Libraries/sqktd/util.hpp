#pragma once

#include <concepts>
#include <type_traits>
#include <optional>
#include <span>

namespace sqktd {
	template<typename T>
	[[nodiscard]] constexpr bool is_among(T searched, std::convertible_to<T> auto ... among) {
		return ((searched == among) || ...);
	};

	template<typename T>
	concept scoped_enum = std::is_scoped_enum_v<T>;

	template<typename T>
	struct enum_description {
		// Specialize with the following members:
		// static constexpr auto valid_values = std::array{ your values };
	};

	template<scoped_enum enum_type>
	[[nodiscard]] constexpr std::optional<enum_type> to_enum(const std::integral auto value) {
		for (const enum_type valid_value : sqktd::enum_description<enum_type>::valid_values) {
			if (std::cmp_equal(value, std::to_underlying(valid_value))) {
				return valid_value;
			}
		}

		return std::nullopt;
	}
}
