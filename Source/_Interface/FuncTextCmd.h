#pragma once

#if defined(__WORLDSERVER)
class CUser;
#elif defined(__CLIENT)
class CMover;
using CUser = CMover;
#else
static_assert(false, "FuncTextCmd.h should only be included in WORLD and in CLIENT");
#endif

#include "boost/container/stable_vector.hpp"

struct TextCmdFunc {
	enum class TCM { CLIENT, SERVER, BOTH };

	using PlayerHandler = BOOL(*)(CScanner & scanner, CUser * pUser);
	using PlayerlessHandler = BOOL(*)(CScanner & scanner);
	using Handler = std::variant<PlayerHandler, PlayerlessHandler>;

	Handler m_handler;
	CString m_pCommand;
	CString m_pKrCommand;

	TCM m_nServer;
	DWORD m_dwAuthorization;
	const TCHAR * m_pszDesc;

	TextCmdFunc(
		TextCmdFunc::TCM nServer, DWORD dwAuthorization, TextCmdFunc::Handler func,
		const TCHAR * pCommand, const TCHAR * pKrCommand, const TCHAR * pszDesc
	) : m_nServer(nServer), m_dwAuthorization(dwAuthorization), m_handler(func), 
		m_pCommand(pCommand), m_pKrCommand(pKrCommand), m_pszDesc(pszDesc) {
	}

	BOOL Call(CScanner & scanner, CUser * pUser) const {
		struct Visitor {
			CScanner & scanner; CUser * pUser;
			BOOL operator()(const PlayerHandler & handler)     const { return handler(scanner, pUser); }
			BOOL operator()(const PlayerlessHandler & handler) const { return handler(scanner);        }
		};

		return std::visit(Visitor{ scanner, pUser }, m_handler);
	}
};

namespace CmdFunc {
	class AllCommands {
	public:
		using TCM = TextCmdFunc::TCM;

		AllCommands();
		BOOL ParseCommand(LPCTSTR lpszString, CUser * pMover, BOOL bItem = FALSE);

		[[nodiscard]] auto begin() const { return m_allCommands.cbegin(); }
		[[nodiscard]] auto end() const   { return m_allCommands.cend();   }

	private:
		boost::container::stable_vector<TextCmdFunc> m_allCommands;
		std::map<CString, TextCmdFunc *> m_aliasToCommand;
		std::map<CString, TextCmdFunc *> m_krAliasToCommand;

		void AddCommand(
			TextCmdFunc::TCM nServer,
			DWORD dwAuthorization,
			TextCmdFunc::Handler func,
			const TCHAR * pCommand,
			std::vector<const TCHAR *> pAbbreviations = {},
			std::vector<const TCHAR *> pKrCommand = {},
			const TCHAR * pszDesc = ""
		);
		
		void RegisterAlias(const TCHAR * name, TextCmdFunc * textCmdFunc, bool isKorean);
	};
}


extern CmdFunc::AllCommands g_textCmdFuncs;
