#include "stdafx.h"
#include "defineText.h"
#include "defineObj.h"
#include "FuncTextCmd.h"
#include "WorldMng.h"
#include "definequest.h"

#ifdef __CLIENT
#include "AppDefine.h"
#include "WndAdminCreateItem.h"
#include "WndIndirectTalk.h"
#include "WndChangeFace.h"
#include "WndCommItem.h"
#include "WndSkillTree.h"
#include "WndSmelt.h"
#include "dpclient.h"
#include "timeLimit.h"
#include "MsgHdr.h"
#endif // __CLIENT

#ifdef __WORLDSERVER
#include "User.h"
#include "UserMacro.h"
#include "DPCoreClient.h"
#include "dpdatabaseclient.h"
#include "dpSrvr.h"
#include "eveschool.h"
#include "WorldDialog.h"
#include "ItemUpgrade.h"
#endif	// __WORLDSERVER

#include "randomoption.h"

#include "playerdata.h"

#include "SecretRoom.h"

#ifdef __CLIENT
#include "clord.h"
#endif	// __CLIENT

#include "Tax.h"

	#include "honor.h"

#ifdef __WORLDSERVER
#include "RainbowRace.h"
#endif // __WORLDSERVER

#include "guild.h"
#include "party.h"
#include "post.h"

#include "couplehelper.h"
#include "couple.h"

#include "ItemMorph.h"

#ifdef __QUIZ
#ifdef __WORLDSERVER
#include "Quiz.h"
#endif // __WORLDSERVER
#endif // __QUIZ

#include "GuildHouse.h"

#ifdef __WORLDSERVER
#include "CampusHelper.h"
#endif // __WORLDSERVER

CmdFunc::AllCommands g_textCmdFuncs;

#include "definesound.h"


BOOL TextCmd_InvenClear(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	D3DXVECTOR3 vPos	= pUser->GetPos();
	CWorld* pWorld	= pUser->GetWorld();
	
	if( !pUser->HasActivatedSystemPet() )
	{
		int nNum = 0;
		int	nSize	= pUser->m_Inventory.GetMax();
		
		for( int i = 0 ; i < nSize; ++i )
		{
			CItemElem* pItemElem = pUser->m_Inventory.GetAtId( i );
			if( pItemElem )
			{
				pUser->UpdateItem(*pItemElem, UI::Num::RemoveAll());
			}
		}
	}
	else
		pUser->AddDefinedText( TID_GAME_PET_NOWUSE );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_CommandList( CScanner& scanner )  
{ 
	return TRUE;
}

BOOL TextCmd_Open( CScanner& scanner )  
{ 
#ifdef __CLIENT
	scanner.GetToken();
	DWORD dwIdApplet = g_WndMng.GetAppletId( scanner.token );
	g_WndMng.CreateApplet( dwIdApplet );
#endif
	return TRUE;
}
BOOL TextCmd_Close( CScanner& scanner )  
{ 
#ifdef __CLIENT
	scanner.GetToken();
	DWORD dwIdApplet = g_WndMng.GetAppletId( scanner.token );
	CWndBase* pWndBase = g_WndMng.GetWndBase( dwIdApplet );
	if( pWndBase ) pWndBase->Destroy();
#endif
	return TRUE;
}

BOOL TextCmd_Time( CScanner& scanner )  
{ 
#ifdef __CLIENT
	CString string;
	CTime time = CTime::GetCurrentTime();
	//time.Get
	string = time.Format( "Real Time - %H:%M:%S" );
	g_WndMng.PutString( string );
	string.Format( "Madrigal Time - %d:%d:%d\n", g_GameTimer.m_nHour, g_GameTimer.m_nMin, g_GameTimer.m_nSec );
	g_WndMng.PutString( string );
	
#endif
	return TRUE;
}

BOOL TextCmd_SetMonsterRespawn(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	D3DXVECTOR3 vPos	= pUser->GetPos();
	CWorld* pWorld	= pUser->GetWorld();
	
	MoverProp* pMoverProp	= NULL;

	scanner.GetToken();
	if( scanner.tokenType == NUMBER ) 
	{
		DWORD dwID	= _ttoi( scanner.Token );
		pMoverProp = prj.GetMoverPropEx( dwID );

	}
	else
		pMoverProp	= prj.GetMoverProp( scanner.Token );

	if( pMoverProp && pMoverProp->dwID != 0 )
	{
		DWORD dwNum	= scanner.GetNumber();
		dwNum = std::clamp(dwNum, 1lu, 30lu);

		DWORD dwAttackNum	= scanner.GetNumber();
		dwAttackNum = std::clamp(dwAttackNum, 0lu, dwNum);

		DWORD dwRect = scanner.GetNumber();
		dwRect = std::clamp(dwRect, 1lu, 255lu);

		DWORD dwTime = scanner.GetNumber();
		dwTime = std::clamp(dwTime, 10lu, 3600lu * 3lu);

		int nAllServer = scanner.GetNumber();
		if( nAllServer != 0 )
		{
			BOOL bFlying = FALSE;
			if( pMoverProp->dwFlying )
				bFlying = TRUE;
			g_DPCoreClient.SendSetMonsterRespawn( pUser->m_idPlayer, pMoverProp->dwID, dwNum, dwAttackNum, dwRect, dwTime, bFlying );
			return TRUE;
		}

		CRespawnInfo ri;
		ri.m_dwType = OT_MOVER;
		ri.m_dwIndex = pMoverProp->dwID;
		ri.m_cb = dwNum;
		ri.m_nActiveAttackNum = dwAttackNum;
		if( pMoverProp->dwFlying != 0 )
			ri.m_vPos = vPos;
		ri.m_rect.left		= (LONG)( vPos.x - dwRect );
		ri.m_rect.right		= (LONG)( vPos.x + dwRect );
		ri.m_rect.top		= (LONG)( vPos.z - dwRect );
		ri.m_rect.bottom	= (LONG)( vPos.z + dwRect );
		ri.m_uTime			= (u_short)( dwTime );
		ri.m_cbTime			= 0;

		char chMessage[512] = {0,};
		pWorld->m_respawner.AddScriptSpawn( ri );

		sprintf( chMessage, "Add Respwan Monster : %s(%d/%d) Rect(%d, %d, %d, %d) Time : %d", 
			pMoverProp->szName, ri.m_cb, ri.m_nActiveAttackNum, ri.m_rect.left, ri.m_rect.right, ri.m_rect.top, ri.m_rect.bottom, ri.m_uTime );
		pUser->AddText( chMessage );
	}
#endif	// __WORLDSERVER
	return TRUE;
}


BOOL TextCmd_PropMonster( CScanner & scanner )
{
#ifdef __CLIENT
	char chMessage[1024] = {0,};
	if( 0 < prj.m_nAddMonsterPropSize )
	{
		for( int i = 0 ; i < prj.m_nAddMonsterPropSize ; ++i )
		{
			sprintf( chMessage, "Monster Prop(%s) AttackPower(%d), Defence(%d), Exp(%d), Hitpioint(%d), ItemDorp(%d), Penya(%d)", 
				prj.m_aAddProp[i].szMonsterName, prj.m_aAddProp[i].nAttackPower, prj.m_aAddProp[i].nDefence, prj.m_aAddProp[i].nExp,
				prj.m_aAddProp[i].nHitPoint, prj.m_aAddProp[i].nItemDrop, prj.m_aAddProp[i].nPenya	);
			g_WndMng.PutString( chMessage, NULL, 0xffff0000, CHATSTY_GENERAL );
		}
	}
	else
	{
		sprintf( chMessage, "Monster Prop Not Data" );
		g_WndMng.PutString( chMessage, NULL, 0xffff0000, CHATSTY_GENERAL );
	}
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_GameSetting(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->AddGameSetting();	
#endif // __WORLDSERVER
	return TRUE;	
}

BOOL TextCmd_ChangeFace(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	DWORD dwFace = scanner.GetNumber();
	if( dwFace < 0 || dwFace >= MAX_HEAD )
		return TRUE;
	
	if( (pUser->m_dwMode & NOTFRESH_MODE) || (pUser->m_dwMode & NOTFRESH_MODE2) )
	{
		pUser->m_skin.headMesh = dwFace;
		g_UserMng.AddChangeFace(*pUser, dwFace);
		if( pUser->m_dwMode & NOTFRESH_MODE )
		{
			pUser->m_dwMode &= ~NOTFRESH_MODE;
			pUser->AddDefinedText( TID_CHANGEFACE_ONE, "" );
//			pUser->AddText( "얼굴변경을 한번 사용하였습니다" );
		}
		else
		{
			pUser->m_dwMode &= ~NOTFRESH_MODE2;
			pUser->AddDefinedText( TID_CHANGEFACE_TWO, "" );
//			pUser->AddText( "얼굴변경을 2번 사용하였습니다" );
		}
	}
	else
	{
		pUser->AddDefinedText( TID_CHANGEFACE_THREE, "" );
//		pUser->AddText( "얼굴변경을 2번 모두 사용하여 사용할수 없습니다" );
	}
#else // __WORLDSERVER
	CWndChangeSex* pWndChangeSex	= (CWndChangeSex*)g_WndMng.GetWndBase( APP_CHANGESEX );
	if( NULL == pWndChangeSex )
	{
		pWndChangeSex	= new CWndChangeSex;
		pWndChangeSex->Initialize();
	}
	pWndChangeSex->SetData( NULL_ID, NULL_ID );
	return FALSE;
#endif
	return TRUE;
}

BOOL TextCmd_AroundKill(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	if( pUser->GetWeaponItem() == NULL )
		return TRUE;

	CWorld* pWorld	= pUser->GetWorld();
	if( pWorld )
		pUser->SendDamageAround( AF_MAGICSKILL, pUser, OBJTYPE_MONSTER, 1, 64.0f, 0.0, 1.0f );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL	TextCmd_PetLevel(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CPet* pPet	= pUser->GetPet();
	if( pPet && pPet->GetExpPercent() == 100 )
		pUser->PetLevelup();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL	TextCmd_MakePetFeed( CScanner & s )
{
#ifdef __CLIENT
	if( g_WndMng.m_pWndPetFoodMill == NULL )
	{
		SAFE_DELETE( g_WndMng.m_pWndPetFoodMill );
		g_WndMng.m_pWndPetFoodMill = new CWndPetFoodMill;
		g_WndMng.m_pWndPetFoodMill->Initialize();
		return FALSE;
	}
#endif	// __CLIENT
	return TRUE;
}

BOOL	TextCmd_PetExp(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CPet* pPet	= pUser->GetPet();
	if( pPet && pPet->GetLevel() != PL_S )
	{
		pPet->SetExp( MAX_PET_EXP );
		pUser->AddPetSetExp( pPet->GetExp() );
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Pet( CScanner & s )
{
	// /pet 1 100
#ifdef __WORLDSERVER
	s.GetToken();
	if( s.tok == FINISHED )
		return TRUE;
	DWORD idPlayer	= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
	if( idPlayer == 0 )	//
		return TRUE;
	CUser* pTarget	= (CUser*)prj.GetUserByID( idPlayer );
	if( IsValidObj( pTarget ) == FALSE )
		return TRUE;
	CPet* pPet	= pTarget->GetPet();
	if( pPet == NULL )	//
		return TRUE;

	// kind
	s.GetToken();
	if( s.tok == FINISHED )
		return TRUE;
	BYTE nKind	= atoi( s.token );
	if( nKind >= PK_MAX )
		return TRUE;

	// exp
	s.GetToken();
	if( s.tok == FINISHED )
		return TRUE;
	BYTE nExpRate	= atoi( s.token );

	s.GetToken();
	if( s.tok == FINISHED )
		return TRUE;
	BYTE nLevel		= s.Token.GetLength();

	if( nLevel > PL_S )
		return TRUE;

	BYTE anAvail[PL_MAX - 1]	= { 0,};
	char sAvail[2]	= { 0,};
	
	for( int i = 0; i < nLevel; i++ )
	{
		sAvail[0]	= s.Token.GetAt( i );
		sAvail[1]	= '\0';
		anAvail[i]	= atoi( sAvail );
		if( anAvail[i] < 1 || anAvail[i] > 9 )
			return TRUE;
	}

	s.GetToken();
	if( s.tok == FINISHED )
		return TRUE;
	BYTE nLife	= atoi( s.token );
	if( nLife > 99 )
		nLife	= 99;

	CItemElem* pItemElem	= pTarget->GetPetItem();
	pPet->SetKind( nKind );
	pPet->SetLevel( nLevel );
	if( nLevel == PL_EGG )
		pPet->SetKind( 0 );	// initialize
	pItemElem->m_dwItemId	= pPet->GetItemId();
	pPet->SetEnergy( pPet->GetMaxEnergy() );
	DWORD dwExp		= pPet->GetMaxExp() * nExpRate / 100;
	pPet->SetExp( dwExp );

	for( int i = PL_D; i <= nLevel; i++ )
		pPet->SetAvailLevel( i, anAvail[i-1] );
	for( int i = nLevel + 1; i <= PL_S; i++ )
		pPet->SetAvailLevel( i, 0 );

	pPet->SetLife( nLife );

	if( pTarget->HasPet() )
		pTarget->RemovePet();

	g_dpDBClient.CalluspPetLog( pTarget->m_idPlayer, pItemElem->GetSerialNumber(), 0, PETLOGTYPE_LEVELUP, pPet );

	pTarget->AddPet( pPet, PF_PET_GET_AVAIL );	// 自
	g_UserMng.AddPetLevelup( pTarget, MAKELONG( (WORD)pPet->GetIndex(), (WORD)pPet->GetLevel() ) );	// 他
#endif	// __WORLDSERVER
	return TRUE;
}


BOOL	TextCmd_MoveItem_Pocket( CScanner & s )
{
#ifdef __CLIENT
	int	nPocket1	= s.GetNumber();
	int nData	= s.GetNumber();
	int nNum	= s.GetNumber();
	int	nPocket2	= s.GetNumber();
	CItemElem* pItem	= NULL;
	if( nPocket1 < 0 )
		pItem	= g_pPlayer->m_Inventory.GetAt( nData );
	else
	{
		pItem	= g_pPlayer->m_Pocket.GetAtId( nPocket1, nData );
	}
	if( pItem )
		g_DPlay.SendMoveItem_Pocket( nPocket1, pItem->m_dwObjId, nNum, nPocket2 );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_AvailPocket( CScanner & s )
{
#ifdef __CLIENT
	int nPocket		= s.GetNumber();
	CItemElem* pItemElem	= g_pPlayer->m_Inventory.GetAt( 0 );
	if( pItemElem )
		g_DPlay.SendAvailPocket( nPocket, pItemElem->m_dwObjId );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_PocketView(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->AddPocketView();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RefineCollector(CScanner & s, CUser * pUser) {
// 0번째
#ifdef __WORLDSERVER
	int nAbilityOption	= s.GetNumber();
	if( s.tok == FINISHED )
		nAbilityOption	= 0;
	if( nAbilityOption > 5 )
		nAbilityOption	= 5;
	CItemElem* pTarget	= pUser->m_Inventory.GetAt( 0 );
	if( pTarget && pTarget->IsCollector( TRUE ) )
	{
		pUser->AddDefinedText( TID_UPGRADE_SUCCEEFUL );
		pUser->AddPlaySound( SND_INF_UPGRADESUCCESS );
		if( pUser->IsMode( TRANSPARENT_MODE ) == 0 )
			g_UserMng.AddCreateSfxObj( pUser, XI_INT_SUCCESS );
		pUser->UpdateItem(*pTarget, UI::AbilityOption::Set(nAbilityOption));
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_StartCollecting(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->StartCollecting();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_StopCollecting(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->StopCollecting();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_DoUseItemBattery(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->DoUseItemBattery();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RefineAccessory(CScanner & s, CUser * pUser) {
	// 0번째
#ifdef __WORLDSERVER
	int nAbilityOption	= s.GetNumber();
	if( s.tok == FINISHED )
		nAbilityOption	= 0;
	CItemElem* pTarget	= pUser->m_Inventory.GetAt( 0 );
	if( pTarget && pTarget->IsAccessory() )
		pUser->UpdateItem(*pTarget, UI::AbilityOption::Set(nAbilityOption));
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_SetRandomOption(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	CItemElem * const pItemElem = pUser->m_Inventory.GetAt( 0 );
	if (!pItemElem) return TRUE;
	
	const int nRandomOptionKind = g_xRandomOptionProperty.GetRandomOptionKind(pItemElem);
	if (nRandomOptionKind < 0) return TRUE;

	g_xRandomOptionProperty.InitializeRandomOption( pItemElem->GetRandomOptItemIdPtr() );
	int cb	= 0;
	int nDst	= s.GetNumber();
	while (s.tok != FINISHED) {
		const int nAdj = s.GetNumber();
		if (s.tok == FINISHED) break;

		const bool isInvalid =
			// Bad DST
			nDst == 48 || nDst < 1 || nDst >= MAX_ADJPARAMARY
			// ADJ over the capacity of a 9 bits unsigned int
			|| nAdj < -512 || nAdj >= 512;

		if (!isInvalid) {
			g_xRandomOptionProperty.SetParam(pItemElem->GetRandomOptItemIdPtr(), nDst, nAdj);
			cb++;
		}

		if (cb >= MAX_RANDOM_OPTION) break;
		nDst = s.GetNumber();
	}

	pUser->UpdateItem(*pItemElem, UI::RandomOptItem::Sync);

#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GenRandomOption(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CItemElem* pItemElem	= pUser->m_Inventory.GetAt( 0 );
	if( pItemElem )
	{
		int nRandomOptionKind	= g_xRandomOptionProperty.GetRandomOptionKind( pItemElem );
		if( nRandomOptionKind >= 0 )	// 아이템 각성, 여신의 축복이 가능한 대상
		{
			g_xRandomOptionProperty.InitializeRandomOption( pItemElem->GetRandomOptItemIdPtr() );
			g_xRandomOptionProperty.GenRandomOption( pItemElem->GetRandomOptItemIdPtr(), nRandomOptionKind, pItemElem->GetProp()->dwParts );
			pUser->UpdateItem(*pItemElem, UI::RandomOptItem::Sync);
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_PickupPetAwakeningCancel( CScanner & )
{
#ifdef __CLIENT
	SAFE_DELETE( g_WndMng.m_pWndPetAwakCancel );
	g_WndMng.m_pWndPetAwakCancel = new CWndPetAwakCancel;
	g_WndMng.m_pWndPetAwakCancel->Initialize();
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_InitializeRandomOption(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	CItemElem* pItemElem	= pUser->m_Inventory.GetAt( 0 );
	if( pItemElem )
	{
		int nRandomOptionKind	= g_xRandomOptionProperty.GetRandomOptionKind( pItemElem );
		if( nRandomOptionKind >= 0 )	// 아이템 각성, 여신의 축복이 가능한 대상
		{
			g_xRandomOptionProperty.InitializeRandomOption( pItemElem->GetRandomOptItemIdPtr() );
			pUser->UpdateItem(*pItemElem, UI::RandomOptItem::Sync);
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ItemLevel(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int i	= s.GetNumber();
	if( s.tok == FINISHED )
		i	= 0;
	CItemElem* pTarget	= pUser->m_Inventory.GetAt( 0 );
	if( pTarget )
	{
		ItemProp* pProp	= pTarget->GetProp();
		if( pProp->dwParts != NULL_ID && pProp->dwLimitLevel1 != 0xFFFFFFFF )
		{
			pTarget->SetLevelDown( i );
			pUser->UpdateItem(*pTarget, UI::RandomOptItem::Sync);
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Level(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	CString strJob = scanner.Token;

	LONG nLevel = scanner.GetNumber();
	if (nLevel == 0) nLevel = 1;

	int nJob = JOB_VAGRANT;

	LONG nLegend = scanner.GetNumber();

	for (int i = 0; i < MAX_JOB; i++) {
		if (strJob == prj.jobs.info[i].szName || strJob == prj.jobs.info[i].szEName) {
			if (nLegend == 0) {
				nJob = i;
				break;
			} else {
				--nLegend;
			}
		}
	}

	pUser->InitLevel(nJob, nLevel);

#endif // __WORLDSERVER
	return TRUE;
}

#ifdef __SFX_OPT
BOOL TextCmd_SfxLv( CScanner & scanner )
{
	int nLevel = scanner.GetNumber();
	if(nLevel > 5) nLevel = 5;
	if(nLevel < 0) nLevel = 0;
	g_Option.m_nSfxLevel = nLevel;

	return TRUE;
}
#endif

BOOL TextCmd_ChangeJob(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	CString strJob = scanner.Token;
	
	int nJob = JOB_VAGRANT;
	
	for (int i = 0; i < MAX_JOB; i++) {
		if (strJob == prj.jobs.info[i].szName || strJob == prj.jobs.info[i].szEName) {
			nJob = i;
			break;
		}
	}
	
	char chMessage[MAX_PATH] = {0,};
	if( nJob == 0 )
	{
		sprintf( chMessage, "Error Job Name or Number" );
		pUser->AddText( chMessage );		
		return TRUE;
	}
	
	if( pUser->AddChangeJob( nJob ) )
	{
		g_UserMng.AddNearSetChangeJob(pUser);
		g_dpDBClient.SendLogLevelUp( pUser, 4 );
		g_dpDBClient.SendUpdatePlayerData( pUser );
		return TRUE;
	}
	else
	{
		sprintf( chMessage, "Error 1ch -> 2ch" );
		pUser->AddText( chMessage );		
		return TRUE;
	}

#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_stat(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	CString strstat = scanner.Token;
	
	DWORD dwNum	= scanner.GetNumber();

	if( 2 <= strstat.GetLength() && strstat.GetLength() <= 7)
	{
		strstat.MakeLower();

		if( strcmp( strstat, "str" ) == 0 )
		{
			pUser->m_nStr = dwNum;
		}
		else
		if( strcmp( strstat, "sta" ) == 0 )
		{
			pUser->m_nSta = dwNum;
		}
		else
		if( strcmp( strstat, "dex" ) == 0 )
		{
			pUser->m_nDex = dwNum;
		}
		else
		if( strcmp( strstat, "int" ) == 0 )
		{
			pUser->m_nInt = dwNum;
		}
		else
		if( strcmp( strstat, "gp" ) == 0 )
		{
			pUser->m_nRemainGP = dwNum;
		}
		else
		if( strcmp( strstat, "restate" ) == 0 )
		{
			pUser->ReState();
			return FALSE;
		}
		else if( strcmp( strstat, "all" ) == 0 )
		{
			pUser->m_nStr = dwNum;
			pUser->m_nSta = dwNum;
			pUser->m_nDex = dwNum;
			pUser->m_nInt = dwNum;
		}
		else
		{
			strstat += "unknown setting target";
			pUser->AddText( strstat );
			return FALSE;
		}
	}
	else
	{
		strstat += "unknown setting target";
		pUser->AddText( strstat );
		return FALSE;
	}

	pUser->AddSetState( pUser->m_nStr, pUser->m_nSta, pUser->m_nDex, pUser->m_nInt, pUser->m_nRemainGP );
	pUser->CheckHonorStat();
	pUser->AddHonorListAck();
	g_UserMng.AddHonorTitleChange( pUser, pUser->m_nHonor);
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_SetSnoop(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	s.GetToken();
	if( s.tok != FINISHED )
	{
		if( lstrcmp( pUser->GetName(), s.Token ) )
		{
			u_long idPlayer	= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
			if( idPlayer > 0 )
			{
				BOOL bRelease	= FALSE;
				s.GetToken();
				if( s.tok != FINISHED )
					bRelease	= (BOOL)atoi( s.Token );
				g_DPCoreClient.SendSetSnoop( idPlayer, pUser->m_idPlayer,  bRelease );
			}
			else
				pUser->AddReturnSay( 3, s.Token );
		}
	}
#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_SetSnoopGuild(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	s.GetToken();
	if( s.tok != FINISHED )
	{
		CGuild* pGuild	= g_GuildMng.GetGuild( s.Token );
		if( pGuild )
		{
			BOOL bRelease	= FALSE;
			s.GetToken();
			if( s.tok != FINISHED )
				bRelease	= (BOOL)atoi( s.Token );
			g_DPCoreClient.SendSetSnoopGuild( pGuild->m_idGuild, bRelease );
		}
	}
#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_QuerySetPlayerName(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	CString strPlayer	= scanner.Token;
	strPlayer.Trim();
	g_dpDBClient.SendQuerySetPlayerName( pUser->m_idPlayer, strPlayer, MAKELONG( 0xffff, 0 ) );
	return TRUE;
#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_QuerySetGuildName(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	CString strGuild	= scanner.Token;
	strGuild.Trim();
	CGuild* pGuild	= g_GuildMng.GetGuild( pUser->m_idGuild );
	if( pGuild && pGuild->IsMaster( pUser->m_idPlayer ) )
	{
		g_DPCoreClient.SendQuerySetGuildName( pUser->m_idPlayer, pUser->m_idGuild, (LPSTR)(LPCSTR)strGuild, 0xff );
	}
	else
	{
		// is not kingpin
	}
	return TRUE;
#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_CreateGuild(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	GUILD_MEMBER_INFO	info;
	info.idPlayer	= pUser->m_idPlayer;
	g_DPCoreClient.SendCreateGuild( &info, 1, scanner.Token );
	return TRUE;
#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_DestroyGuild(CScanner &) {
#ifdef __CLIENT
	g_DPlay.SendDestroyGuild();
#endif
	return TRUE;
}

BOOL TextCmd_RemoveGuildMember( CScanner & scanner )
{
#ifdef __CLIENT
	scanner.GetToken();
	char lpszPlayer[MAX_PLAYER]	= { 0, };
	lstrcpy( lpszPlayer, scanner.Token );
	u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( lpszPlayer );
	if( idPlayer != 0 )
		g_DPlay.SendRemoveGuildMember( g_pPlayer->m_idPlayer, idPlayer );
	return TRUE;
#endif	// __CLIENT
	return FALSE;
}

BOOL TextCmd_GuildChat(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
#ifdef __JEFF_9_20
	int nText	= pUser->GetMuteText();
	if(  nText )
	{
		pUser->AddDefinedText( nText );
		return TRUE;
	}
#endif	// __JEFF_9_20

	char sChat[260]		= { 0, };
	scanner.GetLastFull();
	if( strlen( scanner.token ) >= 260 )
		return TRUE;
	strcpy( sChat, scanner.token );

	StringTrimRight( sChat );
	if( !(pUser->HasBuff( BUFF_ITEM, II_SYS_SYS_SCR_FONTEDIT)))
		ParsingEffect(sChat, strlen(sChat) );
	
	RemoveCRLF( sChat );

	g_DPCoreClient.SendGuildChat( pUser, sChat );
	return TRUE;
#else	// __WORLDSERVER
#ifdef __CLIENT
	CString string	= scanner.m_pProg;
	g_WndMng.WordChange( string );

	if( ::GetLanguage() == LANG_THA )
		string = '"'+string+'"';

	CString strCommand;
	strCommand.Format( "/g %s", string );
	g_DPlay.SendChat( strCommand );
	return FALSE;
#endif	// __CLIENT
#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_DeclWar( CScanner & scanner )
{
#ifdef __CLIENT
	char szGuild[MAX_G_NAME]	= { 0, };
	scanner.GetLastFull();
	if( strlen( scanner.token ) >= MAX_G_NAME )
		return TRUE;
	strcpy( szGuild, scanner.token );
	StringTrimRight( szGuild );
	g_DPlay.SendDeclWar( g_pPlayer->m_idPlayer, szGuild );
#endif	// __CLIENT
	return FALSE;
}


// 길드 랭킹정보를 업데이트 시키는 명령어이다.
BOOL TextCmd_GuildRanking( CScanner & )
{
#ifdef __WORLDSERVER
	// TRANS 서버에게 길드 랭킹 정보가 업데이트 되야함을 알린다.
	g_dpDBClient.UpdateGuildRankingUpdate();
#endif

	return TRUE;
}

// 길드 랭킹정보 DB를 업데이트 시키는 명령어이다.
BOOL TextCmd_GuildRankingDBUpdate( CScanner & )
{
#ifdef __WORLDSERVER
	// TRANS 서버에게 길드 랭킹 정보가 업데이트 되야함을 알린다.
	g_dpDBClient.UpdateGuildRankingUpdate();
#endif
	
	return TRUE;
}

BOOL TextCmd_GuildStat(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	CString strstat = scanner.Token;
	
	strstat.MakeLower();

	if( strstat == "logo" )
	{
		DWORD dwLogo = scanner.GetNumber();
		TRACE("guild Logo:%d\n", dwLogo);
		g_DPCoreClient.SendGuildStatLogo( pUser, dwLogo );
	}
	else
	if( strstat == "pxp" )
	{
		DWORD dwPxpCount = scanner.GetNumber();
		TRACE("guild pxpCount:%d\n", dwPxpCount);
		g_DPCoreClient.SendGuildStatPxp( pUser, dwPxpCount );
	}
	else
	if( strstat == "penya" )
	{
		DWORD dwPenya = scanner.GetNumber();
		TRACE("guild dwPenya:%d\n", dwPenya);
		g_DPCoreClient.SendGuildStatPenya(pUser, dwPenya);
	}
	else
	if( strstat == "notice" )
	{
		scanner.GetToken();
		TRACE("guild notice:%s\n", scanner.Token);
		g_DPCoreClient.SendGuildStatNotice( pUser, scanner.Token.GetString());
	}
	else
	{
		strstat += "syntax error guild stat command.";
		pUser->AddText( strstat );
	}

#endif 
	return FALSE;
}

BOOL TextCmd_SetGuildQuest(CScanner & s) {
#ifdef __WORLDSERVER

	s.GetToken();
	char sGuild[MAX_G_NAME];
	lstrcpy( sGuild, s.Token );
	int nQuestId	= s.GetNumber();
	int nState		= s.GetNumber();
	GUILDQUESTPROP* pProp	= prj.GetGuildQuestProp( nQuestId );
	if( !pProp )
		return FALSE;

	CGuild* pGuild	= g_GuildMng.GetGuild( sGuild );
	if( pGuild )
	{
		if( nState < QS_BEGIN || nState > QS_END )
		{
		}
		else
		{
			pGuild->SetQuest( nQuestId, nState );
			g_dpDBClient.SendUpdateGuildQuest( pGuild->m_idGuild, nQuestId, nState );
		}
	}
	return TRUE;

#endif	// __WORLDSERVER
	return FALSE;
}

BOOL TextCmd_PartyLevel(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	DWORD dwLevel = scanner.GetNumber();
	DWORD dwPoint = scanner.GetNumber();
	if( dwPoint == 0 )
		dwPoint = 100;

	TRACE("plv LV:%d POINT:%d\n", dwLevel, dwPoint);
	
	CParty* pParty;
	pParty = g_PartyMng.GetParty( pUser->GetPartyId() );
	if( pParty )
	{
		pParty->SetPartyLevel( pUser, dwLevel, dwPoint, pParty->GetExp() );
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL  TextCmd_InitSkillExp(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( pUser->InitSkillExp() == TRUE )
		pUser->AddInitSkill();
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_SkillLevel(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	const DWORD dwSkillKind	= scanner.GetNumber();
	const DWORD dwSkillLevel = scanner.GetNumber();

	SKILL * pSkill = pUser->GetSkill(dwSkillKind);
	if (!pSkill) return FALSE;

	pSkill->dwLevel = dwSkillLevel;
	pUser->AddSetSkill(*pSkill);
#endif // __WORLDSERVER
#ifdef __CLIENT

	DWORD dwSkillLevel = scanner.GetNumber();

	CWndSkillTreeCommon * pSkillWnd = g_WndMng.GetWndBase<CWndSkillTreeCommon>(APP_SKILL_);
	if (!pSkillWnd) {
		g_WndMng.PutString(prj.GetText(TID_GAME_CHOICESKILL), NULL, 0xffff0000);
		return FALSE;
	}

	const SKILL * pSkill = pSkillWnd->GetFocusedItem();
	if (!pSkill) {
		g_WndMng.PutString(prj.GetText(TID_GAME_CHOICESKILL), NULL, 0xffff0000);
		return FALSE;
	}

	const ItemProp * pSkillProp = pSkill->GetProp();

	if (pSkillProp->dwExpertMax < 1 || pSkillProp->dwExpertMax < dwSkillLevel) {
		char szMessage[MAX_PATH];
		sprintf(szMessage, prj.GetText(TID_GAME_SKILLLEVELLIMIT), pSkillProp->szName, pSkillProp->dwExpertMax);

		g_WndMng.PutString(szMessage, NULL, 0xffff0000);
		return FALSE;
	}

	char szSkillLevel[MAX_PATH];
	sprintf(szSkillLevel, prj.GetText(TID_GAME_GAMETEXT001), pSkill->dwSkill, dwSkillLevel);
	scanner.SetProg(szSkillLevel);

#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SkillLevelAll(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	for (SKILL & skill : pUser->m_jobSkills) {
		const ItemProp * pSkillProp = skill.GetProp();
		if (!pSkillProp) continue;

		skill.dwLevel = pSkillProp->dwExpertMax;
		pUser->AddSetSkill(skill);
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_whisper(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	static	CHAR	lpString[260];

	if( pUser->IsMode( SAYTALK_MODE ) )
		return TRUE;
#ifdef __JEFF_9_20
	int nText	= pUser->GetMuteText();
	if(  nText )
	{
		pUser->AddDefinedText( nText );
		return TRUE;
	}
#endif	// __JEFF_9_20

	scanner.GetToken();

	if( strcmp( pUser->GetName(), scanner.Token ) )
	{
			u_long idPlayer	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
			if( idPlayer > 0 ) 
			{
				scanner.GetLastFull();
				if( strlen( scanner.token ) >= 260 )	
					return TRUE;
				strcpy( lpString, scanner.token );
				
				StringTrimRight( lpString );
				
				if( !(pUser->HasBuff( BUFF_ITEM, II_SYS_SYS_SCR_FONTEDIT)))
				{
					ParsingEffect(lpString, strlen(lpString) );
				}
				RemoveCRLF( lpString );
				
				if( 0 < strlen(lpString) )
					g_DPCoreClient.SendWhisper( pUser->m_idPlayer, idPlayer, lpString );
			}
			else 
			{
				//scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
				pUser->AddReturnSay( 3, scanner.Token );
			}
	}
	else
	{
		pUser->AddReturnSay( 2, " " );  	// 자기 자신에게 명령했다.
	}
#endif	// __WORLDSERVER

	return TRUE;
}

BOOL TextCmd_say(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	static	CHAR	lpString[1024];

	if( pUser->IsMode( SAYTALK_MODE ) )
		return TRUE;
#ifdef __JEFF_9_20
	int nText	= pUser->GetMuteText();
	if(  nText )
	{
		pUser->AddDefinedText( nText );
		return TRUE;
	}
#endif	// __JEFF_9_20

	scanner.GetToken();
	if( strcmp( pUser->GetName(), scanner.Token ) )
	{
		u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		if( idPlayer > 0 ) 
		{
			scanner.GetLastFull();
			if( strlen( scanner.token ) > 260 )	//
				return TRUE;

			strcpy( lpString, scanner.token );

			StringTrimRight( lpString );
			
			if( !(pUser->HasBuff( BUFF_ITEM, II_SYS_SYS_SCR_FONTEDIT)))
			{
				ParsingEffect(lpString, strlen(lpString) );
			}
			RemoveCRLF( lpString );
			
			g_DPCoreClient.SendSay( pUser->m_idPlayer, idPlayer, lpString );
		}
		else 
		{
			//scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
			pUser->AddReturnSay( 3, scanner.Token );
		}
	}
	else
	{
		pUser->AddReturnSay( 2, " " );  	// 자기 자신에게 명령했다.
	}
	
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ResistItem(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	DWORD dwObjId	= scanner.GetNumber();
	
	auto optItemResist = SAI79::From(scanner.GetNumber());
	if (!optItemResist) return FALSE;
	SAI79::ePropType bItemResist = *optItemResist;

	int nResistAbilityOption = scanner.GetNumber();
	int nAbilityOption	= scanner.GetNumber();

	if( nResistAbilityOption < 0 || CItemUpgrade::GetInstance()->GetMaxAttributeEnchantSize() < nResistAbilityOption 
		|| nAbilityOption < 0 || CItemUpgrade::GetInstance()->GetMaxGeneralEnchantSize() < nAbilityOption )
	{
		return FALSE;
	}

	if( bItemResist == SAI79::NO_PROP )
	{
		nResistAbilityOption = 0;
	}

	CItemElem* pItemElem0	= pUser->m_Inventory.GetAtId( dwObjId );
	if( NULL == pItemElem0 )
		return FALSE;
	if( pUser->m_Inventory.IsEquip( dwObjId ) )
	{
		pUser->AddDefinedText( TID_GAME_EQUIPPUT , "" );
		return FALSE;
	}

	pUser->UpdateItem(*pItemElem0,
		UI::Element{ .kind = bItemResist, .abilityOption = nResistAbilityOption }
	);
	
	pUser->UpdateItem(*pItemElem0, UI::AbilityOption::Set(nAbilityOption));
	if (nAbilityOption > 5 && pItemElem0->GetProp()->dwReferStat1 == WEAPON_ULTIMATE) {
		pUser->UpdateItem(*pItemElem0, UI::Piercing::Size::Ultimate);
	}
#else // __WORLDSEVER
#ifdef __CLIENT
	if( g_WndMng.m_pWndUpgradeBase == NULL )
	{
		SAFE_DELETE( g_WndMng.m_pWndUpgradeBase );
		g_WndMng.m_pWndUpgradeBase = new CWndUpgradeBase;
		g_WndMng.m_pWndUpgradeBase->Initialize();
		return FALSE;
	}

	BYTE bItemResist = scanner.GetNumber();
	int nResistAbilityOption = scanner.GetNumber();
	int nAbilityOption	= scanner.GetNumber();
	if( bItemResist < 0 || 5 < bItemResist )
	{
		return FALSE;
	}
	if( nResistAbilityOption < 0 || 20 < nResistAbilityOption || nAbilityOption < 0 || 10 < nAbilityOption )
	{
		return FALSE;
	}

	if( bItemResist == 0 )
	{
		nResistAbilityOption = 0;
	}

	if( g_WndMng.m_pWndUpgradeBase )
	{
		if( g_WndMng.m_pWndUpgradeBase->m_slot )
		{
			DWORD dwObjId = g_WndMng.m_pWndUpgradeBase->m_slot->m_dwObjId;
			char szSkillLevel[MAX_PATH];
			sprintf( szSkillLevel, "/ritem %d %d %d %d", dwObjId, bItemResist, nResistAbilityOption, nAbilityOption );
			scanner.SetProg( szSkillLevel );		
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
#endif // __CLIENT
#endif // __WORLDSERVER
	return TRUE;
}

#ifdef __WORLDSERVER
namespace SqKItemEnchant {
	void ItemEnchant(CScanner & scanner, CUser * user, CItemElem * item);
}
#endif

BOOL TextCmd_ItemEnchant(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	CItemElem * pItemElem = pUser->m_Inventory.GetAt(0);
	if (!pItemElem) return TRUE;

	SqKItemEnchant::ItemEnchant(scanner, pUser, pItemElem);
#endif
	return TRUE;
}

BOOL TextCmd_CommercialElem( CScanner& )
{
#ifdef __CLIENT
	SAFE_DELETE( g_WndMng.m_pWndCommerialElem );
	g_WndMng.m_pWndCommerialElem = new CWndCommercialElem;
	g_WndMng.m_pWndCommerialElem->Initialize();
#endif // __CLIENT
	return FALSE;
}

BOOL TextCmd_Piercing(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	DWORD dwObjId	= scanner.GetNumber();
	int bPierNum = scanner.GetNumber();

	CItemElem* pItemElem0	= pUser->m_Inventory.GetAtId( dwObjId );
	if( NULL == pItemElem0 )
		return FALSE;

	if( bPierNum < 0 || bPierNum > MAX_PIERCING )
		return FALSE;

	if( pUser->m_Inventory.IsEquip( dwObjId ) )
	{
		pUser->AddDefinedText( TID_GAME_EQUIPPUT , "" );
		return FALSE;
	}

	if( bPierNum < pItemElem0->GetPiercingSize() )
	{
		pUser->UpdateItem(*pItemElem0, UI::Piercing::Size{ UI::Piercing::Kind::Regular, bPierNum });
		return TRUE;
	}

	for( int i=1; i<=bPierNum; i++ )
	{
		if( pItemElem0->IsPierceAble( NULL_ID, TRUE ) )
			pUser->UpdateItem(*pItemElem0, UI::Piercing::Size{ UI::Piercing::Kind::Regular, i });
	}
#else // __WORLDSEVER
#ifdef __CLIENT
	const int bPierNum = scanner.GetNumber();
	if (bPierNum < 0 || bPierNum > MAX_PIERCING) return FALSE;

	const CWndPiercing * const pWndPiercing = (CWndPiercing*)g_WndMng.GetWndBase(APP_PIERCING);
	if (!pWndPiercing) return FALSE;

	if (!pWndPiercing->m_slots[0]) return FALSE;

	const DWORD dwObjId = pWndPiercing->m_slots[0].m_item->m_dwObjId;
	char szSkillLevel[MAX_PATH];
	sprintf(szSkillLevel, "/pier %d %d", dwObjId, bPierNum);
	scanner.SetProg(szSkillLevel);

#endif // __CLIENT
#endif // __WORLDSERVER
	return TRUE;
}

BOOL IsRight( CString &str )
{
	if( str.GetLength() == 1 )
	{
		if( str.Find( "#", 0 ) == 0 )
			return FALSE;
	}
	else
	if( str.GetLength() == 2 )
	{
		if( str.Find( "#u", 0 ) == 0 )
			return FALSE;
		
		if( str.Find( "#b", 0 ) == 0 )
			return FALSE;
	}
	
	int nFind = str.Find( "#c", 0 );	
	if( nFind != -1 )
	{
		if( (str.GetLength() - nFind) < 8 )
			return FALSE;
	}
		
		return TRUE;
}

BOOL TextCmd_shout(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( pUser->IsMode( SHOUTTALK_MODE ) )
		return FALSE;
#ifdef __QUIZ
	if( pUser->GetWorld() && pUser->GetWorld()->GetID() == WI_WORLD_QUIZ )
		return FALSE;
#endif // __QUIZ
#ifdef __JEFF_9_20
	int nText	= pUser->GetMuteText();
	if(  nText )
	{
		pUser->AddDefinedText( nText );
		return FALSE;
	}
	if( ( // 미국 & 유럽
		( ::GetLanguage() == LANG_ENG && ::GetSubLanguage() == LANG_SUB_USA )
		|| ( ::GetLanguage() == LANG_ENG && ::GetSubLanguage() == LANG_SUB_IND )
		|| ::GetLanguage() == LANG_GER
		|| ::GetLanguage() == LANG_FRE
		|| ::GetLanguage() == LANG_VTN
		|| ::GetLanguage() == LANG_POR
		|| ::GetLanguage() == LANG_RUS
		)
		&& pUser->GetLevel() < 20 )
	{
		pUser->AddDefinedText( TID_GAME_CANT_SHOUT_BELOW_20 );
		return FALSE;
	}
#endif	// __JEFF_9_20

	if( ( GetLanguage() == LANG_TWN || GetLanguage() == LANG_HK )  && pUser->GetLevel() < 15 )
	{
		pUser->AddDefinedText( TID_GAME_CANNOT_SHOUT_BELOW_15_LEVEL );
		return FALSE;
	}

	char szString[1024];
	szString[0] = '\0';

	scanner.GetLastFull();
	if( strlen( scanner.token ) > 260 )	//
		return TRUE;

	strcpy( szString, scanner.token );
	StringTrimRight( szString );

	if( !(pUser->HasBuff( BUFF_ITEM, II_SYS_SYS_SCR_FONTEDIT)))
	{
		ParsingEffect( szString, strlen(szString) );
	}
	RemoveCRLF( szString );

	CAr arBlock;
	arBlock << NULL_ID << SNAPSHOTTYPE_SHOUT;
	arBlock << GETID( pUser );
	arBlock.WriteString(pUser->GetName());
	arBlock.WriteString( szString );
	DWORD dwColor	= 0xffff99cc;
	if( pUser->HasBuff(  BUFF_ITEM, II_SYS_SYS_LS_SHOUT ) )
		dwColor		= 0xff00ff00;
	arBlock << dwColor;
	GETBLOCK( arBlock, lpBlock, uBlockSize );

	int nRange = 0x000000ff;
	if( pUser->IsShoutFull() )
		nRange = 0;
	g_UserMng.AddShout( pUser, nRange, lpBlock, uBlockSize );

#else	// __WORLDSERVER
#ifdef __CLIENT

	static CTimer timerDobe;
	static CString stringBack;
	static CTimeLimit timeLimit( g_Neuz.m_nShoutLimitCount, g_Neuz.m_dwShoutLimitSecond );

	CString string = scanner.m_pProg; 

	CString strTrim = string;
	strTrim.TrimLeft();
		
	if( IsRight( strTrim ) == FALSE )
		return FALSE;

#ifdef __BAN_CHATTING_SYSTEM
	if( !g_WndMng.GetBanningTimer().IsTimeOut() )
	{
		CWndChat* pWndChat = ( CWndChat* )g_WndMng.GetWndBase( APP_COMMUNICATION_CHAT );
		if( pWndChat )
		{
			int nOriginalSecond = static_cast< int >( CWndMgr::BANNING_MILLISECOND - static_cast< int >( g_WndMng.GetBanningTimer().GetLeftTime() ) ) / 1000;
			int nMinute = static_cast< int >( nOriginalSecond / 60 );
			int nSecond = static_cast< int >( nOriginalSecond % 60 );
			CString strMessage = _T( "" );
			// 현재 채팅 금지 페널티를 받고 있습니다. (남은 시간: %d분 %d초)
			strMessage.Format( prj.GetText( TID_GAME_ERROR_CHATTING_3 ), nMinute, nSecond );
			pWndChat->PutString( strMessage, 0xffff0000 );
			return FALSE;
		}
	}
	else
	{
		if( stringBack != string || timerDobe.IsTimeOut() )
		{
			if( !g_pPlayer->IsShoutFull()
				|| ( ::GetLanguage() == LANG_ENG && ::GetSubLanguage() == LANG_SUB_USA )
				|| ( ::GetLanguage() == LANG_ENG && ::GetSubLanguage() == LANG_SUB_IND )
				|| ::GetLanguage() == LANG_GER
				|| ::GetLanguage() == LANG_FRE
				|| ::GetLanguage() == LANG_JAP
				|| ::GetLanguage() == LANG_VTN
				|| ::GetLanguage() == LANG_POR
				|| ::GetLanguage() == LANG_RUS
				)
			{
				if( g_Neuz.m_nShoutLimitCount > 0 && timeLimit.Check() == TRUE ) // 제한을 넘었다.
				{
					//외치기는 %n초안에 %d번만 허용됩니다.
					char szMsg[256];
					sprintf( szMsg, prj.GetText( TID_GAME_LIMIT_SHOUT ), g_Neuz.m_dwShoutLimitSecond / 1000, g_Neuz.m_nShoutLimitCount );
					g_WndMng.PutString( szMsg, NULL, prj.GetTextColor( TID_GAME_LIMIT_SHOUT ) );
					return FALSE;
				}
			}

			stringBack = string;
			timerDobe.Set( SEC( 3 ) );
			g_WndMng.WordChange( string );

			if( g_WndMng.IsShortcutCommand() == TRUE )
			{
				if( !g_WndMng.GetShortcutWarningTimer().IsTimeOut() )
				{
					g_WndMng.SetWarningCounter( g_WndMng.GetWarningCounter() + 1 );
					CWndChat* pWndChat = ( CWndChat* )g_WndMng.GetWndBase( APP_COMMUNICATION_CHAT );
					if( pWndChat )
					{
						if( g_WndMng.GetWarningCounter() >= CWndMgr::BANNING_POINT )
						{
							// 과도한 채팅으로 인하여 %d분 동안 채팅 금지 페널티를 받으셨습니다.
							CString strChattingError1 = _T( "" );
							strChattingError1.Format( prj.GetText( TID_GAME_ERROR_CHATTING_2 ), CWndMgr::BANNING_MILLISECOND / 1000 / 60 );
							pWndChat->PutString( strChattingError1, prj.GetTextColor( TID_GAME_ERROR_CHATTING_2 ) );
							g_WndMng.SetWarningCounter( 0 );
							g_WndMng.GetBanningTimer().Reset();
						}
						else
						{
							// 연속 채팅으로 인하여 메시지가 출력되지 않았습니다.
							pWndChat->PutString( prj.GetText( TID_GAME_ERROR_CHATTING_1 ), 0xffff0000 );
						}
					}
				}
				else
				{
					CString strCommand = _T( "/s " ) + string;
					g_DPlay.SendChat( strCommand );
				}
				g_WndMng.GetShortcutWarningTimer().Reset();
			}
			else
			{
				if( !g_WndMng.GetWarningTimer().IsTimeOut() )
				{
					g_WndMng.SetWarningCounter( g_WndMng.GetWarningCounter() + 1 );
					CWndChat* pWndChat = ( CWndChat* )g_WndMng.GetWndBase( APP_COMMUNICATION_CHAT );
					if( pWndChat )
					{
						if( g_WndMng.GetWarningCounter() >= CWndMgr::BANNING_POINT )
						{
							// 과도한 채팅으로 인하여 %d분 동안 채팅 금지 페널티를 받으셨습니다.
							CString strChattingError1 = _T( "" );
							strChattingError1.Format( prj.GetText( TID_GAME_ERROR_CHATTING_2 ), CWndMgr::BANNING_MILLISECOND / 1000 / 60 );
							pWndChat->PutString( strChattingError1, prj.GetTextColor( TID_GAME_ERROR_CHATTING_2 ) );
							g_WndMng.SetWarningCounter( 0 );
							g_WndMng.GetBanningTimer().Reset();
						}
						else
						{
							// 연속 채팅으로 인하여 메시지가 출력되지 않았습니다.
							pWndChat->PutString( prj.GetText( TID_GAME_ERROR_CHATTING_1 ), 0xffff0000 );
						}
					}
				}
				else
				{
					if( !g_WndMng.GetWarning2Timer().IsTimeOut() )
					{
						g_WndMng.SetWarning2Counter( g_WndMng.GetWarning2Counter() + 1 );
						CWndChat* pWndChat = ( CWndChat* )g_WndMng.GetWndBase( APP_COMMUNICATION_CHAT );
						if( pWndChat )
						{
							if( g_WndMng.GetWarning2Counter() >= CWndMgr::BANNING_2_POINT )
							{
								// 과도한 채팅으로 인하여 %d분 동안 채팅 금지 페널티를 받으셨습니다.
								CString strChattingError1 = _T( "" );
								strChattingError1.Format( prj.GetText( TID_GAME_ERROR_CHATTING_2 ), CWndMgr::BANNING_MILLISECOND / 1000 / 60 );
								pWndChat->PutString( strChattingError1, prj.GetTextColor( TID_GAME_ERROR_CHATTING_2 ) );
								g_WndMng.SetWarning2Counter( 0 );
								g_WndMng.GetBanningTimer().Reset();
							}
							else
							{
								CString strCommand = _T( "" );
								strCommand.Format( "/s %s", string );
								g_DPlay.SendChat( strCommand );
							}
						}
					}
					else
					{
						CString strCommand = _T( "" );
						strCommand.Format( "/s %s", string );
						g_DPlay.SendChat( strCommand );
					}
				}
				g_WndMng.GetWarningTimer().Reset();
				g_WndMng.GetWarning2Timer().Reset();
			}
		}
		else
		{
			CWndChat* pWndChat = (CWndChat*)g_WndMng.GetWndBase( APP_COMMUNICATION_CHAT );
			if( pWndChat )
				g_WndMng.PutString( prj.GetText( TID_GAME_CHATSAMETEXT ), NULL, prj.GetTextColor( TID_GAME_CHATSAMETEXT ) );
		}
	}
#else // __BAN_CHATTING_SYSTEM
	if( stringBack != string || timerDobe.TimeOut() )
	{
		if( !g_pPlayer->IsShoutFull()
			|| ( ::GetLanguage() == LANG_ENG && ::GetSubLanguage() == LANG_SUB_USA )
			|| ( ::GetLanguage() == LANG_ENG && ::GetSubLanguage() == LANG_SUB_IND )
			|| ::GetLanguage() == LANG_GER
			|| ::GetLanguage() == LANG_FRE
			|| ::GetLanguage() == LANG_JAP
			|| ::GetLanguage() == LANG_VTN
			|| ::GetLanguage() == LANG_POR
			|| ::GetLanguage() == LANG_RUS
		)
			if( g_Neuz.m_nShoutLimitCount > 0 && timeLimit.Check() == TRUE ) // 제한을 넘었다.
			{
				//외치기는 %n초안에 %d번만 허용됩니다.
				char szMsg[256];
				sprintf( szMsg, prj.GetText( TID_GAME_LIMIT_SHOUT ), g_Neuz.m_dwShoutLimitSecond / 1000, g_Neuz.m_nShoutLimitCount );
				g_WndMng.PutString( szMsg, NULL, prj.GetTextColor( TID_GAME_LIMIT_SHOUT ) );
				return FALSE;
			}

		stringBack = string;
		timerDobe.Set( SEC( 3 ) );
		g_WndMng.WordChange( string );

		CString strCommand;
		strCommand.Format( "/s %s", string );
		g_DPlay.SendChat( strCommand );
	}
	else
	{
		CWndChat* pWndChat = (CWndChat*)g_WndMng.GetWndBase( APP_COMMUNICATION_CHAT );
		if( pWndChat )
			g_WndMng.PutString( prj.GetText( TID_GAME_CHATSAMETEXT ), NULL, prj.GetTextColor( TID_GAME_CHATSAMETEXT ) );
	}
#endif // __BAN_CHATTING_SYSTEM
	return FALSE;
#endif // __CLIENT
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_PartyChat(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	static CHAR lpString[260];
	
#ifdef __JEFF_9_20
	int nText	= pUser->GetMuteText();
	if(  nText )
	{
		pUser->AddDefinedText( nText );
		return TRUE;
	}
#endif	// __JEFF_9_20
	
	lpString[0] = '\0';
	
	scanner.GetLastFull();
	if( lstrlen( scanner.token ) >= 260 )
		return TRUE;
	strcpy( lpString, scanner.token );

	StringTrimRight( lpString );	

	if( !(pUser->HasBuff( BUFF_ITEM, II_SYS_SYS_SCR_FONTEDIT)))
	{
		ParsingEffect(lpString, strlen(lpString) );
	}
	RemoveCRLF( lpString );
	
	CParty* pParty;
	
	pParty	= g_PartyMng.GetParty( pUser->GetPartyId() );
	if( pParty && pParty->IsMember( pUser->m_idPlayer ))
	{
		// 파티가 있어서 파티원들에게 보냄
		g_DPCoreClient.SendPartyChat( pUser, lpString );
	}
	else
	{
		// 월드서버에서 파티가 없는경우
		pUser->AddSendErrorParty( ERROR_NOPARTY );
	}

	// 클라이언트에서 극단참여중이니즐 먼저 검색함
#else // __WORLDSERVER
#ifdef __CLIENT
	if( g_Party.GetSizeofMember() >= 2 )
	{
		CString string = scanner.m_pProg;
		g_WndMng.WordChange( string );

		if( ::GetLanguage() == LANG_THA )
			string = '"'+string+'"';
		
		CString strCommand;
		strCommand.Format( "/p %s", string );
		g_DPlay.SendChat( strCommand );
	}
	else
	{
		// 극단원이 아니므로 
		// 극단에 포함되지 않아 극단채팅을 할수 없습니다.
		//g_WndMng.PutString( "극단에 포함되지 않아 극단채팅을 할수 없습니다", NULL, 0xff99cc00 );
		g_WndMng.PutString( prj.GetText( TID_GAME_PARTYNOTCHAT ), NULL, prj.GetTextColor( TID_GAME_PARTYNOTCHAT ) );
		
	}
	return FALSE;
#endif // __CLIENT
#endif // __WORLDSERVER
	return TRUE;
}


BOOL TextCmd_Music(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	u_long idmusic	= scanner.GetNumber();
	g_DPCoreClient.SendPlayMusic( pUser->GetWorld()->GetID(), idmusic );
#endif	// __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_Sound(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	u_long idsound	= scanner.GetNumber();
	g_DPCoreClient.SendPlaySound( pUser->GetWorld()->GetID(), idsound );
#endif	// __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_Summon(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	if( strcmp( pUser->GetName(), scanner.Token) )
	{
		u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		if( idPlayer > 0 ){
			CUser * summoned = g_UserMng.GetUserByPlayerID(idPlayer);

			if (!IsValidObj(summoned)) {
				pUser->AddDefinedText(TID_GAME_NOTLOGIN);
				return FALSE;
			}

			if (!pUser->GetWorld()) {
				WriteError("PACKETTYPE_SUMMONPLAYER//1");
				return FALSE;
			}

			summoned->Replace(*pUser, REPLACE_FORCE);
		}
		else {
//			scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
			pUser->AddReturnSay( 3, scanner.Token );
		}
	}
	else 
	{
		pUser->AddReturnSay( 2, " " );  		// 자기 자신에게 명령했다.		
	}
	
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ClearPetName( CScanner &)
{
#ifdef __CLIENT
	g_DPlay.SendClearPetName();
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_SetPetName(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	CPet* pPet	= pUser->GetPet();
	if( !pPet )
		return TRUE;
	s.GetToken();
	pPet->SetName( s.token );
	g_UserMng.AddSetPetName( pUser, s.token );
#endif	// __WORLDSERVER
	return TRUE;
}

#ifdef __LAYER_1020
BOOL TextCmd_CreateLayer(CScanner & s) {
#ifdef __WORLDSERVER
	DWORD dwWorld	= s.GetNumber();
	CWorld* pWorld	= g_WorldMng.GetWorld( dwWorld );
	if( pWorld )
	{
		int nLayer	= s.GetNumber();
//		pWorld->m_linkMap.CreateLinkMap( nLayer );
		pWorld->CreateLayer( nLayer );
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_DeleteLayer( CScanner & s )
{
#ifdef __WORLDSERVER
	DWORD dwWorld	= s.GetNumber();
	CWorld* pWorld	= g_WorldMng.GetWorld( dwWorld );
	if( pWorld )
	{
		int nLayer	= s.GetNumber();
//		pWorld->m_linkMap.DeleteLinkMap( nLayer );
		pWorld->ReleaseLayer( nLayer );		// do not call ReleaseLayer directly
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Layer(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	DWORD dwWorld	= s.GetNumber();
	CWorld* pWorld	= g_WorldMng.GetWorld( dwWorld );
	if( pWorld )
	{
		int nLayer	= s.GetNumber();
		CLinkMap* pLinkMap	= pWorld->m_linkMap.GetLinkMap( nLayer );
		if( pLinkMap )
		{
			FLOAT x	= s.GetFloat();
			FLOAT z	= s.GetFloat();
			if( pWorld->VecInWorld( x, z ) && x > 0 && z > 0 )	
				pUser->Replace( pWorld->GetID(), D3DXVECTOR3( x, 0, z ), REPLACE_NORMAL, nLayer );
			else
				pUser->AddText( "OUT OF WORLD" );
		}
		else
		{
			pUser->AddText( "LAYER NO EXISTS" );
		}
	}
	else
	{
		pUser->AddText( "UNDEFINED WORLD" );
	}
#endif	// __WORLDSERVER
	return TRUE;
}
#endif	// __LAYER_1020

BOOL TextCmd_NextCoupleLevel(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CCouple* pCouple	= CCoupleHelper::Instance()->GetCouple( pUser->m_idPlayer );
	if( pCouple )
	{
		if( pCouple->GetLevel() < CCouple::eMaxLevel )
		{
			int nExperience	= CCoupleProperty::Instance()->GetTotalExperience( pCouple->GetLevel() + 1 ) - pCouple->GetExperience();
			g_dpDBClient.SendQueryAddCoupleExperience( pUser->m_idPlayer, nExperience );
		}
		else
			pUser->AddText( "MAX COUPLE LEVEL" );
	}
	else
		pUser->AddText( "COUPLE NOT FOUND" );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Propose(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	s.GetToken();
	CCoupleHelper::Instance()->OnPropose( pUser, s.token );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Refuse(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CCoupleHelper::Instance()->OnRefuse( pUser );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Couple(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CCoupleHelper::Instance()->OnCouple( pUser );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Decouple(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	CCoupleHelper::Instance()->OnDecouple( pUser );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ClearPropose( CScanner & s )
{
#ifdef __WORLDSERVER
	g_dpDBClient.SendClearPropose();
#endif	// __WORLDSERVER
	return TRUE;
}
/*
BOOL TextCmd_CoupleState( CScanner & s )
{
#ifdef __CLIENT
	CCouple* pCouple	= CCoupleHelper::Instance()->GetCouple();
	if( pCouple )
	{
		char szText[200]	= { 0,};
		const char* pszPartner	= CPlayerDataCenter::GetInstance()->GetPlayerString( pCouple->GetPartner( g_pPlayer->m_idPlayer ) );
		if( !pszPartner )	pszPartner	= "";
		sprintf( szText, "%s is partner.", pszPartner );
		g_WndMng.PutString( szText );
	}
	else
	{
		g_WndMng.PutString( "null couple." );
	}
#endif	// __CLIENT
	return TRUE;
}
*/

BOOL TextCmd_Teleport(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	TCHAR *lpszPlayer = NULL;
	int x, z;

	// 플레이어에게 바로 텔레포트 
	int nTok = scanner.GetToken();
	if( nTok != NUMBER )
	{
		// player
		u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		CUser* pUserTarget = static_cast<CUser*>( prj.GetUserByID( idPlayer ) );
		if( IsValidObj( pUserTarget ) )
		{
			CWorld* pWorld = pUserTarget->GetWorld();
			if( pWorld )
			{
				if( CInstanceDungeonHelper::GetInstance()->IsInstanceDungeon( pWorld->GetID() ) )
					if( pWorld != pUser->GetWorld() || pUser->GetLayer() != pUserTarget->GetLayer() )
						return TRUE;

				pUser->Replace( *pUserTarget, REPLACE_NORMAL );
			}
		}
		else 
		{
		#ifdef _DEBUG
			// 플레이어를 못찾으면 NPC로 찾는다.
			CWorld* pWorld	= pUser->GetWorld();
			CMover* pMover = pWorld->FindMover( scanner.Token );
			if( pMover )
			{
				pUser->Replace( *pMover, REPLACE_NORMAL );
				return TRUE;
			}
		#endif // _DEBUG
			pUser->AddReturnSay( 3, scanner.m_mszToken );
		}
	}
	// 첫번째 파라메타는 월드 번호.
	DWORD dwWorldId = atoi( scanner.token );
	if( CInstanceDungeonHelper::GetInstance()->IsInstanceDungeon( dwWorldId ) )
	{
		if( pUser->GetWorld() && pUser->GetWorld()->GetID() != dwWorldId )
			return TRUE;
	}
	if( g_WorldMng.GetWorldStruct( dwWorldId ) )
	{
		// 두번째 파라메타가 스트링이면 리젼 키
		if( scanner.GetToken() != NUMBER )
		{
			if (const REGIONELEM * pRgnElem = g_WorldMng.GetRevivalPos(dwWorldId, scanner.token)) {
				pUser->Replace(*pRgnElem, REPLACE_NORMAL, nRevivalLayer);
			}
		}
		// 스트링이 아니면 좌표 
		else
		{
			x = atoi( scanner.token );
			z = scanner.GetNumber();

			CWorld* pWorld = g_WorldMng.GetWorld( dwWorldId );
			if( pWorld && pWorld->VecInWorld( (FLOAT)( x ), (FLOAT)( z ) ) && x > 0 && z > 0 )
			{
				int nLayer	= pWorld == pUser->GetWorld()? pUser->GetLayer(): nDefaultLayer;
				pUser->Replace( dwWorldId, D3DXVECTOR3( (FLOAT)x, 0, (FLOAT)z ), REPLACE_NORMAL, nLayer );
			}
		}
	}
#endif // __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_Out(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
//	TCHAR lpszPlayer[MAX_PLAYER];
	scanner.GetToken();

	if( strcmp( pUser->GetName(), scanner.Token) )
	{	
		u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		if( idPlayer > 0 ) {
			g_DPCoreClient.SendKillPlayer( pUser->m_idPlayer, idPlayer );
		}
		else {
//			scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
			pUser->AddReturnSay( 3, scanner.Token );
		}
	}
	else
	{
		pUser->AddReturnSay( 2, " " );  		// 자기 자신에게 명령했다.
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RemoveNpc(CScanner & s) {
#ifdef __WORLDSERVER
	OBJID objid	= (OBJID)s.GetNumber();

	CMover* pMover	= prj.GetMover( objid );
	if( IsValidObj( (CObj*)pMover ) )
	{
		if( pMover->IsNPC() )
			pMover->Delete();
	}
#endif	// __WORLDSERVER
	return TRUE;
}


BOOL TextCmd_CreateItem2(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	s.GetToken();
	ItemProp* pItemProp	= prj.GetItemProp( s.Token );
	if( pItemProp && pItemProp->dwItemKind3 != IK3_VIRTUAL )
	{
		int nRandomOptItemId	= s.GetNumber();
		
		if (const auto * const pRandomOptItem = g_RandomOptItemGen.GetRandomOptItem(nRandomOptItemId)) {
			CItemElem itemElem;
			itemElem.m_dwItemId		= pItemProp->dwID;
			itemElem.m_nItemNum	= 1;
			itemElem.m_nHitPoint	= -1;
			itemElem.SetRandomOpt(pRandomOptItem->nId);
			pUser->CreateItem( &itemElem );
		}
	}
	return TRUE;
#else	// __WORLDSERVER
	return TRUE;
#endif	// __WORLDSERVER
}

BOOL TextCmd_CreateItem(CScanner & scanner, CUser * pUser) {
	scanner.GetToken();

#ifdef __CLIENT
	// 클라이언트에서
	if( scanner.tok == FINISHED )
	{
		if( g_WndMng.GetWndBase( APP_ADMIN_CREATEITEM ) == NULL )
		{
			CWndAdminCreateItem* pWndAdminCreateItem = new CWndAdminCreateItem;
			pWndAdminCreateItem->Initialize();
		}
		return FALSE;
	}
	return TRUE;
#else   // __CLIENT
	DWORD dwNum;
	DWORD dwCharged		= 0;
	ItemProp* pProp	= NULL;

	if( scanner.tokenType == NUMBER )
		pProp	= prj.GetItemProp( _ttoi( scanner.Token ) );
	else
		pProp	= prj.GetItemProp( scanner.Token );

	if( pProp && pProp->dwItemKind3 != IK3_VIRTUAL )
	{
		if( pProp->dwItemKind3 == IK3_EGG && pProp->dwID != II_PET_EGG )	// 리어펫을 생성하려고 할 경우 "알"인 경우만 생성 가능하다.
			return TRUE;

		dwNum	= scanner.GetNumber();
		dwNum	= ( dwNum == 0? 1: dwNum );
		
		CItemElem itemElem;
		itemElem.m_dwItemId		= pProp->dwID;
		itemElem.m_nItemNum		= (short)( dwNum );
		itemElem.m_nHitPoint	= -1;

		pUser->CreateItem( &itemElem );
	}
#endif	// !__CLIENT 
	return TRUE;
}

BOOL TextCmd_LocalEvent( CScanner & s )
{
#ifdef __WORLDSERVER
	int id	= s.GetNumber();
	if( id != EVE_18 )	// 이 식별자는 18세 서버를 나타내는 식별자므로 운영자의 명령에 의한 설정 불가
	{
		BYTE nState		= (BYTE)s.GetNumber();
		if( g_eLocal.SetState( id, nState ) )		g_UserMng.AddSetLocalEvent( id, nState );
	}
#endif	// __WORLDSERVER
	return TRUE;
}

// 무버이름 캐릭터키 갯수 선공 
BOOL TextCmd_CreateChar(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	D3DXVECTOR3 vPos = pUser->GetPos();
	CWorld* pWorld	= pUser->GetWorld();
	
	MoverProp* pMoverProp	= NULL;

	scanner.GetToken();
	if( scanner.tokenType == NUMBER ) 
	{
		DWORD dwID	= _ttoi( scanner.Token );
		pMoverProp = prj.GetMoverPropEx( dwID );
	}
	else
		pMoverProp	= prj.GetMoverProp( scanner.Token );

	scanner.GetToken();
	CString strName = scanner.Token;

	if( pMoverProp && pMoverProp->dwID != 0 )
	{
		DWORD dwNum	= scanner.GetNumber();
		if( dwNum > 100 ) dwNum = 100;
		if( dwNum == 0 ) dwNum = 1;

		BOOL bActiveAttack = scanner.GetNumber();
		for( DWORD dw = 0; dw < dwNum; dw++ )
		{
			CMover* pMover = (CMover*)CreateObj( OT_MOVER, pMoverProp->dwID );
			if( NULL == pMover ) return FALSE; // ASSERT( pObj );
			strcpy( pMover->m_szCharacterKey, strName );
			pMover->InitNPCProperty();
			pMover->InitCharacter( pMover->GetCharacter() );
			pMover->SetPos( vPos );
			pMover->InitMotion( MTI_STAND );
			pMover->UpdateLocalMatrix();
			if( bActiveAttack )
				pMover->m_bActiveAttack = bActiveAttack;
			pWorld->ADDOBJ( pMover, TRUE, pUser->GetLayer() );
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_CreateCtrl(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	D3DXVECTOR3 vPos	= pUser->GetPos();
	CWorld* pWorld	= pUser->GetWorld();

	DWORD dwID	= s.GetNumber();
	
	if( dwID == 0 )
		return FALSE;

	CCtrl* pCtrl	= (CCtrl*)CreateObj( OT_CTRL, dwID );
	if( !pCtrl )
		return FALSE;

	pCtrl->SetPos( vPos );
	pWorld->ADDOBJ( pCtrl, TRUE, pUser->GetLayer() );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_PostMail( CScanner & s )
{
#ifdef __CLIENT
	DWORD dwIndex	= s.GetNumber();
	short nItemNum	= s.GetNumber();
	char lpszReceiver[MAX_PLAYER]	= { 0, };
	s.GetToken();
	lstrcpy( lpszReceiver, s.Token );
	int nGold	= s.GetNumber();
	char lpszTitle[MAX_MAILTITLE]	= { 0, };
	s.GetToken();
	lstrcpy( lpszTitle, s.Token );
	char lpszText[MAX_MAILTEXT]	= { 0, };
	s.GetToken();
	lstrcpy( lpszText, s.Token );

	CItemElem* pItemElem	= g_pPlayer->m_Inventory.GetAt( dwIndex );
	if( pItemElem )
	{
		g_DPlay.SendQueryPostMail( (BYTE)( pItemElem->m_dwObjId ), nItemNum, lpszReceiver, nGold, lpszTitle, lpszText );
	}
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_RemoveMail( CScanner & s )
{
#ifdef __CLIENT
	u_long nMail	= s.GetNumber();
	g_DPlay.SendQueryRemoveMail( nMail );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_GetMailItem( CScanner & s )
{
#ifdef __CLIENT
	u_long nMail	= s.GetNumber();
	g_DPlay.SendQueryGetMailItem( nMail );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_GetMailGold( CScanner & s )
{
#ifdef __CLIENT
	u_long nMail	= s.GetNumber();
	g_DPlay.SendQueryGetMailGold( nMail );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_Lua(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	s.GetToken();
	s.Token.MakeLower();
		
	if( s.Token == "event" )
	{
		pUser->AddText( "Event.lua Reload..." );
		Error( "Event.lua Reload... - %s", pUser->GetName() );
		g_dpDBClient.SendEventLuaChanged();
	}
	else if( s.Token == "ms" )
	{
		CMonsterSkill::GetInstance()->LoadScript();
	}
	else if( s.Token == "rr" )
	{
		CRainbowRaceMng::GetInstance()->LoadScript();
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_LuaEventList(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	prj.m_EventLua.GetAllEventList( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_LuaEventInfo(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	prj.m_EventLua.GetEventInfo( pUser, s.GetNumber() );
#endif // __WORLDSERVER
	return TRUE;
}

#ifdef __JEFF_9_20
BOOL TextCmd_Mute(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	s.GetToken();
	if( s.tok == FINISHED )
		return TRUE;
	u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
	
	if( idPlayer == 0 )
	{
		pUser->AddText( "player not found" );
		return TRUE;
	}
	CUser* pTarget	= g_UserMng.GetUserByPlayerID( idPlayer );
	if( IsValidObj( pTarget ) )
	{
		DWORD dwMute	= (DWORD)s.GetNumber();
		if( s.tok == FINISHED )
			return TRUE;
		pTarget->m_dwMute	= dwMute;
	}
#endif	// __WORLDSERVER
	return TRUE;
}
#endif	// __JEFF_9_20

BOOL TextCmd_AngelExp(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int nAddExp	= s.GetNumber();

	if( pUser->HasBuffByIk3( IK3_ANGEL_BUFF ) )
	{
		int nAngel = 100;
		IBuff* pBuff	= pUser->m_buffs.GetBuffByIk3( IK3_ANGEL_BUFF );
		WORD wId	= ( pBuff? pBuff->GetId(): 0 );
		if( wId )
		{
			ItemProp* pItemProp = prj.GetItemProp( wId );
			if( pItemProp )
				nAngel = (int)( (float)pItemProp->nAdjParamVal[0]);
		}
		if( nAngel <= 0 || 100 < nAngel  )
			nAngel = 100;

		EXPINTEGER nMaxAngelExp = prj.m_aExpCharacter[pUser->m_nAngelLevel].nExp1 / 100 * nAngel;
		if( pUser->m_nAngelExp < nMaxAngelExp )
		{
			pUser->m_nAngelExp += nAddExp;
			BOOL bAngelComplete = FALSE;
			if( pUser->m_nAngelExp >= nMaxAngelExp )
			{
				pUser->m_nAngelExp = nMaxAngelExp;
				bAngelComplete = TRUE;
			}
			pUser->AddAngelInfo( bAngelComplete );
		}
	}
#endif // __WORLDSERVER
	return TRUE;	
}

#ifdef __EVENT_1101
BOOL TextCmd_CallTheRoll(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int nCount	= s.GetNumber();
#ifdef __EVENT_1101_2
	pUser->m_nEventFlag = 0;
#else // __EVENT_1101_2
	pUser->m_dwEventFlag	= 0;
#endif // __EVENT_1101_2
	pUser->m_dwEventTime	= 0;
	pUser->m_dwEventElapsed	= 0;
	for(  int i = 0; i < nCount; i++ )
#ifdef __EVENT_1101_2
		pUser->SetEventFlagBit( 62 - i );
#else // __EVENT_1101_2
		pUser->SetEventFlagBit( 31 - i );
#endif // __EVENT_1101_2
#endif	// __WORLDSERVER
	return TRUE;
}

#endif	// __EVENT_1101

BOOL TextCmd_CreateNPC(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	D3DXVECTOR3 vPos	= pUser->GetPos();
	CWorld* pWorld	= pUser->GetWorld();

	MoverProp* pMoverProp	= NULL;

	scanner.GetToken();
	if( scanner.tokenType == NUMBER ) 
	{
		DWORD dwID	= _ttoi( scanner.Token );
		pMoverProp = prj.GetMoverPropEx( dwID );
	}
	else
		pMoverProp	= prj.GetMoverProp( scanner.Token );

	CString strName = scanner.Token;

	if( pMoverProp && pMoverProp->dwID != 0 )
	{
		if( pMoverProp->dwAI != AII_MONSTER
			&& pMoverProp->dwAI != AII_CLOCKWORKS
			&& pMoverProp->dwAI != AII_BIGMUSCLE
			&& pMoverProp->dwAI != AII_KRRR
			&& pMoverProp->dwAI != AII_BEAR
			&& pMoverProp->dwAI != AII_METEONYKER
		)
			return TRUE;

		DWORD dwNum	= scanner.GetNumber();
		if( dwNum > 100 ) dwNum = 100;
		if( dwNum == 0 ) dwNum = 1;

		BOOL bActiveAttack = scanner.GetNumber();
		for( DWORD dw = 0; dw < dwNum; dw++ )
		{
			CObj* pObj	= CreateObj( OT_MOVER, pMoverProp->dwID );
			if( NULL == pObj )	
				return FALSE;	
			pObj->SetPos( vPos );
			pObj->InitMotion( MTI_STAND );
			pObj->UpdateLocalMatrix();

			if( bActiveAttack )
				((CMover*)pObj)->m_bActiveAttack = bActiveAttack;
			
			((CMover*)pObj)->SetGold(((CMover*)pObj)->GetLevel()*15);  // 몬스터 생성시 기본 페냐를 설정
			pWorld->ADDOBJ( pObj, TRUE, pUser->GetLayer() );
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Invisible(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->m_dwMode |= TRANSPARENT_MODE;
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_NoInvisible(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->m_dwMode &= (~TRANSPARENT_MODE);
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_NoUndying(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->m_dwMode &= (~MATCHLESS_MODE);
	pUser->m_dwMode &= (~MATCHLESS2_MODE);
	g_UserMng.AddModifyMode( pUser );
#endif
	return TRUE;
}

// exp상승 금지 명령. 토글방식으로 동작.
BOOL TextCmd_ExpUpStop(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( pUser->m_dwMode & MODE_EXPUP_STOP )
		pUser->m_dwMode &= (~MODE_EXPUP_STOP);
	else
		pUser->m_dwMode |= MODE_EXPUP_STOP;
	
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	
	return TRUE;
}
BOOL TextCmd_PartyInvite(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	MoverProp* pMoverProp = NULL;
	scanner.GetToken();
	u_long uidPlayer	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
	if( 0 < uidPlayer )
	{
		CUser* pUser2	= g_UserMng.GetUserByPlayerID( uidPlayer );	
		if( IsValidObj( pUser2 ) )
			g_DPSrvr.InviteParty( pUser->m_idPlayer, pUser2->m_idPlayer );
		else
			pUser->AddDefinedText( TID_DIAG_0060, "\"%s\"", scanner.Token );
	}
	else
	{
		pUser->AddDefinedText( TID_DIAG_0061, "\"%s\"", scanner.Token );
	}
#endif // __WORLDSERVER

	return TRUE;
}
BOOL TextCmd_GuildInvite(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	MoverProp* pMoverProp = NULL;
	scanner.GetToken();
	u_long uidPlayer	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
	if( 0 < uidPlayer )
	{
		CUser* pUser2	= g_UserMng.GetUserByPlayerID( uidPlayer );	
		if( IsValidObj( pUser2 ) )
		{
			g_DPSrvr.InviteCompany( pUser, pUser2->GetId() );
		}
		else
			pUser->AddDefinedText( TID_DIAG_0060, "\"%s\"", scanner.Token );
	}
	else
	{
		pUser->AddDefinedText( TID_DIAG_0061, "\"%s\"", scanner.Token );
	}
#endif // __WORLDSERVER

	return TRUE;
}

BOOL TextCmd_Undying(CScanner & scanner, CUser * pUser) {
	pUser->m_dwMode &= (~MATCHLESS2_MODE);
	pUser->m_dwMode |= MATCHLESS_MODE;
#ifdef __WORLDSERVER
	g_UserMng.AddModifyMode( pUser );
#endif
	return TRUE;
}

BOOL TextCmd_Undying2(CScanner &, CUser * pUser) {
	pUser->m_dwMode &= (~MATCHLESS_MODE);
	pUser->m_dwMode |= MATCHLESS2_MODE;
#ifdef __WORLDSERVER
	g_UserMng.AddModifyMode( pUser );
#endif
	return TRUE;
}


BOOL TextCmd_NoDisguise(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->NoDisguise( );
	g_UserMng.AddNoDisguise( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

#ifdef __WORLDSERVER
BOOL DoDisguise( CUser* pUser, DWORD dwIndex )
{
	pUser->Disguise( dwIndex );
	g_UserMng.AddDisguise( pUser, dwIndex );
	return TRUE;
}
#endif // __WORLDSERVER


BOOL TextCmd_Disguise(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	MoverProp* pMoverProp = NULL;
	scanner.GetToken();
	if( scanner.tokenType == NUMBER ) 
	{
		DWORD dwID	= _ttoi( scanner.Token );
		pMoverProp = prj.GetMoverPropEx( dwID );
	}
	else
		pMoverProp	= prj.GetMoverProp( scanner.Token );

	if( pMoverProp )
		DoDisguise( pUser, pMoverProp->dwID );
#endif
	return TRUE;
}
BOOL TextCmd_Freeze(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();

	if( strcmp( pUser->GetName(), scanner.Token) )
	{
		u_long idFrom, idTo;
		idFrom	= CPlayerDataCenter::GetInstance()->GetPlayerId( (char*)pUser->GetName() );
		idTo	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		if( idFrom > 0 && idTo > 0 ) 
		{
			// 1 : 추가 m_dwMode
			g_DPCoreClient.SendModifyMode( DONMOVE_MODE, true, idFrom, idTo );					
		}
		else 
		{
//			scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
			pUser->AddReturnSay( 3, scanner.Token );
		}
	}
	else
	{
		pUser->AddReturnSay( 2, " " );		// 자기 자신에게 명령했다.
	}
#endif	// __WORLDSERVER	
	return TRUE;
}

BOOL TextCmd_NoFreeze(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();
	if( strcmp( pUser->GetName(), scanner.Token) )
	{
		u_long idFrom, idTo;
		idFrom	= CPlayerDataCenter::GetInstance()->GetPlayerId( (char*)pUser->GetName() );
		idTo	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		if( idFrom > 0 && idTo > 0 ) 
		{
			g_DPCoreClient.SendModifyMode( DONMOVE_MODE, false, idFrom, idTo );
		}
		else 
		{
			//scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
			pUser->AddReturnSay( 3, scanner.Token );
		}
	}
	else
	{
		pUser->AddReturnSay( 2, " " );		// 자기 자신에게 명령했다.
	}
#endif	// __WORLDSERVER	
	return TRUE;
}

BOOL TextCmd_Talk(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();

	u_long idFrom, idTo;
	idFrom	= CPlayerDataCenter::GetInstance()->GetPlayerId( (char*)pUser->GetName() );
	idTo	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
	if( idFrom > 0 && idTo > 0 ) 
	{
		g_DPCoreClient.SendModifyMode( DONTALK_MODE, false, idFrom, idTo );
	}
	else 
	{
		//scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
		pUser->AddReturnSay( 3, scanner.Token );
	}
#endif	// __WORLDSERVER	
	return TRUE;
}

BOOL TextCmd_NoTalk(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	
	scanner.GetToken();

	{
		u_long idFrom, idTo;
		idFrom	= CPlayerDataCenter::GetInstance()->GetPlayerId( (char*)pUser->GetName() );
		idTo	= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
		if( idFrom > 0 && idTo > 0 ) 
		{
			g_DPCoreClient.SendModifyMode( DONTALK_MODE, true, idFrom, idTo );	// 1 : 추가
		}
		else 
		{
			//scanner.Token라는 이름을 가진 사용자는 이 게임에 존재하지 않는다.
			pUser->AddReturnSay( 3, scanner.Token );
		}
	}
#endif	// __WORLDSERVER	
	return TRUE;
}

BOOL TextCmd_GetGold(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	int nGold = scanner.GetNumber();	
	pUser->AddGold( nGold );
#endif	// __WORLDSERVER	
	return TRUE;
}

// /간접 npcId "대사"
BOOL TextCmd_indirect( CScanner& scanner )         
{ 
#ifdef __WORLDSERVER
	DWORD dwIdNPC = scanner.GetNumber();
	TCHAR szString[ 1024 ];

	scanner.GetLastFull();
	if( strlen( scanner.token ) > 260 )
		return TRUE;
	strcpy( szString, scanner.token );
	StringTrimRight( szString );

	if( szString[ 0 ] )
	{
		CMover* pMover = prj.GetMover( dwIdNPC );
		if( pMover )
			g_UserMng.AddChat( (CCtrl*)pMover, (LPCSTR)szString );
	}
#else // __WORLDSERVER
	scanner.GetToken();
	if( g_pPlayer->IsAuthHigher( AUTH_GAMEMASTER ) )
	{
		if( scanner.tok == FINISHED )
		{
			if( g_WndMng.GetWndBase( APP_ADMIN_INDIRECT_TALK ) == NULL )
			{
				CWndIndirectTalk* pWndIndirectTalk = new CWndIndirectTalk;
				pWndIndirectTalk->Initialize();
			}
			return FALSE;
		}
	}
#endif // __WORLDSERVER
	return TRUE;
}
/*
BOOL TextCmd_sbreport( CScanner & scanner )
{
#ifdef __WORLDSERVER
	if( !g_eLocal.GetState( EVE_SCHOOL ) )
		return FALSE;
	return CEveSchool::GetInstance()->Report();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_sbready( CScanner & scanner )
{
#ifdef __WORLDSERVER
	if( !g_eLocal.GetState( EVE_SCHOOL ) )
		return FALSE;
	return CEveSchool::GetInstance()->Ready();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_sbstart( CScanner & scanner )
{
#ifdef __WORLDSERVER
	if( !g_eLocal.GetState( EVE_SCHOOL ) )
		return FALSE;
	return CEveSchool::GetInstance()->Start();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_sbstart2( CScanner & scanner )
{
#ifdef __WORLDSERVER
	if( !g_eLocal.GetState( EVE_SCHOOL ) )
		return FALSE;
	return CEveSchool::GetInstance()->Start2();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_sbend( CScanner & scanner )
{
#ifdef __WORLDSERVER
	if( !g_eLocal.GetState( EVE_SCHOOL ) )
		return FALSE;
	return CEveSchool::GetInstance()->End();
#endif	// __WORLDSERVER
	return TRUE;
}
*/

BOOL TextCmd_ItemMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetMode( ITEM_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ItemNotMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetNotMode( ITEM_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_AttackMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetMode( NO_ATTACK_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_EscapeReset(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	CMover* pMover = prj.GetUserByID( scanner.GetNumber() );
	if( IsValidObj( pMover ) ) {
		pMover->SetSMMode( SM_ESCAPE, 0 );
	}
	else {
		pUser->SetSMMode( SM_ESCAPE, 0 );
	}
#else	// __WORLDSERVER
#ifdef __CLIENT
	CWorld* pWorld	= g_WorldMng.Get();
	CObj*	pObj;
	
	pObj = pWorld->GetObjFocus();
	if( pObj && pObj->GetType() == OT_MOVER && ((CMover*)pObj)->IsPlayer() )
	{
		CMover* pMover = (CMover*)pObj;
		CString strSend;
		if( pMover->IsPlayer() )
		{
			strSend.Format( "/EscapeReset %d", pMover->m_idPlayer  );
		}

		g_DPlay.SendChat( (LPCSTR)strSend );

		return FALSE;
	}
#endif //__CLIENT
#endif	// __WORLDSERVER
	return TRUE;
}



BOOL TextCmd_AttackNotMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetNotMode( NO_ATTACK_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_CommunityMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetMode( COMMUNITY_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_CommunityNotMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetNotMode( COMMUNITY_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ObserveMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetMode( OBSERVE_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ObserveNotMode(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->SetNotMode( OBSERVE_MODE );
	g_UserMng.AddModifyMode( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_Onekill(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->m_dwMode |= ONEKILL_MODE;
	g_UserMng.AddModifyMode( pUser );
#else // __WORLDSERVER
#ifndef __CLIENT
	CMover* pUser = (CMover*)scanner.dwValue;
	pUser->m_dwMode |= ONEKILL_MODE;
#endif
#endif
	return TRUE;
}
BOOL TextCmd_Position( CScanner& scanner )          
{ 
#ifdef __CLIENT
	CString string;
	D3DXVECTOR3 vPos = g_pPlayer->GetPos();
	//string.Format( "현재좌표 : x = %f, y = %f, z = %f", vPos.x, vPos.y, vPos.z );
	string.Format( prj.GetText(TID_GAME_NOWPOSITION), vPos.x, vPos.y, vPos.z );
	g_WndMng.PutString( string, NULL, prj.GetTextColor( TID_GAME_NOWPOSITION ) );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_NoOnekill(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->m_dwMode &= (~ONEKILL_MODE);
	g_UserMng.AddModifyMode( pUser );
#else // __WORLDSERVER
#ifndef __CLIENT
	CMover* pUser = (CMover*)scanner.dwValue;
	pUser->m_dwMode &= (~ONEKILL_MODE);
#endif
#endif
	return TRUE;
}
BOOL TextCmd_ip(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	scanner.GetToken();

	u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( scanner.token );
	if( idPlayer > 0 )
		g_DPCoreClient.SendGetPlayerAddr( pUser->m_idPlayer, idPlayer );
	else 
		pUser->AddReturnSay( 3, scanner.Token );
#else	// __WORLDSERVER
	#ifdef __CLIENT
	CWorld* pWorld	= g_WorldMng.Get();
	CObj*	pObj;
	
	pObj = pWorld->GetObjFocus();
	if( pObj && pObj->GetType() == OT_MOVER )
	{
		CMover* pMover = (CMover*)pObj;
		CString strSend;
		if( pMover->IsPlayer() )
		{
			strSend.Format( "/ip %s", pMover->GetName() );
		}
		else
		{
			strSend.Format( "%s", scanner.m_pBuf );
		}
		g_DPlay.SendChat( (LPCSTR)strSend );
		return FALSE;
	}
	#endif //__CLIENT
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_userlist(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	g_DPCoreClient.SendGetCorePlayer( pUser->m_idPlayer );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_count(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	g_DPCoreClient.SendGetPlayerCount( pUser->m_idPlayer );

	char szCount[128]	= { 0, };
	sprintf( szCount, "Players online: %d", g_UserMng.GetCount() );
	pUser->AddText( szCount );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_System( CScanner & scanner )
{
#ifdef __WORLDSERVER
	CHAR szString[512] = "";

	scanner.GetLastFull();
	if( strlen( scanner.token ) >= 512 )
		return TRUE;
	strcpy( szString, scanner.token );
	StringTrimRight( szString );

	g_DPCoreClient.SendSystem( szString );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_LoadScript( CScanner & scanner )
{
#ifdef __WORLDSERVER
	#if defined(__REMOVE_SCIRPT_060712)
		if( CWorldDialog::GetInstance().Reload() == FALSE )
			Error("WorldScript.dll reload error");
	#else
		CUser* pUser	 = (CUser*)scanner.dwValue;
		pUser->GetWorld()->LoadAllMoverDialog();
	#endif
#endif
	return TRUE;
}

BOOL TextCmd_FallSnow( CScanner & scanner )
{
#ifdef __WORLDSERVER
	g_DPCoreClient.SendFallSnow();
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_FallRain( CScanner & scanner )
{
#ifdef __WORLDSERVER
	g_DPCoreClient.SendFallRain();
#endif // __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_StopSnow( CScanner & scanner )
{
#ifdef __WORLDSERVER
	g_DPCoreClient.SendStopSnow();
#endif // __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_StopRain( CScanner & scanner )
{
#ifdef __WORLDSERVER
	g_DPCoreClient.SendStopRain();
#endif // __WORLDSERVER
	return TRUE;
}

#ifdef __CLIENT

BOOL TextCmd_tradeagree( CScanner & scanner )
{
	g_Option.m_bTrade = 1;
	g_WndMng.PutString( prj.GetText( TID_GAME_TRADEAGREE ), NULL, prj.GetTextColor( TID_GAME_TRADEAGREE ) );
	return TRUE;
}
BOOL TextCmd_traderefuse( CScanner & scanner )
{
	g_Option.m_bTrade = 0;
	g_WndMng.PutString( prj.GetText( TID_GAME_TRADEREFUSE ), NULL, prj.GetTextColor( TID_GAME_TRADEREFUSE ) );
	return TRUE;
}
BOOL TextCmd_whisperagree( CScanner & scanner )
{
	g_Option.m_bSay = 1;
	g_WndMng.PutString( prj.GetText( TID_GAME_WHISPERAGREE ), NULL, prj.GetTextColor( TID_GAME_WHISPERAGREE ) );
	return TRUE;
}
BOOL TextCmd_whisperrefuse( CScanner & scanner )
{
	g_Option.m_bSay = 0;
	g_WndMng.PutString( prj.GetText( TID_GAME_WHISPERREFUSE ), NULL, prj.GetTextColor( TID_GAME_WHISPERREFUSE ) );
	return TRUE;
}
BOOL TextCmd_messengeragree( CScanner & scanner )
{
	g_Option.m_bMessenger = 1;
	g_WndMng.PutString( prj.GetText( TID_GAME_MSGERAGREE ), NULL, prj.GetTextColor( TID_GAME_MSGERAGREE ) );
	return TRUE;
}
BOOL TextCmd_messengerrefuse( CScanner & scanner )
{
	g_Option.m_bMessenger = 0;
	g_WndMng.PutString( prj.GetText( TID_GAME_MSGERREFUSE ), NULL, prj.GetTextColor( TID_GAME_MSGERREFUSE ) );
	return TRUE;
}
BOOL TextCmd_stageagree( CScanner & scanner )
{
	g_Option.m_bParty = 1;
	g_WndMng.PutString( prj.GetText( TID_GAME_STAGEAGREE ), NULL, prj.GetTextColor( TID_GAME_STAGEAGREE ) );
	return TRUE;
}
BOOL TextCmd_stagerefuse( CScanner & scanner )
{
	g_Option.m_bParty = 0;
	g_WndMng.PutString( prj.GetText( TID_GAME_STAGEREFUSE ), NULL, prj.GetTextColor( TID_GAME_STAGEREFUSE ) );
	return TRUE;
}
BOOL TextCmd_connectagree( CScanner & scanner )
{
	g_Option.m_bMessengerJoin = 1;
	g_WndMng.PutString( prj.GetText( TID_GAME_CONNAGREE ), NULL, prj.GetTextColor( TID_GAME_CONNAGREE ) );
	return TRUE;
}
BOOL TextCmd_connectrefuse( CScanner & scanner )
{
	g_Option.m_bMessengerJoin = 0;
	g_WndMng.PutString( prj.GetText( TID_GAME_CONNREFUSE ), NULL, prj.GetTextColor( TID_GAME_CONNREFUSE ) );
	return TRUE;
}
BOOL TextCmd_shoutagree( CScanner & scanner )
{
	g_Option.m_bShout = 1;
	g_WndMng.PutString( prj.GetText( TID_GAME_SHOUTAGREE ), NULL, prj.GetTextColor( TID_GAME_SHOUTAGREE ) );
	return TRUE;
}
BOOL TextCmd_shoutrefuse( CScanner & scanner )
{
	g_Option.m_bShout = 0;
	g_WndMng.PutString( prj.GetText( TID_GAME_SHOUTREFUSE ), NULL, prj.GetTextColor( TID_GAME_SHOUTREFUSE ) );
	return TRUE;
}

#ifdef __YS_CHATTING_BLOCKING_SYSTEM
BOOL TextCmd_BlockUser( CScanner & scanner )
{
	if( prj.m_setBlockedUserID.size() >= CProject::BLOCKING_NUMBER_MAX )
	{
		// 차단 가능한 유저 수를 초과하였습니다. 차단 목록을 정리한 후에 다시 사용해 주십시오.
		g_WndMng.PutString( prj.GetText( TID_GAME_ERROR_FULL_BLOCKED_USER_LIST ), NULL, prj.GetTextColor( TID_GAME_ERROR_FULL_BLOCKED_USER_LIST ) );
		return FALSE;
	}
	scanner.GetToken();
	CString strUserName = scanner.token;
	if( strUserName == _T( "" ) )
	{
		// 존재하지 않은 아이디입니다. 아이디를 확인해 주십시오.
		g_WndMng.PutString( prj.GetText( TID_GAME_ERROR_INVALID_USER_ID ), NULL, prj.GetTextColor( TID_GAME_ERROR_INVALID_USER_ID ) );
		return FALSE;
	}
	if( g_pPlayer && g_pPlayer->GetName( TRUE ) == strUserName )
	{
		// 자기 캐릭터는 차단할 수 없습니다. 아이디를 확인해 주십시오.
		g_WndMng.PutString( prj.GetText( TID_GAME_ERROR_MY_CHARACTER_CANT_BLOCKING ), NULL, prj.GetTextColor( TID_GAME_ERROR_MY_CHARACTER_CANT_BLOCKING ) );
		return FALSE;
	}
	const auto BlockedUserIterator = prj.m_setBlockedUserID.find( strUserName );
	if( BlockedUserIterator != prj.m_setBlockedUserID.end() )
	{
		// 이미 채팅 차단되어 있는 대상입니다.
		g_WndMng.PutString( prj.GetText( TID_GAME_ERROR_ALREADY_BLOCKED ), NULL, prj.GetTextColor( TID_GAME_ERROR_ALREADY_BLOCKED ) );
	}
	else
	{
		prj.m_setBlockedUserID.insert( strUserName );

		if( g_WndMng.m_pWndChattingBlockingList )
		{
			g_WndMng.m_pWndChattingBlockingList->UpdateInformation();
		}

		CString strMessage = _T( "" );
		// %s 님의 채팅을 차단하였습니다.
		strMessage.Format( prj.GetText( TID_GAME_USER_CHATTING_BLOCKING ), strUserName );
		g_WndMng.PutString( strMessage, NULL, prj.GetTextColor( TID_GAME_USER_CHATTING_BLOCKING ) );
	}
	return TRUE;
}

BOOL TextCmd_CancelBlockedUser( CScanner & scanner )
{
	scanner.GetToken();
	CString strUserName = scanner.token;
	const auto BlockedUserIterator = prj.m_setBlockedUserID.find( strUserName );
	if( BlockedUserIterator != prj.m_setBlockedUserID.end() )
	{
		prj.m_setBlockedUserID.erase( strUserName );

		if( g_WndMng.m_pWndChattingBlockingList )
		{
			g_WndMng.m_pWndChattingBlockingList->UpdateInformation();
		}

		CString strMessage = _T( "" );
		// %s 님의 채팅 차단을 해제하였습니다.
		strMessage.Format( prj.GetText( TID_GAME_USER_CHATTING_UNBLOCKING ), strUserName );
		g_WndMng.PutString( strMessage, NULL, prj.GetTextColor( TID_GAME_USER_CHATTING_UNBLOCKING ) );
	}
	else
	{
		// 채팅 차단 목록에 없는 대상입니다.
		g_WndMng.PutString( prj.GetText( TID_GAME_ERROR_THERE_IS_NO_BLOCKED_TARGET ), NULL, prj.GetTextColor( TID_GAME_ERROR_THERE_IS_NO_BLOCKED_TARGET ) );
	}
	return TRUE;
}

BOOL TextCmd_IgnoreList( CScanner & scanner )
{
	if( g_WndMng.m_pWndChattingBlockingList )
	{
		SAFE_DELETE( g_WndMng.m_pWndChattingBlockingList );
	}

	g_WndMng.m_pWndChattingBlockingList = new CWndChattingBlockingList;
	if( g_WndMng.m_pWndChattingBlockingList )
	{
		g_WndMng.m_pWndChattingBlockingList->Initialize();
	}

	return TRUE;
}
#endif // __YS_CHATTING_BLOCKING_SYSTEM

#endif // __CLIENT

BOOL TextCmd_QuestState(CScanner & s, CUser * pAdmin) {
#ifdef __WORLDSERVER
	CUser* pUser	= NULL;
	const QuestId nQuest	= QuestId::From(s.GetNumber());
	const int nState = s.GetNumber();
	s.GetToken();
	if( s.tok != FINISHED )
	{
		u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
		if( idPlayer )
			pUser	= g_UserMng.GetUserByPlayerID( idPlayer );
		if( pUser == NULL )
		{
			pAdmin->AddDefinedText( TID_DIAG_0061, "%s", s.Token );
			return TRUE;
		}
	}
	else
	{
		pUser	= pAdmin;
	}
	if( nState >= QS_BEGIN && nState < QS_END )
	{
		QUEST quest;
		if( pUser->SetQuest( nQuest, nState, &quest ) )
		{
			pUser->AddSetQuest( &quest );

			char pszComment[100]	= { 0, };
			sprintf( pszComment, "%s %d", pAdmin->GetName(), nState );
			g_dpDBClient.CalluspLoggingQuest(  pUser->m_idPlayer, nQuest, 11, pszComment);
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_BeginQuest(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	const QuestId nQuest = QuestId::From(s.GetNumber());
	QUEST quest;
	if( pUser->SetQuest( nQuest, 0, &quest ) )
		pUser->AddSetQuest( &quest );
#endif
	return TRUE;
}
BOOL TextCmd_EndQuest(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	const QuestId nQuest = QuestId::From(s.GetNumber());
	QUEST quest;
	if( pUser->SetQuest( nQuest, QS_END, &quest ) )
		pUser->AddSetQuest( &quest );
#endif
	return TRUE;
}
// 지정한 것, 현재와 완료 다 뒤져서 삭제 
BOOL TextCmd_RemoveQuest(CScanner & s, CUser * pAdmin) {
#ifdef __WORLDSERVER
	CUser* pUser	= NULL;
	const QuestId nQuest	= QuestId::From(s.GetNumber());
	s.GetToken();
	if( s.tok != FINISHED )
	{
		u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
		if( idPlayer )
			pUser	= g_UserMng.GetUserByPlayerID( idPlayer );
		if( pUser == NULL )
		{
			pAdmin->AddDefinedText( TID_DIAG_0061, "%s", s.Token );
			return TRUE;
		}
	}
	else
	{
		pUser	= pAdmin;
	}

	const auto questState = pUser->GetQuestState(nQuest);
	char pszComment[100]	= { 0, };
	sprintf(pszComment, "%s %d", pAdmin->GetName(), questState.value_or(-1));
	g_dpDBClient.CalluspLoggingQuest(pUser->m_idPlayer, nQuest, 40, pszComment);

	pUser->RemoveQuest( nQuest );
	pUser->AddRemoveQuest( nQuest );
#endif	// __WORLDSERVER
	return TRUE;
}
// 현재, 완료 모두 삭제 
BOOL TextCmd_RemoveAllQuest(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int nQuest = s.GetNumber();
	pUser->RemoveAllQuest();
	pUser->AddRemoveAllQuest();
#endif
	return TRUE;
}
// 완료만 삭제 
BOOL TextCmd_RemoveCompleteQuest(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int nQuest = s.GetNumber();
	pUser->RemoveCompleteQuest();
	pUser->AddRemoveCompleteQuest();
#endif
	return TRUE;
}

BOOL TextCmd_PvpParam(CScanner & scanner, CUser * pUser) {
#if defined(__WORLDSERVER)
	int	nFame       = scanner.GetNumber();
	int nSlaughter  = scanner.GetNumber();

	pUser->m_nFame  = nFame;
	g_UserMng.AddSetFame( pUser, nFame );

#endif
	return TRUE;
}
BOOL TextCmd_PKParam(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	int		nPKValue		= scanner.GetNumber();
	int		nPKPropensity	= scanner.GetNumber();

	if( nPKValue >= 0 )
	{
		pUser->SetPKValue( nPKValue );
		pUser->AddPKValue();
		pUser->CheckHonorStat();
		pUser->AddHonorListAck();
		g_UserMng.AddHonorTitleChange( pUser, pUser->m_nHonor);
	}

	if( nPKPropensity >= 0 )
	{
		pUser->SetPKPropensity( nPKPropensity );
		g_UserMng.AddPKPropensity( pUser );
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_TransyItemList( CScanner& scanner, CUser * pUser )
{
#ifdef __CLIENT
	CString szMsg;
	scanner.GetToken();
	if( scanner.tokenType == STRING )
	{
		szMsg = "Wait : Write " + scanner.Token;
		g_WndMng.PutString( szMsg );

		std::string toOutput = ItemMorph::BuildListOfExistingMorphs();

		FILEOUT(scanner.Token.GetString(), "%s", toOutput.c_str());

		szMsg = "Finish : Finish " + scanner.Token;
		g_WndMng.PutString( szMsg );
	}
	else
	{
		g_WndMng.PutString( "Error : Ex) /TransyItemList Transy.txt&" );
	}
#endif // __CLIENT
	return TRUE;
}

#ifdef _DEBUG
BOOL TextCmd_LoadToolTipColor( CScanner& scanner )
{
#ifdef __CLIENT
	return g_Option.LoadToolTip( "ToolTip.ini" );
#else
	return TRUE;
#endif // __CLIENT
}

#endif //_DEBUG


BOOL TextCmd_ReloadConstant( CScanner& scanner )
{
#ifdef __THA_0808
	if( ::GetLanguage() == LANG_THA )
		return TRUE;
#endif	// __THA_0808
#ifdef __WORLDSERVER
	g_DPCoreClient.SendLoadConstant();
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GuildCombatRequest(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	DWORD dwPenya = scanner.GetNumber();	
	g_GuildCombatMng.GuildCombatRequest( pUser, dwPenya );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GuildCombatCancel(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	g_GuildCombatMng.GuildCombatCancel( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GuildCombatOpen(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( g_GuildCombatMng.m_nState != CGuildCombat::CLOSE_STATE )
	{
		char chMessage[128] = {0,};
		sprintf( chMessage, "Not GuildCombat Open :: Not CLOSE_STATE" );
		pUser->AddText( chMessage );
		return TRUE;
	}

	g_GuildCombatMng.GuildCombatOpen();
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GuildCombatIn(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	g_GuildCombatMng.GuildCombatEnter( pUser );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GuildCombatClose(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	int	nClose       = scanner.GetNumber();

	if( g_GuildCombatMng.m_nState == CGuildCombat::CLOSE_STATE )
	{
		char chMessage[128] = {0,};
		sprintf( chMessage, "Not GuildCombat Close :: Is CLOSE_STATE" );
		pUser->AddText( chMessage );
		return TRUE;
	}

	if( nClose == 0 )
		g_GuildCombatMng.SetGuildCombatClose( TRUE );
	else
		g_GuildCombatMng.SetGuildCombatCloseWait( TRUE );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_GuildCombatNext(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( g_GuildCombatMng.m_nState == CGuildCombat::CLOSE_STATE )
	{
		char chMessage[128] = {0,};
		sprintf( chMessage, "Not GuildCombat Close :: Is CLOSE_STATE" );
		pUser->AddText( chMessage );
		return TRUE;
	}
	
	g_GuildCombatMng.m_dwTime = GetTickCount();
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RemoveAttribute( CScanner& scanner )
{
#ifdef __CLIENT
	if( g_WndMng.m_pWndUpgradeBase == NULL )
	{
		SAFE_DELETE( g_WndMng.m_pWndUpgradeBase );
		g_WndMng.m_pWndUpgradeBase = new CWndUpgradeBase;
		g_WndMng.m_pWndUpgradeBase->Initialize();
		return FALSE;
	}
	
	if( g_WndMng.m_pWndUpgradeBase )
	{
		if( g_WndMng.m_pWndUpgradeBase->m_slot )
		{
			DWORD dwObjId = g_WndMng.m_pWndUpgradeBase->m_slot->m_dwObjId;
			g_DPlay.SendRemoveAttribute( dwObjId );
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
#endif // __CLIENT
	return TRUE;	
}

BOOL	TextCmd_GC1to1Open(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( g_GuildCombat1to1Mng.m_nState != g_GuildCombat1to1Mng.GC1TO1_OPEN )
	{
		BOOL bSrvrDown = FALSE;
		if( scanner.GetNumber() == 1 )
			bSrvrDown = TRUE;
		g_GuildCombat1to1Mng.GuildCombat1to1Open( bSrvrDown );
		return TRUE;
	}

	pUser->AddText( "already OPEN State!!!" );
#endif //__WORLDSERVER
	return TRUE;
}

BOOL	TextCmd_GC1to1Close(CScanner &) {
#ifdef __WORLDSERVER
	g_GuildCombat1to1Mng.m_nState = g_GuildCombat1to1Mng.GC1TO1_WAR;
	for( DWORD i=0; i<g_GuildCombat1to1Mng.m_vecGuilCombat1to1.size(); i++ )
	{
		if( g_GuildCombat1to1Mng.m_vecGuilCombat1to1[i].m_nState != CGuildCombat1to1::GC1TO1WAR_CLOSEWAIT )
			g_GuildCombat1to1Mng.m_vecGuilCombat1to1[i].GuildCombat1to1CloseWait();
		g_GuildCombat1to1Mng.m_vecGuilCombat1to1[i].m_nWaitTime = -1;
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL	TextCmd_GC1to1Next(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	if( g_GuildCombat1to1Mng.m_nState == g_GuildCombat1to1Mng.GC1TO1_CLOSE ) // GC1TO1_CLOSE
	{
		pUser->AddText( "Is CLOSE State!!!" );
		return TRUE;
	}
	if( g_GuildCombat1to1Mng.m_nState == g_GuildCombat1to1Mng.GC1TO1_WAR )
	{
		for( DWORD i=0; i<g_GuildCombat1to1Mng.m_vecGuilCombat1to1.size(); i++ )
			g_GuildCombat1to1Mng.m_vecGuilCombat1to1[i].m_nWaitTime = -1;
		return TRUE;
	}

	g_GuildCombat1to1Mng.m_nWaitTime = -1;
#endif // __WORLDERVER
	return TRUE;
}

#ifdef __EVENTLUA_COUPON
BOOL TextCmd_Coupon(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->m_nCoupon = s.GetNumber();
#endif // __WORLDSERVER
	return TRUE;
}
#endif // __EVENTLUA_COUPON

#ifdef __NPC_BUFF
BOOL TextCmd_RemoveAllBuff(CScanner &, CUser * pUser) {
#ifdef __WORLDSERVER
	pUser->RemoveAllBuff();
	pUser->ClearAllSMMode();
#endif // __WORLDSERVER
	return TRUE;
}
#endif // __NPC_BUFF

BOOL TextCmd_HeavenTower( CScanner& s )
{
#ifdef __CLIENT
	SAFE_DELETE(g_WndMng.m_pWndHeavenTower);
	g_WndMng.m_pWndHeavenTower = new CWndHeavenTower;
	g_WndMng.m_pWndHeavenTower->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RemoveJewel( CScanner& s )
{
#ifdef __CLIENT
	SAFE_DELETE(g_WndMng.m_pWndRemoveJewel);
	g_WndMng.m_pWndRemoveJewel = new CWndRemoveJewel;
	g_WndMng.m_pWndRemoveJewel->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_TransEggs( CScanner& s )
{
#ifdef __CLIENT
	g_WndMng.CreateApplet( APP_INVENTORY );

	if(g_WndMng.m_pWndPetTransEggs)
		SAFE_DELETE(g_WndMng.m_pWndPetTransEggs);
	
	g_WndMng.m_pWndPetTransEggs = new CWndPetTransEggs;

	if(g_WndMng.m_pWndPetTransEggs)
		g_WndMng.m_pWndPetTransEggs->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SecretRoomOpen(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	if( CSecretRoomMng::GetInstance()->m_nState == SRMNG_CLOSE )
		CSecretRoomMng::GetInstance()->SecretRoomOpen();
	else
		pUser->AddText( "Is Not Close State!!!" );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_SecretRoomEntrance( CScanner& s )
{
#ifdef __CLIENT
	if( s.GetNumber() == 1 )
	{
		g_DPlay.SendTeleportToSecretRoomDungeon();
		return TRUE;
	}

	CWndWorld* pWndWorld = (CWndWorld*)g_WndMng.GetWndBase( APP_WORLD );
	if(pWndWorld)
	{
		for(int i=0; i<MAX_KILLCOUNT_CIPHERS; i++)
		{
			pWndWorld->m_stKillCountCiphers[i].bDrawMyGuildKillCount = TRUE;
			pWndWorld->m_stKillCountCiphers[i].szMyGuildKillCount = '0';
			pWndWorld->m_stKillCountCiphers[i].ptPos = CPoint(0,0);
			pWndWorld->m_stKillCountCiphers[i].fScaleX = 1.0f;
			pWndWorld->m_stKillCountCiphers[i].fScaleY = 1.0f;
			pWndWorld->m_stKillCountCiphers[i].nAlpha = 255;

//			pWndWorld->m_bDrawMyGuildKillCount[i] = TRUE;
//			pWndWorld->m_szMyGuildKillCount[i] = '0';
		}
	}
	
	if(g_WndMng.m_pWndSecretRoomMsg)
		SAFE_DELETE( g_WndMng.m_pWndSecretRoomMsg );

	g_DPlay.SendSecretRoomEntrance();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SecretRoomNext( CScanner& s ) {
#ifdef __WORLDSERVER
	CSecretRoomMng::GetInstance()->m_dwRemainTime = 0;
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_SecretRoomTender( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendSecretRoomTenderOpenWnd();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SecretRoomLineUp( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendSecretRoomLineUpOpenWnd();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SecretRoomClose(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	if( CSecretRoomMng::GetInstance()->m_nState == SRMNG_WAR )
	{
		auto it = CSecretRoomMng::GetInstance()->m_mapSecretRoomContinent.begin();
		for( ; it!=CSecretRoomMng::GetInstance()->m_mapSecretRoomContinent.end(); it++ )
		{
			CSecretRoomContinent* pSRCont = it->second;
			if( pSRCont && pSRCont->m_nState != SRCONT_CLOSE )
				pSRCont->m_dwRemainTime = 0;
				//pSRCont->SetContCloseWait();
		}
	}
	else	
		pUser->AddText( "Is Not War State!!!" );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_SecretRoomTenderView( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendSecretRoomTenderView();

	SAFE_DELETE(g_WndMng.m_pWndSecretRoomCheckTaxRate);
	g_WndMng.m_pWndSecretRoomCheckTaxRate = new CWndSecretRoomCheckTaxRate;

	if(g_WndMng.m_pWndSecretRoomCheckTaxRate)
	{
		g_WndMng.m_pWndSecretRoomCheckTaxRate->Initialize();
	}
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SecretRoomTenderCancelReturn( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendSecretRoomTenderCancelReturn();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_ElectionRequirement( CScanner& s )
{
#ifdef __CLIENT
	IElection* pElection	= CCLord::Instance()->GetElection();
	char lpString[100]		= { 0,};
	sprintf( lpString, "election state : total(%d)/requirement(%d)", pElection->GetVote(), pElection->GetRequirement() );
	g_WndMng.PutString( lpString );
#endif	// __CLIENT
	return TRUE;
}

//#ifdef __INTERNALSERVER
BOOL TextCmd_RemoveTotalGold(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	__int64 iGold	= static_cast<__int64>( s.GetInt64() );
	if( iGold > pUser->GetTotalGold() )
	{
		char szText[100]	= { 0,};
		sprintf( szText, "TextCmd_RemoveTotalGold: %I64d", pUser->GetTotalGold() );
		pUser->AddText( szText );
	}
	else
		pUser->RemoveTotalGold( iGold );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ElectionAddDeposit( CScanner& s )
{
#ifdef __CLIENT
	__int64 iDeposit	= static_cast<__int64>( s.GetNumber() );
	g_DPlay.SendElectionAddDeposit( iDeposit );
#endif	// __CLIENT
	return TRUE;
}
BOOL TextCmd_ElectionSetPledge( CScanner& s )
{
#ifdef __CLIENT
	char szPledge[CCandidate::nMaxPledgeLen]	= { 0,};
	
	s.GetLastFull();
	if( lstrlen( s.token ) >= CCandidate::nMaxPledgeLen )
		return TRUE;
	lstrcpy( szPledge, s.token );
	StringTrimRight( szPledge );
//	RemoveCRLF( szPledge );
	g_DPlay.SendElectionSetPledge( szPledge );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_ElectionIncVote( CScanner& s )
{
#ifdef __CLIENT
	s.GetToken();
	u_long idPlayer		= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
	if( idPlayer > 0 )
		g_DPlay.SendElectionIncVote( idPlayer );
	else
		g_WndMng.PutString( prj.GetText( TID_GAME_ELECTION_INC_VOTE_E001 ) );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_ElectionProcess( CScanner& s )
{
#ifdef __WORLDSERVER
	BOOL bRun	= static_cast<BOOL>( s.GetNumber() );
	g_dpDBClient.SendElectionProcess( bRun );
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ElectionBeginCandidacy( CScanner& s )
{
#ifdef __WORLDSERVER
	g_dpDBClient.SendElectionBeginCandidacy();
#endif	// __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_ElectionBeginVote( CScanner& s )
{
#ifdef __WORLDSERVER
	g_dpDBClient.SendElectionBeginVote();
#endif	// __WORLDSERVER
	return TRUE;
}
BOOL TextCmd_ElectionEndVote( CScanner& s )
{
#ifdef __WORLDSERVER
	g_dpDBClient.SendElectionBeginEndVote();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_ElectionState( CScanner& s )
{
#ifdef __CLIENT
	CCElection* pElection	= static_cast<CCElection*>( CCLord::Instance()->GetElection() );
	pElection->State();
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_LEventCreate( CScanner & s )
{
#ifdef __CLIENT
	int iEEvent		= s.GetNumber();
	int iIEvent		= s.GetNumber();
	g_DPlay.SendLEventCreate( iEEvent, iIEvent );
#endif	// __CLIENT
	return TRUE;
}

BOOL TextCmd_LEventInitialize( CScanner & s )
{
#ifdef __WORLDSERVER
	g_dpDBClient.SendLEventInitialize();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_LSkill( CScanner & s )
{
#ifdef __CLIENT
	int nSkill	= s.GetNumber();
	s.GetToken();
	char szTarget[MAX_PLAYER]	= { 0,};
	strncpy( szTarget, s.token, MAX_PLAYER );
	szTarget[MAX_PLAYER-1]	= '\0';
	g_DPlay.SendLordSkillUse( nSkill, szTarget );
#endif	// __CLIENT
	return TRUE;
}
//#endif	// __INTERNALSERVER

BOOL TextCmd_SetTutorialState(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int nTutorialState	= s.GetNumber();
	pUser->SetTutorialState( nTutorialState );
	pUser->AddSetTutorialState();
#endif	// __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_TaxApplyNow( CScanner& s )
{
#ifdef __WORLDSERVER
	CTax::GetInstance()->SetApplyTaxRateNow();
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_HonorTitleSet(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	int nIdx = -1;
	s.GetToken();
	if( s.tokenType == NUMBER ) 
	{
		nIdx	= _ttoi( s.Token );
	}
	else
	{
		nIdx	= CTitleManager::Instance()->GetIdxByName( s.Token );
	}

	if( nIdx < 0 ||  nIdx >= MAX_HONOR_TITLE )
		return FALSE;

	DWORD dwNum	= s.GetNumber();
	if( dwNum > 100000000 ) dwNum = 100000000;
	if( dwNum < 0 ) dwNum = 0;

	pUser->SetHonorCount(nIdx,dwNum);
	pUser->AddHonorListAck();
	g_dpDBClient.SendLogGetHonorTime(pUser,nIdx);
#endif // __WORLDSERVER
	return TRUE;
}


BOOL TextCmd_RainbowRaceApp( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendRainbowRaceApplicationOpenWnd();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRacePass(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	CRainbowRace* pRainbowRace = CRainbowRaceMng::GetInstance()->GetRainbowRacerPtr( pUser->m_idPlayer );
	if( pRainbowRace )
	{
		pRainbowRace->SetNowGameComplete( pUser );
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RainbowRaceOpen( CScanner& s )
{
#ifdef __WORLDSERVER
	if( CRainbowRaceMng::GetInstance()->GetState() == CRainbowRaceMng::RR_CLOSED )
		CRainbowRaceMng::GetInstance()->SetState( CRainbowRaceMng::RR_OPEN );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RainbowRaceNext( CScanner& s )
{
#ifdef __WORLDSERVER
	CRainbowRaceMng::GetInstance()->SetNextTime( 0 );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_RainbowRaceInfo( CScanner& s )
{
#ifdef __CLIENT
	if(g_WndMng.m_pWndRainbowRaceInfo)
		SAFE_DELETE(g_WndMng.m_pWndRainbowRaceInfo);

	g_WndMng.m_pWndRainbowRaceInfo = new CWndRainbowRaceInfo;

	if(g_WndMng.m_pWndRainbowRaceInfo)
		g_WndMng.m_pWndRainbowRaceInfo->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceRule( CScanner& s )
{
#ifdef __CLIENT
	if(g_WndMng.m_pWndRainbowRaceRule)
		SAFE_DELETE(g_WndMng.m_pWndRainbowRaceRule);

	g_WndMng.m_pWndRainbowRaceRule = new CWndRainbowRaceRule;

	if(g_WndMng.m_pWndRainbowRaceRule)
		g_WndMng.m_pWndRainbowRaceRule->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceRanking( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendRainbowRacePrevRankingOpenWnd();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRacePrize( CScanner& s )
{
#ifdef __CLIENT
	if(g_WndMng.m_pWndRainbowRacePrize)
		SAFE_DELETE(g_WndMng.m_pWndRainbowRacePrize);

	g_WndMng.m_pWndRainbowRacePrize = new CWndRainbowRacePrize;

	if(g_WndMng.m_pWndRainbowRacePrize)
		g_WndMng.m_pWndRainbowRacePrize->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceKawiBawiBo( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_GAWIBAWIBO, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceDice( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_DICEPLAY, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceArithmetic( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_ARITHMATIC, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceStopWatch( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_STOPWATCH, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceTyping( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_TYPING, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceCard( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_PAIRGAME, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceLadder( CScanner& s )
{
#ifdef __CLIENT
	CRainbowRace::GetInstance()->SendMinigamePacket( RMG_LADDER, MP_OPENWND );
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_RainbowRaceReqFininsh( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendRainbowRaceReqFinish();
#endif // __CLIENT
	return TRUE;
}


BOOL TextCmd_ChangeAttribute( CScanner& s )
{
#ifdef __CLIENT
	SAFE_DELETE(g_WndMng.m_pWndChangeAttribute);
	g_WndMng.m_pWndChangeAttribute = new CWndChangeAttribute;
	g_WndMng.m_pWndChangeAttribute->Initialize();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_HousingVisitRoom( CScanner& s )
{
#ifdef __CLIENT
	s.GetToken();
	if( s.Token == "" )	// 아무 값 없으면 내방으로...
		g_DPlay.SendHousingVisitRoom( g_pPlayer->m_idPlayer );
	else	// 캐릭터명이 있으면 해당 캐릭터의 방으로...
	{
		DWORD dwPlayerId = CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );
		if( dwPlayerId )
			g_DPlay.SendHousingVisitRoom( dwPlayerId );
	}
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_HousingGMRemoveAll(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	CHousingMng::GetInstance()->ReqGMFunrnitureListAll( pUser );
#endif // __WORLDSERVER
	return TRUE;
}
/*
#if __VER >= 14 // __SMELT_SAFETY
BOOL TextCmd_SmeltSafetyNormal( CScanner& s )
{
#ifdef __CLIENT
	if(g_WndMng.m_pWndSmeltSafety)
		SAFE_DELETE(g_WndMng.m_pWndSmeltSafety);

	g_WndMng.m_pWndSmeltSafety = new CWndSmeltSafety(CWndSmeltSafety::WND_NORMAL);
	if(g_WndMng.m_pWndSmeltSafety)
	{
		g_WndMng.m_pWndSmeltSafety->Initialize();
	}
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SmeltSafetyAccessary( CScanner& s )
{
#ifdef __CLIENT
	if(g_WndMng.m_pWndSmeltSafety)
		SAFE_DELETE(g_WndMng.m_pWndSmeltSafety);

	g_WndMng.m_pWndSmeltSafety = new CWndSmeltSafety(CWndSmeltSafety::WND_ACCESSARY);
	if(g_WndMng.m_pWndSmeltSafety)
	{
		g_WndMng.m_pWndSmeltSafety->Initialize();
	}
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_SmeltSafetyPiercing( CScanner& s )
{
#ifdef __CLIENT
	if(g_WndMng.m_pWndSmeltSafety)
		SAFE_DELETE(g_WndMng.m_pWndSmeltSafety);

	g_WndMng.m_pWndSmeltSafety = new CWndSmeltSafety(CWndSmeltSafety::WND_PIERCING);
	if(g_WndMng.m_pWndSmeltSafety)
	{
		g_WndMng.m_pWndSmeltSafety->Initialize();
	}
#endif // __CLIENT
	return TRUE;
}

#endif //__SMELT_SAFETY
*/
BOOL TextCmd_SmeltSafetyElement( CScanner& s )
{
#ifdef __CLIENT
	if( g_WndMng.m_pWndSmeltSafety )
		SAFE_DELETE( g_WndMng.m_pWndSmeltSafety );

	g_WndMng.m_pWndSmeltSafety = new CWndSmeltSafety( CWndSmeltSafety::WndMode::Element );
	if( g_WndMng.m_pWndSmeltSafety )
		g_WndMng.m_pWndSmeltSafety->Initialize();
#endif // __CLIENT
	return TRUE;
}

#ifdef __QUIZ
BOOL TextCmd_QuizEventOpen( CScanner& s )
{
#ifdef __WORLDSERVER
	if( !CQuiz::GetInstance()->IsRun() )
		g_dpDBClient.SendQuizEventOpen( CQuiz::GetInstance()->GetType() );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_QuizEventEnterance( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendQuizEventEntrance();
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_QuizStateNext(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	if( CQuiz::GetInstance()->IsRun() )
	{
		if( IsValidObj( pUser ) && pUser->GetWorld() && pUser->GetWorld()->GetID() == WI_WORLD_QUIZ )
		{
			CQuiz::GetInstance()->SetNextTime( 0 );
			CQuiz::GetInstance()->Process();
		}
	}
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_QuizEventClose( CScanner& s )
{
#ifdef __WORLDSERVER
	if( CQuiz::GetInstance()->IsRun() )
		CQuiz::GetInstance()->CloseQuizEvent();
#endif // __WORLDSERVER
	return TRUE;
}
#endif // __QUIZ

BOOL TextCmd_BuyGuildHouse( CScanner& s )
{
#ifdef __CLIENT
	g_DPlay.SendPacket<PACKETTYPE_GUILDHOUSE_BUY>();
#endif // __CLIENT
	return TRUE;
}

#ifdef __CLIENT
#include "WndGuildHouse.h"
#endif // __CLIENT
BOOL TextCmd_GuildHouseUpkeep( CScanner & s )
{
#ifdef __CLIENT
	if( !g_WndMng.m_pWndUpkeep )
	{
		g_WndMng.m_pWndUpkeep = new CWndGHUpkeep;
		g_WndMng.m_pWndUpkeep->Initialize();
	}
	else
	{
		g_WndMng.m_pWndUpkeep->Destroy( );
		g_WndMng.m_pWndUpkeep = NULL;
	}
#endif // __CLIENT
	return TRUE;
}

BOOL TextCmd_CampusInvite(CScanner & s, CUser * pRequest) {
#ifdef __WORLDSERVER
	if( !IsValidObj( pRequest ) )
		return FALSE;

	s.GetToken();
	u_long idTarget	= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );

	if( 0 < idTarget )
	{
		CUser* pTarget	= g_UserMng.GetUserByPlayerID( idTarget );	
		if( IsValidObj( pTarget ) )
		{
			CCampusHelper::GetInstance()->OnInviteCampusMember( pRequest, pTarget );
			PlayerData* pPlayerData	= CPlayerDataCenter::GetInstance()->GetPlayerData( idTarget );
			if( pPlayerData )
				pRequest->AddQueryPlayerData( idTarget, pPlayerData );
		}

		else
			pRequest->AddDefinedText( TID_DIAG_0061, "\"%s\"", s.Token );
	}
	else
		pRequest->AddDefinedText( TID_DIAG_0060, "\"%s\"", s.Token );
#endif // __WORLDSERVER

	return TRUE;
}

BOOL TextCmd_RemoveCampusMember(CScanner & s, CUser * pRequest) {
#ifdef __WORLDSERVER
	if( !IsValidObj( pRequest ) )
		return FALSE;

	s.GetToken();
	u_long idTarget	= CPlayerDataCenter::GetInstance()->GetPlayerId( s.token );

	if( idTarget > 0 )
		CCampusHelper::GetInstance()->OnRemoveCampusMember( pRequest, idTarget );
		
	else
		pRequest->AddDefinedText( TID_DIAG_0060, "\"%s\"", s.Token );

#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_UpdateCampusPoint(CScanner & s, CUser * pUser) {
#ifdef __WORLDSERVER
	if( !IsValidObj( pUser ) )
		return FALSE;

	int nCampusPoint = 0;
	nCampusPoint = s.GetNumber();

	if( IsValidObj( pUser ) )
		g_dpDBClient.SendUpdateCampusPoint( pUser->m_idPlayer, nCampusPoint, TRUE, 'G' );
#endif // __WORLDSERVER
	return TRUE;
}

BOOL TextCmd_InvenRemove(CScanner & scanner, CUser * pUser) {
#ifdef __WORLDSERVER
	if( IsValidObj( pUser ) )
	{
		int nSize = pUser->m_Inventory.GetMax();
		for( int i = 0 ; i < nSize; ++i )
		{
			CItemElem* pItemElem = pUser->m_Inventory.GetAtId( i );
			if( IsUsableItem( pItemElem ) && !pUser->m_Inventory.IsEquip( pItemElem->m_dwObjId ) && !pItemElem->IsPet() && !pItemElem->IsEatPet() )
				pUser->UpdateItem(*pItemElem, UI::Num::RemoveAll());
		}
	}
#endif	// __WORLDSERVER
	return TRUE;
}

#include "eveschool.h"
#include <random>
BOOL TextCmd_Arbitrary(CScanner & scanner, CUser * pUser) {
#ifdef __CLIENT

	std::vector<u_long> usablePlayers;
	std::vector<CGuild *> usableGuilds;

	for (u_long i = 0; i != 100; ++i) {
		const char * name = CPlayerDataCenter::GetInstance()->GetPlayerString(i);
		if (name != std::string_view("")) {
			usablePlayers.emplace_back(i);
		}
	}

	for (int i = 0; i != 100; ++i) {
		CGuild * guild = g_GuildMng.GetGuild(i);
		if (guild) usableGuilds.emplace_back(guild);
	}

	if (usableGuilds.size() < 5 || usablePlayers.size() < 1) {
		g_WndMng.PutString("We need more guilds and players");
		return TRUE;
	}

	usableGuilds.resize(5);
	if (usablePlayers.size() >= 25) usablePlayers.resize(25);


	std::vector<CGuildCombat::__GCGETPOINT> kills;
	std::map<u_long, int> guildToPoints;
	std::map<u_long, int> playersToPoints;
	
	std::random_device r;
	std::uniform_int_distribution<int> uniform_dist(1, 25);
	std::default_random_engine e1(r());

	for (int i = 0; i != 9 * 9 * 50; ++i) {
		u_long pAtk = usablePlayers[uniform_dist(e1) % usablePlayers.size()];
		CGuild * gAtk = usableGuilds[uniform_dist(e1) % usableGuilds.size()];
		
		u_long pDef = usablePlayers[uniform_dist(e1) % usablePlayers.size()];
		CGuild * gDef = usableGuilds[uniform_dist(e1) % usableGuilds.size()];

		const BOOL master    = uniform_dist(e1) % 15 == 5;
		const BOOL defender  = uniform_dist(e1) % 15 == 8;
		const BOOL bLastLife = uniform_dist(e1) % 15 == 4;

		int points = 2
			+ (master ? 1 : 0)
			+ (defender ? 1 : 0)
			+ (bLastLife ? 1 : 0);

		CGuildCombat::__GCGETPOINT gc{
			.uidGuildAttack = gAtk->GetGuildId(),
			.uidGuildDefence = gDef->GetGuildId(),
			.uidPlayerAttack = pAtk,
			.uidPlayerDefence = pDef,
			.nPoint = points,
			.bMaster = master,
			.bDefender = defender,
			.bLastLife = bLastLife
		};

		kills.emplace_back(gc);

		guildToPoints[gAtk->GetGuildId()] += points;
		playersToPoints[pAtk] += points;
	}

	CAr ar;
	ar << static_cast<u_long>(kills.size());
	for (const auto & gc : kills) {
		ar << gc;
	}

	int size;
	BYTE * buffer = ar.GetBuffer(&size);
	

	CAr receive(buffer, size);

	CWndWorld * pWndWorld = (CWndWorld *)g_WndMng.GetWndBase(APP_WORLD);
	if (!pWndWorld) return FALSE;

	pWndWorld->m_GCprecedence.guilds.clear();
	pWndWorld->m_GCprecedence.idToGuildName.clear();

	for (const auto & [guildId, points] : guildToPoints) {
		CGuild * guild = g_GuildMng.GetGuild(guildId);
		pWndWorld->m_GCprecedence.guilds.emplace(points, guildId);
		pWndWorld->m_GCprecedence.idToGuildName.emplace(guildId, guild->m_szGuild);
	}

	pWndWorld->m_GCprecedence.players.clear();
	for (const auto & [playerId, points] : playersToPoints) {
		pWndWorld->m_GCprecedence.players.emplace(points, playerId);
	}



	g_DPlay.OnGCLog(receive);
	// g_DPlay.SendPacket<PACKETTYPE_SQUONK_ARBITRARY_PACKET>(v);
		
#endif
	return TRUE;
}

#ifndef __CLIENT
void CDPSrvr::OnSquonKArbitraryPacket(CAr & ar, CUser & thisIsMe) {

}
#endif


namespace CmdFunc {
	AllCommands::AllCommands() {
////////////////////////////////////////////////// AUTH_GENERAL begin/////////////////////////////////////////////////////
    AddCommand(TCM::SERVER, AUTH_GENERAL      , TextCmd_whisper                     , "whisper"                 , { "w" }            , { "귓속말", "귓" }              , "귓속말 [/명령 아이디 내용]"        );
    AddCommand(TCM::SERVER, AUTH_GENERAL      , TextCmd_say                         , "say"                     , {}                 , { "말" }                        , "속삭임 [/명령 아이디 내용]"        );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_Position                    , "position"                , { "pos" }          , { "좌표" }                      , "현재 좌표를 출력해준다."           );
    AddCommand(TCM::BOTH  , AUTH_GENERAL      , TextCmd_shout                       , "shout"                   , { "s" }            , { "외치기", "외" }              , "외치기 [/명령 아이디 내용]"        );
    AddCommand(TCM::BOTH  , AUTH_GENERAL      , TextCmd_PartyChat                   , "partychat"               , { "p" }            , { "극단말", "극" }              , "파티 채팅 [/명령 내용]"            );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_Time                        , "Time"                    , { "ti" }           , { "시간", "시" }                , "시간 보기 [/시간]"                 );
//  AddCommand(TCM::BOTH  , AUTH_GENERAL      , TextCmd_ChangeFace                  , "ChangeFace"              , { "cf" }           , { "얼굴변경", "얼변" }          , "얼굴 변경"                         );
    AddCommand(TCM::BOTH  , AUTH_GENERAL      , TextCmd_GuildChat                   , "GuildChat"               , { "g" }            , { "길드말", "길말" }            , "길드말"                            );
    AddCommand(TCM::SERVER, AUTH_GENERAL      , TextCmd_PartyInvite                 , "PartyInvite"             , { "partyinvite" }  , { "극단초청", "극초" }          , "극단 초청"                         );
    AddCommand(TCM::SERVER, AUTH_GENERAL      , TextCmd_GuildInvite                 , "GuildInvite"             , { "guildinvite" }  , { "길드초청", "길초" }          , "길드 초청"                         );
    AddCommand(TCM::SERVER, AUTH_GENERAL      , TextCmd_CampusInvite                , "CampusInvite"            , { "campusinvite" } , { "사제초청", "사초" }          , "사제 초청"                         );
#ifdef __CLIENT
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_tradeagree                  , "tradeagree"              , { "ta" }           , { "거래승인", "거승" }          , "거래 승인 [/명령] "                );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_traderefuse                 , "traderefuse"             , { "tr" }           , { "거래거절", "거절" }          , "거래 거절 [/명령] "                );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_whisperagree                , "whisperagree"            , { "wa" }           , { "귓속말승인", "귓승" }        , "귓속말 승인 [/명령] "              );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_whisperrefuse               , "whisperrefuse"           , { "wr" }           , { "귓속말거절", "귓절" }        , "귓속말 거절 [/명령] "              );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_messengeragree              , "messengeragree"          , { "ma" }           , { "메신저승인", "메승" }        , "메신저 승인 [/명령] "              );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_messengerrefuse             , "messengerrefuse"         , { "mr" }           , { "메신저거절", "메절" }        , "메신저 거절 [/명령] "              );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_stageagree                  , "stageagree"              , { "ga" }           , { "극단승인", "극승" }          , "극단 승인 [/명령] "                );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_stagerefuse                 , "stagerefuse"             , { "gr" }           , { "극단거절", "극절" }          , "극단 거절 [/명령] "                );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_connectagree                , "connectagree"            , { "ca" }           , { "접속알림", "접알" }          , "접속알림 [/명령] "                 );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_connectrefuse               , "connectrefuse"           , { "cr" }           , { "접속알림해제", "접해" }      , "접속알림 해제 [/명령] "            );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_shoutagree                  , "shoutagree"              , { "ha" }           , { "외치기승인", "외승" }        , "외치기 승인 [/명령] "              );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_shoutrefuse                 , "shoutrefuse"             , { "hr" }           , { "외치기해제", "외해" }        , "외치기 거절 [/명령] "              );
#ifdef __YS_CHATTING_BLOCKING_SYSTEM
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_BlockUser                   , "ignore"                  , { "ig" }           , { "채팅차단", "채차" }          , "채팅차단 [/명령 아이디]"           );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_CancelBlockedUser           , "unignore"                , { "uig" }          , { "채팅차단해제", "채차해" }    , "채팅차단해제 [/명령 아이디]"       );
    AddCommand(TCM::CLIENT, AUTH_GENERAL      , TextCmd_IgnoreList                  , "ignorelist"              , { "igl" }          , { "채팅차단목록", "채차목" }    , "채팅 차단 목록"                    );
#endif // __YS_CHATTING_BLOCKING_SYSTEM
#endif //__CLIENT
////////////////////////////////////////////////// AUTH_GENERAL end/////////////////////////////////////////////////////
	// GM_LEVEL_1
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER   , TextCmd_Teleport                    , "teleport"                , { "te" }           , { "텔레포트", "텔레" }          , "텔레포트"                          );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER   , TextCmd_Invisible                   , "invisible"               , { "inv" }          , { "투명" }                      , "투명화"                            );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER   , TextCmd_NoInvisible                 , "noinvisible"             , { "noinv" }        , { "투명해제", "투해" }          , "투명화 해제"                       );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER   , TextCmd_Summon                      , "summon"                  , { "su" }           , { "소환" }                      , "유저소환"                          );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER   , TextCmd_count                       , "count"                   , { "cnt" }          , { "접속자수" }                  , "접속자 카운트"                     );
	
	// GM_LEVEL_2
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_Out                         , "out"                     , {}                 , { "퇴출" }                      , "퇴출"                              );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_Talk                        , "talk"                    , { "nota" }         , { "말해제", "말해" }            , "말하지 못하게 하기 해제"           );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_NoTalk                      , "notalk"                  , { "ta" }           , { "말정지", "말정" }            , "말하지 못하게 하기"                );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER2  , TextCmd_ip                          , "ip"                      , {}                 , { "아이피" }                    , "상대 IP알기"                       );
#ifdef __JEFF_9_20
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_Mute                        , "Mute"                    , { "mute" }         , { "조용히" }                    , ""                                  );
#endif	// __JEFF_9_20
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_GuildRanking                , "GuildRanking"            , { "ranking" }      , { "길랭" }                      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_FallSnow                    , "FallSnow"                , { "fs" }           , { "눈와라", "눈와" }            , "눈 내리기 토글"                    );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_StopSnow                    , "StopSnow"                , { "ss" }           , { "눈그만", "눈끝" }            , "눈 내리기 못하게 토글"             );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_FallRain                    , "FallRain"                , { "frain" }        , { "비와라", "비와" }            , "비 내리기 토글"                    );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_StopRain                    , "StopRain"                , { "sr" }           , { "비그만", "비끝" }            , "비 내리기 못하게 토글"             );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER2  , TextCmd_System                      , "system"                  , { "sys" }          , { "알림", "알" }                , "시스템 메시지"                     );

	// GM_LEVEL_3
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_PvpParam                    , "PvpParam"                , { "p_Param" }      , { "PVP설정", "피설" }           , "PVP(카오)설정"                     );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_PKParam                     , "PKParam"                 , { "pkparam" }      , { "PK설정", "pk설정" }          , "카오설정"                          );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_Undying                     , "undying"                 , { "ud" }           , { "무적", "무" }                , "무적"                              );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_Undying2                    , "undying2"                , { "ud2" }          , { "반무적", "반무" }            , "반무적"                            );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_NoUndying                   , "noundying"               , { "noud" }         , { "무적해제", "무해" }          , "무적 해제"                         );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_Onekill                     , "onekill"                 , { "ok" }           , { "초필" }                      , "적을 한방에 죽이기"                );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_NoOnekill                   , "noonekill"               , { "nook" }         , { "초필해제", "초해" }          , "적을 한방에 죽이기 해제"           );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_AroundKill                  , "aroundkill"              , { "ak" }           , { "원샷", "원" }                , "어라운드에 있는 몬스터 죽이기"     );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_stat                        , "stat"                    , {}                 , { "스탯" }                      , "스탯 설정 하기"                    );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_Level                       , "level"                   , { "lv" }           , { "레벨", "렙" }                , "레벨 설정 하기"                    );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_InitSkillExp                , "InitSkillExp"            , { "InitSE" }       , { "스킬초기화", "스초" }        , "스킬초기화"                        );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_SkillLevel                  , "skilllevel"              , { "slv" }          , { "스킬레벨", "스렙" }          , "스킬레벨 설정 하기"                );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_SkillLevelAll               , "skilllevelAll"           , { "slvAll" }       , { "스킬레벨올", "스렙올" }      , "스킬레벨 설정 하기"                );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_BeginQuest                  , "BeginQuest"              , { "bq" }           , { "퀘스트시작", "퀘시" }        , "퀘스트 시작 [ID]"                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_EndQuest                    , "EndQuest"                , { "eq" }           , { "퀘스트종료", "퀘종" }        , "퀘스트 종료 [ID]"                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_RemoveQuest                 , "RemoveQuest"             , { "rq" }           , { "퀘스트제거", "퀘제" }        , "퀘스트 제거 [ID]"                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_RemoveAllQuest              , "RemoveAllQuest"          , { "raq" }          , { "퀘스트전체제거", "퀘전제" }  , "퀘스트 전체 제거"                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_RemoveCompleteQuest         , "RemoveCompleteQuest"     , { "rcq" }          , { "퀘스트완료제거", "퀘완제" }  , "퀘스트 완료 제거"                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_ChangeJob                   , "changejob"               , { "cjob" }         , { "전직" }                      , "전직 하기"                         );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_Freeze                      , "freeze"                  , { "fr" }           , { "정지" }                      , "움직이지 못하게 하기"              );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_NoFreeze                    , "nofreeze"                , { "nofr" }         , { "정지해제", "정해" }          , "움직이지 못하게 하기 해제"         );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_PartyLevel                  , "PartyLevel"              , { "plv" }          , { "극단레벨", "극레" }          , "극단레벨 설정 하기"                );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_GuildStat                   , "GuildStat"               , { "gstat" }        , { "길드스탯", "길스탯" }        , "길드 스탯변경"                     );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_CreateGuild                 , "createguild"             , { "cg" }           , { "길드생성", "길생" }          , "길드 생성"                         );
    AddCommand(TCM::CLIENT, AUTH_GAMEMASTER3  , TextCmd_DestroyGuild                , "destroyguild"            , { "dg" }           , { "길드해체", "길해" }          , "길드 해체"                         );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_GuildCombatIn               , "GCIn"                    , { "gcin" }         , { "길드워입장", "길워입" }      , "길드대전 입장"                     );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_GuildCombatOpen             , "GCOpen"                  , { "gcopen" }       , { "길드워오픈", "길워오" }      , "길드대전 오픈"                     );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_GuildCombatClose            , "GCClose"                 , { "gcclose" }      , { "길드워닫기", "길워닫" }      , "길드대전 닫기"                     );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_GuildCombatNext             , "GCNext"                  , { "gcNext" }       , { "길드워다음", "길워다" }      , "길드대전 다음"                     );
    AddCommand(TCM::BOTH  , AUTH_GAMEMASTER3  , TextCmd_indirect                    , "indirect"                , { "id" }           , { "간접" }                      , "상대에게 간접으로 말하게 하기"     );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_CreateNPC                   , "createnpc"               , { "cn" }           , { "엔피씨생성", "엔생" }        , "npc생성"                           );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_LuaEventList                , "EVENTLIST"               , { "eventlist" }    , { "이벤트목록" }                , ""                                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_LuaEventInfo                , "EVENTINFO"               , { "eventinfo" }    , { "이벤트정보" }                , ""                                  );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_GameSetting                 , "gamesetting"             , { "gs" }           , { "게임설정", "게설" }          , "게임 설정 보기"                    );
    AddCommand(TCM::SERVER, AUTH_GAMEMASTER3  , TextCmd_RemoveNpc                   , "rmvnpc"                  , { "rn" }           , { "삭제", "삭" }                , "NPC삭제"                           );

	// GM_LEVEL_4
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Disguise                    , "disguise"                , { "dis" }          , { "변신", "변" }                , "변신"                              );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_NoDisguise                  , "noDisguise"              , { "nodis" }        , { "변신해제", "변해" }          , "변신 해제"                         );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ItemEnchant                 , "itemenchant"             , { "ie" }           , {}                              , "Change various item enchants"      );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_ResistItem                  , "ResistItem"              , { "ritem" }        , { "속성아이템", "속아" }        , "속성아이템"                        );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_GetGold                     , "getgold"                 , { "gg" }           , { "돈줘", "돈" }                , "돈 얻기"                           );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_CreateItem                  , "createitem"              , { "ci" }           , { "아이템생성", "아생" }        , "아이템생성"                        );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_CreateItem2                 , "createitem2"             , { "ci2" }          , { "아이템생성2", "아생2" }      , "아이템생성2"                       );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_QuestState                  , "QuestState"              , { "qs" }           , { "퀘스트상태", "퀘상" }        , "퀘스트 설정 [ID] [State]"          );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_LoadScript                  , "loadscript"              , { "loscr" }        , { "로드스크립트", "로스" }      , "스크립트 다시 읽기"                );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ReloadConstant              , "ReloadConstant"          , { "rec" }          , { "리로드콘스탄트", "리콘" }    , "리로드 콘스탄트파일"               );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_Piercing                    , "Piercing"                , { "pier" }         , { "피어싱", "피싱" }            , "피어싱(소켓)"                      );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_PetLevel                    , "petlevel"                , { "pl" }           , { "펫레벨", "펫레" }            , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_PetExp                      , "petexp"                  , { "pe" }           , { "펫경험치", "펫경" }          , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_MakePetFeed                 , "makepetfeed"             , { "mpf" }          , { "먹이만들기", "먹이" }        , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_Pet                         , "Pet"                     , { "pet" }          , { "펫" }                        , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Lua                         , "Lua"                     , { "lua" }          , { "루아" }                      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_GC1to1Open                  , "GC1TO1OPEN"              , { "gc1to1open" }   , { "일대일대전오픈", "일오" }    , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_GC1to1Close                 , "GC1TO1CLOSE"             , { "gc1to1close" }  , { "일대일대전닫기", "일닫" }    , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_GC1to1Next                  , "GC1TO1NEXT"              , { "gc1to1next" }   , { "일대일대전다음", "일다" }    , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_RefineAccessory             , "RefineAccessory"         , { "ra" }           , { "액세서리제련", "액제" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_RefineCollector             , "RefineCollector"         , { "rc" }           , { "채집기재련", "채제" }        , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_GenRandomOption             , "GenRandomOption"         , { "gro" }          , { "각성축복", "각축" }          , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_InitializeRandomOption      , "InitializeRandomOption"  , { "iro" }          , { "각성축복제거", "각축제거" }  , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SetRandomOption             , "SetRandomOption"         , { "sro" }          , { "각성축복지정", "각지" }      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_SetPetName                  , "SetPetName"              , { "setpetname" }   , { "펫작명", "펫작" }            , "펫작명"                            );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ClearPetName                , "ClearPetName"            , { "cpn" }          , { "펫작명취소", "펫작취" }      , "펫작명취소"                        );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Propose                     , "Propose"                 , { "propose" }      , { "프러포즈" }                  , "프러포즈"                          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Refuse                      , "Refuse"                  , { "refuse" }       , { "프러포즈거절", "프거" }      , "프러포즈거절"                      );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Couple                      , "Couple"                  , { "couple" }       , { "커플" }                      , "커플"                              );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Decouple                    , "Decouple"                , { "decouple" }     , { "커플해지", "커해" }          , "커플해지"                          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ClearPropose                , "ClearPropose"            , { "clearpropose" } , { "프러포즈초기화", "프초" }    , "프러포즈초기화"                    );
//  AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_CoupleState                 , "CoupleState"             , { "couplestate" }  , { "커플상태", "커상" }          , "커플상태"                          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_NextCoupleLevel             , "NextCoupleLevel"         , { "ncl" }          , { "커플레벨업", "커레" }        , "커플레벨업"                        );
#ifdef __NPC_BUFF
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_RemoveAllBuff               , "RemoveBuff"              , { "rb" }           , { "버프해제", "버해" }          , ""                                  );
#endif // __NPC_BUFF
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_HonorTitleSet               , "HonorTitleSet"           , { "hts" }          , { "달인세팅", "달세" }          , ""                                  );


    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_Arbitrary                   , "sqk"                     , {}                 , {}                              , ""                                 );

// 여기부터 국내만 
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_Open                        , "open"                    , {}                 , { "열기" }                      , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_Close                       , "close"                   , {}                 , { "닫기" }                      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Music                       , "music"                   , { "mu" }           , { "음악" }                      , "배경음악"                          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Sound                       , "sound"                   , { "so" }           , { "소리" }                      , "사운드 효과"                       );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_LocalEvent                  , "localevent"              , { "le" }           , { "지역이벤트", "지이" }        , "지역이벤트"                        );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_CommercialElem              , "CommercialElem"          , {}                 , { "속성강화창", "속강" }        , "속성강화창 띄우기"                 );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_QuerySetPlayerName          , "SetPlayerName"           , { "spn" }          , { "플레이어이름", "플이" }      , "플레이어 이름 변경"                );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_QuerySetGuildName           , "SetGuildName"            , { "sgn" }          , { "길드이름", "길이" }          , "길드 이름 변경"                    );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_DeclWar                     , "DeclWar"                 , { "declwar" }      , { "길드전신청", "길신" }        , "길드전 신청"                       );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RemoveGuildMember           , "rgm"                     , {}                 , { "길드추방", "길추" }          , "길드 추방"                         );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_GuildRankingDBUpdate        , "GuildRankingUpdate"      , { "rankingupdate" }, { "길업" }                      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ItemMode                    , "gmitem"                  , {}                 , { "아이템모드", "아모" }        , "아이템 못집고 못떨어트리게 하기"   );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ItemNotMode                 , "gmnotitem"               , {}                 , { "아이템해제", "아모해" }      , "아이템 모드 해제"                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_AttackMode                  , "gmattck"                 , {}                 , { "공격모드", "공모" }          , "공격 못하게 하기"                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_AttackNotMode               , "gmnotattck"              , {}                 , { "공격해제", "공모해" }        , "공격 모드 해제"                    );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_CommunityMode               , "gmcommunity"             , {}                 , { "커뮤니티모드", "커모" }      , "파티, 친구, 거래, 상점 못하게 하기"    );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_CommunityNotMode            , "gmnotcommunity"          , {}                 , { "커뮤니티해제", "커모해" }    , "커뮤니티 모드 해체"                );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ObserveMode                 , "gmobserve"               , {}                 , { "관전모드", "관모" }          , "커뮤니티, 말못하게, 어텍 모드 합한것" );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ObserveNotMode              , "gmnotobserve"            , {}                 , { "관전해제", "관모해" }        , "관전 모드 해제"                    );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_EscapeReset                 , "EscapeReset"             , {}                 , { "탈출초기화", "탈초" }        , "탈출(귀환석) 시간 초기화"          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_userlist                    , "userlist"                , { "ul" }           , { "사용자리스트", "사용자리" }  , "사용자 리스트"                     );
//  AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_sbready                     , "sbready"                 , {}                 , {}                              , "sbready"                           );
//  AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_sbstart                     , "sbstart"                 , {}                 , {}                              , "sbstart"                           );
//  AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_sbstart2                    , "sbstart2"                , {}                 , {}                              , "sbstart2"                          );
//  AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_sbend                       , "sbend"                   , {}                 , {}                              , "sbend"                             );
//  AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_sbreport                    , "sbreport"                , {}                 , {}                              , "sbreport"                          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_SetGuildQuest               , "SetGuildQuest"           , { "sgq" }          , { "길드퀘스트", "길퀘" }        , "길드 퀘스트 상태 변경"             );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_SetSnoop                    , "Snoop"                   , { "snoop" }        , { "감청" }                      , "감청"                              );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_SetSnoopGuild               , "SnoopGuild"              , { "sg" }           , { "길드대화저장", "길저" }      , "길드 대화 저장"                    );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_GuildCombatRequest          , "GCRequest"               , { "gcrquest" }     , { "길드워신청", "길워신" }      , "길드대전 신청"                     );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_GuildCombatCancel           , "GCCancel"                , { "gccancel" }     , { "길드워탈퇴", "길워탈" }      , "길드대전 탈퇴"                     );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_PostMail                    , "PostMail"                , { "pm" }           , { "편지발송", "발송" }          , "편지 발송"                         );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RemoveMail                  , "RemoveMail"              , { "rm" }           , { "편지삭제", "편삭" }          , "편지 삭제"                         );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_GetMailItem                 , "GetMailItem"             , { "gm" }           , { "소포받기", "소포" }          , "소포 받기"                         );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_GetMailGold                 , "GetMailGolg"             , { "gmg" }          , { "수금" }                      , "수금"                              );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_InvenClear                  , "InvenClear"              , { "icr" }          , { "인벤청소", "인청" }          , "인벤토리의 내용을 모두 삭제"       );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_ExpUpStop                   , "ExpUpStop"               , { "es" }           , { "경험치금지", "경금" }        , "사냥으로 오르는 경험치 상승을 금지");

#ifdef _DEBUG
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_CreateChar                  , "createchar"              , { "cc" }           , { "캐릭터생성", "캐생" }        , "캐릭터생성"                        );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_CreateCtrl                  , "createctrl"              , { "ct" }           , { "컨트롤생성", "컨생" }        , "ctrl생성"                          );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_SetMonsterRespawn           , "setmonsterrespawn"       , { "smr" }          , { "리스폰영역설정", "리영설" }  , "리스폰 영역 설정"                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_TransyItemList              , "TransyItemList"          , { "til" }          , { "트랜지리스트", "트아리" }    , "트랜지아이템리스트"                );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_LoadToolTipColor            , "LoadToolTip"             , { "ltt" }          , { "로드툴팁", "로툴팁" }        , "로드 툴팁 컬러"                    );
#endif

    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_PropMonster                 , "monstersetting"          , { "ms" }           , { "몬스터설정", "몬설" }        , "몬스터 설정 보기"                  );

#ifdef __EVENT_1101
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_CallTheRoll                 , "CallTheRoll"             , { "ctr" }          , { "출석설정", "출석" }          , "출석 조작 명령어"                  );
#endif	// __EVENT_1101
	
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_AngelExp                    , "AExp"                    , { "aexp" }         , { "엔젤경험치", "엔경" }        , ""                                  );
	
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RemoveAttribute             , "RemAttr"                 , { "remattr" }      , { "속성제거", "속제" }          , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_StartCollecting             , "StartCollecting"         , { "col1" }         , { "채집시작", "채시" }          , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_StopCollecting              , "StopCollecting"          , { "col2" }         , { "채집끝", "채끝" }            , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_DoUseItemBattery            , "Battery"                 , { "battery" }      , { "채집기충전", "채충" }        , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_AvailPocket                 , "AvailPocket"             , { "ap" }           , { "주머니사용", "주사" }        , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_PocketView                  , "PocketView"              , { "pv" }           , { "주머니보기", "주보" }        , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_MoveItem_Pocket             , "MoveItemPocket"          , { "mip" }          , { "아이템이동", "아이" }        , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_ItemLevel                   , "ItemLevel"               , { "il" }           , { "하락" }                      , ""                                  );

#ifdef __EVENTLUA_COUPON
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_Coupon                      , "COUPON"                  , { "coupon" }       , { "쿠폰설정", "쿠폰" }          , ""                                  );
#endif // __EVENTLUA_COUPON

#ifdef __SFX_OPT
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SfxLv                       , "SfxLevel"                , { "sl" }           , {}                              , ""                                  );
#endif	

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomOpen              , "SROPEN"                  , { "sropen" }       , { "비밀의방오픈", "비오" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomNext              , "SRNEXT"                  , { "srnext" }       , { "비밀의방다음", "비다" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomEntrance          , "SRENTRANCE"              , { "srentrance" }   , { "비밀의방입장", "비입장" }    , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomTender            , "SRTENDER"                , { "srtender" }     , { "비밀의방입찰", "비입" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomLineUp            , "SRLINEUP"                , { "srlineup" }     , { "비밀의방구성", "비구" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomClose             , "SRCLOSE"                 , { "srclose" }      , { "비밀의방닫기", "비닫" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomTenderView        , "SRVIEW"                  , { "srview" }       , { "비밀의방입찰현황", "비현" }  , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_SecretRoomTenderCancelReturn, "SRCANCEL"                , { "srcancel" }     , { "비밀의방입찰취소", "비취" }  , ""                                  );

    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ElectionRequirement         , "ElectionRequirement"     , { "er" }           , { "군주투표현황", "군투현" }    , ""                                  );
//#ifdef __INTERNALSERVER
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ElectionAddDeposit          , "ElectionAddDeposit"      , { "ead" }          , { "군주입찰", "군입" }          , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ElectionSetPledge           , "ElectionSetPledge"       , { "esp" }          , { "군주공약설정", "군공" }      , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ElectionIncVote             , "ElectionIncVote"         , { "eiv" }          , { "군주투표", "군투" }          , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_ElectionProcess             , "ElectionProcess"         , { "ep" }           , { "군주프로세스", "군프" }      , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_ElectionBeginCandidacy      , "ElectionBeginCandidacy"  , { "ebc" }          , { "군주입후보시작", "군입시" }  , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_ElectionBeginVote           , "ElectionBeginVote"       , { "ebv" }          , { "군주투표시작", "군투시" }    , ""                                  );
    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_ElectionEndVote             , "ElectionEndVote"         , { "eev" }          , { "군주투표종료", "군투종" }    , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ElectionState               , "ElectionState"           , { "estate" }       , { "군주투표상태", "군투상" }    , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_LEventCreate                , "LEventCreate"            , { "lecreate" }     , { "군주이벤트시작", "군이시" }  , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_LEventInitialize            , "LEventInitialize"        , { "leinitialize" } , { "군주이벤트초기화", "군이초" }, ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_LSkill                      , "LSkill"                  , { "lskill" }       , { "군주스킬", "군스" }          , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_RemoveTotalGold             , "RemoveTotalGold"         , { "rtg" }          , { "돈삭제", "돈삭" }            , ""                                  );
//#endif	// __INTERNALSERVER

    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_SetTutorialState            , "SetTutorialState"        , { "sts" }          , { "튜토리얼레벨", "튜레" }      , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_TaxApplyNow                 , "TaxApplyNow"             , { "tan" }          , { "세율적용", "세적" }          , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_HeavenTower                 , "HeavenTower"             , { "HTower" }       , { "심연의탑", "심탑" }          , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_RemoveJewel                 , "RemoveJewel"             , { "RJewel" }       , { "보석제거", "보제" }          , ""                                  );

    AddCommand(TCM::BOTH  , AUTH_ADMINISTRATOR, TextCmd_TransEggs                   , "TransEggs"               , { "TEggs" }        , { "알변환", "알변" }            , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_PickupPetAwakeningCancel    , "PickupPetAwakeningCancel", { "ppac" }         , { "픽업펫각성취소", "픽소" }    , ""                                  );

#ifdef __LAYER_1020
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_CreateLayer                 , "CreateLayer"             , { "cl" }           , { "레이어생성", "레생" }        , "레이어생성"                        );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_DeleteLayer                 , "DeleteLayer"             , { "dl" }           , { "레이어파괴", "레파" }        , "레이어파괴"                        );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_Layer                       , "Layer"                   , { "lay" }          , { "레이어이동", "레이" }        , "레이어이동"                        );
#endif	// __LAYER_1020

    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceApp              , "RRApp"                   , { "rrapp" }        , { "레인보우신청", "레신" }      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceOpen             , "RROpen"                  , { "rropen" }       , { "레인보우오픈", "레오" }      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceNext             , "RRNext"                  , { "rrnext" }       , { "레인보우다음", "레다" }      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_RainbowRacePass             , "RRPass"                  , { "rrpass" }       , { "레인보우패스", "레패" }      , ""                                  );
	
//  AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceInfo             , "RRinfo"                  , { "rrinfo" }       , { "레인보우정보", "레정" }      , ""                                  );
//  AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceRule             , "RRRule"                  , { "rrrule" }       , { "레인보우규칙", "레규" }      , ""                                  );
//  AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceRanking          , "RRRanking"               , { "rrranking" }    , { "레인보우랭킹", "레랭" }      , ""                                  );
//  AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRacePrize            , "RRPrize"                 , { "rrprize" }      , { "레인보우상품", "레상" }      , ""                                  );

    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceKawiBawiBo       , "RRKawiBawiBo"            , { "rrkawibawibo" } , { "레인보우가위바위보", "레가" }, ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceDice             , "RRDice"                  , { "rrdice" }       , { "레인보우주사위", "레주" }    , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceArithmetic       , "RRArithmetic"            , { "rrarithmetic" } , { "레인보우수학", "레수" }      , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceStopWatch        , "RRStopWatch"             , { "rrstopwatch" }  , { "레인보우스톱워치", "레스" }  , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceTyping           , "RRTyping"                , { "rrtyping" }     , { "레인보우타자치기", "레타" }  , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceCard             , "RRCard"                  , { "rrcard" }       , { "레인보우카드", "레카" }      , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceLadder           , "RRLadder"                , { "rrladder" }     , { "레인보우사다리", "레사" }    , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_RainbowRaceReqFininsh       , "RRFINISH"                , { "rrfinish" }     , { "레인보우완주", "레완" }      , ""                                  );

//#ifdef __EXT_ENCHANT
//  AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_ChangeAttribute             , "CHATTRIBUTE"             , { "chattribute" }  , { "속성변경", "속변" }          , ""                                  );
//#endif //__EXT_ENCHANT
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_HousingVisitRoom            , "HousingVisit"            , { "hv" }           , { "방문" }                      , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_HousingGMRemoveAll          , "HousingGMRemoveAll"      , { "hgmra" }        , { "가구삭제", "가삭" }          , ""                                  );
/*
#if __VER >= 14 // __SMELT_SAFETY
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_SmeltSafetyNormal           , "SmeltSafetyNormal"       , { "ssn" }          , { "안전제련일반", "안제일" }    , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_SmeltSafetyAccessary        , "SmeltSafetyAccessary"    , { "ssa" }          , { "안전제련악세", "안제악" }    , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_SmeltSafetyPiercing         , "SmeltSafetyPiercing"     , { "ssp" }          , { "안전제련피어싱", "안제피" }  , ""                                  );
#endif //__SMELT_SAFETY
*/
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_SmeltSafetyElement          , "SmeltSafetyElement"      , { "sse" }          , { "안전제련속성", "안제속" }    , ""                                  );
#ifdef __QUIZ
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_QuizEventOpen               , "QuizEventOpen"           , { "qeo" }          , { "퀴즈오픈", "퀴오" }          , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_QuizEventEnterance          , "QuizEventEnterance"      , { "qee" }          , { "퀴즈입장", "퀴입" }          , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_QuizStateNext               , "QuizStateNext"           , { "qsn" }          , { "퀴즈다음", "퀴다" }          , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_QuizEventClose              , "QuizEventClose"          , { "qec" }          , { "퀴즈종료", "퀴종" }          , ""                                  );
#endif // __QUIZ

    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_BuyGuildHouse               , "BuyGuildHouse"           , { "bgh" }          , { "길드하우스구입", "길하구" }  , ""                                  );
    AddCommand(TCM::CLIENT, AUTH_ADMINISTRATOR, TextCmd_GuildHouseUpkeep            , "GuildHouseUpkeep"        , { "ghu" }          , { "길드하우스유지비", "길하유" }, ""                                  );

    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_RemoveCampusMember          , "RemoveCampusMember"      , { "rcm" }          , { "사제해지", "사해" }          , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_UpdateCampusPoint           , "UpdateCampusPoint"       , { "ucp" }          , { "사제포인트업", "사포업" }    , ""                                  );
    AddCommand(TCM::SERVER, AUTH_ADMINISTRATOR, TextCmd_InvenRemove                 , "InvenRemove"             , { "irm" }          , { "인벤삭제", "인삭" }          , ""                                  );
	}


	void AllCommands::AddCommand(
		TextCmdFunc::TCM nServer,
		DWORD dwAuthorization,
		TextCmdFunc::Handler func,
		const TCHAR * pCommand,
		std::vector<const TCHAR *> pAbbreviations,
		std::vector<const TCHAR *> pKrCommand,
		const TCHAR * pszDesc
	) {
		TextCmdFunc * ptr = &m_allCommands.emplace_back(
			nServer, dwAuthorization, func,
			pCommand, pKrCommand.empty() ? _T("") : *pKrCommand.begin(), pszDesc
		);

		RegisterAlias(pCommand, ptr, false);
		
		for (const TCHAR * pAbbreviation : pAbbreviations) {
			RegisterAlias(pAbbreviation, ptr, false);
		}

		for (const TCHAR * pAbbreviation : pKrCommand) {
			RegisterAlias(pAbbreviation, ptr, true);
		}
	}

	void AllCommands::RegisterAlias(const TCHAR * name, TextCmdFunc * textCmdFunc, bool isKorean) {
		CString str = name;

		if (isKorean) {
			m_krAliasToCommand.emplace(str, textCmdFunc);
		} else {
			str.MakeLower();
			m_aliasToCommand.emplace(str, textCmdFunc);
			m_krAliasToCommand.emplace(str, textCmdFunc);
		}
	}
}

BOOL CmdFunc::AllCommands::ParseCommand(LPCTSTR lpszString, CUser * pMover, BOOL bItem) {
	CScanner scanner;
	scanner.SetProg( (LPTSTR)lpszString );
	scanner.GetToken(); // skip /
	scanner.GetToken(); // get command

	CString commandName = scanner.Token;
	commandName.MakeLower();

	const auto & commandMap = ::GetLanguage() == LANG_KOR ? m_krAliasToCommand : m_aliasToCommand;

	const auto itCommand = commandMap.find(commandName);

	if (itCommand != commandMap.end()) {
		TextCmdFunc & textCmdFunc = *itCommand->second;

		if (bItem || textCmdFunc.m_dwAuthorization <= pMover->m_dwAuthorization) {
#ifdef __CLIENT
			if (textCmdFunc.m_nServer == TCM::CLIENT || textCmdFunc.m_nServer == TCM::BOTH) {
				if (textCmdFunc.Call(scanner, pMover)) {
					if (textCmdFunc.m_nServer == TCM::BOTH) {
						g_DPlay.SendChat(scanner.m_pBuf);
					}
				}
			} else {
				g_DPlay.SendChat(lpszString);
			}
#else	// __CLIENT
			if (textCmdFunc.m_nServer == TCM::SERVER || textCmdFunc.m_nServer == TCM::BOTH) {
				textCmdFunc.Call(scanner, pMover);
			}
#endif	// __CLIENT
			return TRUE;
		}
	}

#ifdef __CLIENT
	BOOL bSkip = FALSE;
	CString strTemp = lpszString;
	
	if( strTemp.Find( "#", 0 ) >= 0 )
		bSkip = TRUE;

	int nstrlen = strlen(lpszString);

	if( !bSkip )
	{
		TCHAR	szText[MAX_EMOTICON_STR];

		if(nstrlen < MAX_EMOTICON_STR)
			strcpy( szText, lpszString );
		else
		{
			strncpy(szText, lpszString, MAX_EMOTICON_STR);
			szText[MAX_EMOTICON_STR] = NULL;
		}

		// 이모티콘 명령
		for( int j=0; j < MAX_EMOTICON_NUM; j++ )
		{
			if( stricmp( &(szText[1]), g_DialogMsg.m_EmiticonCmd[ j ].m_szCommand ) == 0 ) {
				g_DPlay.SendChat( (LPCSTR)lpszString );
				return TRUE;
			}
		}
	}
#endif	//__CLIENT

	return FALSE;
}

void RemoveCRLF( char* szString )
{
	CString str		= szString;
	str.Replace( "\\n", " " );
	lstrcpy( szString, (LPCSTR)str );
}

void ParsingEffect( TCHAR* pChar, int nLen )
{
	CString strTemp;
	
	for( int i = 0; i < nLen; i++ )
	{
		if( pChar[ i ] == '#' ) // 인식 코드
		{
			if( ++i >= nLen )
				break;
			switch( pChar[ i ] )
			{
			case 'c':
				{
					if( ++i >= nLen )
						break;
					
					i += 7;
				}
				break;
			case 'u':
				break;
			case 'b':
				break;
			case 's':
				break;
				
			case 'l':
				{
					if(++i >= nLen)
						break;
					
					i += 3;
				}				
				break;
			case 'n':
				if( ++i >= nLen )
					break;
				
				{					
					switch( pChar[ i ] )
					{
					case 'c':
						break;
					case 'b':
						break;
					case 'u':
						break;
					case 's':
						break;
					case 'l':
						break;
					}
				}
				break;
			default: // 명령코드를 발견 못했을 경우 
				{
					// #코드를 넣어준다
					strTemp += pChar[ i - 1 ];
					strTemp += pChar[ i ];
				}
				break;
			}
		}
		else
		{
			if( pChar[ i ] == '\\' && pChar[ i+1 ] == 'n' )
			{
				strTemp += '\n';
				i+=1;
			}
			else
			{
				strTemp += pChar[ i ];
				int nlength = strTemp.GetLength();
			}
		}
	}
	
//	memcpy( pChar, strTemp, sizeof(TCHAR)*nLen );
	strcpy( pChar, strTemp );
}
