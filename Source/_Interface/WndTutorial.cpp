#include "stdafx.h"
#include "resData.h"
#include "WndTutorial.h"


/****************************************************
  WndId : APP_TUTORIAL_VIEW - 튜토리얼
  CtrlId : WIDC_TREE1 - 
  CtrlId : WIDC_TEXT1 - 
  CtrlId : WIDC_TEXT2 - 
****************************************************/

void CWndTutorial::OnDraw(C2DRender * p2DRender) {
	if (m_strKeyword.IsEmpty()) return;

	const DWORD dwLeft = m_rectClient.Width() * 50 / 100;
	CRect rect(dwLeft + 5, 5, m_rectClient.Width() - 5, 23);

	p2DRender->TextOut(rect.left + 12, rect.top + 10, m_strKeyword, 0xff000000);
}

void CWndTutorial::OnInitialUpdate() {
	CWndNeuz::OnInitialUpdate();
	LoadTutorial(MakePath(DIR_CLIENT, _T("tutorial.inc")));
	MoveParentCenter();
}

// 처음 이 함수를 부르면 윈도가 열린다.
BOOL CWndTutorial::Initialize(CWndBase * pWndParent) {
	return CWndNeuz::InitDialog(APP_TUTORIAL_VIEW, pWndParent, 0, CPoint(0, 0));
}

BOOL CWndTutorial::OnChildNotify(UINT message, UINT nID, LRESULT * pLResult) {
	switch (nID) {
		case WIDC_LIST1: // view ctrl
		{
			CWndListBox * pWndListBox = GetDlgItem<CWndListBox>(WIDC_LIST1);
			int sel = pWndListBox->GetCurSel();
			const TUTORIAL_STRING & tutorialString = m_mapTutorial.find(pWndListBox->GetCurSel())->second;
			
			m_strKeyword = tutorialString.strTitle;

			CWndText * pWndText = GetDlgItem<CWndText>(WIDC_TEXT2);
			const auto rect = pWndText->GetClientRect();
			pWndText->m_string.Init(m_pFont, &rect);
			pWndText->m_string.SetString("");
			pWndText->m_string.AddParsingString(tutorialString.strContents.GetString());
			pWndText->UpdateScrollBar();
		}
		break;
	}

	return CWndNeuz::OnChildNotify(message, nID, pLResult);
}

bool CWndTutorial::LoadTutorial(LPCTSTR lpszFileName) {
	CWndListBox * pWndListBox = GetDlgItem<CWndListBox>(WIDC_LIST1);

	CScript s;

	if (s.Load(lpszFileName) == FALSE)
		return false;

	int nIndex = 0;
	m_mapTutorial.clear();

	// {
	//	제목(문자열)
	//	내용(문자열)
	// }
	while (s.tok != FINISHED) {
		TUTORIAL_STRING temp;
		s.GetToken();	// {
		s.GetToken();	//
		temp.strTitle = s.token;
		s.GetToken();
		temp.strContents = s.token;
		s.GetToken();	// }

		// 완료한 레벨까지만 추가한다
		if (g_Option.m_nTutorialLv >= nIndex + 1) {
			m_mapTutorial.emplace(pWndListBox->GetCount(), temp);
			pWndListBox->AddString(temp.strTitle);
		}
		++nIndex;
	}

	return true;
}
