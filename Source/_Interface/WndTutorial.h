#pragma once

class CWndTutorial final : public CWndNeuz {
public:
	struct TUTORIAL_STRING {
		CString strTitle;
		CString strContents;
	};

	CString m_strKeyword;
	std::map<int, TUTORIAL_STRING> m_mapTutorial;

	BOOL Initialize(CWndBase * pWndParent = nullptr);
	BOOL OnChildNotify(UINT message, UINT nID, LRESULT * pLResult) override;
	void OnDraw(C2DRender * p2DRender) override;
	void OnInitialUpdate() override;

private:
	bool LoadTutorial(LPCTSTR lpszFileName);
};
