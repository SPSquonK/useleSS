#ifndef __WNDGUILDBANK__H
#define __WNDGUILDBANK__H

#include "WndGuildMerit.h"

class CWndGuildBank final : public CWndNeuz
{ 
	CWndGuildMerit* m_pwndGuildMerit;		// ������.
public: 
	CWndItemCtrl m_wndItemCtrl;
	CWndGold     m_wndGold;

public:
	CWndGuildBank(); 
	~CWndGuildBank(); 

	BOOL Initialize( CWndBase* pWndParent = nullptr ); 
	virtual BOOL OnChildNotify( UINT message, UINT nID, LRESULT* pLResult ); 
	virtual void OnDraw( C2DRender* p2DRender ); 
	virtual	void OnInitialUpdate(); 
};

class CWndGuildLogGeneric : public CWndBase
{
public:
	BYTE m_type;
	int m_xOffset;

	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;
	
	CWndScrollBar m_wndScrollBar;
	std::vector < CString > m_vLogList;

public:
	CWndGuildLogGeneric(BYTE type, int xOffset);

	void Create( RECT& rect, CWndBase* pParentWnd, UINT nID );
	void UpdateLogList();
	void UpdateScroll();

	// Overridables
	virtual void OnInitialUpdate();
	virtual void OnDraw( C2DRender* p2DRender );

	// UI Func.
	int GetDrawCount( void );
};

class CWndGuildBankLog final : public CWndNeuz
{ 
public: 
	CWndGuildLogGeneric m_wndAddItemLog{ 0x01, 4 };
	CWndGuildLogGeneric	m_wndRemoveItemLog{ 0x02, 4 };
	CWndGuildLogGeneric	m_wndReceivePenyaLog{ 0x03, 10 };
	CWndGuildLogGeneric	m_wndInvestPenyaLog{ 0x04, 10 };

	BOOL Initialize( CWndBase* pWndParent = nullptr ); 
	virtual BOOL OnChildNotify( UINT message, UINT nID, LRESULT* pLResult ); 
	virtual	void OnInitialUpdate(); 

	void UpdateScroll();
};

#endif
