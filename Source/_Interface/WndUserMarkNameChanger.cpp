#include "StdAfx.h"
#ifdef __IMPROVE_MAP_SYSTEM
#include "WndUserMarkNameChanger.h"
#include "ResData.h"
//-----------------------------------------------------------------------------
CWndUserMarkNameChanger::CWndUserMarkNameChanger( void ) : 
m_dwUserMarkPositionInfoID( 0 )
{
}
//-----------------------------------------------------------------------------
BOOL CWndUserMarkNameChanger::Initialize( CWndBase* pWndParent )
{
	return CWndNeuz::InitDialog( APP_USER_MARK_NAME_CHANGER, pWndParent, WBS_MODAL | WBS_KEY, CPoint( 0, 0 ) );
}
//-----------------------------------------------------------------------------
void CWndUserMarkNameChanger::OnInitialUpdate( void )
{
	CWndNeuz::OnInitialUpdate();
	
	if (CWndEdit * pWndEditNameChanger = GetDlgItem<CWndEdit>(WIDC_EDIT_NAME_CHANGER)) {
		pWndEditNameChanger->SetMaxStringNumber(USER_MARK_NAME_MAX_LENGTH);
		pWndEditNameChanger->SetFocus();
	}

	if (CWndButton * pWndButtonOK = GetDlgItem<CWndButton>(WIDC_BUTTON_OK)) {
		pWndButtonOK->SetDefault();
	}

	MoveParentCenter();
}
//-----------------------------------------------------------------------------
BOOL CWndUserMarkNameChanger::OnChildNotify( UINT message, UINT nID, LRESULT* pLResult )
{
	switch( nID )
	{
	case WIDC_BUTTON_OK:
		{
			CWndEdit* pWndEditNameChanger = ( CWndEdit* )GetDlgItem( WIDC_EDIT_NAME_CHANGER );
			if( pWndEditNameChanger == NULL )
			{
				break;
			}

			CUserMarkPositionInfo* pUserMarkPositionInfo = prj.m_MapInformationManager.FindUserMarkPositionInfo( m_dwUserMarkPositionInfoID );
			if( pUserMarkPositionInfo == NULL )
			{
				break;
			}

			CString strChangedName = pWndEditNameChanger->GetString();
			if( strChangedName == _T( "" ) )
			{
				strChangedName = _T( "��ġ" );
			}
			pUserMarkPositionInfo->strName = strChangedName;

			Destroy();

			break;
		}
	}

	return CWndNeuz::OnChildNotify( message, nID, pLResult );
}
//-----------------------------------------------------------------------------
void CWndUserMarkNameChanger::SetInfo( DWORD dwUserMarkPositionInfoID, const CString& strName )
{
	m_dwUserMarkPositionInfoID = dwUserMarkPositionInfoID;

	
	if (CWndEdit * pWndEditNameChanger = GetDlgItem<CWndEdit>(WIDC_EDIT_NAME_CHANGER)) {
		pWndEditNameChanger->SetString(strName);
	}
}
//-----------------------------------------------------------------------------
#endif // __IMPROVE_MAP_SYSTEM
