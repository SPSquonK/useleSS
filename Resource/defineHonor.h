// 대분류
#define	HI_ELASPED_TIME		1	// 시간 달성
#define	HI_COUNT_CHECK		2	// 수치 체크
#define	HI_EARN_TITLE		3	// 지위 획득
#define	HI_HUNT_MONSTER		4	// 몬스터 사냥
#define	HI_USE_ITEM			5	// 아이템 사용

// 시간달성
#define	HS_COLLECT			51	// 채집 시간
#define	HS_TRADE			52	// 상점 시간

// 수치 체크
#define	HS_HATCHING_EGG		61	// 알 부화
#define	HS_COUPLE_COUNT		62	// 커플 맺은 횟수
#define	HS_COUPLE_LV		63	// 커플 레벨
#define	HS_PK_COUNT			64	// PK 수치
#define	HS_STR				65	// 능력치(힘)
#define	HS_STA				66	// 능력치(체력)
#define	HS_DEX				67	// 능력치(민첩)
#define	HS_INT				68	// 능력치(지능)
#define	HS_PVP_POINT01		70	// PVP 점수 단계 1
#define	HS_PVP_POINT02		71	// PVP 점수 단계 2
#define	HS_PVP_POINT03		72	// PVP 점수 단계 3
#define	HS_PVP_POINT04		73	// PVP 점수 단계 4
#define	HS_PVP_POINT05		74	// PVP 점수 단계 5
#define	HS_PVP_POINT06		75	// PVP 점수 단계 6
#define	HS_PVP_POINT07		76	// PVP 점수 단계 7
#define	HS_PVP_POINT08		77	// PVP 점수 단계 8
#define	HS_PVP_POINT09		78	// PVP 점수 단계 9
#define	HS_PVP_POINT10		79	// PVP 점수 단계 10
#define	HS_JUMP				80	// 점프수치

// 지위 획득 - definejob에 있는거라 여긴 참고용으로 주석처리함
#define	HS_LORD				92	// 군주

