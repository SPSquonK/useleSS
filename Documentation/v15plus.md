# Supported and added features post v15


## Jobs

- Characters can have the 3rd job
- Level curve is v22 official. Level max is 190
- Heroes can level up to 130

- All skills are implemented but:
    - Using Rhisis Soul does not cancel the damaged animation
    - As it is not implemented, the templar skills do not increase the aggressivity of monsters
    - The sleeping status is not implemented: instead of stun is applied

- The v16 skill window has been implemented but the v15 window remains. The window can
be chosen through the AppDefine.h file.


*Note*: weapons and class changes has not been currently implemented.

