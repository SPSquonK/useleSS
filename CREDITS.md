# Credits of useleSS

useleSS is a FlyFF v15 refactoring project mainly developed by [SP SquonK](https://squonk.fr).

***Special thanks to Kotori for checking the modifications and providing many small fixes.***

On all commits that are based on the work or comments on some people, there should be a message specifying the person.
Do not hesitate to run commands like `git log | grep thanks/reported/...` to see the list of external contributions per commit.

--------

## This repository takes inspiration from the following public releases

*Unless explicitly specified in another place, these people are not linked to this project
in any way.*

Most of the time, the source code is not taken as released but are a bit reworked. If the link is
not present, it means that the source has been deleted. If you are the author of the release and
have a stable link to the release, feel free to contact me or make a PR.

- Florist
  - Animated wings memory leak
  - Some QOL fixes
  
- Kia
  - Map/Vector concepts inspired from a now deleted post

- Kotori
  - [Fix guild cloak overflow](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/4750630-fix-create-item-guild-cloak.html)
  - Many other things
 
- Louise
  - Arbitrary number of abbreviations in FuncTextCmd ([Pull request #96](https://gitlab.com/SPSquonK/useleSS/-/merge_requests/96))

- Mognakor
  - [`CMover::GetDamageMultiplier` speed up](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/3619936-damage-calc-speedup.html)
  - [ForLinkMap lambda](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/4245799-for_linkmap-lambda.html) (implemented as a range in useleSS)

- Mootie
  - [Guild bank dupe](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/2035066-hack-fixes-including-dupe-hack.html)

- Snow
  - Item drop could not go over ~70% ([Read the commit message](https://gitlab.com/SPSquonK/useleSS/-/commit/44231b1e4dbfe809772ed6b3ae940eb1e7c28004))

- SquonK
  - I wanted to appear on the list as I use my own releases :>
  
- Ssam
  - `CLandscape::AddObjArray`/`CLandscape::RemoveObjArray` mismatch

- Violet
  - Bug reports on some commits, better usage of maybe_owned_ptr API in some functions of MoverItem.cpp

- Zerux
  - [Better string validation code and other fixes](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/4062499-release-security-fixes.html)


--------


## Source of the code in initial commit

This repository is based on the code written by Gala.

Initial commit is the release made by
[Blouflash](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/4244222-visual-studio-2017-source-files.html).


--------

*If you made a contribution, or if one of your release is blatantly used, and
your name does not appear on the list, feel free to ask for your name to appear.*

*If you do not want your name to appear here, also feel free to ask for your name removal.*

